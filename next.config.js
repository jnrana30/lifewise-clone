const withImages = require('next-images');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

const nextConfig = {
  devIndicators: {
    autoPrerender: false
  },
  env: {
    API_URL: process.env.API_URL,
    S3_ACCESS_KEY: process.env.S3_ACCESS_KEY,
    S3_SECRET_KEY: process.env.S3_SECRET_KEY,
    S3_BUCKET_REGION: process.env.S3_BUCKET_REGION,
    S3_BUCKET_NAME: process.env.S3_BUCKET_NAME,
    S3_ASSETS_URL: process.env.S3_ASSETS_URL,
    GTM_ID: process.env.GTM_ID,
  },
  images: {
    loader: 'imgix',
    path: ''
  },
  webpack: config => {
    if (process.env.NODE_ENV === 'production') {
      config.plugins.push(
        new SWPrecacheWebpackPlugin({
          verbose: true,
          staticFileGlobsIgnorePatterns: [/\.next\//],
          runtimeCaching: [
            {
              handler: 'networkFirst',
              urlPattern: /^https?.*/
            }
          ]
        })
      );
    }
    return config;
  }
};
module.exports = withImages(nextConfig);
