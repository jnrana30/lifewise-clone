#!/bin/bash
GITHASH=d8371faf87cfdba90aa1f4f9288ef4ddc538700b
S3_BUCKET=lifewise-frontend
NAMESPACE=local
mv out/_next .
mv out _out
mkdir -p out/builds/$GITHASH
mkdir ./out/$NAMESPACE
mv _out/* out/builds/$GITHASH
rm -rf _out
mv _next out/

cp -r ./out/_next ./out/$NAMESPACE/_next

cp -r ./out/builds/$GITHASH/ ./out/$NAMESPACE

(cd out/$NAMESPACE &&
  find . -type f -name '*.html' | while read HTMLFILE; do
    HTMLFILESHORT=${HTMLFILE:2}
    HTMLFILE_WITHOUT_EXT=${HTMLFILESHORT::${#HTMLFILESHORT}-5}

    cp ./${HTMLFILESHORT} ./$HTMLFILE_WITHOUT_EXT

    if [ $? -ne 0 ]; then
      echo "***** Failed renaming build to $NAMESPACE (html)"
      exit 1
    fi
  done)