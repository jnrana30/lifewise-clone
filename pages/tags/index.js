import React from 'react';
import TagsContainer from 'src/modules/app/container/tags';

function Tags() {
  return <TagsContainer />;
}

export default Tags;
