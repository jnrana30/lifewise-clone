import React from 'react'
import EmployeesContainer from 'src/modules/account/container/employees';

function Employees() {
  return (
    <EmployeesContainer />
  )
}

export default Employees
