import React from 'react';
import PackagesContainer from 'src/modules/packages/container';

function Packages() {
  return <PackagesContainer />;
}

export default Packages;
