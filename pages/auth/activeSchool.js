import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Link from 'next/link';

const useStyles = makeStyles(theme => ({
  mainBox: {
    textAlign: 'center'
  },
  messageHeading: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(3),
    fontWeight: '500'
  },
  mailSendSubHeader: {
    marginBottom: theme.spacing(3)
  },
  mailSendContent: {
    marginBottom: theme.spacing(3)
  },
  footerEmailSend: {
    display: 'flex',
    justifyContent: 'center',
    fontWeight: 400,
    '& p': {
      fontWeight: 400,
      marginLeft: 4
    }
  }
}));

const index = () => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Box mt={5} className={classes.mainBox}>
        <Typography
          align="center"
          component="h1"
          variant="h5"
          gutterBottom
          className={classes.messageHeading}
          paragraph
        >
          Active School
        </Typography>
        <Typography
          align="center"
          gutterBottom
          paragraph
          className={classes.mailSendSubHeader}
        >
          It appears the school is already active on LifeWise and you are trying
          to sign up with an unapproved domain.
        </Typography>

        <Typography
          align="center"
          gutterBottom
          paragraph
          className={classes.mailSendContent}
        >
          Please contact your school administrator to invite you directly to the
          platform.
        </Typography>
        <Box
          display="flex"
          justifyContent="center"
          className={classes.footerEmailSend}
        >
          <Typography color="textSecondary" gutterBottom paragraph>
            Go back to{' '}
          </Typography>
          <Link href="/auth/login">
            <Typography color="primary">lifewise.co.uk/login</Typography>
          </Link>
        </Box>
      </Box>
    </React.Fragment>
  );
};

export default index;
