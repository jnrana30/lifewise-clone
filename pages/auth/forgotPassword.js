import React from 'react';
import Forgot from 'src/modules/auth/container/password/Forgot';
function ForgotPassword() {
  return <Forgot />;
}

export default ForgotPassword;
