import React from 'react';
import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Link from 'next/link';
import { useRouter } from 'next/router';

const useStyles = makeStyles(theme => ({
  mainBox: {
    textAlign: 'center'
  },
  messageHeading: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(3),
    fontWeight: '500'
  },
  mailSendSubHeader: {
    marginBottom: theme.spacing(3)
  },
  mailSendContent: {
    marginBottom: theme.spacing(3)
  },
  footerEmailSend: {
    display: 'flex',
    justifyContent: 'center',
    fontWeight: 400,
    '& p': {
      fontWeight: 400,
      marginLeft: 4
    }
  }
}));

const index = () => {
  const classes = useStyles();
  const router = useRouter();
  return (
    <React.Fragment>
      <Box mt={5} className={classes.mainBox}>
        <Typography
          align="center"
          component="h1"
          variant="h5"
          gutterBottom
          className={classes.messageHeading}
          paragraph
        >
          Verify Email
        </Typography>
        <Typography
          align="center"
          gutterBottom
          paragraph
          className={classes.mailSendSubHeader}
        >
          We have sent you an email to verify your email address on{' '}
          <Typography component="span" color="primary" display="inline">
            {router?.query?.email}
          </Typography>
          .
        </Typography>

        <Typography
          align="center"
          gutterBottom
          paragraph
          className={classes.mailSendContent}
        >
          Once you have verified your email you will get access to all the
          benefits of LifeWise.
        </Typography>
        <Box
          display="flex"
          justifyContent="center"
          className={classes.footerEmailSend}
        >
          <Typography color="textSecondary" gutterBottom paragraph>
            Go back to{' '}
          </Typography>
          <Link href="/auth/login">
            <Typography color="primary">lifewise.co.uk/login</Typography>
          </Link>
        </Box>
      </Box>
    </React.Fragment>
  );
};

export default index;
