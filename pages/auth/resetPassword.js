import React from 'react';
import Reset from 'src/modules/auth/container/password/Reset';

function ResetPassword() {
  return <Reset />;
}

export default ResetPassword;
