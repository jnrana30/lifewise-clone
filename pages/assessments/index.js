import React from 'react';
import AssessmentsContainer from 'src/modules/courses/container/assessments';

function Assessments() {
  return <AssessmentsContainer />;
}

export default Assessments;
