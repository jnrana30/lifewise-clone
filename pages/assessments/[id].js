import React from 'react';
import AssessmentContainer from 'src/modules/courses/container/assessment';

function Assessment() {
  return <AssessmentContainer />;
}

export default Assessment;
