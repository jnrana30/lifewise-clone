import React from 'react';
import LessonAdminContainer from 'src/modules/courses/container/lessonDetails/LessonAdminContainer';

function LessonAdmin() {
  return <LessonAdminContainer />;
}

export default LessonAdmin;
