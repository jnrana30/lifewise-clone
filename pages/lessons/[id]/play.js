import React from 'react';
import LessonPlayer from 'src/modules/courses/container/lessonDetails/LessonPlayer';

function Play() {
  return <LessonPlayer />;
}

export default Play;
