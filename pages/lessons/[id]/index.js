import React from 'react';
import LessonDetailsContainer from 'src/modules/courses/container/lessonDetails';

function index() {
  return <LessonDetailsContainer />;
}

export default index;
