import React from 'react';
import store from 'src/store';
import withRedux from 'next-redux-wrapper';
import { Provider } from 'react-redux';
import Head from 'next/head';
import { Provider as RollbarProvider, ErrorBoundary } from '@rollbar/react';

import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from 'src/config/theme';

import { authPages } from 'src/config';
import AppLayout from 'src/layouts/AppLayout';
import AuthLayout from 'src/layouts/AuthLayout';
import 'src/assets/css/style.css';
import s3Helper from 'src/utils/s3Helper.js';

import { restoreSession } from 'src/modules/auth/actions/authActions';
import {
  fetchRoles,
  fetchRoleTypes,
  fetchSchoolYears
} from 'src/modules/app/actions/appActions';
import { getMySchool } from 'src/modules/school/actions/schoolActions';

// same configuration you would create for the Rollbar.js SDK
const rollbarConfig = {
  accessToken: 'be969560240a45c0be718ece6ce5c19c',
  environment: process.env.NODE_ENV,
  captureUncaught: true,
  captureUnhandledRejections: true
};

function MyApp({ Component, pageProps, router }) {
  React.useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
    store.dispatch(restoreSession());
    store.dispatch(fetchRoles());
    store.dispatch(fetchRoleTypes());
    store.dispatch(fetchSchoolYears());
    store.dispatch(getMySchool());
    s3Helper.init();
  }, []);

  return (
    <>
      <Head>
        <title>LifeWise</title>
        <meta
          name="viewport"
          content="minimum-scale=1, initial-scale=1, width=device-width"
        />
      </Head>
      <RollbarProvider config={rollbarConfig}>
        <Provider store={store}>
          <ErrorBoundary>
            <ThemeProvider theme={theme}>
              <CssBaseline />
              {authPages.includes(router.pathname) ? (
                <AuthLayout>
                  <Component {...pageProps} />
                </AuthLayout>
              ) : (
                <AppLayout>
                  <Component {...pageProps} />
                </AppLayout>
              )}
            </ThemeProvider>
          </ErrorBoundary>
        </Provider>
      </RollbarProvider>
    </>
  );
}

const makeStore = () => store;

export default withRedux(makeStore)(MyApp);
