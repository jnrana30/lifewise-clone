import React from 'react';
import SchoolDetailsContainer from 'src/modules/school/container/schools/SchoolDetailsContainer';

function SchoolDetails() {
  return <SchoolDetailsContainer />;
}

export default SchoolDetails;
