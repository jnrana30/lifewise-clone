import React from 'react';
import ResourcesContainer from 'src/modules/app/container/resources';

function Resource() {
  return <ResourcesContainer />;
}

export default Resource;
