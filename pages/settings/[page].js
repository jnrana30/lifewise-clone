import React from 'react';
import SettingsContainer from 'src/modules/account/container/settings';

function SettingsPage() {
  return <SettingsContainer />;
}

export default SettingsPage;
