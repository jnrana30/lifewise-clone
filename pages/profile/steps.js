import React from 'react';
import ProfileStepsContainer from 'src/modules/account/container/profile/steps';

function ProfileSteps() {
  return <ProfileStepsContainer />;
}

export default ProfileSteps;
