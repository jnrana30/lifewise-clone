import React from 'react';
import SubscriptionsContainer from 'src/modules/subscriptions/container';
function Subscriptions() {
  return <SubscriptionsContainer />;
}

export default Subscriptions;
