import React from 'react';
import Lessons from 'src/modules/courses/container/lessons';

function index() {
  return <Lessons />;
}

export default index;
