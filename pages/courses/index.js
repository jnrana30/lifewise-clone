import React from 'react';
// import Courses from 'src/modules/app/container/courses';
import Courses from 'src/modules/courses/container/courses';

function index() {
  return <Courses />;
}

export default index;
