import React from 'react';
// import CourseDetailContainer from 'src/modules/app/container/courseDetails';
import CourseDetailsContainer from 'src/modules/courses/container/courseDetails/CourseDetailsContainer';

function CourseDetail() {
  return <CourseDetailsContainer />;
}

export default CourseDetail;
