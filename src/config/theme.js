import { createMuiTheme } from '@material-ui/core/styles';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#2B6B00'
    },
    secondary: {
      main: '#264197'
    }
  }
});

export default theme;
export const drawerWidth = 220;
export const drawerCollapseWidth = 55;
