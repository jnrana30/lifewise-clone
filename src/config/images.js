const images = {
  sidebar: {
    LessonsLibrary: require('src/assets/icons/sidebar/LessonLibrary.svg'),
    MyCourses: require('src/assets/icons/sidebar/Courses.svg'),
    MyLessons: require('src/assets/icons/sidebar/LessonHome.svg'),
    PlanningDocs: require('src/assets/icons/sidebar/PlanningDocs.svg'),
    Settings: require('src/assets/icons/sidebar/Settings.svg'),
    Assesments: require('src/assets/icons/sidebar/Assesments.svg'),
    Employees: require('src/assets/icons/sidebar/Employees.svg'),
    Packages: require('src/assets/icons/sidebar/Packages.svg'),
    Subscriptions: require('src/assets/icons/sidebar/Subscriptions.svg'),
    Tags: require('src/assets/icons/sidebar/Tags.svg'),
    Schools: require('src/assets/icons/sidebar/Schools.svg'),
    Assemblies: require('src/assets/icons/sidebar/Assemblies.svg'),
    MenuIcon: require('src/assets/icons/sidebar/MenuIcon.svg'),
    InviteColleague: require('src/assets/icons/sidebar/InviteColleague.svg')
  },
  app: {
    logo: require('src/assets/images/logo.png'),
    courseBanner: require('src/assets/images/course-banner.jpg'),
    courseBanner1: require('src/assets/images/courseBg-1.jpeg'),
    schoolLogo: require('src/assets/images/school-logo.png'),
    coursePlaceholder: require('src/assets/images/course-placeholder.png'),
    lessonPlaceholder: require('src/assets/images/LessonBanner.jpg'),
    subscriptionExpired: require('src/assets/images/subscriptionExpired.png'),
    ViewMorePlaceHolder: require('src/assets/images/ViewMorePlaceHolder.jpg'),
    welcomeImage: require('src/assets/images/welcome.png')
  },
  icons: {
    courses: require('src/assets/icons/courses.svg'),
    courseIcon: require('src/assets/icons/CourseIcon.webp')
  },
  file: {
    zip: require('src/assets/icons/file/zip.svg'),
    ai: require('src/assets/icons/file/ai.svg'),
    avi: require('src/assets/icons/file/avi.svg'),
    doc: require('src/assets/icons/file/doc.svg'),
    gif: require('src/assets/icons/file/gif.svg'),
    jpg: require('src/assets/icons/file/jpg.svg'),
    mkv: require('src/assets/icons/file/mkv.svg'),
    mp3: require('src/assets/icons/file/mp3.svg'),
    pdf: require('src/assets/icons/file/pdf.svg'),
    ppt: require('src/assets/icons/file/ppt.svg'),
    psd: require('src/assets/icons/file/psd.svg'),
    svg: require('src/assets/icons/file/svg.svg'),
    txt: require('src/assets/icons/file/txt.svg'),
    xls: require('src/assets/icons/file/xls.svg')
  },
  lessonDetail: {
    actions: {
      downloadActivityWorkbook: require('src/assets/images/download-activity-workbook.png'),
      downloadActivityWorkbookGreen: require('src/assets/images/download-activity-workbook-green.png'),
      downloadLessonPlan: require('src/assets/images/download-lesson-plan.png'),
      downloadLessonPlanGreen: require('src/assets/images/download-lesson-plan-green.png'),
      lessonCertificate: require('src/assets/images/lessoncertificate.png'),
      lessonCertificateGreen: require('src/assets/images/lessoncertificate-green.png'),
      lessonFeedback: require('src/assets/images/lessonfeedback.png'),
      lessonFeedbackGreen: require('src/assets/images/lessonfeedback-green.png'),
      shareWthParents: require('src/assets/images/sharewithparents.png'),
      shareWthParentsGreen: require('src/assets/images/sharewithparents-green.png')
    }
  }
};

export default images;
