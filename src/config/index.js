export const authPages = [
  '/auth/login',
  '/auth/register',
  '/auth/forgotPassword',
  '/auth/resetPassword',
  '/auth/onboarding',
  '/auth/onboarding/invite',
  '/auth/verificationEmail',
  '/auth/confirmAccount',
  '/auth/activeSchool'
];

export const schoolSourceTypes = [
  { value: 'Facebook', label: 'Facebook' },
  { value: 'Twitter', label: 'Twitter' },
  { value: 'LinkedIn', label: 'LinkedIn' },
  { value: 'Call', label: 'Call' },
  { value: 'Email', label: 'Email' },
  { value: 'Word of mouth', label: 'Word of mouth' }
];

export const getAcademicYearOption = () => {
  const max = new Date().getFullYear() + 1;
  const min = max - 2;
  let data = [];
  for (let i = min; i < max; i++) {
    data.push({
      label: `${i}/${i + 1}`,
      value: `${i}/${i + 1}`
    });
  }
  return data;
};

export const tagTypes = [
  { value: 'course', label: 'Course' },
  { value: 'content', label: 'Content' },
  { value: 'subscription', label: 'Subscription' },
  { value: 'lesson', label: 'Lesson' },
  // { value: 'year', label: 'Year' },
  { value: 'curriculum', label: 'Curriculum' },
  { value: 'category', label: 'Category' },
  { value: 'topic', label: 'Topic' },
  { value: 'yearGroup', label: 'Year Group' }
];

export const lessonStatusOptions = [
  { label: 'Active', value: 'active' },
  { label: 'Previous', value: 'previous' },
  { label: 'Draft', value: 'draft' }
];

export const resourceStatusOptions = [
  { label: 'Published', value: 'published' },
  { label: 'Unpublished', value: 'unpublished' }
];

export const resourceTypeOptions = [
  { label: 'Resource', value: 'resource' },
  { label: 'Certificate', value: 'certificate' }
];

export const subscriptionDurationOptions = [
  { label: 'Week', value: 'w' },
  { label: 'Month', value: 'm' },
  { label: 'Year', value: 'y' }
];

export const courseStatusOptions = [
  { label: 'Active', value: 'active' },
  { label: 'Previous', value: 'previous' },
  { label: 'Draft', value: 'draft' }
];

export const courseVisibilityOptions = [
  { label: 'Public', value: 'public' },
  { label: 'Private', value: 'private' }
];

export const schoolStatusOptions = [
  { label: 'Shadow', value: 'shadow' },
  { label: 'Subscribed', value: 'subscribed' },
  { label: 'Prospect', value: 'prospect' }
];

export const classStatusOptions = [
  { label: 'Active', value: 'active' },
  { label: 'Inactive', value: 'inactive' }
];

export const userStatusOptions = [
  { label: 'Invited', value: 'invited' },
  { label: 'New', value: 'new' },
  { label: 'Completed', value: 'completed' },
  { label: 'Pending', value: 'pending' },
  { label: 'Disabled', value: 'disabled' }
];

export const teacherStatusOptions = [
  { label: 'Invited', value: 'invited' },
  { label: 'Active', value: '["new", "completed"]' },
  { label: 'Inactive', value: 'disabled' }
];

export const editTeacherOptions = [
  // { label: 'New', value: 'new' },
  { label: 'Active', value: 'completed' },
  { label: 'Inactive', value: 'disabled' }
];

export const studentStatusOptions = [
  { label: 'Active', value: 'completed' },
  { label: 'Inactive', value: 'pending' }
];

export const schoolSubscriptionStatusOptions = [
  { label: 'Active', value: 'active' },
  { label: 'Expired', value: 'expired' },
  { label: 'Extended', value: 'extended' },
  { label: 'Suspended', value: 'suspended' }
];

export const psheStatus = [
  { label: 'Nothing in place', value: 'Nothing in place' },
  { label: 'School designed - Help needed', value: 'School designed - Help needed' },
  { label: 'School designed - Happy', value: 'School designed - Happy' },
  { label: 'Other Competitor', value: 'Other Competitor' }
];

export const userTitles = [
  { label: 'Mr', value: 'Mr' },
  { label: 'Mrs', value: 'Mrs' },
  { label: 'Miss', value: 'Miss' },
  { label: 'Ms', value: 'Ms' },
  { label: 'Dr', value: 'Dr' }
];

export const jobRoleTypes = [
  { label: 'Principal', value: 'Principal' },
  { label: 'Counselor', value: 'Counselor' },
  { label: 'Teacher', value: 'Teacher' }
];

export const yearGroup = [
  { label: '1 to 2', value: '1-2' },
  { label: '3 to 6', value: '3-6' },
  { label: '7 to 9', value: '7-9' },
  { label: '10 to 11', value: '10-11' },
  { label: '12 & 13', value: '12-13' }
];

export const lessonContentOptions = [
  {
    type: 'whatPupilsShouldAlreadyKnow',
    title: 'What Pupils Should Already Know',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'learningObjectives',
    title: 'Learning Objectives',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'introduction',
    title: 'Introduction',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'scenario',
    title: 'Scenario',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'keyVocab',
    title: 'Key Vocab',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'development',
    title: 'Development',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'challengeActivities',
    title: 'Challenge Activities',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'plenary',
    title: 'Plenary',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'assessmentToCheckPupilUnderstanding',
    title: 'Assessment To Check Pupil Understanding',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'differentiation',
    title: 'Differentiation',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'nextSteps',
    title: 'Next Steps',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  },
  {
    type: 'psheStudyLink',
    title: 'PSHE Study Link',
    edit: false,
    content: 'MARKDOWN',
    data: ''
  }
];
