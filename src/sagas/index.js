import { all } from 'redux-saga/effects';
import AppSagas from 'src/modules/app/saga/AppSagas';
import AuthSagas from 'src/modules/auth/saga/AuthSagas';
import AccountSagas from 'src/modules/account/saga/AccountSagas';
import SchoolSagas from 'src/modules/school/saga/SchoolSagas';
import CoursesSagas from 'src/modules/courses/saga/CoursesSagas';
import PackagesSagas from 'src/modules/packages/saga/PackagesSagas';
import SubscriptionsSagas from 'src/modules/subscriptions/saga/SubscriptionsSagas';

function* rootSaga() {
  yield all([
    AppSagas(),
    AuthSagas(),
    AccountSagas(),
    SchoolSagas(),
    CoursesSagas(),
    PackagesSagas(),
    SubscriptionsSagas(),
  ]);
}

export default rootSaga;
