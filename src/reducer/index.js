import { combineReducers } from 'redux';

import AppReducer from 'src/modules/app/reducer/AppReducer';
import AuthReducer from 'src/modules/auth/reducer/AuthReducer';
import AccountReducer from 'src/modules/account/reducer/AccountReducer';
import SchoolReducer from 'src/modules/school/reducer/SchoolReducer';
import CoursesReducer from 'src/modules/courses/reducer/CoursesReducer';
import PackagesReducer from 'src/modules/packages/reducer/PackagesReducer';
import SubscriptionsReducer from 'src/modules/subscriptions/reducer/SubscriptionsReducer';

export default combineReducers({
  app: AppReducer,
  auth: AuthReducer,
  account: AccountReducer,
  school: SchoolReducer,
  courses: CoursesReducer,
  packages: PackagesReducer,
  subscriptions: SubscriptionsReducer,
});
