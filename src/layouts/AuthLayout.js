import React from 'react';
import { connect } from 'react-redux';
import Image from 'next/image';
import { Container } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import images from 'src/config/images';
import { Toast } from 'src/components/shared';

import { AppLoader } from 'src/components/App';

const useStyles = makeStyles(theme => ({
  '@global': {
    body: {
      backgroundColor: '#ffffff'
    }
  },
  authRoot: {
    maxWidth: 470
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  }
}));

function AuthLayout(props) {
  const classes = useStyles();

  if (props.appLoading === true) {
    return <AppLoader />;
  }

  return (
    <Container component="main" maxWidth="xs" className={classes.authRoot}>
      <Toast />
      <div className={classes.paper}>
        <Image
          src={images.app.logo}
          alt="Picture of the author"
          width={171}
          height={40}
        />
        {props.children}
      </div>
    </Container>
  );
}

const mapStateToProps = state => ({
  appLoading: state.app.appLoading
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AuthLayout);
