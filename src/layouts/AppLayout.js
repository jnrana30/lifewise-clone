import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Header, Sidebar } from 'src/components/App';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {
  toggleSidebar,
  toggleUpgradeModal,
  fetchJoyRide
} from 'src/modules/app/actions/appActions';

import { renewSubscription } from 'src/modules/auth/actions/authActions';
import { Toast, Dialog, Button } from 'src/components/shared';
import { AppLoader, WelcomeJoyRide } from 'src/components/App';
import SubscriptionNotice from 'src/modules/app/components/SubscriptionNotice';
import images from '../config/images';
import toast from 'src/utils/toast';

const useStyles = makeStyles(theme => ({
  root: { display: 'flex' },
  toolbar: { paddingRight: 24 },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto',
    scrollBehavior: 'smooth'
  },
  container: {
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    marginTop: theme.spacing(14)
  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column'
  }
}));

export const AppLayout = props => {
  const [renewing, setRenewing] = useState(false);
  // const [startJoyRide, setStartJoyRide] = useState(false);
  const classes = useStyles();

  if (props.appLoading === true) {
    return <AppLoader />;
  }

  const onExtendSubscription = async () => {
    setRenewing(true);
    try {
      await props.renewSubscription();
      window.open('https://calendly.com/lifewise-demo/upgrade-call', '_blank');
      setRenewing(false);
    } catch (error) {
      toast.error('An error ocurred! Please try again.');
      setRenewing(false);
    }
  };

  const onUpgrade = () => {
    window.open('https://calendly.com/lifewise-demo/upgrade-call', '_blank');
    props.toggleUpgradeModal();
  };

  const closeUpgradeModal = () => {
    props.toggleUpgradeModal();
  };

  const rootRef = React.createRef();
  const isExpired =
    props.schoolSubscriptionStatus &&
    props.schoolSubscriptionStatus == 'expired'
      ? true
      : false;

  return (
    <div className={classes.root} ref={rootRef}>
      <Toast />
      <Sidebar
        sidebarOpen={props.sidebarOpen}
        toggleSidebar={props.toggleSidebar}
        rootRef={rootRef}
        user={props.user}
      />
      <main className={`${classes.content} joy-ride-main-scroll`}>
        <Container
          maxWidth="xl"
          className={classes.container}
          classes={{
            root: 'main-app-container'
          }}
        >
          {props.children}
        </Container>
      </main>

      <Dialog open={isExpired} onClose={() => {}}>
        <SubscriptionNotice
          type={props.schoolSubscriptionType}
          onClick={onExtendSubscription}
          loading={renewing}
        />
      </Dialog>
      <Dialog open={props.showUpgradeModal} onClose={() => {}}>
        <SubscriptionNotice
          type="upgrade"
          onClick={onUpgrade}
          onClose={closeUpgradeModal}
        />
      </Dialog>
      {props.startJoyRide && (
        <WelcomeJoyRide
          startJoyRide={props.startJoyRide}
          setJoyRide={props.fetchJoyRide}
        />
      )}
    </div>
  );
};

const mapStateToProps = state => ({
  sidebarOpen: state.app.sidebarOpen,
  appLoading: state.app.appLoading,
  startJoyRide: state.app.startJoyRide,
  user: state.auth.user,
  schoolSubscriptionStatus: state.auth.user?.profile?.schoolSubscriptionStatus,
  schoolSubscriptionType: state.auth.user?.profile?.schoolSubscriptionType,
  showUpgradeModal: state.app.showUpgradeModal
});

const mapDispatchToProps = dispatch => ({
  toggleSidebar: () => dispatch(toggleSidebar()),
  toggleUpgradeModal: () => dispatch(toggleUpgradeModal()),
  fetchJoyRide: data => dispatch(fetchJoyRide(data)),
  renewSubscription: () => {
    return new Promise((resolve, reject) => {
      dispatch(renewSubscription(resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(AppLayout);
