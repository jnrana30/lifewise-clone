import { useStore, useSelector } from 'react-redux';
import images from 'src/config/images.js';
import s3Helper from 'src/utils/s3Helper';

export const isColorLightOrDark = color => {
  var r, g, b, hsp;
  if (color.match(/^rgb/)) {
    color = color.match(
      /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
    );
    r = color[1];
    g = color[2];
    b = color[3];
  } else {
    color = +('0x' + color.slice(1).replace(color.length < 5 && /./g, '$&$&'));
    r = color >> 16;
    g = (color >> 8) & 255;
    b = color & 255;
  }
  hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b));
  if (hsp > 127.5) {
    return 'light';
  } else {
    return 'dark';
  }
};

export const getLessonActions = data => {
  const actions = [];
  let activityLink = '';
  if (
    (data?.articulateLink &&
      data?.articulateLink !== '' &&
      !data?.articulateLinkLoading) ||
    data?.articulateLinkLoading === false
  ) {
    actions.push({
      title: 'Play',
      href: s3Helper.getFileUrl(data?.articulateLink)
    });
    activityLink = data?.articulateLink;
  }
  actions.push({
    title: 'Mark as Complete',
    action: 'completeLesson'
  });
  if (data?.lessonPlan && data?.lessonPlan !== '') {
    actions.push({
      title: 'Download Plan',
      href: s3Helper.getFileUrl(data?.lessonPlan),
      icon: images.lessonDetail.actions.downloadLessonPlan
    });
  }
  if (data?.activityWorkbook && data?.activityWorkbook !== '') {
    actions.push({
      title: 'Download Activity Workbook',
      href: s3Helper.getFileUrl(data?.activityWorkbook),
      icon: images.lessonDetail.actions.downloadActivityWorkbook
    });
  }
  if (data?.remoteArticulateLink && data?.remoteArticulateLink !== '') {
    actions.push({
      title: 'Remote Articulate Link',
      href: s3Helper.getFileUrl(data?.remoteArticulateLink)
    });
  }
  if (data?.parentLink && data?.parentLink !== '') {
    actions.push({
      title: 'Share link with parents',
      action: 'share',
      icon: images.lessonDetail.actions.shareWthParents
    });
  }
  actions.push({
    title: 'Download Certificates',
    action: 'certificates',
    icon: images.lessonDetail.actions.lessonCertificate
  });
  actions.push({
    title: 'Complete Feedback',
    action: 'feedback',
    icon: images.lessonDetail.actions.lessonFeedback
  });
  return actions;
};

export const getRole = role => {
  // let state = useStore();
  // state.getState();
  // return useSelector(state => state.app?.roles);
};

export const convertObjectToQuerystring = obj => {
  return Object.keys(obj)
    .map(key => `${key}=${obj[key]}`)
    .join('&');
};

export const getMomentUnit = unitValue => {
  let unit = '';
  switch (unitValue) {
    case 'w': {
      unit = 'days';
      break;
    }
    case 'm': {
      unit = 'months';
      break;
    }
    case 'y': {
      unit = 'years';
      break;
    }
    default: {
      unit = 'days';
      break;
    }
  }
  return unit;
};

export const viewPDFInNewTab = s3URL => {
  try {
    fetch(s3URL, {
      method: 'GET'
    })
      .then(response => response.blob())
      .then(data => {
        const file = new Blob([data], { type: 'application/pdf' });
        const fileURL = URL.createObjectURL(file);
        window.open(fileURL);
      });
  } catch (e) {
    console.log(e);
  }
};
