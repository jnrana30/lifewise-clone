import React, { useEffect, useRef, useState } from 'react';

const useViewport = (mobileBreakPoint = 600) => {
  const [isMobileView, setIsMobileView] = useState(false);
  let mobileViewRef = useRef(false);

  const handleWindowResize = () => {
    if (mobileViewRef.current !== mobileBreakPoint > window.innerWidth) {
      setIsMobileView(mobileBreakPoint > window.innerWidth);
      mobileViewRef.current = mobileBreakPoint > window.innerWidth;
    }
  };

  useEffect(() => {
    setIsMobileView(mobileBreakPoint > window.innerWidth);
    mobileViewRef.current = mobileBreakPoint > window.innerWidth;

    window.addEventListener("resize", handleWindowResize);
    return () => window.removeEventListener("resize", handleWindowResize);
  }, []);

  return { isMobileView };
};

export default useViewport;