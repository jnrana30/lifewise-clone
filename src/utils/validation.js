import React from 'react';
import { checkUsername } from '../modules/auth/api/authApis';

export const cacheTest = asyncValidate => {
  let _valid = false;
  let _value = '';

  return async (value, { createError, ...otherParams }) => {
    if (value !== _value) {
      const response = await asyncValidate(value, {
        createError,
        ...otherParams
      });
      _value = value;
      _valid = response;
      return response;
    }
    return _valid;
  };
};

export const checkEmailUnique = async (
  value,
  { createError, formRegistration, skipAllowed }
) => {
  if (!!value && !/.+@.+\..+/.test(value)) {
    return createError({
      message: 'Enter a valid email address!'
    });
  }
  try {
    const res = await checkUsername(value);
    if (skipAllowed === true && res.available) {
      return true;
    }

    if (res.available && res.allowed) {
      return true;
    } else {
      return createError({
        message:
          !res.allowed && !skipAllowed ? (
            formRegistration ? (
              <>
                Please provide a school email address or{' '}
                <a href="https://lifewise.co.uk/" target="_blank">
                  contact us
                </a>{' '}
                if you encounter any further issues.
              </>
            ) : (
              'Emails like gmail, yahoo, hotmail, etc. are not accepted! Please provide a valid business email address.'
            )
          ) : (
            'This email is already taken!'
          )
      });
    }
  } catch (error) {
    return createError({
      message: 'Enter a valid email address!'
    });
  }
};

const isNilOrEmptyString = value =>
  value === undefined || value === null || value === '';

export const is = {
  match:
    (testFn, message = '') =>
    (value, fieldValues) =>
      !testFn(value, fieldValues) && message,

  required: () => value =>
    isNilOrEmptyString(value) && 'This field is required',

  minLength: min => value =>
    !!value && value.length < min && `Must be at least ${min} characters`,

  maxLength: max => value =>
    !!value && value.length > max && `Must be at most ${max} characters`,

  notEmptyArray: () => value =>
    Array.isArray(value) &&
    value.length === 0 &&
    'Please add at least one item',

  email: () => value =>
    !!value && !/.+@.+\..+/.test(value) && 'Must be a valid email',

  url: () => value =>
    !!value &&
    // eslint-disable-next-line no-useless-escape
    !/^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/.test(
      value
    ) &&
    'Must be a valid URL'
};

export const generateErrors = (fieldValues, fieldValidators) => {
  const errors = {};

  Object.entries(fieldValidators).forEach(([fieldName, validators]) => {
    [validators].flat().forEach(validator => {
      const errorMessage = validator(fieldValues[fieldName], fieldValues);
      if (errorMessage && !errors[fieldName]) {
        errors[fieldName] = errorMessage;
      }
    });
  });
  return errors;
};
