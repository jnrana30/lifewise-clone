import AWS from 'aws-sdk';

const s3Helper = (function () {
  let s3Client;

  const init = () => {
    AWS.config.update({
      region: process.env.S3_BUCKET_REGION,
      accessKeyId: process.env.S3_ACCESS_KEY,
      secretAccessKey: process.env.S3_SECRET_KEY
    });
    s3Client = new AWS.S3({
      apiVersion: '2006-03-01',
      params: { Bucket: process.env.S3_BUCKET_NAME }
    });
  };

  const upload = async (file, folder, fileName) => {
    return new Promise((resolve, reject) => {
      if (!fileName || fileName === '') {
        fileName = +new Date();
      }
      // const extension = file.type.split('/')[1];
      const extension = file.name.substring(file.name.lastIndexOf('.') + 1);
      const fileKey = `${
        folder && folder !== '' ? folder + '/' : ''
      }${fileName}.${extension}`;

      return s3Client.upload(
        {
          Key: fileKey,
          Body: file,
          ACL: 'public-read'
        },
        (err, data) => {
          if (err) {
            reject({ ...err, isUploaded: false });
          }
          resolve({
            key: fileKey,
            location: data.Location,
            type: file.type,
            isUploaded: true
          });
        }
      );
    });
  };

  const deleteFile = async fileName => {
    return new Promise((resolve, reject) => {
      return s3Client.deleteObject(
        {
          Key: fileName
        },
        (err, data) => {
          if (err) {
            reject(err);
          }
          resolve(data);
        }
      );
    });
  };

  const getFileUrl = fileName => {
    return `https://assets.lifewise.co.uk/${fileName}`;
    return `${process.env.S3_ASSETS_PUBLIC_URL}/${fileName}`;
    return `https://${process.env.S3_BUCKET_NAME}.s3.${process.env.S3_BUCKET_REGION}.amazonaws.com/${fileName}`;
  };

  return {
    init: init,
    upload: upload,
    deleteFile: deleteFile,
    getFileUrl: getFileUrl
  };
})();

export default s3Helper;
