export const setUserData = data => {
  localStorage.setItem('userData', JSON.stringify(data));
};
export const setRegisterData = data => {
  localStorage.setItem('registerData', JSON.stringify(data));
};
export const setUserToken = token => {
  localStorage.setItem('token', token);
};

export const getUserToken = () => localStorage.getItem('token');
export const getRegisterData = () => {
  if (typeof window !== 'undefined') {
    return JSON.parse(localStorage.getItem('registerData'));
  }
  return {};
};

export const getUserData = () => JSON.parse(localStorage.getItem('userData'));

export const unsetUserData = () => {
  localStorage.removeItem('userData');
};
export const unsetRegisterData = () => {
  localStorage.removeItem('registerData');
};
export const unsetUserToken = () => {
  localStorage.removeItem('token');
};

export const getJoyRide = () => localStorage.getItem('showWalkThrough');

export const setJoyRide = () => {
  localStorage.setItem('showWalkThrough', 'true');
};

export const unsetJoyRide = () => {
  localStorage.removeItem('showWalkThrough');
};
