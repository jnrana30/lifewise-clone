import S3 from 'aws-s3';
import Crypto, { WordArray } from 'crypto-js';
import moment from 'moment';

const s3Helper = (function () {
  const config = {
    bucketName: process.env.S3_BUCKET_NAME,
    region: process.env.S3_BUCKET_REGION,
    accessKeyId: process.env.S3_ACCESS_KEY,
    secretAccessKey: process.env.S3_SECRET_KEY,
    s3Url: process.env.S3_ASSETS_URL
  };
  const client = new S3(config);

  const upload = async (file, folder, fileName) => {
    return new Promise(async (resolve, reject) => {
      if (!fileName || fileName === '') {
        fileName = +new Date();
      }
      client
        .uploadFile(file, folder ? `${folder}/${fileName}` : fileName)
        .then(data => {
          console.log(data);
          resolve(data);
        })
        .catch(error => {
          console.log(error);
          reject(error);
        });
    });
  };

  const deleteFileOld = async fileName => {
    return new Promise(async (resolve, reject) => {
      client
        .deleteFile(fileName)
        .then(data => {
          console.log(data);
          resolve(data);
        })
        .catch(error => {
          console.log(error);
          reject(error);
        });
    });
  };

  const deleteFile = async fileName => {
    return new Promise(async (resolve, reject) => {
      // const dateISOString = new Date(+new Date()).toISOString();
      // const dateISOString = new Date(+new Date()).toISOString();
      // const dateISOString = moment.utc().valueOf();
      const dateISOString = new Date(moment.utc()).toISOString();

      console.log('dateISOString {DELETE} : ', dateISOString);

      const xAmzDate = dateISOString
        .split('-')
        .join('')
        .split(':')
        .join('')
        .split('.')
        .join('');
      const dateYMD = dateISOString.split('T')[0].split('-').join('');

      // const headers = new Headers({
      //   'Date': xAmzDate,
      //   'X-Amz-Date': xAmzDate,
      //   'Authorization': getSignature(config, dateYMD, getPolicy(config)),
      //   'Content-Type': 'text/plain'
      // });

      console.log('config');
      console.log(config);
      const headers = new Headers();
      const url = `https://${config.bucketName}.s3-${
        config.region
      }.amazonaws.com/${config.dirName ? config.dirName + '/' : ''}${fileName}`;
      headers.append('Date', xAmzDate);
      headers.append('X-Amz-Date', xAmzDate);
      headers.append(
        'Authorization',
        `AWS ${config.secretAccessKey}:${getSignature(
          config,
          dateYMD,
          getPolicy(config)
        )}`
      );
      headers.append('Content-Type', 'text/plain');

      console.log('headers');
      console.log(headers);

      const params = {
        method: 'delete',
        headers: headers
      };

      const deleteResult = await fetch(url, params);
      console.log('deleteResult');
      console.log(deleteResult);
      if (!deleteResult.ok) {
        reject(deleteResult);
      } else {
        resolve({
          ok: deleteResult.ok,
          status: deleteResult.status,
          message: 'File Deleted',
          fileName: fileName
        });
      }
    });
  };

  const getSignature = (config, date, policyBase64) => {
    const getSignatureKey = (key, dateStamp, regionName) => {
      const kDate = Crypto.HmacSHA256(dateStamp, 'AWS4' + key);
      const kRegion = Crypto.HmacSHA256(regionName, kDate);
      const kService = Crypto.HmacSHA256('s3', kRegion);
      const kSigning = Crypto.HmacSHA256('aws4_request', kService);
      return kSigning;
    };
    const signature = policyEncoded => {
      return Crypto.HmacSHA256(
        policyEncoded,
        getSignatureKey(config.secretAccessKey, date, config.region)
      ).toString(Crypto.enc.Hex);
    };
    return signature(policyBase64);
  };

  const getPolicy = config => {
    const dateISOString = new Date(+new Date() + 864e5).toISOString();

    console.log('dateISOString {POLICY} : ', dateISOString);

    const xAmzDate = dateISOString
      .split('-')
      .join('')
      .split(':')
      .join('')
      .split('.')
      .join('');
    const dateYMD = dateISOString.split('T')[0].split('-').join('');
    const policy = () => {
      return {
        expiration: dateISOString,
        conditions: [
          { bucket: config.bucketName },
          [
            'starts-with',
            '$key',
            `${config.dirName ? config.dirName + '/' : ''}`
          ],
          { acl: 'public-read' },
          ['starts-with', '$Content-Type', ''],
          { 'x-amz-meta-uuid': '14365123651274' },
          { 'x-amz-server-side-encryption': 'AES256' },
          ['starts-with', '$x-amz-meta-tag', ''],
          {
            'x-amz-credential': `${config.accessKeyId}/${dateYMD}/${config.region}/s3/aws4_request`
          },
          { 'x-amz-algorithm': 'AWS4-HMAC-SHA256' },
          { 'x-amz-date': xAmzDate }
        ]
      };
    };

    return new Buffer(JSON.stringify(policy()))
      .toString('base64')
      .replace(/\n|\r/, '');
  };

  return {
    upload: upload,
    deleteFile: deleteFile
  };
})();

export default s3Helper;
