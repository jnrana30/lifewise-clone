import * as types from '../actions/appTypes';

const initialState = {
  sidebarOpen: true,
  showUpgradeModal: false,
  appLoading: true,
  homeLoading: false,
  startJoyRide: false,
  roles: [],
  roleTypes: [],
  schoolYears: [],
  tags: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0
    },
    filters: {}
  },
  homeLessons: {
    loading: false,
    data: [],
    filters: {}
  },
  home: {
    filters: {},
    classItem: {},
    lessons: [],
    courses: []
  }
};

const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_HOME_LOADING:
      return {
        ...state,
        homeLoading: action.loading
      };

    case types.SET_HOME_FILTERS:
      let courses = JSON.parse(JSON.stringify(state.home.courses));
      Object.keys(courses).map(key => {
        courses[key].filters = action.filters;
        courses[key].paging.page = 1;
      });

      return {
        ...state,
        home: {
          ...state.home,
          filters: action.filters,
          courses: courses
        }
      };

    case types.UPDATE_HOME_CLASS:
      if (action.data.courseId && action.data.coursesLesson) {
        const courseLessonIndex = state.home.courses[
          action.data.courseId
        ].lessons.findIndex(l => l.id === action.data.coursesLesson?.lessonId);
        return {
          ...state,
          home: {
            ...state.home,
            lessons: action.data.lessons,
            courses: {
              ...state.home.courses,
              [action.data.courseId]: {
                ...state.home.courses[action.data.courseId],
                loading: false,
                lessons: [
                  ...state.home.courses[action.data.courseId].lessons.slice(
                    0,
                    courseLessonIndex
                  ),
                  {
                    ...state.home.courses[action.data.courseId].lessons[
                      courseLessonIndex
                    ],
                    ...action.data.coursesLesson
                  },
                  ...state.home.courses[action.data.courseId].lessons.slice(
                    courseLessonIndex + 1
                  )
                ]
              }
            }
          }
        };
      } else {
        return {
          ...state,
          home: {
            ...state.home,
            lessons: action.data.lessons
          }
        };
      }

    case types.CLEAR_HOME_CLASS:
      return {
        ...state,
        home: {
          filters: {},
          classItem: {},
          lessons: [],
          courses: []
        }
      };

    case types.FETCH_HOME_CLASS_COURSE_LESSONS:
      return {
        ...state,
        home: {
          ...state.home,
          courses: {
            ...state.home.courses,
            [action.courseId]: {
              ...state.home.courses[action.courseId],
              loading: true,
              paging: action.paging
            }
          }
        }
      };

    case types.SET_HOME_CLASS_COURSE_LESSONS:
      return {
        ...state,
        home: {
          ...state.home,
          courses: {
            ...state.home.courses,
            [action.courseId]: {
              ...state.home.courses[action.courseId],
              loading: false,
              paging: {
                ...state.home.courses[action.courseId].paging,
                records:
                  action.recordsCount === false ?
                    state.home.courses[action.courseId]
                      .lessonsIds
                      .reduce((count,lessonId) => count + lessonId.length, 0)
                    : action.recordsCount
              },
              lessons: action.lessons.map(e => ({
                ...e,
                ...(state.home.classItem.courses[action.courseId]?.lessons
                  ? state.home.classItem.courses[action.courseId]?.lessons[e.id]
                  : {})
              }))
            }
          }
        }
      };

    case types.SET_HOME_CLASS:
      return {
        ...state,
        home: {
          ...state.home,
          filters: action.filters,
          classItem: action.classItem
        }
      };

    case types.SET_HOME_CLASS_LESSONS:
      return {
        ...state,
        home: {
          ...state.home,
          lessons: action.lessons,
          courses: action.courses
        }
      };

    case types.FETCH_HOME_DATA:
      return {
        ...state,
        homeLessons: {
          ...state.homeLessons,
          loading: true,
          filters: action.filters
        }
      };

    case types.SET_HOME_DATA:
      return {
        ...state,
        homeLessons: {
          ...state.homeLessons,
          loading: false,
          data: action.data.lessons
        }
      };

    case types.FETCH_TAGS:
      return {
        ...state,
        tags: {
          ...state.tags,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.tags.paging,
            ...action.paging
          }
        }
      };

    case types.SET_TAGS:
      return {
        ...state,
        tags: {
          ...state.tags,
          loading: false,
          data: action.data.tags,
          paging: {
            ...state.tags.paging,
            records: action.data.records
          }
        }
      };

    case types.SET_SCHOOL_YEARS:
      return {
        ...state,
        schoolYears: action.data
      };

    case types.SET_ROLE_TYPES:
      return {
        ...state,
        roleTypes: action.data
      };

    case types.SET_ROLES:
      return {
        ...state,
        roles: action.data
      };

    case types.SET_APP_LOADING:
      return {
        ...state,
        appLoading: action.loading
      };

    case types.TOGGLE_SIDEBAR:
      return {
        ...state,
        sidebarOpen: !state.sidebarOpen
      };

    case types.TOGGLE_UPGRADE_MODAL:
      return {
        ...state,
        showUpgradeModal: !state.showUpgradeModal
      };

    case types.SET_JOY_RIDE:
      return {
        ...state,
        startJoyRide: action.setRide
      };

    default:
      return state;
  }
};
export default AppReducer;
