function createCourses(id, title, desc, tags, banner, status) {
  return { id, title, desc, tags, banner, status };
}

export const courses = [
  createCourses(
    Date.now(),
    'Supporting Mental Health Programme for Kids',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    '',
    true
  ),
  createCourses(
    Date.now(),
    'Supporting Mental Health Programme for Kids',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    '',
    true
  ),
  createCourses(
    Date.now(),
    'Supporting Mental Health Programme for Kids',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    '',
    false
  ),
  createCourses(
    Date.now(),
    'Supporting Mental Health Programme for Kids',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    '',
    true
  ),
  createCourses(
    Date.now(),
    'Supporting Mental Health Programme for Kids',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    '',
    true
  ),
  createCourses(
    Date.now(),
    'Supporting Mental Health Programme for Kids',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    '',
    false
  ),
  createCourses(
    Date.now(),
    'Supporting Mental Health Programme for Kids',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    '',
    false
  ),
  createCourses(
    Date.now(),
    'Supporting Mental Health Programme for Kids',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    '',
    true
  )
];

function createLesson(id, title, desc, tags, banner) {
  return {
    id: Math.random().toString(36).substr(2),
    title,
    desc,
    tags,
    banner,
    category: 'Well Being'
  };
}

export const lessons = [
  createLesson(
    Date.now(),
    'Supporting Mental Health Programme for Kids',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    ''
  ),
  createLesson(
    Date.now(),
    'Excepteur sint obcaecat cupiditat non proident culpa.',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    ''
  ),
  createLesson(
    Date.now(),
    'Inmensae subtilitatis, obscuris et malesuada fames.',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    ''
  ),
  createLesson(
    Date.now(),
    'At nos hinc posthac, sitientis piros Afros.',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    ''
  ),
  createLesson(
    Date.now(),
    'Vivamus sagittis lacus vel augue laoreet rutrum faucibus.',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    ''
  ),
  createLesson(
    Date.now(),
    'Tu quoque, Brute, fili mi, nihil timor populi, nihil!',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    ''
  ),
  createLesson(
    Date.now(),
    'Qui ipsorum lingua Celtae, nostra Galli appellantur.',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    ''
  ),
  createLesson(
    Date.now(),
    'Nec dubitamus multa iter quae et nos invenerat.',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    ''
  )
];

function createResource(name, desc, tags, status) {
  return { id: Math.random().toString(36).substr(2), name, desc, tags, status };
}

export const resources = [
  createResource(
    'Demo resource',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['Year 7', 'help', 'health'],
    true
  ),
  createResource(
    'Demo resource 1',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['demo', 'resource'],
    true
  ),
  createResource(
    'Demo resource 2',
    'Ambitioni dedisse scripsisse iudicaretur. Cum sociis natoque penatibus et magnis dis parturient.',
    ['resource', 'new'],
    true
  )
];

function createAssessment(
  studentName,
  excellent,
  understood,
  development,
  comments
) {
  return {
    id: Math.random().toString(36).substr(2),
    studentName,
    excellent,
    understood,
    development,
    comments
  };
}

export const assessments = [
  createAssessment('John Doe', 30, 20, 10, 4),
  createAssessment('Jane Doe', 4, 10, 6, 2)
];

function createAssessmentItem(
  name,
  excellent,
  understood,
  development,
  comment
) {
  return {
    id: Math.random().toString(36).substr(2),
    name,
    excellent,
    understood,
    development,
    comment
  };
}

export const assessment = [
  createAssessmentItem(
    'Fire Safety',
    true,
    false,
    false,
    'Etiam habebis sem dicantur magna mollis euismod. Integer legentibus erat a ante historiarum dapibus.'
  ),
  createAssessmentItem(
    'Water Safety',
    false,
    true,
    false,
    'Etiam habebis sem dicantur magna mollis euismod. Integer legentibus erat a ante historiarum dapibus.'
  ),
  createAssessmentItem(
    'Bank',
    false,
    false,
    true,
    'Etiam habebis sem dicantur magna mollis euismod. Integer legentibus erat a ante historiarum dapibus.'
  ),
  createAssessmentItem(
    'Tax',
    true,
    false,
    false,
    'Etiam habebis sem dicantur magna mollis euismod. Integer legentibus erat a ante historiarum dapibus.'
  )
];

export const classes = [
  {
    label: 'Class 1 A',
    value: 'class1a'
  },
  {
    label: 'Class 1 B',
    value: 'class1b'
  },
  {
    label: 'Class 2 A',
    value: 'class2a'
  },
  {
    label: 'Class 2 B',
    value: 'class2b'
  }
];

export const assessmentForm = [
  {
    id: 0,
    student: 'John Doe',
    comment: '',
    score: 'understood'
  },
  {
    id: 1,
    student: 'Jane Doe',
    comment: '',
    score: 'understood'
  },
  {
    id: 2,
    student: 'Jeet R.',
    comment: '',
    score: 'understood'
  },
  {
    id: 3,
    student: 'Rich W.',
    comment: '',
    score: 'understood'
  },
  {
    id: 4,
    student: 'Anil V.',
    comment: '',
    score: 'understood'
  }
];

function createSubscription(name, price, courses, features) {
  return {
    id: Math.random().toString(36).substr(2),
    name,
    price,
    courses,
    features
  };
}

export const subscriptions = [
  createSubscription(
    'Monthly Subscription',
    499,
    'All courses',
    'Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Plura mihi bona sunt, inclinet, amari petere vellent.'
  ),
  createSubscription(
    'Quaterly Subscription',
    1199,
    [courses[1], courses[4]],
    'Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Plura mihi bona sunt, inclinet, amari petere vellent.'
  ),
  createSubscription(
    'Monthly Basic',
    399,
    [courses[2], courses[0]],
    'Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Plura mihi bona sunt, inclinet, amari petere vellent.'
  )
];
