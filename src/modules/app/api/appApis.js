import api from 'src/api/index';
import { convertObjectToQuerystring } from 'src/utils/helpers';
const _ = require('lodash');

export const fetchRoles = () => {
  return api('/auth/roles', null, 'get');
};

export const fetchRoleTypes = () => {
  return api('/auth/roleTypes', null, 'get');
};

export const searchSchool = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/schools/search?${filter}`, null, 'get');
};

export const fetchSchoolYears = () => {
  return api('/schools/years', null, 'get');
};

export const fetchTags = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  const type = filters?.type && filters.type !== '' ? `/${filters.type}` : '';
  return api(`/courses/tags${type}?${filter}`, null, 'GET');
};

export const createTags = data => {
  return api(`/courses/tags`, data, 'POST');
};

export const updateTag = (tagId, data) => {
  return api(`/courses/tag/${tagId}`, data, 'PATCH');
};
