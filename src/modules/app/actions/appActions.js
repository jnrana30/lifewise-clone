import * as appTypes from './appTypes';

export const toggleSidebar = () => ({ type: appTypes.TOGGLE_SIDEBAR });
export const toggleUpgradeModal = () => ({
  type: appTypes.TOGGLE_UPGRADE_MODAL
});
export const setAppLoading = loading => ({
  type: appTypes.SET_APP_LOADING,
  loading
});
export const fetchJoyRide = data => ({
  type: appTypes.JOY_RIDE,
  setRide: data
});
export const setJoyRide = data => ({
  type: appTypes.SET_JOY_RIDE,
  setRide: data
});

export const fetchRoles = () => ({ type: appTypes.FETCH_ROLES });
export const setRoles = data => ({ type: appTypes.SET_ROLES, data });
export const fetchRoleTypes = () => ({ type: appTypes.FETCH_ROLE_TYPES });
export const setRoleTypes = data => ({ type: appTypes.SET_ROLE_TYPES, data });
export const fetchSchoolYears = () => ({ type: appTypes.FETCH_SCHOOL_YEARS });
export const setSchoolYears = data => ({
  type: appTypes.SET_SCHOOL_YEARS,
  data
});

export const fetchTags = (filters, paging) => ({
  type: appTypes.FETCH_TAGS,
  filters,
  paging
});
export const setTags = data => ({ type: appTypes.SET_TAGS, data });
export const createTag = (data, resolve, reject) => ({
  type: appTypes.CREATE_TAG,
  data,
  resolve,
  reject
});
export const updateTag = (tagId, data, resolve, reject) => ({
  type: appTypes.UPDATE_TAG,
  tagId,
  data,
  resolve,
  reject
});

export const setHomeLoading = loading => ({
  type: appTypes.SET_HOME_LOADING,
  loading
});

export const fetchHomeData = (filters, paging) => ({
  type: appTypes.FETCH_HOME_DATA,
  filters,
  paging
});
export const setHomeData = data => ({ type: appTypes.SET_HOME_DATA, data });

export const setHomeClass = (classItem, filters) => ({
  type: appTypes.SET_HOME_CLASS,
  classItem,
  filters
});
export const setHomeClassLessons = (lessons, courses) => ({
  type: appTypes.SET_HOME_CLASS_LESSONS,
  lessons,
  courses
});

export const fetchHomeDefaultClass = () => ({
  type: appTypes.FETCH_HOME_DEFAULT_CLASS
});

export const fetchHomeClassCourseLessons = (courseId, filters, paging) => ({
  type: appTypes.FETCH_HOME_CLASS_COURSE_LESSONS,
  courseId,
  filters,
  paging
});

export const setHomeClassCourseLessons = (courseId, lessons, recordsCount) => ({
  type: appTypes.SET_HOME_CLASS_COURSE_LESSONS,
  courseId,
  lessons,
  recordsCount,
});

export const clearHomeClass = () => ({ type: appTypes.CLEAR_HOME_CLASS });

export const updateHomeClass = data => ({
  type: appTypes.UPDATE_HOME_CLASS,
  data
});

export const setHomeFilters = filters => ({
  type: appTypes.SET_HOME_FILTERS,
  filters
});
