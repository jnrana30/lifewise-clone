import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  LinearProgress,
  Box
} from '@material-ui/core/';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import moment from 'moment';

function createData(className, year, lastActivity, progress, nextSession) {
  return { className, year, lastActivity, progress, nextSession };
}

const rows = [
  createData(
    'Little Green Tree School',
    7,
    new Date('2021/08/10'),
    [1, 20],
    'Ecosystem Greenery'
  ),
  createData(
    'Quid securi',
    7,
    new Date('2021/10/10'),
    [4, 8],
    'Ecosystem Greenery'
  ),
  createData(
    'Fugiat nulla pariatur',
    7,
    new Date('2021/05/24'),
    [7, 44],
    'Ecosystem Greenery'
  ),
  createData(
    'Etiam tamquam eu',
    7,
    new Date('2021/01/20'),
    [30, 69],
    'Ecosystem Greenery'
  ),
  createData(
    'Ende unde extricat',
    7,
    new Date('2021/06/20'),
    [6, 10],
    'Ecosystem Greenery'
  )
];

const useStyles = makeStyles({
  table: {
    minWidth: 700
  },
  progressBar: {
    height: 10,
    borderRadius: 6
  }
});

export default function MyClasses() {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <TableCell>Class Name</TableCell>
            <TableCell>Year</TableCell>
            <TableCell>Last Activity</TableCell>
            <TableCell>% Progress</TableCell>
            <TableCell>Play Next Session</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => {
            const progress = (row.progress[0] * 100) / row.progress[1];
            return (
              <TableRow key={row.className}>
                <TableCell component="th" scope="row">
                  {row.className}
                </TableCell>
                <TableCell>{row.year}</TableCell>
                <TableCell>{moment(row.lastActivity).fromNow()}</TableCell>
                <TableCell align="left">
                  <LinearProgress
                    className={classes.progressBar}
                    color="primary"
                    variant="determinate"
                    value={progress}
                  />
                  {row.progress[0]}/{row.progress[1]}
                </TableCell>
                <TableCell>
                  <Box display="flex">
                    <div>
                      <PlayCircleOutlineIcon />
                    </div>
                    <div>{row.nextSession}</div>
                  </Box>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
