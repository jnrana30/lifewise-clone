import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography, Chip, } from '@material-ui/core';

import { CourseHeaderContainer, CourseIcon } from './styles';

function CourseHeader({ image, ...props }) {
  return (
    <CourseHeaderContainer image={image}>
      <Box display="flex" p={2}>
        <Box>
          <CourseIcon />
        </Box>
        <Box>
          <h1>Mental Wellbeing Programme</h1>
          <Typography variant="body1">
            Sed haec quis possit intrepidus aestimare tellus. Vivamus sagittis
            lacus vel augue laoreet rutrum faucibus. Ut enim ad minim veniam,
            quis nostrud exercitation. Ab illo tempore, ab est sed immemorabili.
          </Typography>
          <Box mt={2} display="flex" alignItems="center">
            <Chip label={'10-12'}/>
          </Box>
        </Box>
      </Box>
    </CourseHeaderContainer>
  );
}

export default CourseHeader;
