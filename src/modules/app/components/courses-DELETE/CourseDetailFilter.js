import React from 'react';
import { FilterBar, FilterPicker, FilterSearch } from 'src/components/App';
import { Box } from '@material-ui/core';
import { courseStatusOptions } from '../../../../config';

function CourseDetailFilter({
  viewType,
  setViewType,
  courseCategoryOptions = [],
  courseTopicsOptions = [],
  ...props
}) {
  const [category, setCategory] = React.useState('');
  const [topic, setTopic] = React.useState('');
  const [search, setSearch] = React.useState('');

  return (
    <FilterBar nonStickyHeader>
      <Box display="flex" justifyContent="space-between">
        <Box display="flex">
          <FilterSearch
            placeholder="Search lessons"
            value={search}
            onChange={val => {
              setSearch(val);
            }}
            onClear={() => {
              setSearch('');
            }}
          />
          <FilterPicker
            title="Category"
            options={courseCategoryOptions}
            onChange={val => {
              setCategory(val)
            }}
            selected={category}
            onClear={() => {
              setCategory('');
            }}
            ml={2}
          />
          <FilterPicker
            title="Topics"
            options={courseTopicsOptions}
            onChange={val => {
              setTopic(val)
            }}
            selected={topic}
            onClear={() => {
              setTopic('');
            }}
            ml={2}
          />
        </Box>
        <Box display="flex" />
      </Box>
    </FilterBar>
  );
}

export default CourseDetailFilter;
