import styled, { css } from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  media: {
    paddingTop: '40%'
  },
  expand: {
    marginLeft: 'auto'
  },
  tagsContainer: {
    '& > *': {
      margin: theme.spacing(0.5)
    }
  },
  courseTitleWrap: {
    position: 'absolute',
    top: 14,
    padding: '10px 20px',
    height: 'calc(100% - 28px)'
  },
  courseIconWrap: {
    minWidth: 80,
    minHeight: 80,
    height: 80,
    width: 80,
    borderRadius: 80,
    backgroundColor: '#ffffff'
  },
  courseIcon: {
    maxWidth: 50,
    maxHeight: 50
  },
  courseTitle: {
    color: '#ffffff'
  }
}));

export const actionStyles = makeStyles(theme => ({
  button: {
    borderRadius: 40,
    backgroundColor: '#F6AE32',
    color: '#ffffff',
    height: 54,
    fontSize: 20,
    marginBottom: 40,
    '&:hover': {
      backgroundColor: '#f2a218'
    },
    '&:active': {
      backgroundColor: '#f2a218'
    }
  }
}));

export const CourseIcon = styled.div`
  width: 120px;
  height: 120px;
  background-color: #ffffff;
  border-radius: 120px;
  margin-right: 24px;
`;

export const CourseHeaderContainer = styled.div`
  margin: -30px -24px;
  background-color: #f2f2f2;
  padding: 24px;
  margin-bottom: 30px;
  display: flex;
  flex-direction: column;
  ${props =>
    css`
      background-image: url('${props.image}');
      background-size: cover;
      background-position: center;
    `}
  h1 {
    color: #ffffff;
    font-size: 40px;
    margin: 0;
  }
  p {
    color: #ffffff;
  }
`;
