import React from 'react';
import { Box, Typography, Grid } from '@material-ui/core';
import Link from 'next/link';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';

const tags = [
  {
    label: 'Demo 1',
    value: 'demo1'
  },
  {
    label: 'Demo 2',
    value: 'demo'
  }
];

function CourseForm({ isEdit }) {
  const [loading, setLoading] = React.useState(false);

  return (
    <Form
      initialValues={{
        courseName: '',
        lastName: '',
        class: ''
      }}
      validationSchema={Yup.object().shape({
        firstName: Yup.string().required('First Name is required!'),
        lastName: Yup.string().required('Last Name is required!')
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="course-form"
            noValidate
          >
            <Grid container>
              {isEdit && (
                <Grid item xs={12} md={3}>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    mt={4}
                  >
                    <Form.Field.Avatar
                      fullWidth
                      name="avatar"
                      label="Choose Avatar"
                    />
                  </Box>
                </Grid>
              )}
              <Grid item xs={12} md={isEdit ? 6 : 12}>
                <Form.Field.Input
                  fullWidth
                  name="courseName"
                  label="Course Name"
                />
                {/* <Form.Field.Input
                  name="description"
                  multiline
                  fullWidth
                  label="Course Description"
                  inputProps={{
                    style: {
                      height: 100
                    }
                  }}
                /> */}
                <Form.Field.Input
                  fullWidth
                  multiline
                  name="desc"
                  label="Course Description"
                />
                {isEdit && (
                  <React.Fragment>
                    <Form.Field.File
                      fullWidth
                      name="documents"
                      label="Background Image"
                    />
                    <Form.Field.Select
                      options={[
                        {
                          label: 'Published',
                          value: 'published'
                        },
                        {
                          label: 'Unpublished',
                          value: 'unpublished'
                        }
                      ]}
                      fullWidth
                      variant="standard"
                      name="status"
                      label="Status"
                    />
                  </React.Fragment>
                )}

                <Form.Field.AutoComplete
                  fullWidth
                  options={tags}
                  variant="standard"
                  name="yearGroup"
                  label="Year Group"
                />
                <Form.Field.AutoComplete
                  fullWidth
                  options={tags}
                  variant="standard"
                  name="category"
                  label="Category"
                />
                <Form.Field.AutoComplete
                  fullWidth
                  options={tags}
                  variant="standard"
                  name="curriculum"
                  label="Curriculum"
                />
                {isEdit && (
                  <React.Fragment>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        loading={loading}
                        mt={2}
                      >
                        Submit
                      </Button>
                    </Box>
                  </React.Fragment>
                )}
              </Grid>
            </Grid>
          </form>
        );
      }}
    </Form>
  );
}

export default CourseForm;
