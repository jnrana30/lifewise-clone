import React, { useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Box,
  Typography,
  Chip,
  Avatar,
  Badge
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ReorderIcon from '@material-ui/icons/Reorder';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import AddIcon from '@material-ui/icons/Add';
import { Form, Button, Dialog } from 'src/components/shared';
import * as Yup from 'yup';
import images from 'src/config/images';

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}));

const LessonBuilder = ({ lessons }) => {
  const classes = useStyles();
  const [localItems, setLocalItems] = useState(lessons);
  const [loading, setLoading] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);

  const handleDragEnd = (result, provided) => {
    if (!result.destination) {
      return;
    }

    if (result.destination.index === result.source.index) {
      return;
    }

    setLocalItems(prev => {
      const temp = [...prev];
      const d = temp[result.destination?.index];
      temp[result.destination?.index] = temp[result.source.index];
      temp[result.source.index] = d;
      return temp;
    });
  };

  return (
    <React.Fragment>
      <TableContainer component={Paper}>
        <Table className="paddedTable">
          <colgroup>
            <col style={{ width: '5%' }} />
            <col style={{ width: '30%' }} />
            <col style={{ width: '40%' }} />
            <col style={{ width: '25%' }} />
          </colgroup>
          <TableHead>
            <TableRow>
              <TableCell align="left">
                <Button
                  iconButton={true}
                  onClick={() => {
                    setDialogOpen(true);
                  }}
                  style={{ padding: 0 }}
                >
                  <AddIcon />
                </Button>
              </TableCell>
              <TableCell>Lesson</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Tags</TableCell>
            </TableRow>
          </TableHead>
          <DragDropContext onDragEnd={handleDragEnd}>
            <Droppable droppableId="droppable" direction="vertical">
              {droppableProvided => (
                <TableBody
                  ref={droppableProvided.innerRef}
                  {...droppableProvided.droppableProps}
                >
                  {localItems.map((lesson, index) => (
                    <Draggable
                      key={lesson.id}
                      draggableId={lesson.id}
                      index={index}
                    >
                      {(draggableProvided, snapshot) => {
                        return (
                          <TableRow
                            ref={draggableProvided.innerRef}
                            {...draggableProvided.draggableProps}
                            style={{
                              ...draggableProvided.draggableProps.style,
                              background: snapshot.isDragging
                                ? 'rgba(245,245,245, 0.75)'
                                : 'none'
                            }}
                          >
                            {/* note: `snapshot.isDragging` is useful to style or modify behaviour of dragged cells */}
                            <TableCell align="left">
                              <div {...draggableProvided.dragHandleProps}>
                                <ReorderIcon />
                              </div>
                            </TableCell>
                            <TableCell>
                              <Box display="flex" alignItems="center">
                                {/* <Badge
                                overlap="circle"
                                anchorOrigin={{
                                  vertical: 'top',
                                  horizontal: 'right'
                                }}
                                badgeContent={
                                  <CheckCircleIcon color="primary" />
                                }
                              > */}
                                <Avatar
                                  className={classes.avatar}
                                  src={images.app.courseBanner}
                                  size={36}
                                />
                                {/* </Badge> */}
                                <Box ml={2}>
                                  <Typography variant="body1" color="secondary">
                                    <b>{lesson.title}</b>
                                  </Typography>

                                  <Typography
                                    variant="body2"
                                    color="textSecondary"
                                  >
                                    Life Wellbeing
                                  </Typography>
                                </Box>
                              </Box>
                            </TableCell>
                            <TableCell>
                              <Typography variant="body2" color="textPrimary">
                                {lesson.desc}
                              </Typography>
                            </TableCell>
                            <TableCell>
                              {lesson.tags?.length && (
                                <div className={classes.tagsContainer}>
                                  {lesson.tags.map((tag, tagIndex) => {
                                    return (
                                      <Chip
                                        variant="outlined"
                                        key={`courseList-tags-${tagIndex}`}
                                        label={tag}
                                        style={{
                                          marginLeft: 4,
                                          marginBottom: 4
                                        }}
                                      />
                                    );
                                  })}
                                </div>
                              )}
                            </TableCell>
                          </TableRow>
                        );
                      }}
                    </Draggable>
                  ))}
                  {droppableProvided.placeholder}
                </TableBody>
              )}
            </Droppable>
          </DragDropContext>
        </Table>
      </TableContainer>
      <Dialog
        open={dialogOpen}
        title="Add a Lesson to the course."
        onClose={() => {
          setDialogOpen(false);
        }}
      >
        <Form
          initialValues={{
            lesson: ''
          }}
          validationSchema={Yup.object().shape({
            lesson: ''
          })}
          onSubmit={async (values, form) => {
            console.log('values : ', values);
          }}
        >
          {props => {
            return (
              <form
                onSubmit={e => {
                  e.preventDefault();
                  props.submitForm();
                  return false;
                }}
                id="course-form"
                noValidate
              >
                <Form.Field.AutoComplete
                  fullWidth
                  showAvatar={true}
                  options={lessons}
                  variant="standard"
                  name="Lesson"
                  label="Select a lesson"
                  optLabel="title"
                  optValue="id"
                />

                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    fullWidth
                    loading={loading}
                    mt={2}
                  >
                    Submit
                  </Button>
                </Box>
              </form>
            );
          }}
        </Form>
      </Dialog>
    </React.Fragment>
  );
};

export default LessonBuilder;
