import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  CardActions,
  IconButton,
  Typography,
  Chip,
  Menu,
  MenuItem,
  Fade,
  Box
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
// import Image from 'next/image'
import { CardGrid, CardMediaGrid } from '../../../../components/shared/CardGrid';
import { useRouter } from 'next/router';
import images from 'src/config/images';

const useStyles = makeStyles(theme => ({
  media: {
    paddingTop: '40%'
  },
  expand: {
    marginLeft: 'auto'
  },
  tagsContainer: {
    '& > *': {
      margin: theme.spacing(0.5)
    }
  },
  courseTitleWrap: {
    position: 'absolute',
    top: 14,
    padding: '10px 20px',
    height: 'calc(100% - 28px)'
  },
  courseIconWrap: {
    minWidth: 80,
    minHeight: 80,
    height: 80,
    width: 80,
    borderRadius: 80,
    backgroundColor: '#ffffff'
  },
  courseIcon: {
    maxWidth: 50,
    maxHeight: 50
  },
  courseTitle: {
    color: '#ffffff'
  }
}));

export default function CourseCard({ course, ...props }) {
  const classes = useStyles();
  const router = useRouter();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <CardGrid>
      <CardActionArea
        onClick={() => {
          router.push(`/courses/${course.id}`);
        }}
      >
        <CardMediaGrid
          image={images.app.courseBanner1}
          title={course.title}
        />
        <Box
          display="flex"
          alignItems="center"
          className={classes.courseTitleWrap}
        >
          <Typography variant="h6" className={classes.courseTitle}>
            <b>{course.title}</b>
          </Typography>
          <Box
            className={classes.courseIconWrap}
            display="flex"
            alignItems="center"
            justifyContent="center"
          >
            <img src={images.icons.courseIcon} className={classes.courseIcon} />
          </Box>
        </Box>
      </CardActionArea>
      <CardContent>
        <Typography variant="body1" color="textSecondary" component="p">
          {course.desc}
        </Typography>
      </CardContent>

      <CardActions disableSpacing>
        {course.tags?.length && (
          <div className={classes.tagsContainer}>
            {course.tags.map((tag, index) => {
              return (
                <Chip
                  variant="outlined"
                  key={`course-${props.courseIndex}-tags-${index}`}
                  label={tag}
                />
              );
            })}
          </div>
        )}
        <IconButton
          aria-label="settings"
          className={classes.expand}
          onClick={handleClick}
        >
          <MoreVertIcon color="primary" />
        </IconButton>

        <Menu
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          TransitionComponent={Fade}
        >
          <MenuItem onClick={handleClose}>Action 01</MenuItem>
          <MenuItem onClick={handleClose}>Action 02</MenuItem>
        </Menu>
      </CardActions>
    </CardGrid>
  );
}
