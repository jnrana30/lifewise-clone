import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Box,
  Chip,
  Avatar
} from '@material-ui/core/';
import { Button, ContextMenu } from 'src/components/shared';
import Link from 'next/link';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import images from 'src/config/images';
import { useStyles } from './styles';

function CoursesList({ courses, onEdit, onDelete, ...props }) {
  const classes = useStyles();

  return (
    <TableContainer component={Paper}>
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
            <TableCell>Course</TableCell>
            <TableCell>Tags</TableCell>
            <TableCell>Status</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {courses.map((course, index) => {
            return (
              <TableRow key={`courselist-${index}`}>
                <TableCell>
                  <Box display="flex" alignItems="center">
                    <Box
                      className={classes.courseIconWrap}
                      display="flex"
                      alignItems="center"
                      justifyContent="center"
                    >
                      <img
                        src={images.icons.courseIcon}
                        className={classes.courseIcon}
                      />
                    </Box>
                    <Box>
                      <Link href={`/courses/${course?.id}`}>
                        <Typography variant="body1" color="secondary">
                          {course.title}
                        </Typography>
                      </Link>
                      <Typography variant="body2">{course.desc}</Typography>
                    </Box>
                  </Box>
                </TableCell>
                <TableCell>
                  <div className={classes.tagsContainer}>
                    {course.tags.map((tag, index) => {
                      return (
                        <Chip
                          variant="outlined"
                          key={`course-${props.courseIndex}-tags-${index}`}
                          label={tag.tagName}
                          style={{
                            backgroundColor: tag.color ? tag.color : '#ffffff',
                            color: tag.color ? '#ffffff' : '#333333'
                          }}
                        />
                      );
                    })}
                  </div>
                </TableCell>
                <TableCell>
                  <Typography variant="body2" color="textPrimary">
                    <Chip
                      label={course.status ? 'Active' : 'Inactive'}
                      color={course.status ? 'primary' : 'default'}
                    />
                  </Typography>
                </TableCell>
                <TableCell width="5%" className="flex-justify-content-end">
                  {/* <Button iconButton={true} onClick={() => {}}>
                    <MoreVertIcon />
                  </Button> */}
                  <ContextMenu
                    options={[
                      {
                        label: 'Edit',
                        icon: <EditIcon />,
                        onClick: () => {
                          onEdit(course);
                        }
                      }
                    ]}
                  />
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default CoursesList;
