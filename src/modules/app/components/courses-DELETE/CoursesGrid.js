import React from 'react';
import { Grid } from '@material-ui/core';
import CourseCard from './CourseCard';
import { ContentCardContainer } from '../../../../components/shared/Form/File/styles';
import { GridContainer } from '../../../../components/shared/CardGrid';

function CoursesGrid({ courses }) {
  return (
    <ContentCardContainer>
      <Grid container spacing={2}>
        {courses.map((course, index) => {
          return (
            <GridContainer
              key={`course-${index}`}
              item
              xs={12}
              s={12}
              sm={6}
              lg={3}
              xl={3}
              md={3}
            >
              <CourseCard courseIndex={index} course={course} />
            </GridContainer>
          );
        })}
      </Grid>
    </ContentCardContainer>
  );
}

export default CoursesGrid;
