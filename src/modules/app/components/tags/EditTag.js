import React from 'react';
import { Form } from 'src/components/shared';
import ColorPicker from 'material-ui-color-picker';
import { tagTypes } from 'src/config';
import * as Yup from 'yup';

function EditTag({ tag, onSubmit }) {
  return (
    <Form
      initialValues={{
        tagName: tag?.tagName ? tag?.tagName : '',
        color: tag?.color ? tag?.color : '',
        tagType: tag?.tagType ? tag?.tagType : ''
      }}
      enableReinitialize={true}
      validationSchema={Yup.object().shape({
        tagName: Yup.string().required('Tag name is required!'),
        color: Yup.string().required('Color is required!'),
        tagType: Yup.string().required('Tag type is required!')
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
        onSubmit(values);
        form.prevent;
      }}
    >
      {({ values, ...prop }) => {
        console.log(prop?.errors);
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              prop.submitForm();
              return false;
            }}
            id="tags-form"
            noValidate
          >
            <Form.Field.Input fullWidth name="tagName" label="Tag Name" />
            {/* <ColorPicker
              label="Choose color"
              name="color"
              defaultValue={values.color}
              fullWidth
              variant="standard"
              onChange={color => {
                prop.setFieldValue('color', color);
              }}
            /> */}
            <Form.Field.ColorPicker
              label="Choose color"
              name="color"
              fullWidth
              variant="standard"
            />
            <Form.Field.Select
              options={tagTypes}
              fullWidth
              name="tagType"
              label="Tag type"
            />
          </form>
        );
      }}
    </Form>
  );
}

export default EditTag;
