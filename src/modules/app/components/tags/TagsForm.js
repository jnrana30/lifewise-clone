import React from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { Formik, FieldArray } from 'formik';
import _ from 'lodash';
import { tagTypes } from 'src/config';

const defaultTags = [
  {
    tagName: '',
    color: '',
    tagType: ''
  }
];

export default function ClassDetails({ onSubmit }) {
  return (
    <Formik
      initialValues={{ tags: defaultTags }}
      onSubmit={async values => {
        onSubmit(values);
      }}
      validationSchema={Yup.object().shape({
        tags: Yup.array().of(
          Yup.object().shape({
            tagName: Yup.string().required('required!'),
            color: Yup.string().required('required!'),
            tagType: Yup.string().required('required!')
          })
        )
      })}
      enableReinitialize={true}
    >
      {({ values, ...prop }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            prop.submitForm();
            return false;
          }}
          id="tags-form"
          noValidate
        >
          <FieldArray
            name="tags"
            render={arrayHelpers => (
              <div>
                {values.tags &&
                  values.tags.length > 0 &&
                  values.tags.map((obj, index) => (
                    <React.Fragment key={`class-${index}-${obj.day}`}>
                      <Grid container spacing={2} alignItems="center">
                        <Grid item>
                          <Typography style={{ marginTop: 20 }} variant="h5">
                            {index + 1}.
                          </Typography>
                        </Grid>
                        <Grid
                          container
                          item
                          spacing={2}
                          alignItems="center"
                          xs={10}
                        >
                          <Grid item xs={6}>
                            <Form.Field.Input
                              fullWidth
                              variant="standard"
                              name={`tags[${index}].tagName`}
                              label="Tag name"
                            />
                          </Grid>
                          <Grid item xs={6}>
                            <Form.Field.Select
                              options={tagTypes}
                              fullWidth
                              variant="standard"
                              name={`tags[${index}].tagType`}
                              label="Tag type"
                            />
                          </Grid>
                          <Grid item xs={6}>
                            <Box mt={1}>
                              <Form.Field.ColorPicker
                                label="Choose color"
                                name={`tags[${index}].color`}
                                fullWidth
                                variant="standard"
                              />
                            </Box>
                          </Grid>
                        </Grid>

                        <Grid item xs={1}>
                          {values.tags.length > 1 && (
                            <Button
                              iconButton={true}
                              onClick={() => {
                                arrayHelpers.remove(index);
                              }}
                            >
                              <CloseIcon />
                            </Button>
                          )}
                        </Grid>
                      </Grid>
                    </React.Fragment>
                  ))}
                <Box my={2}>
                  <Button
                    color="primary"
                    onClick={() => {
                      arrayHelpers.push(defaultTags[0]);
                    }}
                  >
                    + Add tag
                  </Button>
                </Box>
              </div>
            )}
          />
        </form>
      )}
    </Formik>
  );
}
