import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Typography,
  Box
} from '@material-ui/core/';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, ContextMenu } from 'src/components/shared';
import EditIcon from '@material-ui/icons/Edit';
import { ContentContainer } from '../../../../components/shared/Form/File/styles';

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}));

function TagsList({ loading, tags, paging, onPaging, onEdit }) {
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  return (
    <ContentContainer>
      <Paper>
        <TableContainer>
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>Tags</TableCell>
                <TableCell>Tag Type</TableCell>
                <TableCell>Color</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loading ? (
                <TableRow>
                  <TableCell colSpan={6}>
                    <Skeleton animation="wave" />
                  </TableCell>
                </TableRow>
              ) : (
                <React.Fragment>
                  {tags && tags.length > 0 ? (
                    <React.Fragment>
                      {tags.map((tag, index) => {
                        return (
                          <TableRow key={`tag-list-${index}`}>
                            <TableCell>
                              <Typography
                                variant="body1"
                                style={{ cursor: 'pointer' }}
                                color="secondary"
                                onClick={() => {
                                  onEdit(tag);
                                }}
                              >
                                {tag.tagName}
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <Typography variant="body2" color="textPrimary">
                                {tag?.tagType}
                              </Typography>
                            </TableCell>
                            <TableCell width="20%" align="center">
                              <div
                                style={{
                                  height: 20,
                                  height: 20,
                                  borderRadius: 20,
                                  backgroundColor: tag?.color
                                }}
                              />
                              <Typography variant="body2" color="textPrimary">
                                {tag?.color}
                              </Typography>
                            </TableCell>
                            <TableCell
                              width="5%"
                              className="flex-justify-content-end"
                            >
                              <ContextMenu
                                options={[
                                  {
                                    label: 'Edit',
                                    icon: <EditIcon />,
                                    onClick: () => {
                                      onEdit(tag);
                                    }
                                  }
                                ]}
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </React.Fragment>
                  ) : (
                    <TableRow>
                      <TableCell colSpan={6} align="center">
                        <Typography color="textSecondary">
                          No data found
                        </Typography>
                      </TableCell>
                    </TableRow>
                  )}
                </React.Fragment>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={paging?.records ? paging.records : 0}
          rowsPerPage={paging.perPage}
          page={paging.page - 1}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </ContentContainer>
  );
}

export default TagsList;
