import React from 'react';
import {FilterBar, FilterSearch, FilterPicker, FilterDrawer} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';
import { tagTypes } from 'src/config';
import useViewport from '../../../../utils/ViewPort';

function EmployeesFilter({ filters, setFilter, unsetFilter, ...props }) {
  const [search, setSearch] = React.useState('');
  const { isMobileView } = useViewport();
  const classes = useStyles();

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={filters['search'] ? filters['search'] : ''}
      onChange={val => {
        setFilter({
          search: val
        });
      }}
      onClear={() => {
        unsetFilter('search');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
      <FilterPicker
        title="Tag Type"
        options={tagTypes}
        onChange={val => {
          setFilter({
            type: val.value
          });
        }}
        selected={filters['type'] ? filters['type'] : ''}
        onClear={() => {
          unsetFilter('type');
        }}
        ml={2}
      />
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView &&
          <Box display="flex">{filterOptions}</Box>
        }
        {isMobileView &&
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        }
        <Box display="flex">
          <Button
            className={classes.addButton}
            variant="contained"
            color="primary"
            aria-controls="simple-menu"
            onClick={() => {
              props.setDrawerOpen(true);
            }}
            startIcon={<AddIcon />}
          >
            Add Tag
          </Button>
        </Box>
      </Box>
    </FilterBar>
  );
}

export default EmployeesFilter;
