import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  CardActionArea,
  CardActions,
  CardContent,
  Typography,
  Tooltip
} from '@material-ui/core';
import { Button } from '../../../../components/shared';
import {
  CardGrid,
  CardMediaGrid
} from '../../../../components/shared/CardGrid';
import s3Helper from 'src/utils/s3Helper';
import { viewPDFInNewTab } from 'src/utils/helpers';

const useStyles = makeStyles(theme => ({
  media: {
    height: 140
  },
  cardContentHeading: {
  },
  cardContent: {
    paddingBottom: 0,
    marginBottom: 46,
  },
  cardContentSubHeading: {
    lineHeight: 1.2,
    fontSize: 14,
    display: '-webkit-box',
    '-webkit-line-clamp': 2,
    '-webkit-box-orient': 'vertical',
    overflow: 'hidden',
    color: '#3A3A3A',
  },
  cardActions: {
    position: 'absolute',
    width: '100%',
    bottom: 0,
  },
  openAction: {
    marginLeft: -6,
  },
}));

const ResourceCard = props => {
  const classes = useStyles();
  const { resource, onOpen = () => {} } = props;

  const handleOpenClick = event => {
    onOpen();
  };

  const icon =
    resource?.image && resource?.image !== ''
      ? s3Helper.getFileUrl(resource?.image)
      : false;

  return (
    <CardGrid>
      <CardActionArea
        onClick={() => {
          viewPDFInNewTab(s3Helper.getFileUrl(resource.documents[0]));
        }}
      >
        <CardMediaGrid image={icon} title={resource.resourceName} />
      </CardActionArea>
      <CardContent className={classes.cardContent}>
        <Box className={classes.cardContentHeading}>
          {/*<Typography*/}
          {/*  color="textPrimary"*/}
          {/*  component="p"*/}
          {/*  style={{ marginBottom: 10 }}*/}
          {/*>*/}
          {/*  <b>{resource.resourceName}</b>*/}
          {/*</Typography>*/}
          <Tooltip title={resource.resourceDescription} arrow>
            <Typography
              color="textSecondary"
              variant="body2"
              className={classes.cardContentSubHeading}
            >
              {resource.resourceDescription}
            </Typography>
          </Tooltip>
        </Box>
      </CardContent>
      <CardActions className={classes.cardActions}>
        <Button
          size="small"
          color="primary"
          onClick={() => {
            viewPDFInNewTab(s3Helper.getFileUrl(resource.documents[0]))
          }}
          className={classes.openAction}
        >
          Open
        </Button>
      </CardActions>
    </CardGrid>
  );
};

export default ResourceCard;
