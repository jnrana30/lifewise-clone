import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Box,
  Chip,
  Avatar
} from '@material-ui/core/';
import Skeleton from '@material-ui/lab/Skeleton';
import EditIcon from '@material-ui/icons/Edit';
import _ from 'lodash';
import { ContextMenu } from 'src/components/shared';
import s3Helper from 'src/utils/s3Helper';
import { ResourceImage } from './style';
import { Tag } from 'src/components/App';
import GetAppIcon from '@material-ui/icons/GetApp';

function ResourcesList({ resources, loading, paging, onPaging, onEdit }) {
  const router = useRouter();

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  return (
    <Paper>
      <TableContainer>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Tags</TableCell>
              <TableCell>Type</TableCell>
              <TableCell>Status</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={5}>
                  <Skeleton animation="wave" />
                </TableCell>
              </TableRow>
            ) : (
              <React.Fragment>
                {resources.map((resource, index) => {
                  const icon =
                    resource?.image && resource?.image !== ''
                      ? s3Helper.getFileUrl(resource?.image)
                      : false;

                  return (
                    <TableRow key={`resourcesList-${index}`}>
                      <TableCell width="25%">
                        <Box display="flex" alignItems="center">
                          <Box mr={2}>
                            {/* <Avatar>{school.name.charAt(0)}</Avatar> */}
                            {icon && <ResourceImage src={icon} />}
                          </Box>
                          <Typography
                            variant="body1"
                            color="secondary"
                            style={{ cursor: 'pointer' }}
                            onClick={() => {
                              onEdit(resource);
                            }}
                          >
                            {resource.resourceName}
                          </Typography>
                        </Box>
                      </TableCell>
                      <TableCell width="40%">
                        <Typography variant="body2" color="textPrimary">
                          {resource.resourceDescription}
                        </Typography>
                      </TableCell>
                      <TableCell width="25%">
                        {resource?.tags?.length ? (
                          <React.Fragment>
                            {resource?.tags?.map((tag, tagIndex) => {
                              return (
                                <Tag
                                  tag={tag}
                                  key={`resource-${resource.id}-tag-${tagIndex}`}
                                />
                              );
                            })}
                          </React.Fragment>
                        ) : null}
                      </TableCell>
                      <TableCell width="10%">{resource.resourceType}</TableCell>
                      <TableCell width="10%">
                        {resource.resourceStatus ? (
                          <Chip
                            label={resource.resourceStatus}
                            color={
                              resource.resourceStatus === 'published'
                                ? 'primary'
                                : 'default'
                            }
                          />
                        ) : (
                          <span>-</span>
                        )}
                      </TableCell>
                      <TableCell
                        width="5%"
                        className="flex-justify-content-end"
                      >
                        <ContextMenu
                          options={[
                            {
                              label: 'Edit',
                              icon: <EditIcon />,
                              onClick: () => {
                                onEdit(resource);
                              }
                            },
                            {
                              label: 'Download',
                              icon: <GetAppIcon />,
                              onClick: () => {
                                window.open(
                                  s3Helper.getFileUrl(resource.documents[0])
                                );
                              }
                            }
                          ]}
                        />
                      </TableCell>
                    </TableRow>
                  );
                })}
              </React.Fragment>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        component="div"
        count={paging?.records ? paging.records : 0}
        rowsPerPage={paging.perPage}
        page={paging.page - 1}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
}

export default ResourcesList;
