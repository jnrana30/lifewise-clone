import React from 'react';
import { Box } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { getTagsByType } from 'src/modules/courses/api/coursesApis';
import { resourceStatusOptions, resourceTypeOptions } from 'src/config/index';
import { FormHelperText } from '@material-ui/core';

function ResourceForm({ resource, onSubmit }) {
  const isEdit = resource && resource?.id !== '';

  const searchTags = async (search, type) => {
    const filter = {
      search
    };
    const res = await getTagsByType(type, filter);
    return res?.tags ? res?.tags : [];
  };

  return (
    <Form
      initialValues={{
        resourceName: resource?.resourceName ? resource?.resourceName : '',
        resourceDescription: resource?.resourceDescription
          ? resource?.resourceDescription
          : '',
        image: resource?.image ? resource?.image : '',
        documents: resource?.documents ? resource?.documents : [],
        resourceStatus: resource?.resourceStatus
          ? resource?.resourceStatus
          : 'published',
        resourceType: resource?.resourceType
          ? resource?.resourceType
          : 'resource',
        yearGroup: resource?.tags
          ? resource?.tags.filter(tag => tag?.tagType === 'yearGroup')
          : [],
        category: resource?.tags
          ? resource?.tags.filter(tag => tag?.tagType === 'category')[0]
            ? resource?.tags.filter(tag => tag?.tagType === 'category')[0]
            : {}
          : {},
        curriculum: resource?.tags
          ? resource?.tags.filter(tag => tag?.tagType === 'curriculum')
          : []
      }}
      enableReinitialize={true}
      validationSchema={Yup.object().shape({
        resourceName: Yup.string().required('Resource Name is required!'),
        resourceDescription: Yup.string().required(
          'Resource Description is required!'
        ),
        image: Yup.object().nullable(true),
        documents: Yup.object()
          .nullable(true)
          .test(
            'document-check',
            'Please upload a document!',
            async (value, { createError }) => {
              if (value && value.key && value.key !== '') {
                return true;
              }
              return false;
            }
          ),
        resourceStatus: Yup.string().nullable(true),
        category: Yup.object().nullable(true),
        curriculum: Yup.array().nullable(true),
        yearGroup: Yup.array().nullable(true)
      })}
      onSubmit={async (values, form) => {
        let data = { ...values };
        data.tags = [
          ...(data?.yearGroup?.length
            ? data?.yearGroup.map(tag => tag.id)
            : []),
          ...(data?.curriculum?.length
            ? data?.curriculum.map(tag => tag.id)
            : []),
          ...(data?.category?.id ? [data?.category?.id] : [])
        ];

        if (
          data.documents &&
          typeof data.documents === 'object' &&
          typeof data.documents?.key !== 'undefined' &&
          data.documents?.key !== ''
        ) {
          data.documents = [data.documents.key];
        }
        if (
          data.image &&
          typeof data.image === 'object' &&
          typeof data.image?.key !== 'undefined' &&
          data.image?.key !== ''
        ) {
          data.image = data.image.key;
        }

        data = _.omit(data, ['category', 'curriculum', 'topics', 'yearGroup']);
        console.log('data : ', data);
        onSubmit(data);
      }}
    >
      {props => {
        // console.log('props.errors : ', props.errors);
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="resource-form"
            noValidate
          >
            <Box display="flex" flexDirection="row" spacing={2} mb={4}>
              <Box mt={4} mr={2}>
                <Form.Field.Avatar
                  fullWidth
                  name="image"
                  label="Choose image"
                  folder="resources/images"
                />
              </Box>
              <Box>
                <Form.Field.Input fullWidth name="resourceName" label="Name" />
                <Form.Field.Input
                  fullWidth
                  multiline
                  name="resourceDescription"
                  label="Description"
                />
              </Box>
            </Box>
            <Form.Field.File
              fullWidth
              name="documents"
              label="Upload Document"
              folder="resources/documents"
              accept=".xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf"
            />
            {props?.errors && props?.errors?.documents && (
              <FormHelperText error={true} style={{ marginTop: -14 }}>
                {props?.errors?.documents}
              </FormHelperText>
            )}

            <Form.Field.Select
              options={resourceTypeOptions}
              fullWidth
              variant="standard"
              name="resourceType"
              label="Resource Type"
              showNone={false}
            />

            {isEdit && (
              <Form.Field.Select
                options={resourceStatusOptions}
                fullWidth
                variant="standard"
                name="resourceStatus"
                label="Status"
              />
            )}
            <Form.Field.AutoComplete
              multiple={true}
              fullWidth
              showAvatar={false}
              options={[]}
              variant="standard"
              remoteMethod={val => searchTags(val, 'yearGroup')}
              name="yearGroup"
              label="Year Group"
              optLabel="tagName"
              optValue="id"
            />
            <Form.Field.AutoComplete
              multiple={false}
              fullWidth
              showAvatar={false}
              options={[]}
              variant="standard"
              remoteMethod={val => searchTags(val, 'category')}
              name="category"
              label="Choose Category"
              optLabel="tagName"
              optValue="id"
            />
            <Form.Field.AutoComplete
              multiple={true}
              fullWidth
              options={[]}
              remoteMethod={val => searchTags(val, 'curriculum')}
              variant="standard"
              name="curriculum"
              label="Curriculum"
              optLabel="tagName"
              optValue="id"
            />
          </form>
        );
      }}
    </Form>
  );
}

export default ResourceForm;
