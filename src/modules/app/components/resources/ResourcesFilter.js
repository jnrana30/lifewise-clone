import React from 'react';
import {
  FilterBar,
  FilterDrawer,
  FilterPicker,
  FilterSearch,
  AccessControl
} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';
import useViewport from '../../../../utils/ViewPort';
import { resourceStatusOptions, resourceTypeOptions } from 'src/config/index';

function ResourcesFilter({ filters, setFilter, unsetFilter, ...props }) {
  const [status, setStatus] = React.useState(false);
  const [search, setSearch] = React.useState('');
  const classes = useStyles();
  const { isMobileView } = useViewport();

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={filters['search'] ? filters['search'] : ''}
      onChange={val => {
        setFilter({
          search: val
        });
      }}
      onClear={() => {
        unsetFilter('search');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
      <AccessControl level={6}>
        <FilterPicker
          title="Status"
          options={resourceStatusOptions}
          onChange={val => {
            setFilter({
              resourceStatus: val.value
            });
          }}
          selected={filters['resourceStatus'] ? filters['resourceStatus'] : ''}
          onClear={() => {
            unsetFilter('resourceStatus');
          }}
          ml={2}
        />
        <FilterPicker
          title="Type"
          options={resourceTypeOptions}
          onChange={val => {
            setFilter({
              type: val.value
            });
          }}
          selected={filters['type'] ? filters['type'] : ''}
          onClear={() => {
            unsetFilter('type');
          }}
          ml={2}
        />
      </AccessControl>
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView && <Box display="flex">{filterOptions}</Box>}
        {isMobileView && (
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        )}
        <Box display="flex">
          <AccessControl level={6}>
            <Button
              className={classes.addButton}
              variant="contained"
              color="primary"
              aria-controls="simple-menu"
              onClick={() => {
                props.setDrawerOpen(true);
              }}
              startIcon={<AddIcon />}
            >
              Add Resource
            </Button>
          </AccessControl>
        </Box>
      </Box>
    </FilterBar>
  );
}

export default ResourcesFilter;
