import React from 'react';
import { Grid, Box } from '@material-ui/core';
import ResourceCard from './ResourceCard';
import Pagination from '@material-ui/lab/Pagination';
import { ContentCardContainer } from 'src/components/shared/Form/File/styles';
import {
  CardSkeleton,
  CardEmpty,
  GridContainer
} from 'src/components/shared/CardGrid';
import _ from 'lodash';

const ResourcesCardList = ({
  resources = [],
  onOpen,
  paging,
  loading,
  onPaging
}) => {
  const pages =
    paging.records > 0 ? Math.ceil(paging.records / paging.perPage) : 0;

  return (
    <ContentCardContainer>
      <Grid container spacing={2}>
        {loading ? (
          <React.Fragment>
            {_.times(paging.perPage, i => (
              <GridContainer
                key={`skeleton-lesson-card-${i}`}
                item
                xs={12}
                s={12}
                sm={6}
                lg={3}
                xl={3}
                md={3}
              >
                <CardSkeleton key={`lessons-loader-${i}`} />
              </GridContainer>
            ))}
          </React.Fragment>
        ) : (
          <React.Fragment>
            {resources.map((resource, index) => {
              return (
                <GridContainer
                  key={`resource-card-list-${index}`}
                  item
                  xs={12}
                  s={12}
                  sm={6}
                  lg={3}
                  xl={3}
                  md={3}
                >
                  <ResourceCard resource={resource} onOpen={onOpen} />
                </GridContainer>
              );
            })}
          </React.Fragment>
        )}
      </Grid>
      <Grid container>
        <Box m="auto" mt={4}>
          <Pagination
            count={pages}
            page={paging.page}
            shape="rounded"
            onChange={(e, val) => {
              onPaging(val - 1);
            }}
          />
        </Box>
      </Grid>
    </ContentCardContainer>
  );
};

export default ResourcesCardList;
