import React from 'react';
import { FilterBar, FilterPicker, FilterSearch } from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ViewListIcon from '@material-ui/icons/ViewList';
import AddIcon from '@material-ui/icons/Add';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import { useStyles } from './style';

function LessonsFilter({ viewType, setViewType, isAdmin, classesOption, userProfile, ...props }) {
  const classes = useStyles();
  const [yearFilter, setYearFilter] = React.useState(false);
  const [status, setStatus] = React.useState(false);
  const [search, setSearch] = React.useState('');

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        <Box display="flex">
          <FilterSearch
            placeholder="Search"
            value={search}
            onChange={val => {
              setSearch(val);
            }}
            onClear={() => {
              setSearch('');
            }}
          />
          <FilterPicker
            title="Class Name"
            options={classesOption}
            onChange={val => {
              setStatus(val);
            }}
            selected={status}
            onClear={() => {
              setStatus(false);
            }}
            ml={2}
            mr={1}
          />

          {/*<FilterPicker*/}
          {/*  title="Year Group 1"*/}
          {/*  icon={<CalendarTodayIcon />}*/}
          {/*  options={[*/}
          {/*    {*/}
          {/*      label: 'Option 01',*/}
          {/*      value: 'Option-01'*/}
          {/*    },*/}
          {/*    {*/}
          {/*      label: 'Option 02',*/}
          {/*      value: 'Option-02'*/}
          {/*    }*/}
          {/*  ]}*/}
          {/*  onChange={val => {*/}
          {/*    setYearFilter(val);*/}
          {/*  }}*/}
          {/*  selected={yearFilter}*/}
          {/*  onClear={() => {*/}
          {/*    setYearFilter(false);*/}
          {/*  }}*/}
          {/*  ml={2}*/}
          {/*  mr={1}*/}
          {/*/>*/}

          <FilterPicker
            title="Status"
            icon={<ViewModuleIcon />}
            options={[
              {
                label: 'Option 01',
                value: 'Option-01'
              },
              {
                label: 'Option 02',
                value: 'Option-02'
              }
            ]}
            onChange={val => {
              setStatus(val);
            }}
            selected={status}
            onClear={() => {
              setStatus(false);
            }}
            ml={2}
            mr={1}
          />
        </Box>
        <Box display="flex">
          {!isAdmin && (
            <React.Fragment>
              <Button
                className={classes.iconButton}
                iconButton={true}
                onClick={() => {
                  setViewType('grid');
                }}
                color={viewType === 'grid' ? 'primary' : 'default'}
              >
                <ViewModuleIcon />
              </Button>
              <Button
                className={classes.iconButton}
                iconButton={true}
                onClick={() => {
                  setViewType('list');
                }}
                color={viewType === 'list' ? 'primary' : 'default'}
              >
                <ViewListIcon />
              </Button>
            </React.Fragment>
          )}
          {userProfile?.role?.level > 5 &&
            <Box display="flex" ml={1}>
              <Button
                className={classes.addButton}
                variant="contained"
                color="primary"
                aria-controls="simple-menu"
                onClick={() => {
                  props.setDrawerOpen(true);
                }}
                startIcon={<AddIcon />}
              >
                Add Lesson
              </Button>
            </Box>
          }
        </Box>
      </Box>
    </FilterBar>
  );
}

export default LessonsFilter;
