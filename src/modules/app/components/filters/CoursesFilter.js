import React from 'react';
import { FilterBar, FilterPicker, FilterSearch } from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';

function CoursesFilter({ isAdmin, userProfile, ...props }) {
  const [yearFilter, setYearFilter] = React.useState(false);
  const [search, setSearch] = React.useState('');
  const [tagFilter, setTagFilter] = React.useState(false);
  const classes = useStyles();
  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        <Box display="flex">
          <FilterSearch
            placeholder="Search"
            value={search}
            onChange={val => {
              setSearch(val);
            }}
            onClear={() => {
              setSearch('');
            }}
          />
          {isAdmin && (
            <React.Fragment>
              <FilterPicker
                title="Tag"
                options={[
                  {
                    label: 'Option 01',
                    value: 'Option-01'
                  },
                  {
                    label: 'Option 02',
                    value: 'Option-02'
                  }
                ]}
                onChange={val => {
                  setTagFilter(val);
                }}
                selected={tagFilter}
                onClear={() => {
                  setTagFilter(false);
                }}
                icon={<LocalOfferIcon />}
                ml={2}
              />
              <FilterPicker
                title="Status"
                options={[
                  {
                    label: 'Active',
                    value: 'active'
                  },
                  {
                    label: 'Inactive',
                    value: 'Inactive'
                  }
                ]}
                onChange={val => {
                  setTagFilter(val);
                }}
                selected={tagFilter}
                onClear={() => {
                  setTagFilter(false);
                }}
                ml={2}
              />
            </React.Fragment>
          )}
          {!isAdmin && (
            <FilterPicker
              title="Year Group"
              options={[
                {
                  label: 'Option 01',
                  value: 'Option-01'
                },
                {
                  label: 'Option 02',
                  value: 'Option-02'
                }
              ]}
              onChange={val => {
                setYearFilter(val);
              }}
              selected={yearFilter}
              onClear={() => {
                setYearFilter(false);
              }}
              icon={<CalendarTodayIcon />}
              ml={2}
              mr={1}
            />
          )}
        </Box>
        {userProfile?.role?.level > 5 &&
          <Button
            className={classes.addButton}
            variant="contained"
            color="primary"
            aria-controls="simple-menu"
            onClick={() => {
              props.setDrawerOpen(true);
            }}
            startIcon={<AddIcon />}
          >
            Add Course
          </Button>
        }
      </Box>
    </FilterBar>
  );
}

export default CoursesFilter;
