import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
  Switch,
  Divider
} from '@material-ui/core';
import images from 'src/config/images';
import { Button } from 'src/components/shared';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import DeleteIcon from '@material-ui/icons/Delete';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  FileIcon: {
    maxWidth: 36
  }
}));

const files = [
  {
    name: 'Annual Presentation.ppt',
    date: '2021/01/01',
    size: '1.5mb'
  },
  {
    name: 'IMPORTANT Documents.doc',
    date: '2021/05/21',
    size: '0.25mb'
  },
  {
    name: 'Class-Recording-06/07/2021.mp3',
    date: '2021/05/30',
    size: '3.4mb'
  },
  {
    name: 'School-Banner.jpg',
    date: '2021/06/01',
    size: '1.1mb'
  },
  {
    name: 'Kids-presentation.psd',
    date: '2021/06/02',
    size: '4.7mb'
  },
  {
    name: 'Handbook.pdf',
    date: '2021/06/10',
    size: '0.4mb'
  }
];

export default function FolderList() {
  const classes = useStyles();
  return (
    <List disablePadding>
      {files.map(file => {
        const extension = file?.name?.split('.').pop();
        return (
          <React.Fragment>
            <ListItem dense>
              <ListItemText
                primary={file.name}
                secondary={`${moment(file.date).fromNow()} | ${file.size}`}
              />
              <ListItemSecondaryAction>
                <Switch color="secondary" />
                <Button iconButton={true} onClick={() => {}}>
                  <CloudDownloadIcon />
                </Button>
                <Button iconButton={true} onClick={() => {}}>
                  <DeleteIcon />
                </Button>
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
          </React.Fragment>
        );
      })}
    </List>
  );
}
