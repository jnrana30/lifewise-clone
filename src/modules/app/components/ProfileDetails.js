import React from 'react';
import {
  Grid,
  List,
  ListItem,
  Card,
  Box,
  CardContent,
  Typography,
  LinearProgress
} from '@material-ui/core';
import { Avatar } from 'src/components/shared';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  columns: {
    display: 'flex'
  },
  avatar: {
    width: theme.spacing(8),
    height: theme.spacing(8)
  },
  progressBar: {
    height: 10,
    borderRadius: 6
  }
}));

function ProfileDetails() {
  const classes = useStyles();

  return (
    <Box mb={3}>
      <Card pa={4}>
        <CardContent>
          <Box mb={2}>
            <Typography variant="h6" color="primary">
              Profile Details
            </Typography>
          </Box>
          <div className={classes.columns}>
            <Avatar name="John Doe" size={64} />
            <Box pl={2}>
              <List dense={true}>
                <ListItem>
                  <Typography variant="body1">
                    <b>John Doe</b>
                  </Typography>
                </ListItem>
                <ListItem>
                  <Typography variant="body2">demo@demo.com</Typography>
                </ListItem>
                <ListItem>
                  <Typography variant="body2">John Doe</Typography>
                </ListItem>
                <ListItem>
                  <Typography variant="body2">Principal</Typography>
                </ListItem>
                <ListItem>
                  <Typography variant="body2">Change Password</Typography>
                </ListItem>
              </List>
            </Box>
          </div>
          <Box my={2}>
            <LinearProgress
              className={classes.progressBar}
              color="primary"
              variant="determinate"
              value={70}
            />
            <Typography align="right" color="textSecondary" variant="body2">
              4/7 set up
            </Typography>
          </Box>
        </CardContent>
      </Card>
    </Box>
  );
}

export default ProfileDetails;
