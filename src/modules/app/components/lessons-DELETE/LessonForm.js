import React from 'react';
import { Box, Grid } from '@material-ui/core';
import Link from 'next/link';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';

const tags = [
  {
    label: 'Demo Tag',
    value: 'demo-tag'
  },
  {
    label: 'Demo',
    value: 'demo'
  }
];

function LessonForm({ isEdit }) {
  const [loading, setLoading] = React.useState(false);
  return (
    <Form
      initialValues={{
        firstName: '',
        lastName: '',
        class: '',
        documents: []
      }}
      validationSchema={Yup.object().shape({
        firstName: Yup.string().required('First Name is required!'),
        lastName: Yup.string().required('Last Name is required!')
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="lesson-form"
            noValidate
          >
            <Grid container>
              {isEdit && (
                <Grid item xs={12} md={3}>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    mt={4}
                  >
                    <Form.Field.Avatar
                      fullWidth
                      name="avatar"
                      label="Choose Avatar"
                    />
                  </Box>
                </Grid>
              )}
              <Grid item xs={12} md={isEdit ? 6 : 12}>
                <Form.Field.Input fullWidth name="name" label="Lesson Name" />
                <Form.Field.Input
                  fullWidth
                  name="desc"
                  label="Lesson Description"
                />
                <Form.Field.File
                  fullWidth
                  folder="courses/icons"
                  multiple={true}
                  name="documents"
                  label="Background Image"
                />
                <Form.Field.AutoComplete
                  fullWidth
                  options={tags}
                  variant="standard"
                  name="tags"
                  label="Year Group"
                />
                <Form.Field.AutoComplete
                  fullWidth
                  options={tags}
                  variant="standard"
                  name="tags"
                  label="Categories"
                />
                <Form.Field.AutoComplete
                  fullWidth
                  options={tags}
                  variant="standard"
                  name="tags"
                  label="Curriculum"
                />
                {isEdit && (
                  <React.Fragment>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        loading={loading}
                        mt={2}
                      >
                        Submit
                      </Button>
                    </Box>
                  </React.Fragment>
                )}
              </Grid>
            </Grid>
          </form>
        );
      }}
    </Form>
  );
}

export default LessonForm;
