import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  CardActionArea,
  CardMedia,
  CardContent,
  CardActions,
  IconButton,
  Typography,
  Chip,
  Box
} from '@material-ui/core';
import { Button, ContextMenu } from 'src/components/shared';
import { useRouter } from 'next/router';
import images from 'src/config/images';
import { CardGrid } from '../../../../components/shared/CardGrid';

import s3Helper from 'src/utils/s3Helper';

const useStyles = makeStyles(theme => ({
  cardRoot: {
    width: '100%',
    height: '100%',
    [theme.breakpoints.down(1615)]: {
      maxWidth: 320
    },
    [theme.breakpoints.up(1615)]: {
      maxWidth: 360
    },
    [theme.breakpoints.down(600)]: {
      maxWidth: '100%',
    },
    position: 'relative',
  },
  media: {
    paddingTop: '56.11%'
  },
  expand: {
    marginLeft: 'auto'
  },
  tagsContainer: {
    overflow: 'auto',
    height: 56,
    boxSizing: 'contentBox',
    display: 'flex',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(0.25),
      height: 30,
      '& .MuiChip-label': {
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 12
      }
    }
  },
  lessonCategory: {
    width: 'auto',
    display: 'inline-block',
    position: 'absolute',
    top: 14,
    color: '#ffffff',
    backgroundColor: theme.palette.primary.main,
    padding: '4px 10px',
    fontWeight: 'bold',
    fontSize: 12
  },
  cardActions: {
    paddingTop: 6,
    paddingBottom: 0,
    display: 'flex',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: 0,
    overflow: 'hidden',
    width: '100%',
  },
  mainCardContent: {
    paddingBottom: 0,
    padding: 12,
    marginBottom: theme.spacing(7),
  },
  cardContentHeading: {
    '& .MuiTypography-root': {
      lineHeight: 1.1
    }
  },
  cardContentSubHeading: {
    lineHeight: 1.1,
    fontSize: 12,
  }
}));

export default function LessonCard({ course, ...props }) {
  const classes = useStyles();
  const router = useRouter();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const icon =
    course?.data?.icon && course?.data?.icon !== ''
      ? s3Helper.getFileUrl(course?.data?.icon)
      : images.app.coursePlaceholder;

  const background =
    course?.data?.background && course?.data?.background !== ''
      ? s3Helper.getFileUrl(course?.data?.background)
      : images.app.courseBanner1;

  return (
    <CardGrid className={classes.cardRoot}>
      <CardActionArea
        onClick={() => {
          router.push(`/lessons/${course.id}`);
        }}
      >
        <CardMedia
          className={classes.media}
          image={background}
          title={course.title}
        />
        <Typography variant="body2" className={classes.lessonCategory}>
          {course.category}
        </Typography>
      </CardActionArea>
      <CardContent className={classes.mainCardContent}>
        <Box mb={1} className={classes.cardContentHeading}>
          <Typography color="textPrimary" component="p">
            <b>{course.title}</b>
          </Typography>
        </Box>
        <Typography
          variant="body2"
          color="textSecondary"
          component="p"
          className={classes.cardContentSubHeading}
        >
          {course.desc}
        </Typography>
      </CardContent>

      <CardActions className={classes.cardActions} disableSpacing>
        {course.tags?.length && (
          <div className={classes.tagsContainer}>
            {course.tags.map((tag, index) => {
              return (
                <Chip
                  variant="outlined"
                  key={`course-${props.courseIndex}-tags-${index}`}
                  label={tag}
                />
              );
            })}
          </div>
        )}
        <ContextMenu
          options={[
            {
              label: 'Complete Lesson',
              onClick: () => {
                props.onComplete(course);
              }
            }
          ]}
        />
      </CardActions>
    </CardGrid>
  );
}
