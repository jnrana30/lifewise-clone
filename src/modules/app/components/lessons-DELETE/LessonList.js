import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Box,
  Avatar,
  Badge
} from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { Button, Chip, ContextMenu } from 'src/components/shared';
import Link from 'next/link';
import images from 'src/config/images';
import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles(theme => ({
  paperWrapper: {
    maxWidth: 1500,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  avatar: {
    width: theme.spacing(19),
    height: 'auto',
    borderRadius: 4
  },
  tagsContainer: {
    marginLeft: '-4px',
    position: 'absolute',
    bottom: 20
  },
  checkBadge: {
    top: '4%',
    right: '4%'
  },
  tableImageCol: {
    width: theme.spacing(19)
  },
  tableActionCol: {
    maxWidth: 200,
    width: 200
  },
  contentCol: {
    position: 'relative',
    verticalAlign: 'unset'
  },
  lessonDesc: {
    width: 550
  },
  contextMenu: {
    '& .MuiIconButton-root': {
      padding: 0
    }
  }
}));

function LessonGrid({ lessons, isAdmin, fromHome = false, ...props }) {
  const classes = useStyles();

  return (
    <Paper className={classes.paperWrapper}>
      <TableContainer>
        <Table aria-label="customized table" className="paddedTable">
          <TableBody>
            {lessons.map((lesson, index) => {
              const icon =
                lesson?.data?.icon && lesson?.data?.icon !== ''
                  ? s3Helper.getFileUrl(lesson?.data?.icon)
                  : images.app.coursePlaceholder;

              const background =
                lesson?.data?.background && lesson?.data?.background !== ''
                  ? s3Helper.getFileUrl(lesson?.data?.background)
                  : images.app.lessonPlaceholder;
              return (
                <TableRow key={`lesson-list-${index}`}>
                  <TableCell className={classes.tableImageCol}>
                    <Box display="flex" alignItems="center">
                      <Link
                        href={
                          isAdmin
                            ? `/lessons/admin/${lesson.id}`
                            : `/lessons/${lesson.id}`
                        }
                      >
                        <Badge
                          overlap="circle"
                          anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right'
                          }}
                          classes={{
                            badge: classes.checkBadge
                          }}
                          badgeContent={
                            fromHome && <CheckCircleIcon color="primary" />
                          }
                        >
                          <Avatar
                            className={classes.avatar}
                            src={images.app.lessonBanner}
                            size={36}
                          />
                        </Badge>
                      </Link>
                    </Box>
                  </TableCell>
                  <TableCell className={classes.contentCol}>
                    <Box mb={2}>
                      <Link
                        href={
                          isAdmin
                            ? `/lessons/admin/${lesson.id}`
                            : `/lessons/${lesson.id}`
                        }
                      >
                        <Typography variant="body1">
                          {/* <b>{lesson.title}</b> */}
                        </Typography>
                      </Link>
                      <Typography
                        variant="body2"
                        color="textSecondary"
                        className={classes.lessonDesc}
                      >
                        {lesson.desc}
                      </Typography>
                    </Box>
                    {lesson.tags?.length && (
                      <div className={classes.tagsContainer}>
                        {lesson.tags.map((tag, tagIndex) => {
                          return (
                            <Chip
                              key={`lessonList-${props.index}-tags-${tagIndex}`}
                              variant="outlined"
                              label={tag}
                              size="small"
                              className={classes.chipRoot}
                              style={{ margin: '4px 4px 0px 4px' }}
                            />
                          );
                        })}
                      </div>
                    )}
                  </TableCell>
                  <TableCell size={'small'} className={classes.tableActionCol}>
                    <Box
                      display="flex"
                      alignItems="center"
                      justifyContent="space-between"
                    >
                      <Button variant="outlined" color="primary">
                        View lesson
                      </Button>
                      <ContextMenu
                        options={[
                          {
                            label: 'Edit',
                            icon: <EditIcon />,
                            onClick: () => {}
                          }
                        ]}
                      />
                    </Box>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}

export default LessonGrid;
