import React, {useEffect, useRef, useState} from 'react';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Card,
  Box,
  Typography,
  Avatar,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { Button } from 'src/components/shared';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';

import { actionStyles, LessonActionImage } from './styles';
import images from '../../../../../config/images';

const useStyles = makeStyles(theme => ({
  lessonActionBox: {
    top: -170,
    width: 'calc(100% - 24px)',
    maxWidth: 380,
    height: 'max-content',
    transform: 'translate3d(0, 0, 0)',
    willChange: 'position, transform',
    position: 'absolute',
    overflow: 'auto',
    background: '#fff',
    borderRadius: theme.spacing(0.5),
    padding: `${theme.spacing(1.5)}px 6px`,
    boxShadow: '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
    [theme.breakpoints.up(1500)]: {
      maxWidth: 360,
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      height: '100%',
      position: 'unset !important',
    },
  },
  actionImage: {
    display: 'block',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
  },
  buttonBottomListItem: {
    textAlign: 'center',
    marginTop: theme.spacing(1),
    paddingTop: 0,
    paddingBottom: 0,
  },
  cardWrapper: {
    boxShadow: 'unset',
  },
  listItem: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
  },
  listItemIcon: {
    paddingLeft: 0,
    paddingRight: 0,
    minWidth: theme.spacing(4),
  },
  avatarImg: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
}));

function LessonActions(props) {
  const classes = useStyles();
  const classesAction = actionStyles();
  const lessonBox = useRef();

  useEffect(() => {
    const mainTag = document.getElementsByTagName('main')[0];
    mainTag && mainTag.addEventListener('scroll', (event) => {
      if (event.target.scrollTop > 32) {
        lessonBox.current.style.top = `${event.target.scrollTop - (170 + 32)}px`;
      } else {
        lessonBox.current.style.top = '-170px';
      }
    });
    return () => {
      if (mainTag) {
        mainTag.removeEventListener('scroll', () => {});
      }
    }
  }, []);

  const lessonActionList = [
    {
      label: 'Activity Workbook',
    },
    {
      label: 'Lesson Plan',
    },
    {
      label: 'Share Parent Link',
    },
    {
      label: 'Certificates',
    },
    {
      label: 'Feedback',
    },
  ];

  return (
    <Box className={classes.lessonActionBox} ref={lessonBox}>
      <LessonActionImage image={images.app.courseBanner} />
      <Box mr={'16%'} ml={'16%'}>
        <Button
          className={classesAction.button}
          variant="contained"
          size="large"
          startIcon={<PlayCircleOutlineIcon />}
          fullWidth={true}
          onClick={props.onPlay}
        >
          Play Lesson
        </Button>
      </Box>
      <ListItem button className={classes.buttonBottomListItem}>
        <ListItemText primary={'Mark as complete'} />
      </ListItem>
      <Card className={classes.cardWrapper}>
        <Box p={2}>
          <Typography variant="h6">Action & Download</Typography>
          <List>
            {lessonActionList.map((lesson, index) =>
              <ListItem className={classes.listItem} button key={`lesson-${lesson.label}-${index}`}>
                <ListItemIcon className={classes.listItemIcon}>
                  <Avatar alt={lesson.label} src="/static/images/avatar/1.jpg" className={classes.avatarImg} />
                </ListItemIcon>
                <ListItemText primary={lesson.label} />
              </ListItem>
            )}
          </List>
        </Box>
      </Card>
    </Box>
  );
}

export default LessonActions;
