import styled, { css } from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';

export const SchoolLogoContainer = styled.div`
  display: flex;
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const SchoolLogoImage = styled.img`
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const actionStyles = makeStyles(theme => ({
  button: {
    borderRadius: 40,
    backgroundColor: '#F6AE32',
    color: '#ffffff',
    height: theme.spacing(4),
    padding: theme.spacing(1),
    fontSize: 18,
    [theme.breakpoints.down('xs')]: {
      fontSize: 12,
    },
    '&:hover': {
      backgroundColor: '#f2a218',
    },
    '&:active': {
      backgroundColor: '#f2a218',
    },
  },
}));

export const LessonHeaderContainer = styled.div`
  margin: -30px -24px;
  background-color: #f2f2f2;
  padding: 24px;
  margin-bottom: 30px;
  height: 200px;
  display: flex;
  flex-direction: column;
  ${props =>
    css`
      background-image: url('${props.image}');
      background-size: cover;
      background-position: center;
    `}
  h1 {
    color: #ffffff;
    font-size: 40px;
  }
`;
export const LessonActionImage = styled.div`
  ${props => 
    css`
      background-image: url('${props.image}');     
  `}
  display: block;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  padding-top: 58.1%;
  margin: -12px -6px 12px -6px;
`;

