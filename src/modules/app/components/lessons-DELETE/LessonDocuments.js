import React from 'react';
import {
  Box,
  Card,
  Divider,
  CardHeader,
  CardContent,
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
  Grid
} from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import moment from 'moment';

const documents = [
  {
    type: 'articulate',
    title: 'Main Articulate',
    subTitle: 'Quam temere in vitiis, legem sancimus haerentia.',
    data: [
      {
        name: 'Annual Presentation.ppt',
        date: '2021/01/01',
        size: '1.5mb'
      }
    ]
  },
  {
    type: 'remote-articulate',
    title: 'Remote Articulate',
    subTitle: 'Quam temere in vitiis, legem sancimus haerentia.',
    data: [
      {
        name: 'IMPORTANT Documents.doc',
        date: '2021/05/31',
        size: '2.4mb'
      }
    ]
  },
  {
    type: 'activity-Workbook',
    title: 'Activity Workbook ',
    subTitle: 'Quam temere in vitiis, legem sancimus haerentia.',
    data: [
      {
        name: 'Handbook.pdf',
        date: '2021/06/24',
        size: '0.5mb'
      }
    ]
  },
  {
    type: 'parent-link',
    title: 'Parent Link',
    subTitle: 'Quam temere in vitiis, legem sancimus haerentia.',
    data: [
      {
        name: 'https://lifewise.co.uk/parents-link',
        date: '2021/07/04'
      }
    ]
  }
];

function LessonDocuments({ onEdit }) {
  const [loading, setLoading] = React.useState(false);
  return (
    <React.Fragment>
      <Grid container spacing={2}>
        {documents.map((document, index) => (
          <Grid
            item
            lg={6}
            md={6}
            sm={12}
            key={`Lesson-documents-${document.type}-${index}`}
          >
            <Card>
              <CardHeader title={document.title} />
              <Divider />
              <CardContent>
                <Box>
                  <List component="div" disablePadding>
                    <ListItem>
                      <ListItemText
                        primary={document.data[0].name}
                        secondary={`${moment('2021/01/01').fromNow()} | 1.5mb`}
                      />
                      <ListItemSecondaryAction>
                        <Button
                          iconButton={true}
                          onClick={() => {
                            onEdit(document.type);
                          }}
                        >
                          <EditIcon />
                        </Button>
                      </ListItemSecondaryAction>
                    </ListItem>
                  </List>
                </Box>
              </CardContent>
            </Card>
          </Grid>
        ))}
      </Grid>
    </React.Fragment>
  );
}

export default LessonDocuments;
