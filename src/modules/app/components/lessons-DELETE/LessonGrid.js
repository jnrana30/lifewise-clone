import React from 'react';
import { Grid } from '@material-ui/core';
import LessonCard from './LessonCard';
import Link from 'next/link';
import { makeStyles } from '@material-ui/core/styles';
import { ContentCardContainer } from '../../../../components/shared/Form/File/styles';
import {
  GridContainer,
  CardGrid
} from '../../../../components/shared/CardGrid';

const useStyles = makeStyles(theme => ({
  gridRoot: {
    width: '100%',
    height: 'auto',
    [theme.breakpoints.down(1615)]: {
      maxWidth: 336
    },
    [theme.breakpoints.up(1615)]: {
      maxWidth: 376
    }
  }
}));

function LessonGrid({ lessons, onComplete }) {
  const classes = useStyles();
  return (
    <ContentCardContainer>
      <Grid container spacing={2}>
        {lessons.map((course, index) => {
          return (
            <GridContainer
              key={`course-${index}`}
              item
              xs={12}
              s={12}
              sm={6}
              lg={3}
              xl={3}
              md={3}
            >
              <LessonCard
                courseIndex={index}
                onComplete={onComplete}
                course={course}
              />
            </GridContainer>
          );
        })}
      </Grid>
    </ContentCardContainer>
  );
}

export default LessonGrid;
