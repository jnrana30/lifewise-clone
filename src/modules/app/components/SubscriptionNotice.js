import React from 'react';
import styled from 'styled-components';
import { Button } from 'src/components/shared';
import images from 'src/config/images';
import { Typography, Box } from '@material-ui/core';

const Container = styled.div``;

const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  .image {
    max-width: 400px;
    margin: 20px 0px;
  }
  .welcome-image {
    max-width: 200px;
    margin: 20px 0px;
  }
`;

function SubscriptionNotice({ onClick, type, onClose, loading, image }) {
  const getContent = () => {
    switch (type) {
      case 'subscription':
        return {
          title: 'Your subscription has expired!',
          desc: 'Thank you for being part of the LifeWise community. To continue accessing our content, simply click below to activate a 14 day interim subscription. One of the team will be in touch shortly to discuss renewal packages.',
          buttonTitle: 'Extend Subscription'
        };
        break;
      case 'upgrade':
        return {
          title: 'Upgrade your account!',
          desc: 'Thank you for being part of the LifeWise community. To upgrade your account, simply click below to book a call with one of the team.',
          buttonTitle: 'Upgrade'
        };
        break;
      case 'welcome':
        return {
          title: 'Welcome',
          desc: 'Together, positively preparing children for life!',
          buttonTitle: `Let's get started`
        };
        break;
      case 'trial':
      default:
        return {
          title: 'Your trial has ended!',
          desc: 'Thank you for trialling LifeWise. To continue accessing our content simply click below to extend your trial for a further 14 days. One of the team will be in touch to discuss packages shortly.',
          buttonTitle: 'Extend Trial'
        };
        break;
    }
  };

  return (
    <Container>
      <Content>
        {image ? image : <img className="image" src={images.app.subscriptionExpired} />}
        <div style={{ height: 16 }} />
        <Typography variant="h5">
          <b>{getContent().title}</b>
        </Typography>
        <div style={{ height: 16 }} />
        <Typography align="center">{getContent().desc}</Typography>
        <div style={{ height: 24 }} />
        <Box display="flex">
          {type === 'upgrade' && (
            <Box mr={2}>
              <Button variant="contained" onClick={onClose} mt={2}>
                Cancel
              </Button>
            </Box>
          )}
          <Button
            variant="contained"
            color="primary"
            type="submit"
            onClick={onClick}
            loading={loading}
            mt={2}
          >
            {getContent().buttonTitle}
          </Button>
        </Box>

        <div style={{ height: 16 }} />
      </Content>
    </Container>
  );
}

export default SubscriptionNotice;
