import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
  Switch,
  Divider
} from '@material-ui/core';
import images from 'src/config/images';
import { Button } from 'src/components/shared';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import DeleteIcon from '@material-ui/icons/Delete';
import moment from 'moment';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  FileIcon: {
    maxWidth: 36
  }
}));

const logs = [
  {
    title:
      'Consectetur labore mollit mollit esse ipsum incididunt laboris amet deserunt.',
    date: '2021/01/01'
  },
  {
    title: 'Veniam voluptate in deserunt consectetur do.',
    date: '2021/05/21'
  },
  {
    title:
      'Veniam enim laborum ullamco laborum proident officia duis incididunt minim ex labore nisi consequat.',
    date: '2021/05/30'
  },
  {
    title:
      'Quis cillum velit adipisicing consequat sunt consectetur non ullamco exercitation in sint sunt ut.',
    date: '2021/06/01'
  },
  {
    title: 'In proident enim anim adipisicing ea in.',
    date: '2021/06/02'
  },
  {
    title: 'Exercitation cupidatat eu dolor proident sunt minim nisi.',
    date: '2021/06/10'
  }
];

export default function ActivityLog() {
  const classes = useStyles();
  return (
    <List disablePadding>
      {logs.map((log, index) => {
        return (
          <React.Fragment key={`activity-log-${index}`}>
            <ListItem dense>
              <ListItemText
                primary={log.title}
                secondary={`${moment(log.date).fromNow()}`}
              />
              {/* <ListItemSecondaryAction>
                <Switch color="secondary" />
                <Button iconButton={true} onClick={() => {}}>
                  <CloudDownloadIcon />
                </Button>
                <Button iconButton={true} onClick={() => {}}>
                  <DeleteIcon />
                </Button>
              </ListItemSecondaryAction> */}
            </ListItem>
            <Divider />
          </React.Fragment>
        );
      })}
    </List>
  );
}
