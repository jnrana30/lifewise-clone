import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
  Switch,
  Divider
} from '@material-ui/core';
import images from 'src/config/images';
import { Button } from 'src/components/shared';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import DeleteIcon from '@material-ui/icons/Delete';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

import moment from 'moment';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  FileIcon: {
    maxWidth: 36
  }
}));

export default function DocumentList({ data }) {
  const classes = useStyles();
  return (
    <List disablePadding>
      {data.map((item, index) => {
        return (
          <React.Fragment key={`document-list-${index}`}>
            <ListItem dense>
              <ListItemText
                primary={item.name}
                secondary={`${moment(item.date).fromNow()} ${
                  item.size ? `| ${item.size}` : ''
                }`}
              />
              <ListItemSecondaryAction>
                <Switch color="secondary" />
                {item.isLink ? (
                  <Button iconButton={true} onClick={() => {}}>
                    <OpenInNewIcon />
                  </Button>
                ) : (
                  <Button iconButton={true} onClick={() => {}}>
                    <CloudDownloadIcon />
                  </Button>
                )}

                <Button iconButton={true} onClick={() => {}}>
                  <DeleteIcon />
                </Button>
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
          </React.Fragment>
        );
      })}
    </List>
  );
}
