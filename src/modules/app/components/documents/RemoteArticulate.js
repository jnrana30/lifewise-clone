import React from 'react';
import { Typography, Box, Divider, Tabs, Tab, Paper } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';
import * as Yup from 'yup';

import ActivityLog from './ActivityLog';
import DocumentList from './DocumentList';

function RemoteArticulate() {
  const [loading, setLoading] = React.useState(false);
  const [tab, setTab] = React.useState(0);

  const files = [
    {
      name: 'Annual Presentation.ppt',
      date: '2021/01/01',
      size: '1.5mb'
    },
    {
      name: 'IMPORTANT Documents.doc',
      date: '2021/05/21',
      size: '0.25mb'
    },
    {
      name: 'Class-Recording-06/07/2021.mp3',
      date: '2021/05/30',
      size: '3.4mb'
    },
    {
      name: 'School-Banner.jpg',
      date: '2021/06/01',
      size: '1.1mb'
    },
    {
      name: 'Kids-presentation.psd',
      date: '2021/06/02',
      size: '4.7mb'
    },
    {
      name: 'Handbook.pdf',
      date: '2021/06/10',
      size: '0.4mb'
    }
  ];

  return (
    <div>
      <Typography variant="body1" color="textPrimary">
        <b>Upload & publish your remote lesson.</b>
      </Typography>

      <Form
        initialValues={{
          articulate: ''
        }}
        validationSchema={Yup.object().shape({
          articulate: Yup.string().required('Articulate file is required!')
        })}
        onSubmit={async (values, form) => {
          console.log('values : ', values);
        }}
      >
        {props => {
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                props.submitForm();
                return false;
              }}
              id="lesson-form"
              noValidate
            >
              <Form.Field.File
                fullWidth
                name="documents"
                label="Remote Articulate"
              />
            </form>
          );
        }}
      </Form>
      <Box mt={4} mb={2}>
        <Paper>
          <Tabs
            value={tab}
            textColor="primary"
            onChange={(e, val) => {
              setTab(val);
            }}
            TabIndicatorProps={{
              style: {
                backgroundColor: '#ff9800'
              }
            }}
          >
            <Tab label="Version control" />
            <Tab label="Activity" />
          </Tabs>
        </Paper>
      </Box>

      {tab === 0 && <DocumentList data={files} />}
      {tab === 1 && <ActivityLog />}
    </div>
  );
}

export default RemoteArticulate;
