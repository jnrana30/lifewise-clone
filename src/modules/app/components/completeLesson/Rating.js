import React from 'react';
import {
  Box,
  Card,
  CardHeader,
  CardContent,
  Typography,
  Grid
} from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import { Form, Button } from 'src/components/shared';

function RatingComponent() {
  return (
    <Grid container>
      <Grid item lg={1} md={1} sm={12}></Grid>
      <Grid item lg={10} md={10} sm={12}>
        <Card>
          <CardContent>
            <Form
              initialValues={{
                link: ''
              }}
            >
              <form>
                <Typography>
                  <b>Overall Rating</b>
                </Typography>
                <Rating name="size-large" defaultValue={2} size="large" />
                <Form.Field.Input
                  name="link"
                  multiline
                  variant="outlined"
                  fullWidth
                  placeholder="Why did you choose this rating"
                  inputProps={{
                    style: {
                      height: 100
                    }
                  }}
                />
              </form>
            </Form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}

export default RatingComponent;
