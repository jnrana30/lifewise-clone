import React from 'react';
import { connect } from 'react-redux';
import { Box } from '@material-ui/core';

import { Drawer, Stepper } from 'src/components/shared';

import Lesson from './Lesson';
import Assessment from './Assessment';
import Share from './Share';
import Rating from './Rating';

import { completeLesson } from 'src/modules/courses/actions/coursesActions';

function CompleteLesson({ action, lesson, onClose }) {
  const [activeStep, setActiveStep] = React.useState(0);

  // const steps = ['Lesson', 'Assessment', 'Share with Parent', 'Feedback'];
  const steps = ['Lesson', 'Share with Parent', 'Feedback'];

  const onSubmit = () => {
    setActiveStep(activeStep + 1);
  };

  console.log('lesson');
  console.log(lesson);

  return (
    <Drawer
      open={action && action !== ''}
      title="Complete Lesson"
      onClose={onClose}
      fullWidth={true}
      form="lesson-complete-form"
      buttonText="Continue"
    >
      <div>
        <Stepper steps={steps} activeStep={activeStep} />
        <Box mt={4}>
          {activeStep == 0 && <Lesson onSubmit={onSubmit} />}
          {activeStep == 1 && <Share onSubmit={onSubmit} />}
          {activeStep == 2 && <Rating onSubmit={onSubmit} />}
        </Box>
      </div>
    </Drawer>
  );
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  completeLesson: (classId, data, data) => {
    return new Promise((resolve, reject) => {
      dispatch(completeLesson(classId, data, data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CompleteLesson);
