import React from 'react';
import { Typography } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';

import { lessons, classes } from '../../api/data';

function LessonForm({ isEdit, onSubmit }) {
  const [loading, setLoading] = React.useState(false);

  return (
    <Form
      initialValues={{
        lesson: '',
        class: ''
      }}
      validationSchema={Yup.object().shape({
        lesson: Yup.string().required('Class is required!'),
        class: Yup.string().required('Lesson is required!')
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
        onSubmit();
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="lesson-complete-form"
            noValidate
          >
            <Typography variant="body1" color="textPrimary">
              <b>Choose Lesson and your class.</b>
            </Typography>
            <Form.Field.Select
              fullWidth
              options={lessons}
              variant="standard"
              name="lesson"
              label="Lesson"
              optLabel="title"
              optValue="id"
            />
            <Form.Field.Select
              fullWidth
              options={classes}
              variant="standard"
              name="class"
              label="Class"
            />
          </form>
        );
      }}
    </Form>
  );
}

export default LessonForm;
