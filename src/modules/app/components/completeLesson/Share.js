import React from 'react';
import {
  Box,
  Card,
  CardHeader,
  CardContent,
  Typography,
  Grid
} from '@material-ui/core';
import { Form, Button } from 'src/components/shared';

function Share({ onSubmit }) {
  return (
    <Grid container>
      <Grid item lg={1} md={1} sm={12}></Grid>
      <Grid item lg={10} md={10} sm={12}>
        <Card>
          <CardHeader
            title="Share with Parents"
            subheader="Lorem ipsum dolor sit amet, consectetur adipisici elit."
          />

          <CardContent>
            <Form
              initialValues={{
                link: 'https://lifewise.co.uk/assessment/sdf45sdf4'
              }}
              onSubmit={async values => {
                console.log('values');
                console.log(values);
                onSubmit();
              }}
            >
              {props => {
                return (
                  <form
                    onSubmit={e => {
                      e.preventDefault();
                      props.submitForm();
                      return false;
                    }}
                    id="lesson-complete-form"
                    noValidate
                  >
                    <Form.Field.Input
                      name="link"
                      disabled={true}
                      fullWidth
                      style={{ color: 'red' }}
                    />
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="primary"
                        mt={2}
                        onClick={() => {
                          navigator.clipboard.writeText(
                            'https://lifewise.co.uk/assessment/sdf45sdf4'
                          );
                        }}
                      >
                        Copy to clipboard
                      </Button>
                    </Box>
                  </form>
                );
              }}
            </Form>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  );
}

export default Share;
