import { makeStyles } from '@material-ui/core/styles';
import styled, { css } from 'styled-components';

export const ReviewContainer = styled.div`
  display: flex;
  flex: 1;
  background-color: #dddddd;
  height: 54px;
  border-radius: 4px;
  margin-top: 8px;
`;

export const ExcellentContainer = styled.div`
  display: flex;
  flex: 1;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  cursor: pointer;
  ${props =>
    props.isChecked &&
    css`
      background-color: #42b752;
    `}
`;
export const UnderstoodContainer = styled.div`
  display: flex;
  flex: 1;
  cursor: pointer;
  border-left: 1px solid #999999;
  border-right: 1px solid #999999;
  ${props =>
    props.isChecked &&
    css`
      background-color: #fab03c;
    `}
`;
export const DevelopmentContainer = styled.div`
  display: flex;
  flex: 1;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  cursor: pointer;
  ${props =>
    props.isChecked &&
    css`
      background-color: #ff3b36;
    `}
`;

export const SchoolLogoContainer = styled.div`
  display: flex;
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const SchoolLogoImage = styled.img`
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const Counter = styled.div`
  display: flex;
  font-size: 20px;
  align-items: center;
  justify-content: center;
  font-weight: 500;
  margin-top: 26px;
`;

export const useStyles = makeStyles(theme => ({
  buttonRoot: {
    display: 'flex',
    justifyContent: 'space-around',
    minWidth: 180,
    borderRadius: 30,
    backgroundColor: '#ffffff',
    marginRight: 10
  },
  root: {
    padding: '0',
    display: 'flex',
    alignItems: 'center',
    borderRadius: 40,
    borderColor: 'rgba(43, 107, 0, 0.5)',
    marginRight: 10
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    height: 28,
    margin: 4
  },
  addButton: {
    borderRadius: 4,
    padding: '00px 20px',
    height: 36,
    minHeight: 36
  }
}));
