import styled from 'styled-components';
import { Button } from 'src/components/shared';
import { makeStyles } from '@material-ui/core/styles';

export const FixedButton = styled(Button).attrs({
  variant: 'contained',
  color: 'primary',
  type: 'submit',
  fullWidth: true,
  mt: 2
})`
  position: fixed;
  top: 138px;
  right: 36px;
  width: 100px;
  z-index: 999999;
`;

export const useStyles = makeStyles(theme => ({
  buttonRoot: {
    display: 'flex',
    justifyContent: 'space-around',
    minWidth: 180,
    borderRadius: 30,
    backgroundColor: '#ffffff'
  },
  root: {
    padding: '0',
    display: 'flex',
    alignItems: 'center',
    borderRadius: 40,
    borderColor: 'rgba(43, 107, 0, 0.5)'
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    height: 28,
    margin: 4
  },
  addButton: {
    borderRadius: 4,
    padding: '00px 20px',
    height: 36,
    minHeight: 36
  },
  iconButton: {
    height: 36,
    width: 36
  }
}));
