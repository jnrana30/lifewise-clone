import React, { useState, useRef, useCallback } from 'react';
import _, { debounce } from 'lodash';
import {
  FilterBar,
  FilterPicker,
  FilterSearch,
  FilterDrawer,
  AccessControl
} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import { lessonStatusOptions } from 'src/config';
// import { getCategories, getTagsByType } from '../../../courses/api/coursesApis';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ViewListIcon from '@material-ui/icons/ViewList';
import {
  fetchClasses,
  getTeacherClasses
} from '../../../school/api/schoolApis';
import useViewport from '../../../../utils/ViewPort';
import { useStyles } from './style';

function HomeFilter({
  filters,
  setFilter,
  unsetFilter,
  isAdmin,
  setClass,
  viewType,
  setViewType,
  ...props
}) {
  const classes = useStyles();
  const [tags, setTags] = React.useState([]);
  const [isMount, setIsMount] = React.useState(true);
  const { isMobileView } = useViewport();
  const [classFilter, setClassFilter] = React.useState({});

  React.useEffect(() => {
    if (isMount) {
      setIsMount(false);
      return;
    }
    if (classFilter && classFilter.id !== '') {
      setFilter({ classId: classFilter.id });
      setClass(classFilter);
    } else {
      unsetFilter('classId');
      setClass({});
    }
  }, [classFilter]);

  const getClasses = async search => {
    let filter = {};
    if (search && search !== '') {
      filter.search = search;
    }
    const res = await getTeacherClasses(filter);
    return res?.classes ? res?.classes : [];
  };

  const onSearch = useCallback(
    debounce(async val => {
      setFilter({ lessonName: val });
    }, 0),
    []
  );

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={filters['lessonName'] ? filters['lessonName'] : ''}
      onChange={val => {
        setFilter({ lessonName: val });
      }}
      onClear={() => {
        unsetFilter('lessonName');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
      <AccessControl levelLt={5}>
        <FilterPicker
          title="Class"
          cancellable={false}
          options={[]}
          onChange={val => {
            setClassFilter(val);
          }}
          remoteMethod={val => getClasses(val)}
          selected={
            props.classItem && props.classItem?.id ? props.classItem : ''
          }
          onClear={() => {
            setClassFilter({});
          }}
          optValue="id"
          optLabel="className"
          ml={2}
          mr={1}
          className={'home-page-filter'}
        />
      </AccessControl>
      <AccessControl level={6}>
        <FilterPicker
          title="Status"
          options={lessonStatusOptions}
          onChange={val => {
            setFilter({
              status: val.value
            });
          }}
          selected={filters['status'] ? filters['status'] : ''}
          onClear={() => {
            unsetFilter('status');
          }}
          ml={2}
        />
      </AccessControl>
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView && <Box display="flex">{filterOptions}</Box>}
        {isMobileView && (
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        )}

        <Box display="flex">
          {!isAdmin && !isMobileView && (
            <React.Fragment>
              <Button
                className={classes.iconButton}
                iconButton={true}
                onClick={() => {
                  setViewType('grid');
                }}
                color={viewType === 'grid' ? 'primary' : 'default'}
              >
                <ViewModuleIcon />
              </Button>
              <Button
                className={classes.iconButton}
                iconButton={true}
                onClick={() => {
                  setViewType('list');
                }}
                color={viewType === 'list' ? 'primary' : 'default'}
              >
                <ViewListIcon />
              </Button>
            </React.Fragment>
          )}
        </Box>
      </Box>
    </FilterBar>
  );
}

export default HomeFilter;
