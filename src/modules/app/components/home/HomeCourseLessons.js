import React from 'react';
import { Typography, makeStyles } from '@material-ui/core';
import { ContentCardContainer } from 'src/components/shared/Form/File/styles';
import LessonGrid from 'src/modules/courses/components/lessons/LessonGrid';
import LessonList from 'src/modules/courses/components/lessons/LessonList';
import _ from 'lodash';

const useStyles = makeStyles({
  titleBar: {
    marginBottom: 20,
    marginTop: 40
  },
  title: {
    paddingRight: 10
  }
});

function HomeView({ course, selectedClass, viewType, ...props }) {
  const classes = useStyles();

  const { lessonsIds, loading } = course;
  let { paging, filters } = course;

  React.useEffect(() => {
    fetchCourseLessons();
  }, []);

  React.useEffect(() => {
    if (filters && !_.isEmpty(filters)) {
      fetchCourseLessons();
    } else {
      fetchCourseLessons();
    }
  }, [filters]);

  // console.log('filters : ', filters);
  const fetchCourseLessons = () => {
    // let filter = {
    //   // ...filters,
    //   // courseId: course.id,
    //   // ...paging
    //   ids: lessonsIds[paging.page - 1]
    // };

    let filter = {};
    if (filters && filters.lessonName && filters.lessonName !== '') {
      filter = {
        ...filter,
        ...filters,
        courseId: course.id,
        ...paging
      };
    } else {
      filter = {
        ids: lessonsIds[paging.page - 1]
      };
    }
    props.fetchHomeClassCourseLessons(course.id, filter, paging);
  };

  const onPaging = (page, perPage = 0) => {
    paging.page = page + 1;
    fetchCourseLessons();
  };

  return (
    <ContentCardContainer>
      <Typography className={classes.titleBar}>
        <Typography
          variant="h5"
          component="span"
          className={classes.title}
          color="textPrimary"
        >
          Assigned Course: <b>{course.courseName}</b>
        </Typography>
      </Typography>

      {viewType === 'grid' ? (
        <LessonGrid
          courseId={course.id}
          lessons={course.lessons}
          onComplete={() => {}}
          loading={loading}
          showPaging={true}
          paging={paging}
          onPaging={onPaging}
          selectedClass={selectedClass}
          showJoyRide
        />
      ) : (
        <LessonList
          lessons={course.lessons}
          isAdmin={false}
          loading={loading}
          showPaging={true}
          paging={paging}
          onPaging={onPaging}
          rowsPerPageOptions={[]}
          onEdit={() => {}}
        />
      )}
      {/* <LessonGrid
        lessons={course.lessons}
        onComplete={() => {}}
        loading={loading}
        showPaging={true}
        paging={paging}
        onPaging={onPaging}
      /> */}
    </ContentCardContainer>
  );
}

export default HomeView;
