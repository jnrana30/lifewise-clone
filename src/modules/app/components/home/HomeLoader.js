import React from 'react';
import { ContentCardContainer } from 'src/components/shared/Form/File/styles';
import { Grid, Box, Typography } from '@material-ui/core';
import Skeleton from '@material-ui/lab/Skeleton';

import {
  CardSkeleton,
  CardEmpty,
  GridContainer
} from 'src/components/shared/CardGrid';

function HomeLoader() {
  return (
    <div>
      <Box mb={2}>
        <Skeleton animation="wave" width="40%" height={32} />
      </Box>
      <ContentCardContainer>
        <Grid container spacing={2}>
          {_.times(4, i => (
            <GridContainer
              key={`skeleton-home1-card-${i}`}
              item
              xs={12}
              s={12}
              sm={6}
              lg={3}
              xl={3}
              md={3}
            >
              <CardSkeleton />
            </GridContainer>
          ))}
        </Grid>
      </ContentCardContainer>

      <Box mb={2} mt={4}>
        <Skeleton animation="wave" width="40%" height={32} />
      </Box>
      <ContentCardContainer>
        <Grid container spacing={2}>
          {_.times(4, i => (
            <GridContainer
              key={`skeleton-home2-card-${i}`}
              item
              xs={12}
              s={12}
              sm={6}
              lg={3}
              xl={3}
              md={3}
            >
              <CardSkeleton />
            </GridContainer>
          ))}
        </Grid>
      </ContentCardContainer>
    </div>
  );
}

export default HomeLoader;
