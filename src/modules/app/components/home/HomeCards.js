import React from 'react';
import { Header } from 'src/components/App';
import {
  Grid,
  Box,
  Card,
  CardContent,
  Typography,
  Paper,
  Tab,
  Tabs
} from '@material-ui/core';

const CardItem = ({ title, subtitle, no }) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h4" color="primary">
          <b>{no}</b>
        </Typography>
        <Typography variant="h6">{title}</Typography>
        <Typography variant="subtitle2" color="textSecondary">
          {subtitle}
        </Typography>
      </CardContent>
    </Card>
  );
};

function HomeCards({ user, ...props }) {
  return (
    <Box mb={3}>
      <Grid container spacing={3}>
        <Grid item lg={3} md={3} xs={6}>
          <CardItem no="120" title="Loren ipsum" subtitle="This month" />
        </Grid>
        <Grid item lg={3} md={3} xs={6}>
          <CardItem no="26" title="Lesson played" subtitle="This week" />
        </Grid>
        <Grid item lg={3} md={3} xs={6}>
          <CardItem no="369" title="Lesson played" subtitle="This month" />
        </Grid>
        <Grid item lg={3} md={3} xs={6}>
          <CardItem
            no="36%"
            title="Lesson completed"
            subtitle="Academic year 2021"
          />
        </Grid>
      </Grid>
    </Box>
  );
}

export default HomeCards;
