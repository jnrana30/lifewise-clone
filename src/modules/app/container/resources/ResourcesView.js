import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';

import ResourcesFilter from '../../components/resources/ResourcesFilter';
import ResourcesList from '../../components/resources/ResourcesList';
import ResourceForm from '../../components/resources/ResourceForm';
import ResourcesCardList from '../../components/resources/ResourcesCardList';

function ResourcesView({ resources, ...props }) {
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [resource, setResource] = React.useState({});
  const [formLoading, setFormLoading] = React.useState(false);
  const { user } = props;
  const isAdmin = user?.profile?.role?.level > 5;

  React.useEffect(() => {
    if (drawerOpen === false) {
      setResource({});
    }
  }, [drawerOpen]);
  const { data, loading } = resources;
  let { filters, paging } = resources;

  React.useEffect(() => {
    fetchResources(
      {},
      {
        perPage: isAdmin ? 100 : 12
      }
    );
  }, []);

  const fetchResources = (filter, pagingData) => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    if (typeof filter !== 'pagingData' && !_.isEmpty(pagingData)) {
      paging = { ...paging, ...pagingData };
    }
    if (!isAdmin) {
      filters.resourceStatus = 'published';
      filters.type = 'resource';
    }
    props.fetchResources(filters, paging);
  };

  const setFilter = filter => {
    fetchResources(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchResources();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchResources();
  };

  const onSubmit = async data => {
    setFormLoading(true);
    if (!!resource?.id && resource?.id !== '') {
      await props.updateResource(resource?.id, data);
    } else {
      await props.createResource({ resources: [data] });
    }
    fetchResources();
    setDrawerOpen(false);
    setFormLoading(false);
  };

  const onEdit = async data => {
    await setResource(data);
    await setDrawerOpen(true);
  };

  return (
    <React.Fragment>
      <Header title={isAdmin ? 'Resources' : 'Planning Docs'}>
        <ResourcesFilter
          setDrawerOpen={setDrawerOpen}
          profile={user?.profile}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
        />
      </Header>

      {user?.profile?.role.level > 5 ? (
        <ResourcesList
          resources={data}
          loading={loading}
          paging={paging}
          onPaging={onPaging}
          onEdit={onEdit}
        />
      ) : (
        <ResourcesCardList
          resources={data}
          loading={loading}
          paging={paging}
          onPaging={onPaging}
        />
      )}
      <Drawer
        open={drawerOpen}
        title={resource?.id ? 'Edit Resource' : 'Add Resource'}
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="resource-form"
        buttonLoading={formLoading}
      >
        <Typography variant="body1" color="textPrimary">
          <b>{resource?.id ? 'Edit Resource' : 'Add Resource'}</b>
        </Typography>
        <ResourceForm resource={resource} onSubmit={onSubmit} />
      </Drawer>
    </React.Fragment>
  );
}

export default ResourcesView;
