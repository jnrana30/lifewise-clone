import React from 'react';
import { connect } from 'react-redux';
import ResourcesView from './ResourcesView';
import {
  fetchResources,
  createResource,
  updateResource
} from 'src/modules/courses/actions/coursesActions';

export const ResourcesContainer = props => {
  return <ResourcesView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  resources: state.courses.resources
});

const mapDispatchToProps = dispatch => ({
  fetchResources: (filters, paging) =>
    dispatch(fetchResources(filters, paging)),
  createResource: data => {
    return new Promise((resolve, reject) =>
      dispatch(createResource(data, resolve, reject))
    );
  },
  updateResource: (resourceId, data) => {
    return new Promise((resolve, reject) =>
      dispatch(updateResource(resourceId, data, resolve, reject))
    );
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ResourcesContainer);
