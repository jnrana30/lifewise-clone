import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';
import Head from 'next/head';

import TagsFilter from '../../components/tags/TagsFilter';
import TagsList from '../../components/tags/TagsList';
import TagsForm from '../../components/tags/TagsForm';
import EditTag from '../../components/tags/EditTag';
// import SchoolList from '../../components/schools/SchoolList';
// import SchoolForm from '../../components/schools/SchoolForm';

import { schools } from '../../api/data';

function TagsView({ tags, ...props }) {
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [formLoading, setFormLoading] = React.useState(false);
  const [editTag, setEditTag] = React.useState({});

  const { data, loading } = tags;
  let { filters, paging } = tags;

  React.useEffect(() => {
    fetchTags();
  }, []);

  React.useEffect(() => {
    if (drawerOpen === false) {
      setEditTag({});
    }
  }, [drawerOpen]);

  const fetchTags = filter => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    props.fetchTags(filters, paging);
  };

  const setFilter = filter => {
    fetchTags(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchTags();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchTags();
  };

  const onSubmit = async data => {
    setFormLoading(true);
    try {
      await props.createTag(data);
      fetchTags();
      setFormLoading(false);
      setDrawerOpen(false);
    } catch (error) {
      console.log(error);
      setFormLoading(false);
    }
  };

  const onEdit = tag => {
    console.log('TAG : ', tag);
    setEditTag(tag);
    setDrawerOpen(true);
  };

  const onEditSubmit = async data => {
    setFormLoading(true);
    try {
      await props.updateTag(editTag.id, data);
      fetchTags();
      setFormLoading(false);
      setDrawerOpen(false);
    } catch (error) {
      console.log(error);
      setFormLoading(false);
    }
  };

  return (
    <React.Fragment>
      <Header title="Tags">
        <TagsFilter
          setDrawerOpen={setDrawerOpen}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
        />
      </Header>
      <TagsList
        tags={data}
        loading={loading}
        paging={paging}
        onPaging={onPaging}
        onEdit={onEdit}
      />

      <Drawer
        open={drawerOpen}
        title="Add Tag"
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="tags-form"
        buttonLoading={formLoading}
      >
        {editTag?.id ? (
          <EditTag tag={editTag} onSubmit={onEditSubmit} />
        ) : (
          <TagsForm onSubmit={onSubmit} />
        )}
      </Drawer>
    </React.Fragment>
  );
}

export default TagsView;
