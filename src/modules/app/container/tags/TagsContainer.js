import React from 'react';
import { connect } from 'react-redux';
import TagsView from './TagsView';
import { fetchTags, createTag, updateTag } from '../../actions/appActions';

export const TagsContainer = props => {
  return <TagsView {...props} />;
};

const mapStateToProps = state => ({
  tags: state.app.tags
});

const mapDispatchToProps = dispatch => ({
  fetchTags: (filter, paging) => dispatch(fetchTags(filter, paging)),
  createTag: data => {
    return new Promise((resolve, reject) => {
      dispatch(createTag(data, resolve, reject));
    });
  },
  updateTag: (tagId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(updateTag(tagId, data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(TagsContainer);
