import React, { useState } from 'react';
import { Header } from 'src/components/App';
import { Typography, makeStyles } from '@material-ui/core';
import _ from 'lodash';

import { ContentCardContainer } from '../../../../components/shared/Form/File/styles';
import LessonGrid from 'src/modules/courses/components/lessons/LessonGrid';
import LessonList from 'src/modules/courses/components/lessons/LessonList';
import HomeFilters from '../../components/home/HomeFilters';
import HomeCourseLessons from '../../components/home/HomeCourseLessons';
import HomeLoader from '../../components/home/HomeLoader';
import SubscriptionNotice from '../../components/SubscriptionNotice';
import { Dialog } from '../../../../components/shared';
import { getJoyRide, unsetJoyRide } from 'src/utils/storageUtils';
import images from '../../../../config/images';

const useStyles = makeStyles({
  titleBar: {
    marginBottom: 20,
    marginTop: 40
  },
  title: {
    paddingRight: 10
  }
});

function HomeView({ homeLessons, home, user, homeLoading, ...props }) {
  const [viewType, setViewType] = useState('grid');
  const [showViewMore, setShowViewMore] = useState(false);
  const [viewMore, setViewMore] = useState(false);
  const [viewMoreText, setViewMoreText] = useState(false);
  const [lessonsArr, setLessonsArr] = useState([]);
  const [isWelcomed, setIsWelcomed] = useState(false);
  const classes = useStyles();
  const paging = { perPage: 4, page: 1 };

  const { lessons, classItem } = home;
  let { filters, courses } = home;
  // console.log('classItem : ', classItem, home, home.filters);

  React.useEffect(() => {
    if (viewType !== 'grid') {
      setViewMore(true);
    }
  }, [viewType]);

  React.useEffect(() => {
    if (getJoyRide()) {
      setIsWelcomed(true);
      unsetJoyRide();
    }
    return () => {
      props.fetchJoyRide(false);
    }
  }, []);

  React.useEffect(() => {
    const tempLessons = [];
    if (lessons && Object.keys(lessons).length) {
      let lessonArray = _.values(lessons);

      if (filters.lessonName) {
        lessonArray = lessonArray.filter(l =>
          l?.instance?.lessonName
            ? l?.instance?.lessonName
                .toLocaleLowerCase()
                .includes(filters.lessonName.toLocaleLowerCase())
            : false
        );
        setShowViewMore(lessonArray.length > 3);

        lessonArray.map((lesson, index) => {
          if (viewMore === false && index > 2) return null;
          tempLessons.push({
            ...lesson?.instance,
            status: lesson?.status
          });
        });
      } else {
        setShowViewMore(lessonArray.length > 3);
        lessonArray.map((lesson, index) => {
          if (viewMore === false && index > 2) return null;
          tempLessons.push({
            ...lesson?.instance,
            status: lesson?.status
          });
        });
      }
      setViewMoreText(
        `Show ${Object.keys(lessonArray).length - 3} ${
          viewMore == true ? 'less' : 'more'
        } lessons.`
      );
      setLessonsArr(tempLessons);
    } else {
      setShowViewMore(false);
      setViewMoreText('');
      setLessonsArr([]);
    }
  }, [lessons, viewMore, filters]);

  const fetchHomeData = (classObj, filter) => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
    }
    props.setHomeClass(classObj, filters);
  };

  const setFilter = filter => {
    props.setHomeFilters(filter);
    // fetchHomeData(classItem, filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    // fetchHomeData(classItem);
    props.setHomeFilters(filters);
  };

  const setClass = classObj => {
    props.clearHomeClass();
    fetchHomeData(classObj);
  };

  const toggleShowMore = () => {
    setViewMore(!viewMore);
  };

  const onCloseWelcome = async () => {
    setIsWelcomed(false);
    props.fetchJoyRide(true);
  };

  return (
    <React.Fragment>
      <Header title="My Lessons">
        <HomeFilters
          viewType={viewType}
          setViewType={setViewType}
          filters={filters}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          setClass={setClass}
          classItem={classItem}
        />
      </Header>
      {homeLoading ? (
        <HomeLoader />
      ) : (
        <React.Fragment>
          {lessonsArr && lessonsArr.length ? (
            <React.Fragment>
              <ContentCardContainer>
                <Typography className={classes.titleBar}>
                  <Typography
                    variant="h5"
                    component="span"
                    className={classes.title}
                    color="textPrimary"
                  >
                    <b>Lessons you have added</b>
                  </Typography>
                </Typography>

                {viewType === 'grid' ? (
                  <LessonGrid
                    lessons={lessonsArr}
                    onComplete={() => {}}
                    loading={false}
                    showPaging={false}
                    paging={paging}
                    onPaging={() => {}}
                    showViewMore={showViewMore}
                    toggleShowMore={toggleShowMore}
                    showMoreText={viewMoreText}
                    selectedClass={classItem}
                  />
                ) : (
                  <LessonList
                    lessons={lessonsArr}
                    isAdmin={false}
                    loading={false}
                    showPaging={false}
                    paging={paging}
                    rowsPerPageOptions={[]}
                    onPaging={() => {}}
                    onEdit={() => {}}
                  />
                )}
              </ContentCardContainer>
            </React.Fragment>
          ) : null}
          {courses && !_.isEmpty(courses) && (
            <React.Fragment>
              {Object.keys(courses).map(courseId => (
                <HomeCourseLessons
                  key={`home-class-${classItem.id}-course-${courseId}`}
                  course={courses[courseId]}
                  viewType={viewType}
                  selectedClass={classItem}
                  fetchHomeClassCourseLessons={
                    props.fetchHomeClassCourseLessons
                  }
                />
              ))}
            </React.Fragment>
          )}
        </React.Fragment>
      )}
      <Dialog
        maxWidth={'xs'}
        open={isWelcomed}
        onClose={() => {
          setIsWelcomed(false);
        }}
      >
        <SubscriptionNotice
          type={'welcome'}
          onClick={onCloseWelcome}
          loading={false}
          image={
            <img className="welcome-image" src={images.app.welcomeImage} />
          }
        />
      </Dialog>
    </React.Fragment>
  );
}

export default HomeView;
