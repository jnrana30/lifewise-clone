import React from 'react';
import { connect } from 'react-redux';
import HomeView from './HomeView';
import {
  fetchHomeData,
  setHomeClass,
  fetchHomeDefaultClass,
  fetchHomeClassCourseLessons,
  clearHomeClass,
  fetchJoyRide,
  setHomeFilters
} from '../../actions/appActions';

export const HomeContainer = props => {
  return <HomeView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  homeLessons: state.app.homeLessons,
  home: state.app.home,
  homeLoading: state.app.homeLoading
});

const mapDispatchToProps = dispatch => ({
  setHomeFilters: filters => dispatch(setHomeFilters(filters)),
  fetchHomeData: (filters, paging) => dispatch(fetchHomeData(filters, paging)),
  fetchJoyRide: data => dispatch(fetchJoyRide(data)),
  clearHomeClass: () => dispatch(clearHomeClass()),
  fetchHomeClassCourseLessons: (courseId, filters, paging) =>
    dispatch(fetchHomeClassCourseLessons(courseId, filters, paging)),
  setHomeClass: (classItem, search) =>
    dispatch(setHomeClass(classItem, search)),
  fetchHomeDefaultClass: () => dispatch(fetchHomeDefaultClass())
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
