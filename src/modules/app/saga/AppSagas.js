import {
  takeLatest,
  all,
  fork,
  put,
  call,
  takeEvery
} from 'redux-saga/effects';
import * as appTypes from '../actions/appTypes';
import * as appApis from '../api/appApis';
import {
  fetchLessons,
  getLessons,
  fetchCourses
} from 'src/modules/courses/api/coursesApis';
import * as appActions from '../actions/appActions';
import { getTeacherClasses } from 'src/modules/school/api/schoolApis';
import toast from 'src/utils/toast';
import _ from 'lodash';

function* fetchRoles() {
  try {
    const data = yield call(appApis.fetchRoles);
    yield put(appActions.setRoles(data));
  } catch (error) {}
}

function* fetchRoleTypes() {
  try {
    const data = yield call(appApis.fetchRoleTypes);
    yield put(appActions.setRoleTypes(data));
  } catch (error) {}
}

function* fetchSchoolYears() {
  try {
    const yearGroups = yield call(appApis.fetchTags, { type: 'yearGroup' });
    const years = [
      ...(yearGroups?.tags && yearGroups?.tags.length
        ? yearGroups?.tags.map(tag => ({
            id: tag.id,
            yearName: tag.tagName
          }))
        : [])
    ];
    // console.log('data1 : ', data1);
    // const data = yield call(appApis.fetchSchoolYears);
    yield put(appActions.setSchoolYears(years));
  } catch (error) {}
}

function* fetchTags({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const users = yield call(appApis.fetchTags, filter);
    yield put(appActions.setTags(users));
  } catch (error) {
    yield put(appActions.setTags([]));
  }
}

function* createTag({ data, resolve, reject }) {
  try {
    const res = yield call(appApis.createTags, data);
    resolve(res);
    toast.success('Tag(s) added successfully.');
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* updateTag({ tagId, data, resolve, reject }) {
  try {
    const res = yield call(appApis.updateTag, tagId, data);
    resolve(res);
    toast.success('Tag updated successfully.');
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* fetchHomeData({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const data = yield call(fetchLessons, filter);
    yield put(appActions.setHomeData(data));
  } catch (error) {
    console.log('error : ', error);
    yield put(appActions.setHomeData([]));
  }
}

function* setHomeClass({ classItem, filters }) {
  let lessons = classItem.lessons;
  const classYear = classItem?.schoolYear?.id ? classItem?.schoolYear?.id : '';
  let courses = {};
  if (classItem.courses) {
    Object.keys(classItem.courses).map(key => {
      const courseItem = classItem.courses[key].instance;

      const records = courseItem.courseLessons.length;
      const lessonsIds = [
        ...Array(Math.ceil(courseItem.courseLessons.length / 16))
      ].map(_ =>
        courseItem.courseLessons.splice(0, 16).map(item => item.lessonId)
      );

      courses[courseItem.id] = {
        courseName: courseItem.courseName,
        id: courseItem.id,
        paging: {
          page: 1,
          perPage: 16,
          records: records
        },
        lessonsIds,
        loading: false,
        lessons: [],
        filters
      };
    });
  }
  yield put(appActions.setHomeClassLessons(lessons, _.cloneDeep(courses)));
}

function* fetchHomeDefaultClass() {
  try {
    const res = yield call(getTeacherClasses);
    if (res && res.classes.length > 0) {
      yield put(appActions.setHomeClass(res.classes[0], {}));
    }
  } catch (error) {
    console.log('error : ', error);
  }
}

function* fetchJoyRide(data) {
  yield put(appActions.setJoyRide(data.setRide));
}

function* fetchHomeClassCourseLessons({ courseId, filters, paging }) {
  // yield put(appActions.setHomeLoading(true));
  try {
    let res = {};

    if (filters.ids && filters.ids.length) {
      filters.ids = `["${filters.ids.join('","')}"]`;
      res = yield call(getLessons, filters);
      yield put(
        appActions.setHomeClassCourseLessons(courseId, res.lessons, false)
      );
    } else if (filters.lessonName && filters.lessonName !== '') {
      res = yield call(fetchLessons, filters);
      yield put(
        appActions.setHomeClassCourseLessons(courseId, res.lessons, res.records)
      );
    }
    // yield put(appActions.setHomeLoading(false));
  } catch (error) {
    // yield put(appActions.setHomeLoading(false));
    yield put(appActions.setHomeClassCourseLessons(courseId, [], 0));
    console.log('error : ', error);
  }
}

export function* watchSagas() {
  yield takeLatest(appTypes.FETCH_ROLES, fetchRoles);
  yield takeLatest(appTypes.FETCH_ROLE_TYPES, fetchRoleTypes);
  yield takeLatest(appTypes.FETCH_SCHOOL_YEARS, fetchSchoolYears);

  yield takeLatest(appTypes.FETCH_TAGS, fetchTags);
  yield takeLatest(appTypes.CREATE_TAG, createTag);
  yield takeLatest(appTypes.UPDATE_TAG, updateTag);

  yield takeLatest(appTypes.FETCH_HOME_DATA, fetchHomeData);
  yield takeLatest(appTypes.SET_HOME_CLASS, setHomeClass);
  yield takeLatest(appTypes.FETCH_HOME_DEFAULT_CLASS, fetchHomeDefaultClass);

  yield takeLatest(appTypes.JOY_RIDE, fetchJoyRide);

  yield takeEvery(
    appTypes.FETCH_HOME_CLASS_COURSE_LESSONS,
    fetchHomeClassCourseLessons
  );
}

export default function* runSagas() {
  yield all([fork(watchSagas)]);
}
