import React, { useState } from 'react';
import { Box, Grid } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { getTagsByType } from '../api/packagesApis';

function CourseForm({ packData, onSubmit }) {
  const isEdit = packData && packData?.id;

  const searchTags = async (search) => {
    const filter = {
      search
    };
    const res = await getTagsByType(filter);
    return res?.tags ? res?.tags : [];
  };

  const handleSubmit = (data) => {
    const payload = { ...data };
    onSubmit({ payload, id: packData?.id, isEdit });
  };

  return (
    <Form
      initialValues={{
        packageName: packData?.packageName ? packData?.packageName : '',
        tags: packData?.tags ? packData?.tags : [],
      }}
      enableReinitialize={true}
      validationSchema={Yup.object().shape({
        packageName: Yup.string().required('Package name is required!'),
        tags: Yup.array().required('Tag is required!'),
      })}
      onSubmit={async (values, form) => {
        let data = { ...values };
        handleSubmit(data);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="package-form"
            noValidate
          >
            <Grid container>
              <Grid item xs={12} md={12}>
                <Form.Field.Input
                  fullWidth
                  name="packageName"
                  label="Package Name"
                />

                <Form.Field.AutoComplete
                  multiple={true}
                  fullWidth
                  showAvatar={false}
                  options={[]}
                  variant="standard"
                  remoteMethod={val => searchTags(val)}
                  name="tags"
                  label="Tags"
                  optLabel="tagName"
                  optValue="id"
                />
              </Grid>
            </Grid>
          </form>
        );
      }}
    </Form>
  );
}

export default CourseForm;
