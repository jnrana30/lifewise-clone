import React from 'react';
import {
  FilterBar,
  FilterDrawer,
  FilterPicker,
  FilterSearch,
} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';
import useViewport from '../../../utils/ViewPort';

function PackagesFilter({ viewType, setViewType, profile, ...props }) {
  const [status, setStatus] = React.useState(false);
  const [search, setSearch] = React.useState('');
  const classes = useStyles();
  const { isMobileView } = useViewport();

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={search}
      onChange={val => {
        setSearch(val);
      }}
      onClear={() => {
        setSearch('');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView && <Box display="flex">{filterOptions}</Box>}
        {isMobileView &&
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        }
        <Box display="flex">
          {profile?.role.level > 5 &&
            <Button
              className={classes.addButton}
              variant="contained"
              color="primary"
              aria-controls="simple-menu"
              onClick={() => {
                props.setDrawerOpen(true);
              }}
              startIcon={<AddIcon />}
            >
              Add Package
            </Button>
          }
        </Box>
      </Box>
    </FilterBar>
  );
}

export default PackagesFilter;
