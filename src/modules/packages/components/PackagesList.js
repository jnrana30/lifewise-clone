import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Box,
  Chip,
  TablePagination,
  Typography,
} from '@material-ui/core/';
import EditIcon from '@material-ui/icons/Edit';

import { ContextMenu } from 'src/components/shared';
import { ContentContainer } from '../../../components/shared/Form/File/styles';

function PackagesList({ packages, onEdit, onPaging, paging, ...props }) {
  const router = useRouter();

  const handleEditClick = (data) => {
    onEdit(data);
  };

  const handleNameClick = (data) => {
    onEdit(data);
  };

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  return (
    <ContentContainer>
      <Paper>
        <TableContainer>
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>Package</TableCell>
                <TableCell>Tags</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(packages || []).map((pack, index) => {
                return (
                  <TableRow key={`packagesList-${pack.id}-${index}`}>
                    <TableCell width="25%">
                      <Box display="flex" alignItems="center" onClick={() => handleNameClick(pack)}>
                        <Typography
                          variant="body1"
                          color="secondary"
                          style={{ cursor: 'pointer' }}
                        >
                          {pack.packageName}
                        </Typography>
                      </Box>
                    </TableCell>
                    <TableCell width="70%">
                      {pack.tags?.length && (
                        <div>
                          {pack.tags.map((tag, tagIndex) => {
                            return (
                              <Chip
                                variant="outlined"
                                key={`packList-${props.index}-tags-${tag.id}-${tagIndex}`}
                                label={tag.tagName}
                                style={{ marginLeft: 4, marginBottom: 4 }}
                              />
                            );
                          })}
                        </div>
                      )}
                    </TableCell>
                    <TableCell width="5%" className="flex-justify-content-end">
                      <ContextMenu
                        options={[
                          {
                            label: 'Edit',
                            icon: <EditIcon />,
                            onClick: () => {
                              handleEditClick(pack);
                            }
                          }
                        ]}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
          <TablePagination
            component="div"
            count={paging?.records ? paging.records : 0}
            rowsPerPage={paging.perPage}
            page={paging.page - 1}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </TableContainer>
      </Paper>
    </ContentContainer>
  );
}

export default PackagesList;
