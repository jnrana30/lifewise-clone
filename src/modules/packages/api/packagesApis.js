import api from 'src/api/index';
import { convertObjectToQuerystring } from 'src/utils/helpers';
const _ = require('lodash');

/* PACKAGES APIs START */
export const fetchPackages = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/subscriptions/packages?${filter}`, null, 'GET');
};

export const getTagsByType = (filters) => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/courses/tags?${filter}`, null, 'GET');
};

/* PACKAGE Create api */
export const createPackage = data => {
  return api(`/subscriptions/packages`, data, 'POST');
};

/* PACKAGE Update api */
export const updatePackage = (data, packageId) => {
  return api(`/subscriptions/packages/${packageId}`, data, 'PATCH');
};
