import React from 'react';
import { connect } from 'react-redux';

import PackageView from './PackageView'
import { fetchPackages, createPackage, patchPackage } from '../actions/packagesActions';

export const PackagesContainer = props => {
  return <PackageView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  packages: state.packages.packages,
});

const mapDispatchToProps = dispatch => ({
  fetchPackages: (filter, paging) => dispatch(fetchPackages(filter, paging)),
  createPackage: data => {
    return new Promise((resolve, reject) => {
      dispatch(createPackage(data, resolve, reject));
    });
  },
  patchPackage: (data, packageId) => {
    return new Promise((resolve, reject) => {
      dispatch(patchPackage(data, packageId, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(PackagesContainer);
