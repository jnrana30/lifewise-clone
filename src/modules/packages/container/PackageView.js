import React, { useEffect, useState } from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';

import PackagesFilter from '../components/PackagesFilter';
import PackagesList from '../components/PackagesList';
import PackageForm from '../components/PackageForm';

function PackagesView(props) {
  const { user, packages, createPackage, patchPackage } = props;

  const [drawerOpen, setDrawerOpen] = useState(false);
  const [selectedPackage, setSelectedPackage] = useState();
  const [loading, setLoading] = useState(false);

  let { filters, paging } = packages;

  useEffect(() => {
    fetchPackages(
      {},
      { page: 1, perPage: 100 },
    );
  }, []);

  const onSubmitToPackage = async (data) => {
    const payload = {
      ...data.payload,
      tags: (data.payload.tags || []).map(e => e.id),
    };
    setLoading(true);
    if (data.id) {
      await patchPackage(payload, data.id)
    } else {
      await createPackage(payload);
    }
    setSelectedPackage(null);
    fetchPackages();
    setDrawerOpen(false);
    setLoading(false);
  };

  const onEditStart = (data) => {
    setSelectedPackage(data);
    setDrawerOpen(true);
  };

  const fetchPackages = (filter, pagingData) => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
    }
    if (typeof filter !== 'pagingData' && !_.isEmpty(pagingData)) {
      paging = { ...paging, ...pagingData };
    }
    props.fetchPackages(filters, paging);
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchPackages();
  };

  return (
    <React.Fragment>
      <Header title="Packages">
        <PackagesFilter setDrawerOpen={setDrawerOpen} profile={user?.profile} />
      </Header>
      <PackagesList
        packages={packages.data}
        paging={paging}
        onPaging={onPaging}
        onEdit={onEditStart}
      />

      <Drawer
        open={drawerOpen}
        title={selectedPackage?.id ? 'Edit Package' : 'Add Package'}
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="package-form"
        buttonLoading={loading}
      >
        <Typography variant="body1" color="textPrimary">
          <b>{selectedPackage?.id ? 'Edit Package' : 'Add new Package'}</b>
        </Typography>
        <PackageForm onSubmit={onSubmitToPackage} packData={selectedPackage} />
      </Drawer>
    </React.Fragment>
  );
}

export default PackagesView;
