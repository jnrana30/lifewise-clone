import { takeLatest, all, fork, put, call, select } from 'redux-saga/effects';
import * as packagesTypes from '../actions/packagesTypes';
import * as packagesActions from '../actions/packagesActions';
import * as packagesApis from '../api/packagesApis';
import toast from 'src/utils/toast';
import _ from 'lodash';

function* fetchPackages ({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const fetchTags = yield all([call(packagesApis.fetchPackages, filter), call(packagesApis.getTagsByType)]);
    const data = {
      ...fetchTags[0],
      packages: fetchTags[0].packages.map(p => ({ ...p, tags: fetchTags[1].tags.filter(t => p.tags.find(t1 => t.id  === t1.tag))}))
    };

    yield put(packagesActions.setPackages(data));
  } catch (error) {
    yield put(packagesActions.setPackages([]));
  }
}

function* createPackage({ data, resolve, reject }) {
  try {
    const res = yield call(packagesApis.createPackage, data);
    toast.success('Package created successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* updatePackage({ data, packageId, resolve, reject }) {
  try {
    const res = yield call(packagesApis.updatePackage, data, packageId);
    toast.success('Package updated successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

export function* watchSagas() {
  yield takeLatest(packagesTypes.FETCH_PACKAGES, fetchPackages);

  yield takeLatest(packagesTypes.CREATE_PACKAGES, createPackage);
  yield takeLatest(packagesTypes.UPDATE_PACKAGES, updatePackage);
}

export default function* runSagas() {
  yield all([fork(watchSagas)]);
}
