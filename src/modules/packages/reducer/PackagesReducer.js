import * as types from '../actions/packagesTypes';

const initialState = {
  packages: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0,
    },
    filters: {},
  },
  package: {},
  loading: {
    package: false,
  },
};

const PackagesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_PACKAGES:
      return {
        ...state,
        packages: {
          ...state.packages,
          loading: true,
        },
      };

    case types.SET_PACKAGES:
      return {
        ...state,
        packages: {
          ...state.packages,
          loading: false,
          data: action.data.packages,
          paging: {
            ...state.packages.paging,
            records: action.data.records
          }
        }
      };

    default:
      return state;
  }
};

export default PackagesReducer;
