import * as types from './packagesTypes';

export const fetchPackages = (filters, paging) => ({
  type: types.FETCH_PACKAGES,
  filters,
  paging,
});


export const createPackage = (data, resolve, reject) => ({
  type: types.CREATE_PACKAGES,
  data,
  resolve,
  reject
});

export const patchPackage = (data, packageId, resolve, reject) => ({
  type: types.UPDATE_PACKAGES,
  data,
  packageId,
  resolve,
  reject
});

export const setPackages = data => ({ type: types.SET_PACKAGES, data });



