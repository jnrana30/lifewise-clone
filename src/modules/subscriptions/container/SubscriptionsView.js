import React, { useEffect, useState } from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';

import SubscriptionsForm from '../components/SubscriptionsForm';
import SubscriptionsFilter from '../components/SubscriptionsFilter';
import SubscriptionsList from '../components/SubscriptionsList';

function SubscriptionsView(props) {
  const { subscriptions, createSubscriptions, tagOfPackages, fetchTagsByPackage, patchSubscription } = props;

  const [drawerOpen, setDrawerOpen] = useState(false);
  const [selectedSubscription, setSelectedSubscription] = useState();
  const [buttonLoading, setButtonLoading] = useState(false);

  let { filters, paging } = subscriptions;

  useEffect(() => {
    fetchTagsByPackage();
    fetchSubscriptions(
      {},
      { page: 1, perPage: 100 },
    );
  }, []);

  const fetchSubscriptions = (filter, pagingData) => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    if (typeof filter !== 'pagingData' && !_.isEmpty(pagingData)) {
      paging = { ...paging, ...pagingData };
    }
    props.fetchSubscriptions(filters, paging);
  };

  const handleSubmit = async (data) => {
    const payload = {
      typeName: data.typeName,
      defaultPrice: data.defaultPrice || '0',
      freeTrial: Boolean(data.freeTrial),
      timePeriod: data.timePeriod + data.duration,
      packages: data.packages.map(p => p.id),
    };
    // setButtonLoading(true);
    if (data.id) {
      await patchSubscription(payload, data.id);
    } else {
      await createSubscriptions(payload);
    }
    setDrawerOpen(false);
    setSelectedSubscription(null);
    fetchSubscriptions();
    setButtonLoading(false);
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchSubscriptions();
  };

  const onEditStart = (data) => {
    setSelectedSubscription(data);
    setDrawerOpen(true);
  };

  return (
    <React.Fragment>
      <Header title="Subscriptions">
        <SubscriptionsFilter setDrawerOpen={setDrawerOpen} />
      </Header>
      <SubscriptionsList
        subscriptions={subscriptions.data}
        paging={paging}
        onPaging={onPaging}
        onEdit={onEditStart}
        loading={subscriptions.loading}
      />

      <Drawer
        open={drawerOpen}
        title={selectedSubscription?.id ? 'Edit Subscriptions' : 'Add Subscriptions'}
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="subscriptions-form"
        buttonLoading={buttonLoading}
      >
        <Typography variant="body1" color="textPrimary">
          <b>{selectedSubscription?.id ? 'Edit subscription.' : 'Add new subscription.'}</b>
        </Typography>
        <SubscriptionsForm
          subscriptionData={selectedSubscription}
          onSubmit={handleSubmit}
          tagsOfPackage={tagOfPackages.data || []}
        />
      </Drawer>
    </React.Fragment>
  );
}

export default SubscriptionsView;
