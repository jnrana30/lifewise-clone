import React from 'react';
import { connect } from 'react-redux';
import SubscriptionsView from './SubscriptionsView';
import { createSubscriptions, fetchSubscriptions, fetchTagsByPackage, patchSubscriptions } from "../actions/subscriptionsActions";

export const SubscriptionsContainer = props => {
  return <SubscriptionsView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  subscriptions: state.subscriptions.subscriptions,
  tagOfPackages: state.subscriptions.tagOfPackages,
});

const mapDispatchToProps = dispatch => ({
  fetchSubscriptions: (filter, paging) => dispatch(fetchSubscriptions(filter, paging)),
  fetchTagsByPackage: (filter, paging) => dispatch(fetchTagsByPackage(filter, paging)),
  createSubscriptions: data => {
    return new Promise((resolve, reject) => {
      dispatch(createSubscriptions(data, resolve, reject));
    });
  },
  patchSubscription: (data, subscriptionId) => {
    return new Promise((resolve, reject) => {
      dispatch(patchSubscriptions(data, subscriptionId, resolve, reject));
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubscriptionsContainer);
