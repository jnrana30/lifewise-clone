import { takeLatest, all, fork, put, call, select } from 'redux-saga/effects';
import * as subscriptionsTypes from '../actions/subscriptionsTypes';
import * as subscriptionsActions from '../actions/subscriptionsActions';
import * as subscriptionsApis from '../api/subscriptionsApis';
import toast from 'src/utils/toast';
import _ from 'lodash';

function* fetchSubscriptions ({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };

    filter = _.omit(filter, ['records']);
    const data = yield call(subscriptionsApis.fetchSubscriptions, filter);

    yield put(subscriptionsActions.setSubscriptions(data));
  } catch (error) {
    yield put(subscriptionsActions.setSubscriptions([]));
  }
}

function* fetchTagsPackage () {
  try {
    const data = yield call(subscriptionsApis.getTagsByPackage);

    yield put(subscriptionsActions.setPackageTags(data));
  } catch (error) {
    yield put(subscriptionsActions.setPackageTags([]));
  }
}

function* createSubscription({ data, resolve, reject }) {
  try {
    const res = yield call(subscriptionsApis.createSubscription, data);
    toast.success('Subscription created successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* updateSubscription({ data, subscriptionId, resolve, reject }) {
  try {
    const res = yield call(subscriptionsApis.updateSubscription, data, subscriptionId);
    toast.success('Subscription updated successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

export function* watchSagas() {
  yield takeLatest(subscriptionsTypes.FETCH_SUBSCRIPTIONS, fetchSubscriptions);
  yield takeLatest(subscriptionsTypes.FETCH_PACKAGE_TAGS, fetchTagsPackage);

  yield takeLatest(subscriptionsTypes.CREATE_SUBSCRIPTIONS, createSubscription);
  yield takeLatest(subscriptionsTypes.UPDATE_SUBSCRIPTIONS, updateSubscription);
}

export default function* runSagas() {
  yield all([fork(watchSagas)]);
}
