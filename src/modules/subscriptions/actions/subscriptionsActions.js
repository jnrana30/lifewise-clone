import * as types from './subscriptionsTypes';

export const fetchSubscriptions = (filters, paging) => ({
  type: types.FETCH_SUBSCRIPTIONS,
  filters,
  paging,
});

export const fetchTagsByPackage = (filters, paging) => ({
  type: types.FETCH_PACKAGE_TAGS,
  filters,
  paging,
});

export const createSubscriptions = (data, resolve, reject) => ({
  type: types.CREATE_SUBSCRIPTIONS,
  data,
  resolve,
  reject
});

export const patchSubscriptions = (data, subscriptionId, resolve, reject) => ({
  type: types.UPDATE_SUBSCRIPTIONS,
  data,
  subscriptionId,
  resolve,
  reject
});

export const setSubscriptions = data => ({ type: types.SET_SUBSCRIPTIONS, data });
export const setPackageTags = data => ({ type: types.SET_PACKAGE_TAGS, data });
