import React, { useState } from 'react';
import { capitalize } from 'lodash';
import EditIcon from '@material-ui/icons/Edit';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Box,
  Collapse,
  Chip,
  IconButton,
  TablePagination,
} from '@material-ui/core/';
import { Button, ContextMenu } from 'src/components/shared';
import { makeStyles } from '@material-ui/core/styles';
import { ContentContainer } from '../../../components/shared/Form/File/styles';
import Skeleton from "@material-ui/lab/Skeleton";

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset'
    }
  }
});

function Row({ subscription, editClick, nameClick }) {
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell width="20%">
          <Box display="flex" alignItems="center" onClick={() => nameClick(subscription)}>
            <Typography variant="body1" color="secondary">
              {subscription.typeName}
            </Typography>
          </Box>
        </TableCell>
        <TableCell width="25%">
          <Typography variant="body2" color="textPrimary">
            {(subscription.subscriptionPackages || []).map((sPack, index) => (
              <Chip
                key={`package-${sPack.id}-name-${index}`}
                variant="outlined"
                style={{ marginLeft: 4, marginBottom: 4 }}
                label={capitalize(sPack.packageName)}
              />
            ))}
            {/*packageName*/}
          </Typography>
        </TableCell>
        <TableCell width="15%">
          <Typography variant="body2" color="textPrimary">
            {subscription.defaultPrice}
          </Typography>
        </TableCell>
        <TableCell width="15%">
          <Box display="flex" alignItems="center">
            <Typography variant="body2" color="textPrimary">
              {/*{Array.isArray(subscription.courses) &&*/}
              {/*subscription.courses.length > 0*/}
              {/*  ? `${subscription.courses.length} courses`*/}
              {/*  : subscription.courses}*/}
              {subscription.timePeriod}
            </Typography>
            {/*{Array.isArray(subscription.courses) &&*/}
            {/*  subscription.courses.length > 0 && (*/}
            {/*    <IconButton*/}
            {/*      aria-label="expand row"*/}
            {/*      size="small"*/}
            {/*      onClick={() => setOpen(!open)}*/}
            {/*    >*/}
            {/*      {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}*/}
            {/*    </IconButton>*/}
            {/*  )}*/}
          </Box>
        </TableCell>
        <TableCell width="20%">
          <Typography variant="body2" color="textPrimary">
            <Chip
              label={capitalize(subscription.freeTrial ? 'Active': 'Inactive')}
              color={
                subscription.freeTrial ? 'primary' : 'default'
              }
            />
          </Typography>
        </TableCell>
        <TableCell width="5%">
          <ContextMenu
            options={[
              {
                label: 'Edit',
                icon: <EditIcon />,
                onClick: () => {
                  editClick(subscription);
                }
              }
            ]}
          />
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

function SubscriptionsList({ subscriptions, onEdit, onPaging, paging, loading, ...props }) {

  const handleEditClick = (data) => {
    onEdit(data);
  };

  const handleNameClick = (data) => {
    onEdit(data);
  };

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  return (
    <ContentContainer>
      <TableContainer component={Paper}>
        <Table aria-label="customized table" className="autoHeight">
          <TableHead>
            <TableRow>
              <TableCell>Subscription Name</TableCell>
              <TableCell>Package</TableCell>
              <TableCell>Base Price</TableCell>
              <TableCell>Time Period</TableCell>
              <TableCell>Free Trial</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={6}>
                  <Skeleton animation="wave"/>
                </TableCell>
              </TableRow>
            ) : (
              <React.Fragment>
                {subscriptions && subscriptions.length > 0 ? subscriptions.map((subscription, index) => {
                  return (
                    <Row
                      key={`SubscriptionsList-${index}`}
                      subscription={subscription}
                      editClick={handleEditClick}
                      nameClick={handleNameClick}
                    />
                  );
                }) : (
                  <TableRow>
                    <TableCell colSpan={4} align="center">
                      <Typography color="textSecondary">
                        No subscription found
                      </Typography>
                    </TableCell>
                  </TableRow>
                )}
              </React.Fragment>
            )}

          </TableBody>
        </Table>
        <TablePagination
          component="div"
          count={paging?.records ? paging.records : 0}
          rowsPerPage={paging.perPage}
          page={paging.page - 1}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </TableContainer>
    </ContentContainer>
  );
}

export default SubscriptionsList;
