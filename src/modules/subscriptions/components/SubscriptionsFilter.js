import React from 'react';
import {FilterBar, FilterDrawer, FilterPicker, FilterSearch} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';
import useViewport from '../../../utils/ViewPort';

function SubscriptionsFilter({ viewType, setViewType, ...props }) {
  const [status, setStatus] = React.useState(false);
  const [search, setSearch] = React.useState('');
  const classes = useStyles();
  const { isMobileView } = useViewport();

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={search}
      onChange={val => {
        setSearch(val);
      }}
      onClear={() => {
        setSearch('');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
      <FilterPicker
        title="Status"
        options={[
          {
            label: 'Active',
            value: 'active'
          },
          {
            label: 'InActive',
            value: 'inactive'
          }
        ]}
        onChange={val => {
          setStatus(val);
        }}
        selected={status}
        onClear={() => {
          setStatus(false);
        }}
        ml={2}
        mr={1}
      />
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">

        {!isMobileView &&
          <Box display="flex">{filterOptions}</Box>
        }
        {isMobileView &&
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        }

        <Box display="flex">
          <Button
            className={classes.addButton}
            variant="contained"
            color="primary"
            aria-controls="simple-menu"
            onClick={() => {
              props.setDrawerOpen(true);
            }}
            startIcon={<AddIcon />}
          >
            Add Subscription
          </Button>
        </Box>
      </Box>
    </FilterBar>
  );
}

export default SubscriptionsFilter;
