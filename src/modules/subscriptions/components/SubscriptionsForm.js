import React from 'react';
import { Box } from '@material-ui/core';
import * as Yup from 'yup';
import { Form } from 'src/components/shared';

import { subscriptionDurationOptions } from '../../../config';

function SubscriptionsForm({ subscriptionData, tagsOfPackage, onSubmit }) {
  const searchTags = async (search) => {
    if (search) {
      return tagsOfPackage.filter(pack => pack.packageName.toLocaleLowerCase().includes(search.toLocaleLowerCase()))
    } else {
      return tagsOfPackage;
    }
  };

  const handleSubmit = (data) => {
    const payload = { ...data };
    onSubmit({ ...payload, id: subscriptionData?.id });
  };

  return (
    <Form
      initialValues={{
        typeName: subscriptionData?.typeName || '',
        packages: subscriptionData?.subscriptionPackages || [],
        defaultPrice: subscriptionData?.defaultPrice || '',
        freeTrial: subscriptionData?.freeTrial || false,
        timePeriod:
          (subscriptionData?.timePeriod || '')
            .substring(0, (subscriptionData?.timePeriod || '').length - 1) || '',
        duration:
          (subscriptionData?.timePeriod || '')
            .charAt((subscriptionData?.timePeriod || '').length - 1) || '',
      }}
      enableReinitialize={true}
      validationSchema={Yup.object().shape({
        typeName: Yup.string().required('Name is required!'),
        defaultPrice: Yup.string().required('Base price is required!'),
        packages: Yup.array().required('Package is required!'),
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
        handleSubmit(values);
      }}
    >
      {({ values, ...props }) => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="subscriptions-form"
            noValidate
          >
            <Form.Field.Input fullWidth name="typeName" label="Name" />
            <Form.Field.AutoComplete
              multiple={true}
              fullWidth
              showAvatar={false}
              options={[]}
              remoteMethod={val => searchTags(val)}
              variant="standard"
              name="packages"
              label="Packages"
              optLabel="packageName"
              optValue="id"
            />
            <Form.Field.Input fullWidth name="defaultPrice" label="Base price" />
            <Box display="flex" flexDirection="row" spacing={2}>
              <Box pr={2} width={0.5}>
                <Form.Field.Input fullWidth name="timePeriod" type='number' label="Time Period" />
              </Box>
              <Box width={0.5}>
                <Form.Field.Select
                  options={subscriptionDurationOptions}
                  fullWidth
                  variant="standard"
                  name="duration"
                  label="Duration"
                />
              </Box>
            </Box>
            <Form.Field.Checkbox
              name={`freeTrial`}
              label={'Free Trial'}
            />
          </form>
        );
      }}
    </Form>
  );
}

export default SubscriptionsForm;
