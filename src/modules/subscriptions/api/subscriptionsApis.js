import api from 'src/api/index';
import { convertObjectToQuerystring } from 'src/utils/helpers';
const _ = require('lodash');

/* PACKAGES APIs START */
export const fetchSubscriptions = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/subscriptions/types?${filter}`, null, 'GET');
};

/* PACKAGE Create api */
export const createSubscription = data => {
  return api(`/subscriptions/types`, data, 'POST');
};

/* PACKAGE Update api */
export const updateSubscription = (data, subscriptionId) => {
  return api(`/subscriptions/types/${subscriptionId}`, data, 'PATCH');
};

export const getTagsByPackage = (filters) => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/subscriptions/packages`, null, 'GET');
};
