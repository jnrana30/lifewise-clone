import * as types from '../actions/subscriptionsTypes';

const initialState = {
  subscriptions: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0,
    },
    filters: {},
  },
  subscription: {},
  tagOfPackages: {
    data: [],
  },
  loading: {
    subscription: false,
  },
};

const SubscriptionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_SUBSCRIPTIONS:
      return {
        ...state,
        subscriptions: {
          ...state.subscriptions,
          loading: true,
        },
      };

    case types.SET_SUBSCRIPTIONS:
      return {
        ...state,
        subscriptions: {
          ...state.subscriptions,
          loading: false,
          data: action.data.types,
          paging: {
            ...state.subscriptions.paging,
            records: action.data.records
          }
        }
      };

    case types.SET_PACKAGE_TAGS:
      return {
        ...state,
        tagOfPackages: {
          ...state.tagOfPackages,
          loading: false,
          data: action.data.packages || [],
        }
      };

    default:
      return state;
  }
};

export default SubscriptionsReducer;
