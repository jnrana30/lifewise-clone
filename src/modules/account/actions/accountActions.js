import * as types from './accountTypes';

export const patchProfile = (data, resolve, reject) => ({
  type: types.PATCH_PROFILE,
  data,
  resolve,
  reject
});

export const createUser = (data, resolve, reject) => ({
  type: types.CREATE_USER,
  data,
  resolve,
  reject
});

export const patchUser = (data, userId, resolve, reject) => ({
  type: types.PATCH_USER,
  data,
  userId,
  resolve,
  reject
});

export const deleteUser = (userId, resolve, reject) => ({
  type: types.DELETE_USER,
  userId,
  resolve,
  reject
});

export const fetchTeachers = (filters, paging) => ({
  type: types.FETCH_TEACHERS,
  filters,
  paging
});
export const setTeachers = data => ({ type: types.SET_TEACHERS, data });

export const fetchEmployees = (filters, paging) => ({
  type: types.FETCH_EMPLOYEES,
  filters,
  paging
});
export const setEmployees = data => ({ type: types.SET_EMPLOYEES, data });

export const fetchStudents = (filters, paging) => ({
  type: types.FETCH_STUDENTS,
  filters,
  paging
});
export const setStudents = data => ({ type: types.SET_STUDENTS, data });

export const updateTeacherClasses = (teacherId, data, resolve, reject) => ({
  type: types.UPDATE_TEACHER_CLASSES,
  teacherId,
  data,
  resolve,
  reject
});
