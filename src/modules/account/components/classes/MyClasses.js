import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableHead,
  TableRow,
  Paper,
  LinearProgress,
  Box,
  Typography
} from '@material-ui/core/';
import { useRouter } from 'next/router';
import Skeleton from '@material-ui/lab/Skeleton';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import moment from 'moment';

const useStyles = makeStyles({
  table: {
    minWidth: 700
  },
  progressBar: {
    height: 10,
    borderRadius: 6
  },
  cursorPointer: {
    cursor: 'pointer'
  }
});

export default function MyClasses({
  classes,
  loading,
  paging,
  onPaging,
  onEdit
}) {
  const styles = useStyles();

  const router = useRouter();

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  return (
    <Paper>
      <TableContainer>
        <Table className={styles.table} aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>Class Name</TableCell>
              <TableCell>Year</TableCell>
              <TableCell>Last Activity</TableCell>
              <TableCell>% Progress</TableCell>
              <TableCell>Play Next Session</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={6}>
                  <Skeleton animation="wave" />
                </TableCell>
              </TableRow>
            ) : (
              <React.Fragment>
                {classes && classes.length > 0 ? (
                  <React.Fragment>
                    {classes.map(classItem => {
                      let total = 0;
                      let completed = 0;
                      let nextLessonId = null;
                      if (
                        classItem.courses &&
                        Object.keys(classItem.courses).length
                      ) {
                        total = Object.keys(classItem.courses).reduce(
                          (sum, data) => {
                            if (classItem.courses[data].lessons) {
                              return (
                                Object.keys(classItem.courses[data].lessons)
                                  .length + sum
                              );
                            } else {
                              return sum;
                            }
                          },
                          0
                        );
                        completed = Object.keys(classItem.courses).reduce(
                          (sum, data) => {
                            if (classItem.courses[data].lessons) {
                              return (
                                Object.keys(
                                  classItem.courses[data].lessons
                                ).filter(
                                  l =>
                                    classItem.courses[data].lessons[l]
                                      .status === 'completed'
                                ).length + sum
                              );
                            } else {
                              return sum;
                            }
                          },
                          0
                        );
                        Object.keys(classItem.courses).forEach(data => {
                          if (classItem.courses[data].lessons) {
                            nextLessonId = Object.keys(
                              classItem.courses[data].lessons
                            ).find(
                              l =>
                                classItem.courses[data].lessons[l].status !==
                                'completed'
                            );
                          }
                        });
                      }

                      const progress =
                        total > 0 ? (completed * 100) / total : 0;
                      return (
                        <TableRow key={`my-class-${classItem.id}`}>
                          <TableCell component="th" scope="row">
                            <Typography
                              variant="body1"
                              color="secondary"
                              style={{ cursor: 'pointer' }}
                              onClick={() => {
                                onEdit(classItem);
                              }}
                            >
                              {classItem.className}
                            </Typography>
                          </TableCell>
                          <TableCell>
                            <Typography variant="body2" color="textPrimary">
                              {classItem?.schoolYear?.yearName
                                ? classItem?.schoolYear?.yearName
                                : '-'}
                            </Typography>
                          </TableCell>

                          <TableCell>
                            {/* {moment(row.lastActivity).fromNow()} */}
                          </TableCell>
                          <TableCell align="left">
                            <LinearProgress
                              className={styles.progressBar}
                              color="primary"
                              variant="determinate"
                              value={progress}
                            />
                            {completed}/{total}
                          </TableCell>
                          <TableCell>
                            {nextLessonId && (
                              <Box
                                display="flex"
                                alignItems="center"
                                className={styles.cursorPointer}
                                onClick={() => {
                                  router.push(`/lessons/${nextLessonId}`);
                                }}
                              >
                                <Box display="flex" mr={1}>
                                  <PlayCircleOutlineIcon />
                                </Box>
                                <div>Play Next Session</div>
                              </Box>
                            )}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </React.Fragment>
                ) : (
                  <TableRow>
                    <TableCell colSpan={6} align="center">
                      <Typography color="textSecondary">
                        No assigned class found
                      </Typography>
                    </TableCell>
                  </TableRow>
                )}
              </React.Fragment>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        component="div"
        count={paging?.records ? paging.records : 0}
        rowsPerPage={paging.perPage}
        page={paging.page - 1}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
}
