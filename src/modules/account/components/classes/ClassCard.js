import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  CardActions,
  IconButton,
  Typography,
  Chip,
  Menu,
  MenuItem,
  Fade
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
// import Image from 'next/image'
import { useRouter } from 'next/router';
import images from 'src/config/images';

const useStyles = makeStyles(theme => ({
  media: {
    height: 0,
    paddingTop: '56.25%' // 16:9
  },
  expand: {
    marginLeft: 'auto'
  },
  tagsContainer: {
    '& > *': {
      margin: theme.spacing(0.5)
    }
  }
}));

export default function RecipeReviewCard({ course, ...props }) {
  const classes = useStyles();
  const router = useRouter();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Card>
      <CardActionArea
        onClick={() => {
          router.push(`/lessons/${course.id}`);
        }}
      >
        <CardMedia
          className={classes.media}
          image={images.app.courseBanner}
          title={course.title}
        />
      </CardActionArea>
      <CardContent>
        <Typography variant="body1" color="textSecondary" component="p">
          {course.desc}
        </Typography>
      </CardContent>

      <CardActions disableSpacing>
        {course.tags?.length && (
          <div className={classes.tagsContainer}>
            {course.tags.map((tag, index) => {
              return (
                <Chip
                  key={`course-${props.courseIndex}-tags-${index}`}
                  label={tag}
                />
              );
            })}
          </div>
        )}
        <IconButton
          aria-label="settings"
          className={classes.expand}
          onClick={handleClick}
        >
          <MoreVertIcon />
        </IconButton>

        <Menu
          id="fade-menu"
          anchorEl={anchorEl}
          keepMounted
          open={open}
          onClose={handleClose}
          TransitionComponent={Fade}
        >
          <MenuItem onClick={handleClose}>Action 01</MenuItem>
          <MenuItem onClick={handleClose}>Action 02</MenuItem>
        </Menu>
      </CardActions>
    </Card>
  );
}
