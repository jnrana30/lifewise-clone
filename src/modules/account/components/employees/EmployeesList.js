import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Typography,
  Box,
  Chip
} from '@material-ui/core/';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Avatar, ContextMenu } from 'src/components/shared';
import EditIcon from '@material-ui/icons/Edit';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Link from 'next/link';
import moment from 'moment';
import { ContentContainer } from '../../../../components/shared/Form/File/styles';

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}));

function EmployeesList({ loading, employees, paging, onPaging, onEdit }) {
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  return (
    <ContentContainer>
      <Paper>
        <TableContainer>
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>Employee Name</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>Role</TableCell>
                <TableCell>Last Activity</TableCell>
                <TableCell>Status</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loading ? (
                <TableRow>
                  <TableCell colSpan={6}>
                    <Skeleton animation="wave" />
                  </TableCell>
                </TableRow>
              ) : (
                <React.Fragment>
                  {employees && employees.length > 0 ? (
                    <React.Fragment>
                      {employees.map((employee, index) => {
                        return (
                          <TableRow key={`employee-list-${index}`}>
                            <TableCell>
                              <Box display="flex" alignItems="center">
                                <Avatar name={employee.firstName} size={36} />
                                <Box ml={2}>
                                  <Typography
                                    variant="body1" style={{ cursor: 'pointer' }} color="secondary"
                                    onClick={() => {
                                      onEdit(employee);
                                    }}
                                  >
                                    {employee.title} {employee.firstName}{' '}
                                    {employee.lastName}
                                  </Typography>
                                </Box>
                              </Box>
                            </TableCell>
                            <TableCell>
                              <Typography variant="body2" color="textPrimary">
                                {employee.workEmail}
                              </Typography>
                            </TableCell>
                            <TableCell width="20%">
                              <Typography variant="body2" color="textPrimary">
                                {employee.role?.name ? employee.role?.name : '-'}
                              </Typography>
                            </TableCell>
                            <TableCell width="14%">
                              <Typography variant="body2" color="textPrimary">
                                {moment(employee.registeredOn).fromNow()}
                              </Typography>
                            </TableCell>
                            <TableCell width="5%">
                              <Chip
                                label={_.capitalize(employee.status)}
                                color={employee.status ? 'primary' : 'default'}
                              />
                            </TableCell>
                            <TableCell width="5%" className="flex-justify-content-end">
                              <ContextMenu
                                options={[
                                  {
                                    label: 'Edit',
                                    icon: <EditIcon />,
                                    onClick: () => {
                                      onEdit(employee);
                                    }
                                  }
                                ]}
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </React.Fragment>
                  ) : (
                    <TableRow>
                      <TableCell colSpan={6} align="center">
                        <Typography color="textSecondary">
                          No data found
                        </Typography>
                      </TableCell>
                    </TableRow>
                  )}
                </React.Fragment>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={paging?.records ? paging.records : 0}
          rowsPerPage={paging.perPage}
          page={paging.page - 1}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </ContentContainer>
  );
}

export default EmployeesList;
