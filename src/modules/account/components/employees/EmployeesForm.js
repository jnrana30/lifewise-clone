import React, { useRef } from 'react';
import * as Yup from 'yup';
import { Box } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';
import { userTitles, userStatusOptions } from 'src/config';
import { checkUsername } from 'src/modules/auth/api/authApis';
import { cacheTest, checkEmailUnique } from 'src/utils/validation';
import _ from 'lodash';
import toast from 'src/utils/toast';
var chars = 'abcdefghijklmnopqrstuvwxyzABCDEDGHIJKLMNOPQRSTUVWXYZ1234567890';

function EmployeesForm({ employee, roleTypes, roles, onSubmit, user }) {
  let rolesSelected = roles.filter(role => role.level > 5);

  rolesSelected.map((role, index) => {
    rolesSelected[index].disabled = role.level > user?.profile?.role?.level;
  });

  const emailUniqueTest = useRef(cacheTest(checkEmailUnique));

  return (
    <Form
      initialValues={{
        title: employee?.title ? employee?.title : '',
        firstName: employee?.firstName ? employee?.firstName : '',
        lastName: employee?.lastName ? employee?.lastName : '',
        workEmail: employee?.workEmail ? employee?.workEmail : '',
        // jobTitle: employee?.jobTitle ? employee?.jobTitle : '',
        role: employee?.role?.id ? employee?.role?.id : '',
        status: employee?.status ? employee?.status : '',
        password: '',
        confPassword: ''
      }}
      validateOnBlur={true}
      validateOnChange={false}
      validationSchema={Yup.object().shape({
        title: Yup.string().required('Title is required!'),
        firstName: Yup.string().required('First Name is required!'),
        lastName: Yup.string().required('Last Name is required!'),
        workEmail: Yup.string()
          .required('Email is required!')
          .email('Please enter a valid email address.')
          .test(
            'username-backend-validation',
            'This email is already taken!',
            async (value, { createError }) => {
              if (value === employee?.workEmail) {
                return true;
              } else {
                return emailUniqueTest.current(value, { createError });
              }
            }
          ),
        role: Yup.string().required('Role is required!'),
        status: Yup.string()
        // password: Yup.string(),
        // confPassword: Yup.string()
      })}
      onSubmit={async (values, form) => {
        const data = values;
        if (
          !employee?.id &&
          values.password !== '' &&
          values.confPassword !== ''
        ) {
          data.login = {
            username: values.workEmail,
            password: values.password,
            confPassword: values.confPassword
          };
        } else {
          data.login = {
            username: values.workEmail
          };
        }
        onSubmit(values);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="employee-form"
            noValidate
          >
            <Form.Field.Select
              options={userTitles}
              fullWidth
              variant="standard"
              name="title"
              label="Title"
            />
            <Form.Field.Input fullWidth name="firstName" label="First Name" />
            <Form.Field.Input fullWidth name="lastName" label="Last Name" />
            <Form.Field.Input fullWidth name="workEmail" label="Email" />
            <Form.Field.Select
              options={rolesSelected}
              fullWidth
              variant="standard"
              name="role"
              label="Permission"
              optLabel="name"
              optValue="id"
            />
            {/*<Form.Field.Select*/}
            {/*  options={roleTypes}*/}
            {/*  fullWidth*/}
            {/*  variant="standard"*/}
            {/*  name="jobTitle"*/}
            {/*  label="Job Role Type"*/}
            {/*  optLabel="name"*/}
            {/*  optValue="id"*/}
            {/*/>*/}
            {employee?.id > 0 && (
              <Form.Field.Select
                options={userStatusOptions}
                fullWidth
                variant="standard"
                name="status"
                label="Status"
              />
            )}

            {!employee?.id && (
              <React.Fragment>
                <Form.Field.Input
                  fullWidth
                  variant="standard"
                  name="password"
                  label="Password"
                  type="password"
                />
                <Form.Field.Input
                  fullWidth
                  variant="standard"
                  name="confPassword"
                  label="Confirm Password"
                  type="password"
                />
                <Box display="flex" justifyContent="flex-end">
                  <Button
                    onClick={() => {
                      const pass = _.sampleSize(chars, 10).join('');
                      props.setFieldValue('password', pass);
                      props.setFieldValue('confPassword', pass);
                      navigator.clipboard.writeText(pass);
                      toast.info('Password is coped to your clipboard!');
                    }}
                  >
                    Generate password
                  </Button>
                </Box>
              </React.Fragment>
            )}
          </form>
        );
      }}
    </Form>
  );
}

export default EmployeesForm;
