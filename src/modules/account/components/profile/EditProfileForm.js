import React, { useRef } from 'react';
import { Box, Typography, Grid } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { userTitles, jobRoleTypes } from 'src/config';
import { cacheTest, checkEmailUnique } from 'src/utils/validation';

export default function EditProfileForm({
  roles,
  roleTypes,
  onSubmit,
  profile
}) {
  const emailUniqueTest = useRef(cacheTest(checkEmailUnique));

  const [loading, setLoading] = React.useState(false);
  return (
    <React.Fragment>
      <Form
        initialValues={{
          title: profile?.title ? profile?.title : '',
          workEmail: profile?.workEmail ? profile?.workEmail : '',
          firstName: profile?.firstName ? profile?.firstName : '',
          lastName: profile?.lastName ? profile?.lastName : '',
          jobTitle: profile?.jobTitle ? profile?.jobTitle : ''
        }}
        validateOnBlur={true}
        validateOnChange={false}
        validationSchema={Yup.object().shape({
          title: Yup.string().required('Title is required!'),
          firstName: Yup.string().required('First Name is required!'),
          lastName: Yup.string().required('Last Name is required!'),
          jobTitle: Yup.string().required('Job Role is required!'),
          workEmail: Yup.string()
            .required('Email is required!')
            .email('Please enter a valid email address.')
            .test(
              'username-backend-validation',
              'This email is already taken!',
              async (value, { createError }) => {
                if (value === profile?.workEmail) {
                  return true;
                } else {
                  return emailUniqueTest.current(value, { createError });
                }
              }
            )
        })}
        onSubmit={async (values, form) => {
          onSubmit(values);
          form.prevent;
        }}
        enableReinitialize={true}
      >
        {props => {
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                props.submitForm();
                return false;
              }}
              id="edit-profile-form"
              noValidate
            >
              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <Form.Field.Select
                    options={userTitles}
                    fullWidth
                    variant="standard"
                    name="title"
                    label="Title"
                  />
                </Grid>
                <Grid item xs={8} ml={2}>
                  <Form.Field.Input
                    fullWidth
                    variant="standard"
                    name="firstName"
                    label="First Name"
                  />
                </Grid>
              </Grid>
              <Form.Field.Input
                fullWidth
                variant="standard"
                name="lastName"
                label="Last Name"
              />
              <Form.Field.Input fullWidth name="workEmail" label="Email" />
              <Form.Field.Select
                options={roleTypes}
                fullWidth
                variant="standard"
                name="jobTitle"
                label="Job Role Type"
                optLabel="name"
                optValue="id"
              />
            </form>
          );
        }}
      </Form>
    </React.Fragment>
  );
}
