import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import {
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Typography,
  Chip,
  Link,
} from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import CancelIcon from '@material-ui/icons/Cancel';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import images from '../../../../config/images';

const useStyles = makeStyles((theme) => ({
  checkIcon: {
    marginRight: theme.spacing(1),
    fontSize: '1.9rem',
  },
  crossIcon: {
    marginRight: theme.spacing(1),
    color: '#9f9f9f',
    fontSize: '1.9rem',
  },
  accordionExpand: {
    margin: `${theme.spacing(1.5)}px 0 !important`,
  },
  accordionSummaryRoot: {
    margin: `0 !important`,
    minHeight: `${theme.spacing(6)}px !important`,
  },
  details: {
    display: 'block',
    padding: `${theme.spacing(1)}px ${theme.spacing(3)}px`,
  },
  detailImage: {
    display: 'block',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    paddingTop: '60%',
  },
  detailHeading: {
    marginLeft: theme.spacing(4),
    marginBottom: theme.spacing(2),
    '& h1': {
      fontWeight: 400,
    },
  },
  contentLink: {
    color: '#3f51b5',
  },
}));

const ProfileStepsDetails = () => {
  const classes = useStyles();

  const stepsDetailList = [
    {
      id: 1,
      title: 'Update your profile',
      status: true,
      isComplete: true,
      subTitle: 'Learn how to and manage classes',
      content: {
        header: 'Additional Resources',
        links: [1,2,3],
      },
    },
    {
      id: 2,
      title: 'Manage Classes',
      status: false,
      isComplete: true,
      subTitle: 'Learn how to and manage classes',
      content: {
        header: 'Additional Resources',
        links: [1,2,3],
      },
    },
    {
      id: 3,
      title: 'Manage teachers',
      status: true,
      isComplete: true,
      subTitle: 'Learn how to and manage classes',
      content: {
        header: 'Additional Resources',
        links: [1,2,3],
      },
    },
    {
      id: 4,
      title: 'Play Lessons',
      status: true,
      isComplete: true,
      subTitle: 'Learn how to and manage classes',
      content: {
        header: 'Additional Resources',
        links: [1,2,3],
      },
    },
    {
      id: 5,
      title: 'Complete Lessons',
      status: true,
      isComplete: true,
      subTitle: 'Learn how to and manage classes',
      content: {
        header: 'Additional Resources',
        links: [1,2,3],
      },
    },
    {
      id: 6,
      title: 'Assess Students',
      status: true,
      isComplete: true,
      subTitle: 'Learn how to and manage classes',
      content: {
        header: 'Additional Resources',
        links: [1,2,3],
      },
    },
    {
      id: 7,
      title: 'Share with parents',
      status: true,
      isComplete: true,
      subTitle: 'Learn how to and manage classes',
      content: {
        header: 'Additional Resources',
        links: [1,2,3],
      },
    },
    {
      id: 8,
      title: 'Get Ofsted Documents',
      status: true,
      isComplete: true,
      subTitle: 'Learn how to and manage classes',
      content: {
        header: 'Additional Resources',
        links: [1,2,3],
      },
    },
  ];

  const preventDefault = (event) => event.preventDefault();

  return (
    <Box>
      {stepsDetailList.map((stepDetail, index) =>
        <Box mb={2}>
          <Accordion defaultExpanded>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1c-content"
              id="panel1c-header"
              classes={{
                root: classes.accordionSummaryRoot,
                expanded: classes.accordionExpand,
              }}
            >
              {stepDetail.status ?
                <CheckCircleIcon color="primary" className={classes.checkIcon}/> :
                <CancelIcon color="primary" className={classes.crossIcon}/>
              }
              <div className={classes.column}>
                <Typography variant="h5">{stepDetail.title}</Typography>
              </div>
            </AccordionSummary>
            <AccordionDetails className={classes.details}>
              <div className={classes.detailHeading}>
                <Typography variant="h6" color="textSecondary">{stepDetail.subTitle}</Typography>
              </div>

              <div className={classes.column}>
                <div className={classes.detailImage} style={{ backgroundImage: `url(${images.app.courseBanner1})` }}>
                </div>
              </div>

              <Box mt={2}>
                <Box display="flex" justifyContent="space-between">
                  <Typography variant="h6">{stepDetail.content.header}</Typography>
                  <Chip color="primary" label={'Mark as Complete'}/>
                </Box>
                {stepDetail.content.links.map((link, lIndex) =>
                  <Box mt={1} mb={1} key={`profile-content-${stepDetail.id}-${link}-${index}-${lIndex}`}>
                    <Link className={classes.contentLink} href="#" onClick={preventDefault} variant="h6">
                      Link
                    </Link>
                  </Box>
                )}
              </Box>
            </AccordionDetails>
          </Accordion>
        </Box>
      )}
    </Box>
  )
};

export default ProfileStepsDetails;