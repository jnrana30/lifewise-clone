import React from 'react';
import {
  Grid,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Card,
  CardContent,
  Box,
  Switch,
  Typography
} from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

function NotificationForm() {
  return (
    <Box mb={3}>
      <Card pa={4}>
        <CardContent>
          <Box mb={2}>
            <Typography variant="h6" color="primary">
              Notifications
            </Typography>
          </Box>
          <List dense>
            <ListItem>
              <ListItemText primary="Lesson reminders" />
              <ListItemSecondaryAction>
                <Switch color="primary" edge="end" />
              </ListItemSecondaryAction>
            </ListItem>
            <ListItem>
              <ListItemText primary="Product Updates" />
              <ListItemSecondaryAction>
                <Switch color="primary" edge="end" />
              </ListItemSecondaryAction>
            </ListItem>
            <ListItem>
              <ListItemText primary="PSHES News" />
              <ListItemSecondaryAction>
                <Switch color="primary" edge="end" />
              </ListItemSecondaryAction>
            </ListItem>
            <ListItem>
              <ListItemText primary="Blogs" />
              <ListItemSecondaryAction>
                <Switch color="primary" edge="end" />
              </ListItemSecondaryAction>
            </ListItem>
          </List>
        </CardContent>
      </Card>
    </Box>
  );
}

export default NotificationForm;
