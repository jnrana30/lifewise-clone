import React from 'react';
import {
  FilterBar,
  FilterSearch,
  FilterPicker,
  AccessControl,
  FilterDrawer
} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';
import { userStatusOptions, teacherStatusOptions } from 'src/config';
import { searchSchools } from 'src/modules/school/api/schoolApis';
import useViewport from '../../../../../utils/ViewPort';

function TeachersFilter({
  filters,
  setFilter,
  unsetFilter,
  isAdmin,
  ...props
}) {
  const [status, setStatus] = React.useState('');
  const [school, setSchool] = React.useState('');
  const [search, setSearch] = React.useState('');
  const classes = useStyles();
  const { isMobileView } = useViewport();

  const [isMount, setIsMount] = React.useState(true);

  React.useEffect(() => {
    if (isAdmin == true) {
      teacherStatusOptions.push({ label: 'Pending', value: 'pending' });
    }
  }, []);

  React.useEffect(() => {
    if (isMount) {
      setIsMount(false);
      return;
    }
    if (school && school?.id && school?.id !== '') {
      setFilter({ school: school.id });
    } else {
      unsetFilter('school');
    }
  }, [school]);

  React.useEffect(() => {
    if (isMount) {
      setIsMount(false);
      return;
    }
    if (status && status?.value && status?.value !== '') {
      setFilter({ status: status.value });
    } else {
      unsetFilter('status');
    }
  }, [status]);

  const getSchools = async search => {
    const filter = {
      schoolName: search,
      perPage: 16
    };
    const res = await searchSchools(filter);
    return res;
  };

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={filters['search'] ? filters['search'] : ''}
      onChange={val => {
        setFilter({
          search: val
        });
      }}
      onClear={() => {
        unsetFilter('search');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
      <FilterPicker
        title="Status"
        options={teacherStatusOptions}
        onChange={val => {
          // setFilter({
          //   status: val.value
          // });
          setStatus(val);
        }}
        selected={status}
        onClear={() => {
          // unsetFilter('status');
          setStatus('');
        }}
        ml={2}
      />
      <AccessControl level={6}>
        <FilterPicker
          title="School"
          options={[]}
          onChange={val => {
            setSchool(val);
          }}
          remoteMethod={val => getSchools(val)}
          selected={school}
          onClear={() => {
            setSchool('');
          }}
          optLabel="schoolName"
          optValue="id"
          ml={2}
          mr={1}
          searchable={true}
        />
      </AccessControl>
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView && <Box display="flex">{filterOptions}</Box>}
        {isMobileView && (
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        )}
        <Box display="flex">
          <Button
            className={classes.addButton}
            variant="contained"
            color="primary"
            aria-controls="simple-menu"
            onClick={() => {
              props.setDrawerOpen(true);
            }}
            startIcon={<AddIcon />}
          >
            Add Teacher
          </Button>
        </Box>
      </Box>
    </FilterBar>
  );
}

export default TeachersFilter;
