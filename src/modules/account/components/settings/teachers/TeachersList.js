import React from 'react';
import { useSelector } from 'react-redux';
import Link from 'next/link';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Typography,
  Box,
  Chip
} from '@material-ui/core/';
import { AccessControl } from 'src/components/App';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Avatar, ContextMenu } from 'src/components/shared';
import EditIcon from '@material-ui/icons/Edit';
import LockIcon from '@material-ui/icons/Lock';
import DeleteIcon from '@material-ui/icons/Delete';
import SendIcon from '@material-ui/icons/Send';
import moment from 'moment';
import _ from 'lodash';
import { ContentContainer } from '../../../../../components/shared/Form/File/styles';
import { resendInvite } from '../../../api/accountApis';
import { forgotPassword } from 'src/modules/auth/api/authApis';
import toast from 'src/utils/toast';

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}));

function TeachersList({
  teachers,
  loading,
  paging,
  onPaging,
  onEdit,
  onDelete
}) {
  const classes = useStyles();
  const userRole = useSelector(state => state.auth.user?.profile?.role);

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  const resentInvitation = userId => {
    resendInvite(userId)
      .then(res => {
        toast.success('Invitation email resent successfully!');
      })
      .catch(err => {
        toast.error('An error ocurred while sending invitation email!');
      });
  };

  const passwordReset = email => {
    forgotPassword({ workEmail: email })
      .then(res => {
        toast.success(`Instruction for reset password sent to ${email}!`);
      })
      .catch(err => {
        toast.error('An error ocurred while sending password reset request!');
      });
  };

  const handleNameClick = teacher => () => {
    onEdit(teacher);
  };

  const getStatus = status => {
    switch (status) {
      case 'disabled':
        return 'Inactive';
        break;

      case 'completed':
        return 'Active';
        break;

      default:
        return _.capitalize(status);
        break;
    }
  };

  return (
    <ContentContainer>
      <Paper className={classes.root}>
        <TableContainer>
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>Teacher Name</TableCell>
                <TableCell>Email</TableCell>
                <AccessControl level={6}>
                  <TableCell>School</TableCell>
                </AccessControl>
                <TableCell>Last Activity</TableCell>
                <TableCell>Status</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loading ? (
                <TableRow>
                  <TableCell colSpan={5}>
                    <Skeleton animation="wave" />
                  </TableCell>
                </TableRow>
              ) : (
                <React.Fragment>
                  {teachers && teachers.length > 0 ? (
                    <React.Fragment>
                      {teachers.map((teacher, index) => {
                        return (
                          <TableRow key={`courselist-${index}`}>
                            <TableCell>
                              <Box display="flex" alignItems="center">
                                <Avatar
                                  name={
                                    teacher.firstName !== null
                                      ? teacher.firstName
                                      : ''
                                  }
                                  size={36}
                                />
                                <Box
                                  ml={2}
                                  onClick={handleNameClick(teacher)}
                                  style={{ cursor: 'pointer' }}
                                >
                                  <Typography variant="body1" color="secondary">
                                    {teacher.title} {teacher.firstName}{' '}
                                    {teacher.lastName}
                                  </Typography>
                                </Box>
                              </Box>
                            </TableCell>
                            <TableCell width="20%">
                              <Typography variant="body2" color="textPrimary">
                                {teacher.workEmail}
                              </Typography>
                            </TableCell>
                            <AccessControl level={6}>
                              <TableCell width="20%">
                                <Link href={`/schools/${teacher.schoolId}`}>
                                  <Typography variant="body2" color="textPrimary" style={{ cursor: 'pointer' }}>
                                    {teacher?.school?.schoolName}
                                  </Typography>
                                </Link>
                              </TableCell>
                            </AccessControl>
                            <TableCell width="20%">
                              <Typography variant="body2" color="textPrimary">
                                {moment(teacher.registeredOn).fromNow()}
                              </Typography>
                            </TableCell>
                            <TableCell width="10%">
                              <Chip
                                label={getStatus(teacher.status)}
                                color={
                                  teacher.status !== 'disabled'
                                    ? 'primary'
                                    : 'default'
                                }
                              />
                            </TableCell>
                            <TableCell
                              width="5%"
                              className="flex-justify-content-end"
                            >
                              <ContextMenu
                                options={[
                                  {
                                    label: 'Edit',
                                    icon: <EditIcon />,
                                    onClick: () => {
                                      onEdit(teacher);
                                    }
                                  },
                                  ...(userRole.level > 4
                                    ? [
                                        {
                                          label: 'Delete',
                                          confirm: true,
                                          confirmMessage:
                                            'Are you sure you want to delete this user?',
                                          icon: <DeleteIcon />,
                                          onClick: () => {
                                            onDelete(teacher);
                                          }
                                        }
                                      ]
                                    : []),
                                  ...(userRole.level >= 2 &&
                                  teacher.status === 'invited'
                                    ? [
                                        {
                                          label: 'Resend invitation',
                                          icon: <SendIcon />,
                                          onClick: () => {
                                            resentInvitation(teacher.id);
                                          }
                                        }
                                      ]
                                    : []),
                                  ...(userRole.level > 4 &&
                                  teacher.status !== 'invited'
                                    ? [
                                        {
                                          label: 'Reset password',
                                          confirm: true,
                                          confirmMessage:
                                            'Are you sure you want to reset password?',
                                          icon: <LockIcon />,
                                          onClick: () => {
                                            passwordReset(teacher.workEmail);
                                            // onDelete(teacher);
                                          }
                                        }
                                      ]
                                    : [])
                                ]}
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </React.Fragment>
                  ) : (
                    <TableRow>
                      <TableCell colSpan={5} align="center">
                        <Typography color="textSecondary">
                          No data found
                        </Typography>
                      </TableCell>
                    </TableRow>
                  )}
                </React.Fragment>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={paging?.records ? paging.records : 0}
          rowsPerPage={paging.perPage}
          page={paging.page - 1}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </ContentContainer>
  );
}

export default TeachersList;
