import React, { useRef } from 'react';
import { useSelector } from 'react-redux';
import * as Yup from 'yup';
import { Box, Grid } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { Form, Button } from 'src/components/shared';
import { Formik, FieldArray } from 'formik';
import { AccessControl } from 'src/components/App';
import { searchSchools } from 'src/modules/school/api/schoolApis';
import { Counter } from './style';
import _ from 'lodash';
import { userTitles } from 'src/config';
import { cacheTest, checkEmailUnique } from 'src/utils/validation';

const teachers = [
  {
    title: '',
    firstName: '',
    lastName: '',
    workEmail: '',
    jobTitle: ''
  }
];

export default function TeachersForm(props) {
  const formRef = React.createRef();
  const userRole = useSelector(state => state.auth.user?.profile?.role);

  const getSchools = async search => {
    const filter = {
      schoolName: search
    };
    const res = await searchSchools(filter);
    return res;
  };

  let validationShape = {};

  const emailUniqueTest = useRef(cacheTest(checkEmailUnique));

  return (
    <Formik
      initialValues={{ teachers, school: {} }}
      validateOnBlur={true}
      validateOnChange={false}
      onSubmit={async values => {
        if (values?.school?.id && values?.school?.id !== '') {
          if (values.teachers && values.teachers.length) {
            values.teachers.map((teacher, index) => {
              teacher.schoolId = values?.school?.id;
            });
          }
        }
        values = _.omit(values, ['school']);
        props.onSubmit(values);
      }}
      validationSchema={Yup.object().shape({
        school: Yup.object()
          .nullable(true)
          .test(
            'school-conditional-validation',
            'Please select a school',
            async school => {
              if (
                userRole.level > 5 &&
                (!school ||
                  school == null ||
                  _.isEmpty(school) ||
                  !school?.id ||
                  school?.id == '')
              ) {
                return false;
              }
              return true;
            }
          ),
        teachers: Yup.array().of(
          Yup.object().shape({
            workEmail: Yup.string()
              .required('Email is required!')
              .email('Please enter a valid email address.')
              .test(
                'username-backend-validation',
                'This email is already taken!',
                async (value, { createError }) => {
                  return emailUniqueTest.current(value, {
                    createError,
                    skipAllowed: true
                  });
                }
              )
          })
        )
      })}
      enableReinitialize={true}
      innerRef={formRef}
    >
      {({ values, ...prop }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            prop.submitForm();
            return false;
          }}
          id="teachers-form"
          noValidate
        >
          <AccessControl level={6}>
            <Form.Field.AutoComplete
              multiple={false}
              fullWidth
              showAvatar={false}
              options={[{}]}
              variant="standard"
              remoteMethod={getSchools}
              name="school"
              label="Choose School"
              optLabel="schoolName"
              optValue="id"
            />
          </AccessControl>
          <FieldArray
            name="teachers"
            render={arrayHelpers => (
              <div>
                {values.teachers &&
                  values.teachers.length > 0 &&
                  values.teachers.map((obj, index) => (
                    <React.Fragment key={`class-${index}-${obj.day}`}>
                      <Grid container spacing={2} alignItems="flex-start">
                        <Grid item xs={1}>
                          <Counter>{index + 1}.</Counter>
                        </Grid>
                        <Grid item container xs={9} spacing={2}>
                          <AccessControl level={6}>
                            <React.Fragment>
                              <Grid item xs={2}>
                                <Form.Field.Select
                                  options={userTitles}
                                  fullWidth
                                  variant="standard"
                                  name={`teachers[${index}].title`}
                                  label="Title"
                                />
                              </Grid>
                              <Grid item xs={5}>
                                <Form.Field.Input
                                  fullWidth
                                  variant="standard"
                                  name={`teachers[${index}].firstName`}
                                  label="First Name"
                                />
                              </Grid>
                              <Grid item xs={5}>
                                <Form.Field.Input
                                  fullWidth
                                  variant="standard"
                                  name={`teachers[${index}].lastName`}
                                  label="Last Name"
                                />
                              </Grid>
                              <Grid item xs={12}>
                                <Form.Field.Select
                                  options={props.roleTypes}
                                  fullWidth
                                  variant="standard"
                                  name={`teachers[${index}].jobTitle`}
                                  label="Job Role Type"
                                  optLabel="name"
                                  optValue="id"
                                />
                              </Grid>
                            </React.Fragment>
                          </AccessControl>
                          <Grid item xs={12}>
                            <Form.Field.Input
                              fullWidth
                              variant="standard"
                              name={`teachers[${index}].workEmail`}
                              label="Add Teacher Email Address"
                            />
                          </Grid>
                        </Grid>
                        <Grid
                          item
                          xs={2}
                          container
                          direction="row"
                          justify="flex-end"
                          alignItems="flex-end"
                        >
                          {values.teachers.length > 1 && (
                            <Button
                              iconButton={true}
                              onClick={() => {
                                arrayHelpers.remove(index);
                              }}
                            >
                              <CloseIcon />
                            </Button>
                          )}
                        </Grid>
                      </Grid>
                    </React.Fragment>
                  ))}
                <Box my={2}>
                  <Button
                    color="primary"
                    onClick={() => {
                      arrayHelpers.push(teachers[0]);
                    }}
                  >
                    + Add new Teacher
                  </Button>
                </Box>
              </div>
            )}
          />
        </form>
      )}
    </Formik>
  );
}
