import React, { useRef } from 'react';
import { Grid, Box } from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import * as Yup from 'yup';
import { Form } from 'src/components/shared';
import { AccessControl } from 'src/components/App';
import { userTitles, userStatusOptions, editTeacherOptions } from 'src/config';
import {
  fetchSchoolById,
  searchSchools,
  getTeacherClasses,
  fetchClasses
} from 'src/modules/school/api/schoolApis';
import _ from 'lodash';
import toast from 'src/utils/toast';
import { cacheTest, checkEmailUnique } from 'src/utils/validation';

export default function EditTeacher({ teacher, roleTypes, onSubmit }) {
  const [loading, setLoading] = React.useState(false);
  const [defaultSchool, setDefaultSchool] = React.useState({});
  const [defaultClasses, setDefaultClasses] = React.useState([]);

  const getSchools = async search => {
    const filter = {
      schoolName: search
    };
    const res = await searchSchools(filter);
    return res;
  };

  const getDefaultClasses = async () => {
    const filter = {
      schoolId: teacher?.schoolId
    };
    const res = await getTeacherClasses(filter, teacher.id);
    return res?.classes ? res?.classes : [];
  };

  const getClasses = async (search, index) => {
    let filter = {
      search,
      status: 'active',
      year: new Date().getFullYear().toString(),
      schoolId: teacher?.schoolId
    };
    const res = await fetchClasses(filter);
    return res?.classes ? res?.classes : [];
  };

  const emailUniqueTest = useRef(cacheTest(checkEmailUnique));

  React.useEffect(() => {
    async function fetchDefaultData() {
      try {
        if (teacher?.schoolId && teacher?.schoolId !== '') {
          fetchSchoolById(teacher?.schoolId)
            .then(res => {
              res.schoolName = `${res.schoolName} (${
                res?.schoolAddresses[0]?.city
                  ? res?.schoolAddresses[0]?.city
                  : ''
              })`;
              setDefaultSchool(res);
            })
            .catch(err => {
              setDefaultSchool({});
            });
        } else {
          setDefaultSchool({});
        }

        const classes = await getDefaultClasses();
        setDefaultClasses(classes);
      } catch (error) {}
    }
    fetchDefaultData();
  }, [teacher]);

  return (
    <React.Fragment>
      {teacher?.status && teacher?.status === 'invited' && (
        <Box mt={1}>
          <Alert severity="info">Invited user cannot be edited!</Alert>
        </Box>
      )}
      <Form
        initialValues={{
          title: teacher.title ? teacher.title : '',
          firstName: teacher.firstName ? teacher.firstName : '',
          lastName: teacher.lastName ? teacher.lastName : '',
          workEmail: teacher.workEmail ? teacher.workEmail : '',
          jobTitle: teacher.jobTitle ? teacher.jobTitle : '',
          status: teacher.status ? teacher.status : '',
          schoolId: defaultSchool,
          classes: defaultClasses
        }}
        validationSchema={Yup.object().shape({
          title: Yup.string().required('Title is required!'),
          firstName: Yup.string().required('First Name is required!'),
          lastName: Yup.string().required('Last Name is required!'),
          workEmail: Yup.string()
            .required('Email is required!')
            .email('Please enter a valid email address.')
            .test(
              'username-backend-validation',
              'This email is already taken!',
              async (value, { createError }) => {
                if (value === teacher?.workEmail) {
                  return true;
                } else {
                  return emailUniqueTest.current(value, {
                    createError,
                    skipAllowed: true
                  });
                }
              }
            ),
          jobTitle: Yup.string().required('Job Role is required!')
        })}
        onSubmit={async (values, form) => {
          if (teacher.status === 'invited') {
            toast.error('Invited user cannot be edited!');
          } else {
            setLoading(true);
            try {
              let data = { ...values };
              data.login = {
                username: data.workEmail
              };
              if (_.isObject(data.schoolId)) {
                data.schoolId = data.schoolId.id;
              }
              const classes = {
                add: values.classes
                  .filter(o1 => !defaultClasses.some(o2 => o1.id === o2.id))
                  .map(item => item.id),
                remove: defaultClasses
                  .filter(o1 => !values.classes.some(o2 => o1.id === o2.id))
                  .map(item => item.id)
              };
              data = _.omit(data, ['classes']);
              await onSubmit(data, classes);
              setLoading(false);
            } catch (error) {
              console.log('error : ', error);
              setLoading(false);
            }
          }

          form.prevent;
        }}
        enableReinitialize={true}
      >
        {props => {
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                props.submitForm();
                return false;
              }}
              noValidate
              id="teachers-form"
            >
              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <Form.Field.Select
                    options={userTitles}
                    fullWidth
                    variant="standard"
                    name="title"
                    label="Title"
                    disabled={teacher?.status === 'invited'}
                  />
                </Grid>
                <Grid item xs={8} ml={2}>
                  <Form.Field.Input
                    fullWidth
                    variant="standard"
                    name="firstName"
                    label="First Name"
                    disabled={teacher?.status === 'invited'}
                  />
                </Grid>
              </Grid>
              <Form.Field.Input
                fullWidth
                variant="standard"
                name="lastName"
                label="Last Name"
                disabled={teacher?.status === 'invited'}
              />
              <AccessControl level={6}>
                <Form.Field.AutoComplete
                  multiple={false}
                  fullWidth
                  showAvatar={false}
                  options={!_.isEmpty(defaultSchool) ? [defaultSchool] : [{}]}
                  variant="standard"
                  remoteMethod={getSchools}
                  name="schoolId"
                  label="Choose School"
                  optLabel="schoolName"
                  optValue="id"
                  disabled={teacher?.status === 'invited'}
                />
              </AccessControl>
              <Form.Field.AutoComplete
                multiple={true}
                fullWidth
                showAvatar={false}
                options={!_.isEmpty(defaultClasses) ? defaultClasses : [{}]}
                variant="standard"
                remoteMethod={getClasses}
                name="classes"
                label="Classes"
                optLabel="className"
                optValue="id"
                disabled={teacher?.status === 'invited'}
              />
              <Form.Field.Input
                fullWidth
                variant="standard"
                name="workEmail"
                label="Email"
                disabled={teacher?.status === 'invited'}
              />
              <Form.Field.Select
                options={roleTypes}
                fullWidth
                variant="standard"
                name="jobTitle"
                label="Job Role Type"
                optLabel="name"
                optValue="id"
                disabled={teacher?.status === 'invited'}
              />
              <Form.Field.Select
                options={editTeacherOptions}
                fullWidth
                variant="standard"
                name="status"
                label="Status"
                disabled={teacher?.status === 'invited'}
                showNone={false}
              />
              {/* <Form.Field.Phone
                fullWidth
                variant="standard"
                name="mobileNumber"
                label="Phone number"
              /> */}
            </form>
          );
        }}
      </Form>
    </React.Fragment>
  );
}
