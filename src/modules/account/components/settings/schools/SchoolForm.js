import React from 'react';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';

function StudentsForm() {
  return (
    <Form
      initialValues={{
        schoolName: '',
        location: '',
        webAddress: ''
      }}
      validationSchema={Yup.object().shape({
        schoolName: Yup.string().required('School Name is required!'),
        location: Yup.string().required('Location is required!'),
        webAddress: Yup.string().required('Location is required!')
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="school-form"
            noValidate
          >
            <Form.Field.Input fullWidth name="schoolName" label="School Name" />
            <Form.Field.Input fullWidth name="location" label="Location" />
            <Form.Field.Input fullWidth name="webAddress" label="Web Address" />
          </form>
        );
      }}
    </Form>
  );
}

export default StudentsForm;
