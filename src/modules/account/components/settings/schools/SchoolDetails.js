import React from 'react';
import * as Yup from 'yup';
import { Box } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';

function StudentsDetails() {
  return (
    <Form
      initialValues={{
        schoolName: '',
        address1: '',
        address2: '',
        webAddress: '',
        city: '',
        region: ''
      }}
      validationSchema={Yup.object().shape({
        schoolName: Yup.string().required('School Name is required!'),
        webAddress: Yup.string().required('Location is required!'),
        address1: Yup.string().required('Address 1 is required!'),
        address2: Yup.string().required('Address 2 is required!'),
        city: Yup.string().required('City is required!'),
        region: Yup.string().required('Region is required!')
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="school-form"
            noValidate
          >
            <Form.Field.Input fullWidth name="schoolName" label="School Name" />
            <Form.Field.Input
              fullWidth
              name="webAddress"
              label="School Website"
            />
            <Form.Field.Input
              fullWidth
              name="address1"
              label="Address Line 1"
            />
            <Form.Field.Input
              fullWidth
              name="address2"
              label="Address Line 2"
            />
            <Form.Field.Input fullWidth name="city" label="City" />
            <Form.Field.Input fullWidth name="region" label="Region" />
            <Box mt={2}>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                fullWidth
                loading={false}
              >
                Submit
              </Button>
            </Box>
          </form>
        );
      }}
    </Form>
  );
}

export default StudentsDetails;
