import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Chip
} from '@material-ui/core';
import moment from 'moment';

function createData(subscription, startDate, endDate, status) {
  return { subscription, startDate, endDate, status };
}

const rows = [
  createData('January', '2021/01/01', '2021/01/30', true),
  createData('Fabruary', '2021/02/01', '2021/02/30', true),
  createData('March', '2021/03/01', '2021/03/30', true),
  createData('April', '2021/04/01', '2021/04/30', true),
  createData('May', '2021/05/01', '2021/05/30', false)
];

export default function SchoolSubscriptions() {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Subscription</TableCell>
            <TableCell align="right">Start Date</TableCell>
            <TableCell align="right">End Date</TableCell>
            <TableCell align="right">Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.subscription}>
              <TableCell>{row.subscription}</TableCell>
              <TableCell align="right">{row.startDate}</TableCell>
              <TableCell align="right">{row.endDate}</TableCell>
              <TableCell align="right">
                <Chip
                  label={row.status ? 'Active' : 'Inactive'}
                  color={row.status ? 'primary' : 'default'}
                />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
