import React from 'react';
import * as Yup from 'yup';
import { Box } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';

function StudentsBilling() {
  return (
    <Form
      initialValues={{
        companyName: '',
        address1: '',
        address2: '',
        city: '',
        region: '',
        country: '',
        postcode: '',
        vat: '',
        invoiceName: '',
        invoiceEmail: ''
      }}
      validationSchema={Yup.object().shape({
        companyName: Yup.string().required('Company name is required!'),
        address1: Yup.string().required('Address 1 is required!'),
        address2: Yup.string().required('Address 2 is required!'),
        city: Yup.string().required('City is required!'),
        region: Yup.string().required('Region is required!'),
        country: Yup.string().required('Country is required!'),
        postcode: Yup.string().required('Post Code is required!'),
        vat: Yup.string().required('VAT number is required!'),
        invoiceName: Yup.string().required('Incoive Name is required!'),
        invoiceEmail: Yup.string().required('Incoive Name is required!')
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="school-form"
            noValidate
          >
            <Form.Field.Input
              fullWidth
              name="companyName"
              label="Company Name"
            />
            <Form.Field.Input
              fullWidth
              name="address1"
              label="Address Line 1"
            />
            <Form.Field.Input
              fullWidth
              name="address2"
              label="Address Line 2"
            />
            <Form.Field.Input fullWidth name="city" label="City" />
            <Form.Field.Input fullWidth name="region" label="Region" />
            <Form.Field.Input fullWidth name="country" label="Country" />
            <Form.Field.Input fullWidth name="postcode" label="Post code" />
            <Form.Field.Input fullWidth name="vat" label="VAT Number" />
            <Form.Field.Input
              fullWidth
              name="invoiceName"
              label="Invoice Name"
            />
            <Form.Field.Input
              fullWidth
              name="invoiceEmail"
              label="Invoice Email"
            />
            <Box mt={2}>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                fullWidth
                loading={false}
              >
                Submit
              </Button>
            </Box>
          </form>
        );
      }}
    </Form>
  );
}

export default StudentsBilling;
