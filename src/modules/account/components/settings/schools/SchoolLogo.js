import React from 'react';
import { SchoolLogoContainer, SchoolLogoImage } from './style';
import images from 'src/config/images';

function SchoolLogo() {
  return (
    <SchoolLogoContainer>
      <SchoolLogoImage src={images.app.schoolLogo} />
    </SchoolLogoContainer>
  );
}

export default SchoolLogo;
