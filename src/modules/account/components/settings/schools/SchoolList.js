import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Box,
  Chip,
  Avatar
} from '@material-ui/core/';
import { Button } from 'src/components/shared';
import Link from 'next/link';
import MoreVertIcon from '@material-ui/icons/MoreVert';

function SchoolList({ schools, ...props }) {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="customized table">
        <TableHead>
          <TableRow>
            <TableCell>School Name</TableCell>
            <TableCell>Address</TableCell>
            <TableCell>No. of Teachers</TableCell>
            <TableCell>Status</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {schools.map((school, index) => {
            return (
              <TableRow key={`courselist-${index}`}>
                <TableCell>
                  <Box display="flex" alignItems="center">
                    <Box mr={2}>
                      <Avatar>{school.name.charAt(0)}</Avatar>
                    </Box>
                    <Link href={`/schools/${school.id}`}>
                      <Typography variant="body1" color="secondary">
                        {school.name}
                      </Typography>
                    </Link>
                  </Box>
                </TableCell>
                <TableCell>
                  <Typography variant="body2" color="textPrimary">
                    {school.address}
                  </Typography>
                </TableCell>
                <TableCell width="15%">
                  <Typography variant="body2" color="textPrimary">
                    {school.noTeachers}
                  </Typography>
                </TableCell>
                <TableCell width="10%">
                  <Typography variant="body2" color="textPrimary">
                    <Chip
                      label={school.status ? 'Active' : 'Inactive'}
                      color={school.status ? 'primary' : 'default'}
                    />
                  </Typography>
                </TableCell>
                <TableCell width="5%" className="flex-justify-content-end">
                  <Button iconButton={true} onClick={() => {}}>
                    <MoreVertIcon color="primary" />
                  </Button>
                </TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default SchoolList;
