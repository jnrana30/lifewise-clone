import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Box,
  Chip
} from '@material-ui/core/';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';
import EditIcon from '@material-ui/icons/Edit';
import { Button, ContextMenu } from 'src/components/shared';
import { ContentContainer } from '../../../../../components/shared/Form/File/styles';

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}));

function StudentsList({ students, loading, paging, onPaging, onEdit }) {
  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };



  return (
    <ContentContainer>
      <Paper>
        <TableContainer>
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>Student Name</TableCell>
                <TableCell>Year</TableCell>
                <TableCell>Class Name</TableCell>
                <TableCell>Academic Year</TableCell>
                <TableCell>Status</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loading ? (
                <TableRow>
                  <TableCell colSpan={6}>
                    <Skeleton animation="wave" />
                  </TableCell>
                </TableRow>
              ) : (
                <React.Fragment>
                  {students && students.length > 0 ? (
                    <React.Fragment>
                      {students.map((student, index) => {
                        console.log(student.classes);
                        return (
                          <TableRow key={`courselist-${index}`}>
                            <TableCell>
                              <Typography
                                variant="body1"
                                color="secondary"
                                style={{ cursor: 'pointer' }}
                                onClick={() => {
                                  onEdit(student);
                                }}
                              >
                                {student.firstName} {student.lastName}
                              </Typography>
                            </TableCell>
                            <TableCell width="15%">
                              <Typography variant="body2" color="textPrimary">
                                {student.classes?.length > 0 ? student.classes[0]?.schoolYear?.yearName || '' : ''}
                              </Typography>
                            </TableCell>
                            <TableCell width="20%">
                              <Typography variant="body2" color="textPrimary">
                              {student.classes?.length > 0 ? student.classes[0]?.className || '' : ''}
                              </Typography>
                            </TableCell>
                            <TableCell width="15%">
                              <Typography variant="body2" color="textPrimary">
                              {student.classes?.length > 0 ? `${student.classes[0]?.academicYearStart || ''}/${student.classes[0]?.academicYearEnd || ''}` : ''}
                              </Typography>
                            </TableCell>
                            <TableCell width="10%">
                              <Chip
                                label={_.capitalize(
                                  student.status
                                    ? student.status == 'completed'
                                      ? 'Active'
                                      : student.status == 'pending'
                                      ? 'Inactive'
                                      : student.status
                                    : ''
                                )}
                                color={student.status ? 'primary' : 'default'}
                              />
                            </TableCell>
                            <TableCell
                              width="5%"
                              className="flex-justify-content-end"
                            >
                              <ContextMenu
                                options={[
                                  {
                                    label: 'Edit',
                                    icon: <EditIcon />,
                                    onClick: () => {
                                      onEdit(student);
                                    }
                                  }
                                ]}
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </React.Fragment>
                  ) : (
                    <TableRow>
                      <TableCell colSpan={6} align="center">
                        <Typography color="textSecondary">
                          No data found
                        </Typography>
                      </TableCell>
                    </TableRow>
                  )}
                </React.Fragment>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={paging?.records ? paging.records : 0}
          rowsPerPage={paging.perPage}
          page={paging.page - 1}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </ContentContainer>
  );
}

export default StudentsList;
