import React from 'react';
import {
  FilterBar,
  FilterDrawer,
  FilterPicker,
  FilterSearch
} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';
import {
  userStatusOptions,
  studentStatusOptions,
  getAcademicYearOption
} from 'src/config';
import useViewport from '../../../../../utils/ViewPort';

function StudentsFilter({ filters, setFilter, unsetFilter, ...props }) {
  const [yearFilter, setYearFilter] = React.useState(false);
  const [status, setStatus] = React.useState(false);
  const [search, setSearch] = React.useState('');

  const { isMobileView } = useViewport();
  const classes = useStyles();

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={filters['search'] ? filters['search'] : ''}
      onChange={val => {
        setFilter({
          search: val
        });
      }}
      onClear={() => {
        unsetFilter('search');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
      <FilterPicker
        title="Status"
        options={studentStatusOptions}
        onChange={val => {
          setFilter({
            status: val.value
          });
        }}
        selected={
          filters['status']
            ? filters['status'] == 'completed'
              ? 'Active'
              : 'Inactive'
            : ''
        }
        onClear={() => {
          unsetFilter('status');
        }}
        ml={2}
      />
      {/* <FilterPicker
        title="Academic Year"
        options={getAcademicYearOption()}
        onChange={val => {
          setYearFilter(val);
        }}
        selected={yearFilter}
        onClear={() => {
          setYearFilter(false);
        }}
        ml={2}
      /> */}
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView && <Box display="flex">{filterOptions}</Box>}
        {isMobileView && (
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        )}
        <Box display="flex">
          <Button
            className={classes.addButton}
            variant="contained"
            color="primary"
            aria-controls="simple-menu"
            onClick={() => {
              props.setDrawerOpen(true);
            }}
            startIcon={<AddIcon />}
          >
            Add Student
          </Button>
        </Box>
      </Box>
    </FilterBar>
  );
}

export default StudentsFilter;
