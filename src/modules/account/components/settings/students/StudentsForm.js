import React from 'react';
import { useSelector } from 'react-redux';
import { Box, Grid } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';
import { AccessControl } from 'src/components/App';
import { Formik, FieldArray } from 'formik';
import { fetchClasses, searchSchools } from '../../../../school/api/schoolApis';
import { userTitles } from 'src/config';
import CloseIcon from '@material-ui/icons/Close';
import { Counter } from './style';
import _ from 'lodash';
import * as Yup from 'yup';

const students = [
  {
    firstName: '',
    lastName: '',
    class: {},
    schoolId: ''
  }
];

function StudentsForm({ onSubmit, student }) {
  const formRef = React.createRef();
  const userRole = useSelector(state => state.auth.user?.profile?.role);

  const getClasses = async (search, index) => {
    let filter = {
      search,
      status: 'active',
      year: new Date().getFullYear().toString()
    };

    const formData = formRef.current?.values;
    if (
      formData &&
      formData?.school &&
      formData?.school?.id &&
      formData?.school?.id !== ''
    ) {
      filter.schoolId = formData?.school?.id;
    }
    const res = await fetchClasses(filter);
    return res?.classes ? res?.classes : [];
  };

  const getSchools = async search => {
    const filter = {
      schoolName: search
    };
    const res = await searchSchools(filter);
    return res;
  };

  return (
    <Formik
      initialValues={{ students, school: {} }}
      validationSchema={Yup.object().shape({
        school: Yup.object()
          .nullable(true)
          .test(
            'school-conditional-validation',
            'Please select a school',
            async school => {
              if (
                userRole.level > 5 &&
                (!school ||
                  school == null ||
                  _.isEmpty(school) ||
                  !school?.id ||
                  school?.id == '')
              ) {
                return false;
              }
              return true;
            }
          )
      })}
      onSubmit={async values => {
        // console.log(values);
        onSubmit(values);
      }}
      innerRef={formRef}
      enableReinitialize={true}
    >
      {({ values, ...props }) => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="student-form"
            noValidate
          >
            <AccessControl level={6}>
              <Grid item xs={10}>
                <Form.Field.AutoComplete
                  multiple={false}
                  fullWidth
                  showAvatar={false}
                  options={[{}]}
                  variant="standard"
                  remoteMethod={getSchools}
                  name="school"
                  label="Choose School"
                  optLabel="schoolName"
                  optValue="id"
                  onChange={val => {
                    if (values.students && values.students.length > 0) {
                      values.students.map((item, index) => {
                        props.setFieldValue(`students[${index}].class`, {});
                      });
                    }
                  }}
                />
              </Grid>
            </AccessControl>
            <FieldArray
              name="students"
              render={arrayHelpers => (
                <div>
                  {values.students &&
                    values.students.length > 0 &&
                    values.students.map((obj, index) => (
                      <React.Fragment key={`student-${index}-${obj.day}`}>
                        <Grid container spacing={2} alignItems="flex-start">
                          <Grid item xs={1}>
                            <Counter>{index + 1}.</Counter>
                          </Grid>
                          <Grid
                            container
                            item
                            xs
                            spacing={2}
                            alignItems="flex-start"
                          >
                            {/* <Grid item xs={4}>
                              <Form.Field.Select
                                options={userTitles}
                                fullWidth
                                variant="standard"
                                name={`students[${index}].title`}
                                label="Title"
                              />
                            </Grid> */}
                            <Grid item xs={6}>
                              <Form.Field.Input
                                fullWidth
                                variant="standard"
                                name={`students[${index}].firstName`}
                                label="First Name"
                              />
                            </Grid>
                            <Grid item xs={6}>
                              <Form.Field.Input
                                fullWidth
                                variant="standard"
                                name={`students[${index}].lastName`}
                                label="Last Name"
                              />
                            </Grid>
                            <Grid item xs={12}>
                              <Form.Field.AutoComplete
                                multiple={false}
                                fullWidth
                                showAvatar={false}
                                options={
                                  student?.classes?.length
                                    ? student?.classes
                                    : [{}]
                                }
                                variant="standard"
                                remoteMethod={value => getClasses(value, index)}
                                name={`students[${index}].class`}
                                label="Choose Class"
                                optLabel="className"
                                optValue="id"
                              />
                            </Grid>
                          </Grid>
                          <Grid
                            item
                            xs={2}
                            container
                            direction="row"
                            justify="flex-end"
                            alignItems="flex-end"
                          >
                            {values.students.length > 1 && (
                              <Button
                                iconButton={true}
                                onClick={() => {
                                  arrayHelpers.remove(index);
                                }}
                              >
                                <CloseIcon />
                              </Button>
                            )}
                          </Grid>
                        </Grid>
                      </React.Fragment>
                    ))}
                  <Box my={2}>
                    <Button
                      color="primary"
                      onClick={() => {
                        arrayHelpers.push(students[0]);
                      }}
                    >
                      + Add new Student
                    </Button>
                  </Box>
                </div>
              )}
            />
          </form>
        );
      }}
    </Formik>
  );
}

export default StudentsForm;
