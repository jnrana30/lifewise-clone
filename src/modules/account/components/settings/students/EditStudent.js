import React, { createRef, useRef } from 'react';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { AccessControl } from 'src/components/App';
import { userTitles } from 'src/config';
import {
  fetchClasses,
  fetchSchoolById,
  searchSchools
} from '../../../../school/api/schoolApis';
import _ from 'lodash';

function EditStudent({ onSubmit, student }) {
  const [defaultSchool, setDefaultSchool] = React.useState({});
  const formRef = useRef();

  const getClasses = async search => {
    let filter = {
      status: 'active',
    };
    if (search && search !== '') {
      filter.search = search;
    }
    const formData = formRef.current?.values;
    if (
      formData &&
      formData?.schoolId &&
      formData?.schoolId?.id &&
      formData?.schoolId?.id !== ''
    ) {
      filter.schoolId = formData?.schoolId?.id;
    }
    const res = await fetchClasses(filter);
    return res?.classes ? res?.classes : [];
  };

  const getSchools = async search => {
    let filter = {};
    if (search && search !== '') {
      filter.schoolName = search;
    }
    const res = await searchSchools(filter);
    return res;
  };

  React.useEffect(() => {
    try {
      if (student?.schoolId && student?.schoolId !== '') {
        fetchSchoolById(student?.schoolId)
          .then(res => {
            res.schoolName = `${res.schoolName} (${
              res?.schoolAddresses[0]?.city ? res?.schoolAddresses[0]?.city : ''
            })`;
            setDefaultSchool(res);
          })
          .catch(err => {
            setDefaultSchool({});
          });
      } else {
        setDefaultSchool({});
      }
    } catch (error) {}
  }, [student]);

  console.log('student');
  console.log(student);

  return (
    <Form
      initialValues={{
        firstName: student?.firstName ? student?.firstName : '',
        lastName: student?.lastName ? student?.lastName : '',
        class: student?.classes?.length ? student?.classes[0] : {},
        schoolId: defaultSchool
      }}
      validationSchema={Yup.object().shape({
        firstName: Yup.string().required('First Name is required!'),
        lastName: Yup.string().required('Last Name is required!'),
        class: Yup.object().required('Please choose a class!')
      })}
      onSubmit={(values, form) => {
        if (_.isObject(values.schoolId)) {
          values.schoolId = values.schoolId.id;
        }
        onSubmit(values);
      }}
      innerRef={formRef}
      enableReinitialize={true}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="student-form"
            noValidate
          >
            {/* <Form.Field.Select
              options={userTitles}
              fullWidth
              variant="standard"
              name="title"
              label="Title"
            /> */}
            <Form.Field.Input fullWidth name="firstName" label="First Name" />
            <Form.Field.Input fullWidth name="lastName" label="Last Name" />
            <AccessControl level={6}>
              <Form.Field.AutoComplete
                multiple={false}
                fullWidth
                showAvatar={false}
                options={!_.isEmpty(defaultSchool) ? [defaultSchool] : [{}]}
                variant="standard"
                remoteMethod={getSchools}
                name="schoolId"
                label="Choose School"
                optLabel="schoolName"
                optValue="id"
              />
            </AccessControl>
            <Form.Field.AutoComplete
              multiple={false}
              fullWidth
              showAvatar={false}
              options={student?.classes?.length ? student?.classes : [{}]}
              variant="standard"
              remoteMethod={val => getClasses(val, props.values)}
              name="class"
              label="Choose Class"
              optLabel="className"
              optValue="id"
            />
          </form>
        );
      }}
    </Form>
  );
}

export default EditStudent;
