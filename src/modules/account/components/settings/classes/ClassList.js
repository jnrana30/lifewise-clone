import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Typography,
  Box,
  Chip
} from '@material-ui/core/';
import Skeleton from '@material-ui/lab/Skeleton';
import { AccessControl } from 'src/components/App';
import { Button, ContextMenu } from 'src/components/shared';
import EditIcon from '@material-ui/icons/Edit';
import _ from 'lodash';
import { ContentContainer } from '../../../../../components/shared/Form/File/styles';

function ClassList({ classes, loading, paging, onPaging, onEdit }) {
  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  return (
    <ContentContainer>
      <Paper>
        <TableContainer>
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>Class Name</TableCell>
                <TableCell>Year</TableCell>
                {/* <AccessControl level={5}>
                  <TableCell>School</TableCell>
                </AccessControl> */}
                <TableCell>No. of Students</TableCell>
                <TableCell>Academic Year</TableCell>
                <TableCell align="center">Status</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loading ? (
                <TableRow>
                  <TableCell colSpan={6}>
                    <Skeleton animation="wave" />
                  </TableCell>
                </TableRow>
              ) : (
                <React.Fragment>
                  {classes && classes.length > 0 ? (
                    <React.Fragment>
                      {classes.map((classItem, index) => {
                        return (
                          <TableRow key={`courselist-${index}`}>
                            <TableCell>
                              <Typography
                                variant="body1"
                                color="secondary"
                                style={{ cursor: 'pointer' }}
                                onClick={() => {
                                  onEdit(classItem);
                                }}
                              >
                                {classItem.className}
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <Typography variant="body2" color="textPrimary">
                                {classItem?.schoolYear?.yearName
                                  ? classItem?.schoolYear?.yearName
                                  : '-'}
                              </Typography>
                            </TableCell>
                            {/* <AccessControl level={5}>
                              <TableCell>Hello</TableCell>
                            </AccessControl> */}
                            <TableCell>
                              <Typography variant="body2" color="textPrimary">
                                {classItem.students.length}
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <Typography variant="body2" color="textPrimary">
                                {classItem?.academicYearStart}-
                                {classItem?.academicYearEnd}
                              </Typography>
                            </TableCell>
                            <TableCell align="center">
                              {classItem.classStatus ? (
                                <Chip
                                  label={_.capitalize(classItem.classStatus)}
                                  color={
                                    classItem.classStatus
                                      ? 'primary'
                                      : 'default'
                                  }
                                />
                              ) : (
                                <span>-</span>
                              )}
                            </TableCell>
                            <TableCell
                              width="5%"
                              className="flex-justify-content-end"
                            >
                              <ContextMenu
                                options={[
                                  {
                                    label: 'Edit',
                                    icon: <EditIcon />,
                                    onClick: () => {
                                      onEdit(classItem);
                                    }
                                  }
                                ]}
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </React.Fragment>
                  ) : (
                    <TableRow>
                      <TableCell colSpan={6} align="center">
                        <Typography color="textSecondary">
                          No data found
                        </Typography>
                      </TableCell>
                    </TableRow>
                  )}
                </React.Fragment>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={paging?.records ? paging.records : 0}
          rowsPerPage={paging.perPage}
          page={paging.page - 1}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </ContentContainer>
  );
}

export default ClassList;
