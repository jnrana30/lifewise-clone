import React from 'react';
import { useSelector } from 'react-redux';
import { Box, Grid, Divider } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { AccessControl } from 'src/components/App';
import { Form, Button } from 'src/components/shared';
import { Formik, FieldArray } from 'formik';
import _ from 'lodash';
import * as Yup from 'yup';
import { Counter } from './style';
import { fetchUsers } from '../../../api/accountApis';
import { searchSchools } from 'src/modules/school/api/schoolApis';

const defaultClasses = [
  {
    className: '',
    schoolYear: '',
    teachers: [],
    academicYearStart: new Date().getFullYear(),
    academicYearEnd: new Date().getFullYear() + 1
  }
];

export default function ClassForm({ onSubmit, schoolYears }) {
  const formRef = React.createRef();
  const userRole = useSelector(state => state.auth.user?.profile?.role);
  const [loading, setLoading] = React.useState(false);

  const getTeachers = async (search, index) => {
    const filter = {
      search,
      role: '[1, 8]'
    };
    const formData = formRef.current?.values;
    if (
      formData &&
      formData?.school &&
      formData?.school?.id &&
      formData?.school?.id !== ''
    ) {
      filter.school = formData?.school?.id;
    }
    const res = await fetchUsers(filter);
    return res?.profiles ? res?.profiles : [];
  };

  const getSchools = async search => {
    const filter = {
      schoolName: search
    };
    const res = await searchSchools(filter);
    return res;
  };

  return (
    <Formik
      initialValues={{
        classes: defaultClasses,
        school: {}
      }}
      validationSchema={Yup.object().shape({
        school: Yup.object()
          .nullable(true)
          .test(
            'school-conditional-validation',
            'Please select a school',
            async school => {
              if (
                userRole.level > 5 &&
                (!school ||
                  school == null ||
                  _.isEmpty(school) ||
                  !school?.id ||
                  school?.id == '')
              ) {
                return false;
              }
              return true;
            }
          ),
        classes: Yup.array().of(
          Yup.object().shape({
            className: Yup.string().required('Please enter Class Name!'),
            schoolYear: Yup.object().required('Please choose a year group!')
          })
        )
      })}
      onSubmit={async values => {
        values.classes.map((classItem, index) => {
          const teachers = classItem.teachers.map(teacher => teacher.id);
          values.classes[index].teachers = teachers;
          values.classes[index].classStatus = 'active';
        });
        onSubmit(values);
      }}
      innerRef={formRef}
      enableReinitialize={true}
    >
      {({ values, ...props }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            props.submitForm();
            return false;
          }}
          noValidate
          id="class-form"
        >
          <AccessControl level={6}>
            <Grid item xs={12}>
              <Form.Field.AutoComplete
                multiple={false}
                fullWidth
                showAvatar={false}
                options={[]}
                variant="standard"
                remoteMethod={getSchools}
                name="school"
                label="Choose School"
                optLabel="schoolName"
                optValue="id"
                onChange={val => {
                  if (values.classes && values.classes.length > 0) {
                    values.classes.map((item, index) => {
                      props.setFieldValue(`classes[${index}].teachers`, []);
                    });
                  }
                }}
              />
            </Grid>
          </AccessControl>
          <FieldArray
            name="classes"
            render={arrayHelpers => (
              <div>
                {values.classes &&
                  values.classes.length > 0 &&
                  values.classes.map((obj, index) => (
                    <React.Fragment key={`class-${index}-${obj.day}`}>
                      <Grid container spacing={2} alignItems="flex-start">
                        <Grid item xs={1}>
                          <Counter>{index + 1}.</Counter>
                        </Grid>
                        <Grid
                          container
                          item
                          xs
                          spacing={2}
                          alignItems="flex-start"
                        >
                          <Grid item xs={8}>
                            <Form.Field.Input
                              fullWidth
                              variant="standard"
                              name={`classes[${index}].className`}
                              label="Add Class Name"
                            />
                          </Grid>
                          <Grid item xs={4}>
                            <Form.Field.AutoComplete
                              multiple={false}
                              options={schoolYears}
                              fullWidth
                              variant="standard"
                              name={`classes[${index}].schoolYear`}
                              label="Year Group"
                              optLabel="yearName"
                              optValue="id"
                            />
                          </Grid>

                          <Grid item xs={12}>
                            <Form.Field.AutoComplete
                              multiple={true}
                              fullWidth
                              showAvatar={true}
                              options={[]}
                              variant="standard"
                              remoteMethod={value => getTeachers(value, index)}
                              name={`classes[${index}].teachers`}
                              label="Select teachers"
                              optLabel="workEmail"
                              optValue="id"
                            />
                          </Grid>
                        </Grid>
                        <Grid
                          item
                          xs={2}
                          container
                          direction="row"
                          justify="flex-end"
                          alignItems="flex-end"
                        >
                          {values.classes.length > 1 && (
                            <Box mt={10}>
                              <Button
                                iconButton={true}
                                onClick={() => {
                                  arrayHelpers.remove(index);
                                }}
                              >
                                <CloseIcon />
                              </Button>
                            </Box>
                          )}
                        </Grid>
                      </Grid>
                      <div style={{ height: 24 }} />
                      {/* <Divider variant="fullWidth" />
                      <div style={{ height: 24 }} /> */}
                    </React.Fragment>
                  ))}
                <Box my={2}>
                  <Button
                    color="primary"
                    onClick={() => {
                      arrayHelpers.push(defaultClasses[0]);
                    }}
                  >
                    + Add new class
                  </Button>
                </Box>
              </div>
            )}
          />
        </form>
      )}
    </Formik>
  );
}
