import React, { useEffect, useRef } from 'react';
import * as Yup from 'yup';
import { Grid, Box, Typography } from '@material-ui/core';
import _ from 'lodash';
import { makeStyles } from '@material-ui/core/styles';
import { Form, Button } from 'src/components/shared';
import { classStatusOptions, getAcademicYearOption } from 'src/config';
import { AccessControl } from 'src/components/App';
import { fetchUsers } from '../../../api/accountApis';
import {
  fetchSchools,
  fetchSchoolById,
  searchSchools
} from 'src/modules/school/api/schoolApis';

const useStyles = makeStyles(theme => ({
  switchContainer: {
    '& .MuiFormControlLabel-labelPlacementStart': {
      marginLeft: 0
    }
  }
}));

export default function EditClassForm({ editClass, schoolYears, onSubmit }) {
  const classes = useStyles();
  const [loading, setLoading] = React.useState(false);
  const [teachers, setTeachers] = React.useState([]);
  const [defaultSchool, setDefaultSchool] = React.useState({});
  const [classStatus, newClassStatus] = React.useState(
    editClass?.classStatus === 'active'
  );
  const formRef = useRef();

  const getSchools = async search => {
    const filter = {
      schoolName: search
    };
    const res = await searchSchools(filter);
    return res;
  };

  useEffect(() => {
    if (editClass.teachers && editClass.teachers.length > 0) {
      const filter = {
        ids: `[${editClass.teachers}]`,
        propagate: false
      };
      fetchUsers(filter)
        .then(res => {
          setTeachers(res.profiles);
        })
        .catch(err => {
          setTeachers([]);
        });
    }

    if (editClass?.schoolId && editClass?.schoolId !== '') {
      fetchSchoolById(editClass?.schoolId)
        .then(res => {
          res.schoolName = `${res.schoolName} (${
            res?.schoolAddresses[0]?.city ? res?.schoolAddresses[0]?.city : ''
          })`;
          setDefaultSchool(res);
        })
        .catch(err => {
          setDefaultSchool({});
        });
    } else {
      setDefaultSchool({});
    }
  }, [editClass]);

  const handleSwitchChange = value => {
    newClassStatus(value);
  };

  const getTeachers = async search => {
    const filter = {
      search,
      role: '[1, 8]',
      perPage: 20
    };
    const formData = formRef.current?.values;
    if (
      formData &&
      formData?.schoolId &&
      formData?.schoolId?.id &&
      formData?.schoolId?.id !== ''
    ) {
      filter.school = formData?.schoolId?.id;
    }
    const res = await fetchUsers(filter);
    return res?.profiles ? res?.profiles : [];
  };

  return (
    <React.Fragment>
      <Form
        initialValues={{
          className: editClass?.className ? editClass?.className : '',
          schoolYear: editClass?.schoolYear ? editClass?.schoolYear : '',
          teachers: teachers,
          classStatus: editClass?.classStatus
            ? editClass?.classStatus === 'active'
            : false,
          academicYear:
            editClass.academicYearStart && editClass.academicYearEnd
              ? `${editClass.academicYearStart}/${editClass.academicYearEnd}`
              : '',
          schoolId: defaultSchool
        }}
        enableReinitialize={true}
        validationSchema={Yup.object().shape({
          className: Yup.string().required('Class Name is required!'),
          schoolYear: Yup.object()
            .nullable(false)
            .required('School Year is required!'),
          academicYear: Yup.string().required('Academic year is required!')
        })}
        onSubmit={async (values, form) => {
          let data = { ...values };
          data.teachers = data.teachers.map(teacher => teacher.id);
          data.classStatus = data.classStatus === true ? 'active' : 'inactive';

          if (data.academicYear && data.academicYear != '') {
            const [academicYearStart, academicYearEnd] =
              data.academicYear.split('/');
            data.academicYearEnd = academicYearEnd * 1;
            data.academicYearStart = academicYearStart * 1;
          }
          data = _.omit(data, ['academicYear', 'schoolId']);
          // DISABLE CHANGING SCHOOL FOR THE CLASS
          // if (_.isObject(data.schoolId)) {
          //   data.schoolId = data.schoolId.id;
          // }
          onSubmit(data);
          form.prevent;
        }}
        innerRef={formRef}
      >
        {props => {
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                props.submitForm();
                return false;
              }}
              noValidate
              id="class-form"
            >
              <Form.Field.Input
                fullWidth
                variant="standard"
                name="className"
                label="Class Name"
              />
              <Form.Field.Select
                options={getAcademicYearOption()}
                fullWidth
                variant="standard"
                name="academicYear"
                label="Academic Year"
              />
              <Form.Field.AutoComplete
                options={schoolYears}
                fullWidth
                variant="standard"
                name="schoolYear"
                label="School Year"
                optLabel="yearName"
                optValue="id"
              />

              <AccessControl level={6}>
                <Form.Field.AutoComplete
                  multiple={false}
                  fullWidth
                  showAvatar={false}
                  options={!_.isEmpty(defaultSchool) ? [defaultSchool] : [{}]}
                  variant="standard"
                  remoteMethod={getSchools}
                  name="schoolId"
                  label="Choose School"
                  optLabel="schoolName"
                  optValue="id"
                  disabled={true}
                />
              </AccessControl>
              <Form.Field.AutoComplete
                multiple={true}
                fullWidth
                showAvatar={true}
                options={teachers}
                variant="standard"
                remoteMethod={getTeachers}
                name="teachers"
                label="Select teachers"
                optLabel="workEmail"
                optValue="id"
              />

              <Grid
                container
                alignItems={'center'}
                className={classes.switchContainer}
              >
                <Box mt={2}>
                  <Typography variant="body2">Class Status:</Typography>
                </Box>
                <Form.Field.Switch
                  name="classStatus"
                  label=""
                  onChange={handleSwitchChange}
                />
                <Box mt={2} ml={1.5}>
                  <Typography variant="body2" color="textSecondary">
                    {classStatus ? '(Active)' : '(Deactive)'}
                  </Typography>
                </Box>
              </Grid>

              {/* <Box
                mt={2}
                display="flex"
                justifyContent="flex-start"
                alignItems="center"
              >
                <Typography variant="body">Status : </Typography>
                <Switch
                  color="primary"
                  checked={props.values.classStatus === 'active'}
                  onChange={() => {

                  }}
                />
              </Box> */}
            </form>
          );
        }}
      </Form>
    </React.Fragment>
  );
}
