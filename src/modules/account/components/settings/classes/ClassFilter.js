import React from 'react';
import {
  FilterBar,
  FilterPicker,
  FilterSearch,
  AccessControl,
  FilterDrawer
} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';
import { classStatusOptions, getAcademicYearOption } from 'src/config';
const years = getAcademicYearOption();
import { searchSchools } from 'src/modules/school/api/schoolApis';
import useViewport from '../../../../../utils/ViewPort';

function ClassFilter({ filters, setFilter, unsetFilter, ...props }) {
  const [yearFilter, setYearFilter] = React.useState(false);
  const [school, setSchool] = React.useState('');
  const [isMount, setIsMount] = React.useState(true);
  const classes = useStyles();
  const { isMobileView } = useViewport();

  React.useEffect(() => {
    if (isMount) {
      setIsMount(false);
      return;
    }
    if (school && school?.id && school?.id !== '') {
      setFilter({ schoolId: school.id });
    } else {
      unsetFilter('schoolId');
    }
  }, [school]);

  const getSchools = async search => {
    const filter = {
      schoolName: search,
      perPage: 16
    };
    const res = await searchSchools(filter);
    return res;
  };

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={filters['search'] ? filters['search'] : ''}
      onChange={val => {
        setFilter({
          search: val
        });
      }}
      onClear={() => {
        unsetFilter('search');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
      <FilterPicker
        title="Status"
        options={classStatusOptions}
        onChange={val => {
          setFilter({
            status: val.value
          });
        }}
        selected={filters['status'] ? filters['status'] : ''}
        onClear={() => {
          unsetFilter('status');
        }}
        ml={2}
      />
      <FilterPicker
        title="Academic Year"
        options={years}
        onChange={val => {
          setFilter({
            year: val.value.split('/')[0]
          });
        }}
        selected={
          filters['year']
            ? years.filter(
                year => year.value.split('/')[0] === filters['year']
              )[0]
            : ''
        }
        onClear={() => {
          unsetFilter('year');
        }}
        ml={2}
      />
      <AccessControl level={6}>
        <FilterPicker
          title="School"
          options={[]}
          onChange={val => {
            setSchool(val);
          }}
          remoteMethod={val => getSchools(val)}
          selected={school}
          onClear={() => {
            setSchool('');
          }}
          optLabel="schoolName"
          optValue="id"
          ml={2}
          mr={1}
          searchable={true}
        />
      </AccessControl>
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView && <Box display="flex">{filterOptions}</Box>}
        {isMobileView && (
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        )}

        <Box display="flex">
          <Button
            className={classes.addButton}
            variant="contained"
            color="primary"
            aria-controls="simple-menu"
            onClick={() => {
              props.setDrawerOpen(true);
            }}
            startIcon={<AddIcon />}
          >
            Add Class
          </Button>
        </Box>
      </Box>
    </FilterBar>
  );
}

export default ClassFilter;
