export { default as SchoolDetails } from './SchoolDetails';
export { default as UserDetails } from './UserDetails';
export { default as ClassDetails } from './ClassDetails';
export { default as InviteColleagues } from './InviteColleagues';
