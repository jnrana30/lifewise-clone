import React from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { Formik, FieldArray } from 'formik';
import _ from 'lodash';
import {
  fetchSchoolById,
  searchSchools,
  getTeacherClasses,
  fetchClasses
} from 'src/modules/school/api/schoolApis';

const defaultClasses = [
  {
    className: '',
    yearGroup: '',
    academicYearStart: new Date().getFullYear(),
    academicYearEnd: new Date().getFullYear() + 1
  }
];

export default function ClassDetails({
  schoolYears,
  schoolId,
  userId,
  ...props
}) {
  const [existingClassesData, setExistingClassesData] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [loadingExisting, setLoadingExisting] = React.useState(false);
  const [availableClasses, setAvailableClasses] = React.useState([]);

  const getDefaultClasses = async () => {
    const filter = {
      schoolId
    };
    const res = await getTeacherClasses(filter, userId);
    return res?.classes ? res?.classes : [];
  };

  const getClasses = async (search, index) => {
    let filter = {
      status: 'active',
      year: new Date().getFullYear().toString(),
      schoolId: schoolId
    };
    if (search && search !== '') {
      filter.search = search;
    }
    const res = await fetchClasses(filter);
    return res?.classes ? res?.classes : [];
  };

  React.useEffect(() => {
    async function fetchDefaultData() {
      try {
        setLoadingExisting(true);
        const classes = await getDefaultClasses();
        setExistingClassesData(classes);
        const availableClasses = await getClasses();
        setAvailableClasses(availableClasses);
        setLoadingExisting(false);
      } catch (error) {
        setAvailableClasses([]);
        setLoadingExisting(false);
      }
    }
    fetchDefaultData();
  }, []);

  return (
    <Formik
      initialValues={{
        classes:
          availableClasses.length == 0 && !loadingExisting
            ? defaultClasses
            : [],
        existingClasses: existingClassesData
      }}
      validationSchema={Yup.object().shape({
        classes: Yup.array().of(
          Yup.object().shape({
            className: Yup.string().required('Class name is required!'),
            yearGroup: Yup.string().required('Year Group is required!')
          })
        ),
        existingClasses: Yup.array()
          .nullable(true)
          .test(
            'choose-class-validation',
            'Please choose a class or create a new class below.',
            async (value, { ...otherProps }) => {
              if (
                otherProps.parent.classes.length === 0 &&
                value.length === 0
              ) {
                return false;
              } else {
                return true;
              }
            }
          )
      })}
      enableReinitialize={true}
      onSubmit={async values => {
        setLoading(true);
        let data = { ...values };
        data.classes.map((classItem, index) => {
          classItem.schoolYear = schoolYears.filter(
            obj => obj.id == classItem.yearGroup
          )[0];
          classItem.classStatus = 'active';
          if (props?.registerData?.profile?.role?.level === 2) {
            classItem.teachers = [props?.registerData?.profile?.id];
          }
        });
        const existingClasses = {
          add: data.existingClasses
            .filter(o1 => !existingClassesData.some(o2 => o1.id === o2.id))
            .map(item => item.id),
          remove: existingClassesData
            .filter(o1 => !data.existingClasses.some(o2 => o1.id === o2.id))
            .map(item => item.id)
        };
        data = _.omit(data, ['existingClasses']);
        await props.onSubmit(data, existingClasses);
        setLoading(false);
      }}
    >
      {({ values, ...prop }) => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              prop.submitForm();
              return false;
            }}
            noValidate
          >
            <Box mt={2}>
              <Typography>
                <b>Assign yourself to a class.</b>
              </Typography>
            </Box>
            {availableClasses && availableClasses.length > 0 && (
              <Form.Field.AutoComplete
                multiple={true}
                fullWidth
                showAvatar={false}
                options={
                  !_.isEmpty(existingClassesData) ? existingClassesData : [{}]
                }
                variant="standard"
                remoteMethod={getClasses}
                name="existingClasses"
                label="Classes"
                optLabel="className"
                optValue="id"
                disabled={loadingExisting}
              />
            )}
            <FieldArray
              name="classes"
              render={arrayHelpers => (
                <div>
                  {values.classes &&
                    values.classes.length > 0 &&
                    values.classes.map((obj, index) => (
                      <React.Fragment key={`class-${index}-${obj.day}`}>
                        <Grid container spacing={2} alignItems="flex-end">
                          <Grid item xs={7}>
                            <Form.Field.Input
                              fullWidth
                              variant="standard"
                              name={`classes[${index}].className`}
                              label="Add Class Name"
                            />
                          </Grid>
                          <Grid item xs={4} ml={2}>
                            <Form.Field.Select
                              options={schoolYears}
                              fullWidth
                              variant="standard"
                              name={`classes[${index}].yearGroup`}
                              label="Year Group"
                              optLabel="yearName"
                              optValue="id"
                            />
                          </Grid>
                          <Grid item xs={1} ml={2}>
                            {((availableClasses.length == 0 &&
                              values.classes.length > 1) ||
                              (availableClasses.length > 0 &&
                                values.classes.length > 0)) && (
                              <Button
                                iconButton={true}
                                onClick={() => {
                                  arrayHelpers.remove(index);
                                }}
                              >
                                <CloseIcon />
                              </Button>
                            )}
                          </Grid>
                        </Grid>
                      </React.Fragment>
                    ))}
                  <Box my={2}>
                    <Button
                      color="primary"
                      onClick={() => {
                        arrayHelpers.push(defaultClasses[0]);
                      }}
                    >
                      + Add new class
                    </Button>
                  </Box>
                </div>
              )}
            />
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  fullWidth
                  loading={loading || loadingExisting}
                  mt={2}
                >
                  Next
                </Button>
              </Grid>
            </Grid>
          </form>
        );
      }}
    </Formik>
  );
}
