import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { psheStatus } from 'src/config';
import _ from 'lodash';
import { searchSchools } from 'src/modules/school/api/schoolApis';

export default function SchoolDetails(props) {
  const [loading, setLoading] = React.useState(false);

  const { schools, user } = props;

  const getSchools = async search => {
    const filter = {
      schoolName: search
    };
    const res = await searchSchools(filter, true);
    return res;
  };

  return (
    <Box mt={4}>
      <Form
        initialValues={{
          school: '',
          schoolName: '',
          location: '',
          details: {
            psheStatus: ''
          },
          createNew: schools && schools.length > 0 ? false : true
        }}
        enableReinitialize={true}
        validationSchema={Yup.object().shape({
          // schoolName: Yup.string()
          //   .required('School name is required!')
          //   .when('school', {
          //     is: value => value == '',
          //     then: Yup.string().required('School name is required!')
          //   })
          // location: Yup.string().required('Location is required!'),
          // psheStatus: Yup.string().required('PSHE Status is required!')
        })}
        onSubmit={async (values, form) => {
          let data = { ...values };
          if (data.location && !_.isEmpty(data.location)) {
            data.schoolAddresses = [data.location];
            data = _.omit(data, ['location']);
          }
          // if (data.school && data.school !== '') {
          //   data.school = schools.filter(item => item.id === data.school)[0];
          // }
          setLoading(true);
          await props.onSubmit(data);
          setLoading(false);
        }}
      >
        {({ values, ...prop }) => {
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                prop.submitForm();
                return false;
              }}
              noValidate
            >
              {schools.length > 0 && (
                <React.Fragment>
                  {/* <Form.Field.Select
                    options={schools}
                    fullWidth
                    variant="standard"
                    name="school"
                    label="Choose your school"
                    optLabel="schoolName"
                    optValue="id"
                    disabled={schools.length == 1 || values.createNew}
                    onChange={val => {
                      if (val && val !== '') {
                        prop.setFieldValue('createNew', false);
                      }
                    }}
                  /> */}
                  <Form.Field.AutoComplete
                    multiple={false}
                    fullWidth
                    showAvatar={false}
                    options={[{}]}
                    variant="standard"
                    remoteMethod={getSchools}
                    name="school"
                    label="Choose your School"
                    optLabel="schoolName"
                    optValue="id"
                  />
                  <Box mt={2}>
                    <Form.Field.Checkbox
                      name="createNew"
                      label="Couldn't find your school in the list? Click here to create a new school!"
                      onChange={val => {
                        if (val === true) {
                          prop.setFieldValue('school', '');
                        }
                      }}
                    />
                  </Box>
                </React.Fragment>
              )}
              {values.createNew && (
                <React.Fragment>
                  <Form.Field.Input
                    fullWidth
                    variant="standard"
                    name="schoolName"
                    label="School Name"
                  />
                  <Form.Field.LocationAutoComplete
                    fullWidth
                    name="location"
                    label="Location"
                  />
                  <Form.Field.Select
                    options={psheStatus}
                    fullWidth
                    variant="standard"
                    name="details.psheStatus"
                    label="PSHE Current Status"
                  />
                </React.Fragment>
              )}

              <Box mt={2}>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  loading={loading}
                  fullWidth
                >
                  Next
                </Button>
              </Box>
            </form>
          );
        }}
      </Form>
    </Box>
  );
}
