import React, { useRef } from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { Formik, Field, FieldArray } from 'formik';
import { cacheTest, checkEmailUnique } from 'src/utils/validation';
import { setJoyRide } from 'src/utils/storageUtils';

const colleagues = [
  {
    workEmail: ''
  }
];

export default function InviteColleagues(props) {
  const [loading, setLoading] = React.useState(false);

  const emailUniqueTest = useRef(cacheTest(checkEmailUnique));

  return (
    <Formik
      initialValues={{ colleagues }}
      validateOnBlur={true}
      validateOnChange={false}
      validationSchema={Yup.object().shape({
        colleagues: Yup.array().of(
          Yup.object().shape({
            workEmail: Yup.string()
              .required('Email is required!')
              .email('Please enter a valid email address.')
              .test(
                'username-backend-validation',
                'This email is already taken!',
                emailUniqueTest.current
              )
          })
        )
      })}
      onSubmit={async values => {
        setLoading(true);
        const data = values;
        setJoyRide();
        // data.colleagues.map((item, index) => {
        //   data.colleagues[index].login = {
        //     username: item.workEmail
        //   };
        // });
        await props.onSubmit(values);
        setLoading(false);
      }}
    >
      {({ values, ...prop }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            prop.submitForm();
            return false;
          }}
          noValidate
        >
          <Box mt={2}>
            <Typography>
              <b>Invite your colleagues to get maximum value.</b>
            </Typography>
          </Box>
          <FieldArray
            name="colleagues"
            render={arrayHelpers => (
              <div>
                {values.colleagues &&
                  values.colleagues.length > 0 &&
                  values.colleagues.map((obj, index) => (
                    <React.Fragment key={`class-${index}-${obj.day}`}>
                      <Grid container spacing={2} alignItems="flex-end">
                        <Grid item xs={11}>
                          <Form.Field.Input
                            fullWidth
                            variant="standard"
                            name={`colleagues[${index}].workEmail`}
                            label="Add Teacher Email Address"
                          />
                        </Grid>
                        <Grid item xs={1}>
                          {values.colleagues.length > 1 && (
                            <Button
                              iconButton={true}
                              onClick={() => {
                                arrayHelpers.remove(index);
                              }}
                            >
                              <CloseIcon />
                            </Button>
                          )}
                        </Grid>
                      </Grid>
                    </React.Fragment>
                  ))}
                <Box my={2}>
                  <Button
                    color="primary"
                    onClick={() => {
                      arrayHelpers.push(colleagues[0]);
                    }}
                  >
                    + Add new colleague
                  </Button>
                </Box>
              </div>
            )}
          />
          <Box mt={2}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  fullWidth
                  loading={loading}
                  mt={2}
                >
                  Next
                </Button>
              </Grid>
              <Grid item xs={12} container justify="flex-end">
                <Button color="default" onClick={props.onSkip}>
                  Skip
                </Button>
              </Grid>
            </Grid>
          </Box>
        </form>
      )}
    </Formik>
  );
}
