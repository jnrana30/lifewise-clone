import React from 'react';
import { Box, Typography, Grid } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { userTitles, jobRoleTypes } from 'src/config';

export default function UserDetails({
  roles,
  roleTypes,
  onSubmit,
  registerData
}) {
  const [loading, setLoading] = React.useState(false);
  return (
    <React.Fragment>
      <Form
        initialValues={{
          title: registerData?.profile?.title
            ? registerData?.profile?.title
            : '',
          firstName: registerData?.profile?.firstName
            ? registerData?.profile?.firstName
            : '',
          lastName: registerData?.profile?.lastName
            ? registerData?.profile?.lastName
            : '',
          jobTitle: registerData?.profile?.jobTitle
            ? registerData?.profile?.jobTitle
            : ''
        }}
        validationSchema={Yup.object().shape({
          title: Yup.string().required('Title is required!'),
          firstName: Yup.string().required('First Name is required!'),
          lastName: Yup.string().required('Last Name is required!'),
          jobTitle: Yup.string().required('Job Role is required!')
        })}
        onSubmit={async (values, form) => {
          setLoading(true);
          try {
            values.login = {};
            await onSubmit(values);
            setLoading(false);
          } catch (error) {
            setLoading(false);
          }
          form.prevent;
        }}
        enableReinitialize={true}
      >
        {props => {
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                props.submitForm();
                return false;
              }}
              noValidate
            >
              <Grid container spacing={2}>
                <Grid item xs={4}>
                  <Form.Field.Select
                    options={userTitles}
                    fullWidth
                    variant="standard"
                    name="title"
                    label="Title"
                  />
                </Grid>
                <Grid item xs={8} ml={2}>
                  <Form.Field.Input
                    fullWidth
                    variant="standard"
                    name="firstName"
                    label="First Name"
                  />
                </Grid>
              </Grid>
              <Form.Field.Input
                fullWidth
                variant="standard"
                name="lastName"
                label="Last Name"
              />
              <Form.Field.Select
                options={roleTypes}
                fullWidth
                variant="standard"
                name="jobTitle"
                label="Job Role Type"
                optLabel="name"
                optValue="id"
              />
              {/* <Form.Field.Phone
                fullWidth
                variant="standard"
                name="mobileNumber"
                label="Phone number"
              /> */}
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  size="large"
                  fullWidth
                  loading={loading}
                >
                  Next
                </Button>
              </Box>
            </form>
          );
        }}
      </Form>
    </React.Fragment>
  );
}
