import React from 'react';
import {
  Grid,
  List,
  ListItem,
  Card,
  Box,
  CardContent,
  Typography,
  LinearProgress,
  Button
} from '@material-ui/core';
import Link from 'next/link';
import { Avatar } from 'src/components/shared';
import { makeStyles } from '@material-ui/core/styles';
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
  columns: {
    display: 'flex'
  },
  avatar: {
    width: theme.spacing(8),
    height: theme.spacing(8)
  },
  progressBar: {
    height: 10,
    borderRadius: 6
  }
}));

function ProfileDetails({ user, setDialogOpen, setDrawerOpen }) {
  const classes = useStyles();

  return (
    <Box mb={3}>
      <Card pa={4}>
        <CardContent>
          <Box mb={2}>
            <Typography variant="h6" color="primary">
              Profile Details
            </Typography>
          </Box>
          <div className={classes.columns}>
            <Avatar name={user?.profile?.firstName} size={64} />
            <Box pl={2}>
              <List dense={true}>
                <ListItem>
                  <Typography variant="body1">
                    <b>
                      {user?.profile?.firstName} {user?.profile?.lastName}
                    </b>
                  </Typography>
                </ListItem>
                <ListItem>
                  <Typography variant="body2">
                    {user?.profile?.workEmail}
                  </Typography>
                </ListItem>
                {user?.profile?.school && !_.isEmpty(user?.profile?.school) && (
                  <ListItem>
                    <Typography variant="body2">
                      {user?.profile?.school?.schoolName}
                    </Typography>
                  </ListItem>
                )}
                <ListItem>
                  <Typography variant="body2">
                    {user?.profile?.role?.name}
                  </Typography>
                </ListItem>
              </List>
            </Box>
          </div>
          <Button
            onClick={() => {
              setDialogOpen(true);
            }}
          >
            Change Password
          </Button>

          <Button
            onClick={() => {
              setDrawerOpen(true);
            }}
          >
            Edit Profile
          </Button>
          {/* <Link  href={`/profile/steps`}>
            <Box my={2}>
              <LinearProgress
                className={classes.progressBar}
                color="primary"
                variant="determinate"
                value={70}
              />
              <Typography align="right" color="textSecondary" variant="body2">
                4/7 set up
              </Typography>
            </Box>
          </Link> */}
        </CardContent>
      </Card>
    </Box>
  );
}

export default ProfileDetails;
