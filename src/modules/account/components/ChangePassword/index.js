import React from 'react';
import { Form, Button, Dialog } from 'src/components/shared';
import {
  Box,
  List,
  ListItem,
  ListItemIcon,
  ListItemText
} from '@material-ui/core';
import { green, grey } from '@material-ui/core/colors';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import * as Yup from 'yup';
// patchProfile
import toast from 'src/utils/toast';

function ChangePassword({ user, visible, onClose, patchProfile }) {
  const [loading, setLoading] = React.useState(false);

  const lowerCaseRegex = new RegExp('^(?=.*[a-z])');
  const upperCaseRegex = new RegExp('^(?=.*[A-Z])');
  const numberRegex = new RegExp('^(?=.*[0-9])');
  // const specialCharRegex = new RegExp('^(?=.*[!@#$%^&*])');
  const specialCharRegex = new RegExp(/[!"#$%&'()*+,-./:;<=>?@[\\\]^_`{|}~]/);
  const eightChatRegex = new RegExp('^(?=.{8,})');

  return (
    <div>
      <Dialog open={visible} title="Change Password" onClose={onClose}>
        <Form
          initialValues={{
            password: '',
            confPassword: ''
          }}
          validationSchema={Yup.object().shape({
            password: Yup.string()
              .required('Password is required!')
              .min(8, 'Password must be at least 8 characters long!'),
            // .matches(
            //   /^(?=.*[A-Za-z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/,
            //   'Password must contain 8 Characters, One Uppercase, One Lowercase, One Number and one Special Character'
            // ),
            confPassword: Yup.string()
              .required('Confirm Password is required!')
              .oneOf([Yup.ref('password'), null], 'Passwords must match')
          })}
          onSubmit={async (values, form) => {
            setLoading(true);
            try {
              const data = {
                login: values
              };
              await patchProfile(data);
              toast.success('Password changed successfully!');
              onClose();
            } catch (error) {
              console.log('error : ', error);
              toast.error(error?.error);
            }
            setLoading(false);
          }}
        >
          {props => {
            return (
              <form
                onSubmit={e => {
                  e.preventDefault();
                  props.submitForm();
                  return false;
                }}
                id="course-form"
                noValidate
              >
                <Form.Field.Input
                  fullWidth
                  variant="outlined"
                  name="password"
                  type="password"
                  label="Enter your new password"
                />

                {/* <List>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: eightChatRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="At least 8 characters long" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: lowerCaseRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="One lowercase character" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: upperCaseRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="One uppercase character" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: numberRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="One numeric character" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: specialCharRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="One special character" />
                  </ListItem>
                </List> */}

                <Form.Field.Input
                  fullWidth
                  variant="outlined"
                  type="password"
                  name="confPassword"
                  label="Confirm Password"
                />

                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    fullWidth
                    loading={loading}
                    mt={2}
                  >
                    Submit
                  </Button>
                </Box>
              </form>
            );
          }}
        </Form>
      </Dialog>
    </div>
  );
}

export default ChangePassword;
