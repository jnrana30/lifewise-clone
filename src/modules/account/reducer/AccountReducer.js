import * as types from '../actions/accountTypes';

const initialState = {
  teachers: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0
    },
    filters: {
      role: '[1, 8]'
    }
  },
  employees: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0
    },
    filters: {
      role: '[6, 7]'
    }
  },
  students: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0
    },
    filters: {
      role: 5
    }
  }
};

const AccountReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.FETCH_STUDENTS:
      return {
        ...state,
        students: {
          ...state.students,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.students.paging,
            ...action.paging
          }
        }
      };

    case types.SET_STUDENTS:
      return {
        ...state,
        students: {
          ...state.students,
          loading: false,
          data: action.data.profiles,
          paging: {
            ...state.students.paging,
            records: action.data.records
          }
        }
      };

    case types.FETCH_TEACHERS:
      return {
        ...state,
        teachers: {
          ...state.teachers,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.teachers.paging,
            ...action.paging
          }
        }
      };

    case types.SET_TEACHERS:
      return {
        ...state,
        teachers: {
          ...state.teachers,
          loading: false,
          data: action.data.profiles,
          paging: {
            ...state.teachers.paging,
            records: action.data.records
          }
        }
      };

    case types.FETCH_EMPLOYEES:
      return {
        ...state,
        employees: {
          ...state.employees,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.employees.paging,
            ...action.paging
          }
        }
      };

    case types.SET_EMPLOYEES:
      return {
        ...state,
        employees: {
          ...state.employees,
          loading: false,
          data: action.data.profiles,
          paging: {
            ...state.employees.paging,
            records: action.data.records
          }
        }
      };

    default:
      return state;
  }
};
export default AccountReducer;
