import { takeLatest, all, fork, put, call, select } from 'redux-saga/effects';
import * as accountTypes from '../actions/accountTypes';
import * as accountActions from '../actions/accountActions';
import * as accountApis from '../api/accountApis';
import { setProfile } from 'src/modules/auth/actions/authActions';
import toast from 'src/utils/toast';
import _ from 'lodash';

export const getTeachers = state => state.account.teachers;
export const getMySchool = state => state.school.mySchool;

function* patchProfile({ data, resolve, reject }) {
  try {
    const res = yield call(accountApis.patchProfile, data);
    yield put(setProfile(res.profile));
    resolve(res);
  } catch (error) {
    toast.error(error?.message);
    reject(error);
  }
}

function* createUser({ data, resolve, reject }) {
  try {
    const school = yield select(getMySchool);
    if (school && school?.id && !data.schoolId) {
      data.schoolId = school?.id;
    }
    // if (data.workEmail && data.workEmail !== '') {
    //   data.login = {
    //     username: data.workEmail
    //   };
    // }
    // console.log('data : ', data);
    const res = yield call(accountApis.createUser, data);
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* patchUser({ data, userId, resolve, reject }) {
  try {
    const res = yield call(accountApis.patchUser, data, userId);
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* deleteUser({ userId, resolve, reject }) {
  try {
    const res = yield call(accountApis.deleteUser, userId);
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* fetchTeachers({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const users = yield call(accountApis.fetchUsers, filter);
    yield put(accountActions.setTeachers(users));
  } catch (error) {
    yield put(accountActions.setTeachers([]));
  }
}

function* fetchEmployees({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const users = yield call(accountApis.fetchUsers, filter);
    yield put(accountActions.setEmployees(users));
  } catch (error) {
    yield put(accountActions.setEmployees([]));
  }
}

function* fetchStudents({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const users = yield call(accountApis.fetchUsers, filter);
    yield put(accountActions.setStudents(users));
  } catch (error) {
    yield put(accountActions.setStudents([]));
  }
}

function* updateTeacherClasses({ teacherId, data, resolve, reject }) {
  try {
    const res = yield call(accountApis.updateTeacherClasses, teacherId, data);
    resolve(res);
  } catch (error) {
    console.log('error: ', error);
    reject(error);
  }
}

export function* watchSagas() {
  yield takeLatest(accountTypes.PATCH_PROFILE, patchProfile);
  yield takeLatest(accountTypes.CREATE_USER, createUser);
  yield takeLatest(accountTypes.PATCH_USER, patchUser);
  yield takeLatest(accountTypes.DELETE_USER, deleteUser);
  yield takeLatest(accountTypes.FETCH_TEACHERS, fetchTeachers);
  yield takeLatest(accountTypes.FETCH_EMPLOYEES, fetchEmployees);
  yield takeLatest(accountTypes.FETCH_STUDENTS, fetchStudents);
  yield takeLatest(accountTypes.UPDATE_TEACHER_CLASSES, updateTeacherClasses);
}

export default function* runSagas() {
  yield all([fork(watchSagas)]);
}
