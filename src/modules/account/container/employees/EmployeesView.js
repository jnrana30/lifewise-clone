import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';

import EmployeesFilter from '../../components/employees/EmployeesFilter';
import EmployeesList from '../../components/employees/EmployeesList';
import EmployeesForm from '../../components/employees/EmployeesForm';

import { employees } from '../../api/data';

function EmployeesView({ employees, ...props }) {
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [employee, setEmployee] = React.useState({});
  const [creatingUser, setCreatingUser] = React.useState(false);
  const { data, loading } = employees;
  let { filters, paging } = employees;

  React.useEffect(() => {
    fetchEmployees();
  }, []);

  React.useEffect(() => {
    if (drawerOpen === false) {
      setEmployee({});
    }
  }, [drawerOpen]);

  const fetchEmployees = filter => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    props.fetchEmployees(filters, paging);
  };

  const setFilter = filter => {
    fetchEmployees(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchEmployees();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchEmployees();
  };

  const onSubmit = async ({ status, ...data }) => {
    setCreatingUser(true);
    // removed the status if value is empty string
    let payload = { ...data };
    if (status) {
      payload = { ...payload, status };
    }
    try {
      if (employee?.id > 0) {
        data.login = {};
        await await props.patchUser(payload, employee.id);
      } else {
        await await props.createUser(payload);
      }
      setCreatingUser(false);
      fetchEmployees();
      setDrawerOpen(false);
    } catch (error) {
      setCreatingUser(false);
    }
  };

  const onEdit = async data => {
    await setEmployee(data);
    await setDrawerOpen(true);
  };

  return (
    <React.Fragment>
      <Header title="Employees">
        <EmployeesFilter
          setDrawerOpen={setDrawerOpen}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
        />
      </Header>

      <EmployeesList
        employees={data}
        loading={loading}
        paging={paging}
        onPaging={onPaging}
        onEdit={onEdit}
      />

      <Drawer
        open={drawerOpen}
        title={employee?.id > 0 ? 'Edit employee' : 'Add employee'}
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="employee-form"
        buttonLoading={creatingUser}
      >
        <Typography variant="body1" color="textPrimary">
          {employee?.id > 0 ? (
            <b>Edit employee details.</b>
          ) : (
            <b>Enter employee details.</b>
          )}
        </Typography>

        <EmployeesForm
          employee={employee}
          roleTypes={props.roleTypes}
          roles={props.roles}
          onSubmit={onSubmit}
          user={props.user}
        />
      </Drawer>
    </React.Fragment>
  );
}

export default EmployeesView;
