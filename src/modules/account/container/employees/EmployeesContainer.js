import React from 'react';
import { connect } from 'react-redux';
import EmployeesView from './EmployeesView';
import {
  fetchEmployees,
  createUser,
  patchUser
} from '../../actions/accountActions';

export const EmployeesContainer = props => {
  return <EmployeesView {...props} />;
};

const mapStateToProps = state => ({
  employees: state.account.employees,
  roleTypes: state.app.roleTypes,
  roles: state.app.roles,
  user: state.auth.user
});

const mapDispatchToProps = dispatch => ({
  fetchEmployees: (filter, paging) => dispatch(fetchEmployees(filter, paging)),
  createUser: data => {
    return new Promise((resolve, reject) => {
      dispatch(createUser(data, resolve, reject));
    });
  },
  patchUser: (data, userId) => {
    return new Promise((resolve, reject) => {
      dispatch(patchUser(data, userId, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(EmployeesContainer);
