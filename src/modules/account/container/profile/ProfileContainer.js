import React from 'react';
import { connect } from 'react-redux';
import ProfileView from './ProfileView';
import { patchProfile } from '../../actions/accountActions';
import {
  fetchMyClasses,
  patchClass
} from 'src/modules/school/actions/schoolActions';

export const ProfileContainer = props => {
  return <ProfileView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  roles: state.app.roles,
  roleTypes: state.app.roleTypes,
  myClasses: state.school.myClasses,
  mySchool: state.school.mySchool,
  schoolYears: state.app.schoolYears
});

const mapDispatchToProps = dispatch => ({
  fetchMyClasses: (filters, paging) =>
    dispatch(fetchMyClasses(filters, paging)),
  patchProfile: data => {
    return new Promise((resolve, reject) => {
      dispatch(patchProfile(data, resolve, reject));
    });
  },
  patchClass: (schoolId, classId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(patchClass(schoolId, classId, data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
