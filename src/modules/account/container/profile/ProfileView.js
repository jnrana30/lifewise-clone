import React from 'react';
import { Header } from 'src/components/App';
import {
  Grid,
  Box,
  Card,
  CardContent,
  Typography,
  Paper,
  Tab,
  Tabs
} from '@material-ui/core';
import { Drawer } from 'src/components/shared';
import ProfileDetails from '../../components/ProfileDetails';
import MyClasses from '../../components/classes/MyClasses';
import ChangePassword from '../../components/ChangePassword';
import EditProfileForm from '../../components/profile/EditProfileForm';
import EditClassForm from 'src/modules/account/components/settings/classes/EditClassForm';

const CardItem = ({ title, subtitle, no }) => {
  return (
    <Card>
      <CardContent>
        <Typography variant="h3" color="primary">
          <b>{no}</b>
        </Typography>
        <Typography variant="h6">{title}</Typography>
        <Typography variant="subtitle2" color="textSecondary">
          {subtitle}
        </Typography>
      </CardContent>
    </Card>
  );
};

function ProfileView({ myClasses, user, ...props }) {
  const [activeTab, setActiveTab] = React.useState(0);
  const [dialogOpen, setDialogOpen] = React.useState(false);
  const [editClass, setEditClass] = React.useState({});
  const [updatingClass, setUpdatingClass] = React.useState(false);

  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [editingUser, setEditingUser] = React.useState(false);

  const { data, loading } = myClasses;
  let { filters, paging } = myClasses;

  React.useEffect(() => {
    fetchMyClasses();
  }, []);

  const fetchMyClasses = (filter, pagingData) => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
    }
    if (typeof filter !== 'pagingData' && !_.isEmpty(pagingData)) {
      paging = { ...paging, ...pagingData };
    }
    props.fetchMyClasses(filters, paging);
  };

  const setFilter = filter => {
    fetchMyClasses(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchMyClasses();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchMyClasses();
  };

  const onProfileSubmit = async data => {
    try {
      setEditingUser(true);
      const res = await props.patchProfile(data);
      setEditingUser(false);
      setDrawerOpen(false);
    } catch (error) {
      console.log('error : ', error);
      setEditingUser(false);
    }
  };

  const onEditClassSubmit = async data => {
    setUpdatingClass(true);
    try {
      const schoolId = props?.mySchool?.id;
      const res = await props.patchClass(schoolId, editClass.id, data);
      setUpdatingClass(false);
      fetchMyClasses();
      setEditClass({});
    } catch (error) {
      setUpdatingClass(false);
    }
  };

  const onEditClass = data => {
    setEditClass(data);
  };

  return (
    <React.Fragment>
      <Header title="User Profile" />
      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={4} lg={3}>
          <ProfileDetails
            user={user}
            setDialogOpen={setDialogOpen}
            setDrawerOpen={setDrawerOpen}
          />
          {/* <NotificationForm /> */}
        </Grid>

        <Grid item xs={12} sm={12} md={8} lg={9}>
          {/* <Box mb={3}>
            <Grid container spacing={3}>
              <Grid item lg={4} md={4} xs={12}>
                <CardItem no="26" title="Lesson played" subtitle="This week" />
              </Grid>
              <Grid item lg={4} md={4} xs={12}>
                <CardItem
                  no="369"
                  title="Lesson played"
                  subtitle="This month"
                />
              </Grid>
              <Grid item lg={4} md={4} xs={12}>
                <CardItem
                  no="36%"
                  title="Lesson completed"
                  subtitle="Academic year 2021"
                />
              </Grid>
            </Grid>
          </Box> */}

          <Box mb={3}>
            <Paper>
              <Tabs
                value={activeTab}
                indicatorColor="primary"
                textColor="primary"
                onChange={(e, val) => {
                  setActiveTab(val);
                }}
              >
                <Tab label="My Classes" />
                {/* <Tab label="Recent Labels" /> */}
              </Tabs>
            </Paper>
          </Box>
          <MyClasses
            classes={data}
            loading={loading}
            paging={paging}
            onPaging={onPaging}
            onEdit={onEditClass}
          />
        </Grid>
      </Grid>
      <ChangePassword
        user={user}
        visible={dialogOpen}
        patchProfile={props.patchProfile}
        onClose={() => {
          setDialogOpen(false);
        }}
      />

      <Drawer
        open={drawerOpen}
        title="Edit profile"
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="edit-profile-form"
        buttonLoading={editingUser}
      >
        <Typography variant="body1" color="textPrimary">
          <b>Edit your profile</b>
        </Typography>
        <EditProfileForm
          profile={user.profile}
          {...props}
          onSubmit={onProfileSubmit}
        />
      </Drawer>

      <Drawer
        open={!!editClass?.id}
        title="Edit class"
        onClose={() => {
          setEditClass({});
        }}
        form="class-form"
        buttonLoading={updatingClass}
      >
        <Typography>
          <b>Update class information below.</b>
        </Typography>
        <EditClassForm
          onSubmit={onEditClassSubmit}
          editClass={editClass}
          schoolYears={props.schoolYears}
        />
      </Drawer>
    </React.Fragment>
  );
}

export default ProfileView;
