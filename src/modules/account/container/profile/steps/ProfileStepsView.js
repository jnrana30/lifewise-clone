import React, { useState } from 'react';
import { Header } from '../../../../../components/App';
import {Grid, Typography} from '@material-ui/core';
import ProfileDetails from '../../../components/ProfileDetails';
import { ProfileStepsDetails } from '../../../components/ProfileStepsDetials';
import NotificationForm from '../../../components/NotificationForm';
import ChangePassword from '../../../components/ChangePassword';
import Link from "next/link";


const ProfileStepsView = ({ user, ...props }) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <React.Fragment>
      <Header title="User Profile" />
      <Typography
        color="textSecondary"
        gutterBottom
      >
        <Link href="/profile">
          <Link href="/profile" variant="body1">
            Back
          </Link>
        </Link>
      </Typography>

      <Grid container spacing={3}>
        <Grid item xs={12} sm={12} md={4} lg={3}>
          <ProfileDetails user={user} setDialogOpen={setDialogOpen} />
          <NotificationForm />
        </Grid>
        <Grid item xs={12} sm={12} md={8} lg={9}>
          <ProfileStepsDetails user={user} />
        </Grid>
      </Grid>
      <ChangePassword
        user={user}
        visible={dialogOpen}
        patchProfile={props.patchProfile}
        onClose={() => {
          setDialogOpen(false);
        }}
      />
    </React.Fragment>
  )
};

export default ProfileStepsView;
