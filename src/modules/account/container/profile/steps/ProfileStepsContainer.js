import React from 'react';
import { connect } from 'react-redux';
import { patchProfile } from '../../../actions/accountActions';
import ProfileStepsView from './ProfileStepsView'

export const ProfileStepsContainer = props => {
  return <ProfileStepsView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user
});

const mapDispatchToProps = dispatch => ({
  patchProfile: data => {
    return new Promise((resolve, reject) => {
      dispatch(patchProfile(data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileStepsContainer);
