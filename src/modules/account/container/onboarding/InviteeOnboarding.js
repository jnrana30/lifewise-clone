import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from 'src/components/shared/Stepper';

import {
  UserDetails,
  SchoolDetails,
  ClassDetails,
  InviteColleagues
} from '../../components/onboarding';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    minWidth: 800,
    marginTop: 40,
    [theme.breakpoints.down('xs')]: {
      minWidth: 0
    }
  },
  form: {
    maxWidth: 444,
    margin: '0 auto'
  }
}));

const AdminOnboarding = () => {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);

  const steps = ['User Details', 'Assigned Classes', 'Invite Colleagues'];

  const onSubmit = async data => {
    console.log('onSubmit : ', data);
    if (activeStep <= 3) {
      setActiveStep(activeStep + 1);
    }
  };

  return (
    <div className={classes.root}>
      <Stepper steps={steps} activeStep={activeStep} />
      <div className={classes.form}>
        {activeStep === 0 && <UserDetails onSubmit={onSubmit} />}
        {activeStep === 1 && <ClassDetails onSubmit={onSubmit} />}
        {activeStep === 2 && <InviteColleagues onSubmit={onSubmit} />}
      </div>
    </div>
  );
};

export default AdminOnboarding;
