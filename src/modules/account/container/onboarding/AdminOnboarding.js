import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import _ from 'lodash';
import { useRouter } from 'next/router';
import Stepper from 'src/components/shared/Stepper';
import {
  UserDetails,
  SchoolDetails,
  ClassDetails,
  InviteColleagues
} from '../../components/onboarding';
import { searchSchool } from 'src/modules/app/api/appApis';
import { setJoyRide } from 'src/utils/storageUtils';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    minWidth: 800,
    marginTop: 40,
    [theme.breakpoints.down('xs')]: {
      minWidth: 0
    }
  },
  form: {
    maxWidth: 444,
    margin: '0 auto'
  }
}));

const AdminOnboarding = props => {
  const classes = useStyles();
  const router = useRouter();
  const [activeStep, setActiveStep] = React.useState(0);
  const [schoolId, setSchoolId] = React.useState('');
  const [schools, setSchools] = React.useState([]);
  const [steps, setSteps] = React.useState([]);
  const [userId, setUserId] = React.useState(0);

  React.useEffect(() => {
    if (steps.length > 0) {
      if (router?.query?.step === 'class-details') {
        setActiveStep(steps.length === 3 ? 1 : 2);
      }
    }
  }, [steps]);

  React.useEffect(() => {
    if (!_.isEmpty(props?.registerData)) {
      const { matchingSchools, profile } = props?.registerData;
      setUserId(profile?.id);
      if (!matchingSchools || typeof matchingSchools == 'undefined') {
        setSteps(['User Details', 'Class Details', 'Invite Colleagues']);
      } else {
        setSteps([
          'User Details',
          'School Details',
          'Class Details',
          'Invite Colleagues'
        ]);
        if (Array.isArray(matchingSchools) && matchingSchools.length > 0) {
          setSchools(matchingSchools);
        } else {
          setSchools([]);
        }
      }
    } else {
      // router.push('/auth/login');
    }
  }, [props.registerData]);

  React.useEffect(() => {
    if (props.mySchool?.id !== '') {
      setSchoolId(props.mySchool?.id);
    }
  }, [props.mySchool]);

  const onUserDetailsSubmit = async data => {
    try {
      const res = await props.patchProfile(data);
      if (
        schoolId &&
        schoolId !== '' &&
        props?.registerData?.profile?.verificationRequired === true
      ) {
        props.logoutSilently();
        router.push({
          pathname: '/auth/verificationEmail',
          query: { email: props.registerData?.profile?.workEmail }
        });
      } else {
        setActiveStep(activeStep + 1);
      }
    } catch (error) {}
  };

  const onSchoolSubmit = async values => {
    if (values.createNew === true) {
      try {
        values = _.omit(values, ['createNew', 'school']);
        const res = await props.createSchool(values);
        const loginData = {
          username: props?.registerData?.login?.username,
          password: props?.registerData?.login?.password
        };
        await setSchoolId(res.id);
        const profileData = {
          role: props.registerData?.profile?.role?.id,
          schoolId: res.id
        };
        const profileRes = await props.patchProfile(profileData);
        if (
          (profileRes?.profile?.verificationRequired &&
            profileRes?.profile?.verificationRequired === true) ||
          props?.registerData?.profile?.verificationRequired === true
        ) {
          props.logoutSilently();
          router.push({
            pathname: '/auth/verificationEmail',
            query: { email: props.registerData?.profile?.workEmail }
          });
        } else {
          setActiveStep(activeStep + 1);
        }
      } catch (error) {}
    } else {
      try {
        const schoolId = values.school.id;
        setSchoolId(schoolId);

        if (values.school.schoolStatus === 'shadow') {
          const data = {
            role: props.registerData?.profile?.role?.id,
            schoolId
          };
          const profileRes = await props.patchProfile(data);
          if (
            (profileRes?.profile?.verificationRequired &&
              profileRes?.profile?.verificationRequired === true) ||
            props?.registerData?.profile?.verificationRequired === true
          ) {
            props.logoutSilently();
            router.push({
              pathname: '/auth/verificationEmail',
              query: { email: props.registerData?.profile?.workEmail }
            });
          } else {
            setActiveStep(activeStep + 1);
          }
        } else {
          const data = {
            status: 'completed',
            deleted: true
          };
          const profileRes = await props.patchProfile(data);
          props.logoutSilently();
          router.push('/auth/activeSchool');
        }
      } catch (error) {
        console.log('error : ', error);
      }
    }
  };

  const onClassSubmit = async (data, existingClasses) => {
    try {
      if (data.classes && data.classes.length > 0) {
        const res = await props.createClasses(schoolId, data);
        if (res?.classes && res?.classes.length > 0) {
          props.setHomeClass(res?.classes[0], {});
          if (props.registerData?.profile?.role.level !== 2) {
            existingClasses.add = [
              ...existingClasses.add,
              ...res?.classes.map(classItem => classItem.id)
            ];
          }
        }
      }
      if (existingClasses.add.length || existingClasses.remove.length) {
        await props.updateTeacherClasses(userId, existingClasses);
      }
      if (!data.classes || data.classes.length <= 0) {
        props.fetchHomeDefaultClass();
      }
      setActiveStep(activeStep + 1);
    } catch (error) {
      console.log('error : ', error);
    }
  };

  const onInviteUsers = async data => {
    if (data.colleagues.length > 0) {
      const promises = [];
      data.colleagues.map(async user => {
        user.schoolId = schoolId;
        promises.push(await props.createUser(user));
      });
      await Promise.all(promises)
        .then(res => {
          completeProfile();
          setJoyRide();
          router.replace('/');
        })
        .catch(err => {
          console.log(err);
        });
    }
  };

  let step = steps[activeStep];
  step = step?.replace(/\s+/g, '-').toLowerCase();

  const onSkip = () => {
    if (step == 'invite-colleagues') {
      completeProfile();
      setJoyRide();
      router.replace('/');
    } else {
      setActiveStep(activeStep + 1);
    }
  };

  const completeProfile = () => {
    props.patchProfile({
      status: 'completed'
    });
  };

  return (
    <div className={classes.root}>
      <Stepper steps={steps} activeStep={activeStep} />
      <div className={classes.form}>
        {step == 'user-details' && (
          <UserDetails
            {...props}
            onSubmit={onUserDetailsSubmit}
            registerData={props.registerData}
          />
        )}
        {step == 'school-details' && (
          <SchoolDetails
            {...props}
            onSubmit={onSchoolSubmit}
            schools={schools}
          />
        )}
        {step == 'class-details' && (
          <ClassDetails
            {...props}
            onSubmit={onClassSubmit}
            onSkip={onSkip}
            schoolId={schoolId}
            userId={userId}
          />
        )}
        {step == 'invite-colleagues' && (
          <InviteColleagues
            {...props}
            onSubmit={onInviteUsers}
            onSkip={onSkip}
          />
        )}
      </div>
    </div>
  );
};

export default AdminOnboarding;
