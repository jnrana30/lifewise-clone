import React from 'react';
import { connect } from 'react-redux';
import AdminOnboarding from './AdminOnboarding';
import InviteeOnboarding from './InviteeOnboarding';
import { useRouter } from 'next/router';
import {
  patchProfile,
  createUser,
  updateTeacherClasses
} from '../../actions/accountActions';
import {
  loginSilently,
  logoutSilently
} from 'src/modules/auth/actions/authActions';

import {
  createSchool,
  updateSchool,
  createClasses
} from '../../../school/actions/schoolActions';

import { getMySchool } from 'src/modules/school/actions/schoolActions';
import {
  setHomeClass,
  fetchHomeDefaultClass
} from 'src/modules/app/actions/appActions';
import { getRegisterData } from 'src/utils/storageUtils';

export const OnboardingContainer = props => {
  const router = useRouter();
  const registerData = getRegisterData();

  React.useEffect(() => {
    if (registerData?.profile?.role?.level <= 5) {
      props.getMySchool();
    }
  }, []);

  if (router.route == '/auth/onboarding/invite') {
    return <InviteeOnboarding {...props} registerData={registerData} />;
  }

  return <AdminOnboarding {...props} registerData={registerData} />;
};

const mapStateToProps = state => ({
  roles: state.app.roles,
  roleTypes: state.app.roleTypes,
  user: state.auth.user,
  schoolYears: state.app.schoolYears,
  mySchool: state.school.mySchool
});

const mapDispatchToProps = dispatch => ({
  fetchHomeDefaultClass: () => dispatch(fetchHomeDefaultClass()),
  getMySchool: () => dispatch(getMySchool()),
  logoutSilently: () => dispatch(logoutSilently()),
  setHomeClass: (classItem, search) =>
    dispatch(setHomeClass(classItem, search)),
  patchProfile: data => {
    return new Promise((resolve, reject) => {
      dispatch(patchProfile(data, resolve, reject));
    });
  },
  createSchool: data => {
    return new Promise((resolve, reject) => {
      dispatch(createSchool(data, resolve, reject));
    });
  },
  updateSchool: (schoolId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(updateSchool(schoolId, data, resolve, reject));
    });
  },
  createClasses: (schoolId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(createClasses(schoolId, data, resolve, reject));
    });
  },
  updateTeacherClasses: (teacherId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(updateTeacherClasses(teacherId, data, resolve, reject));
    });
  },
  createUser: data => {
    return new Promise((resolve, reject) => {
      dispatch(createUser(data, resolve, reject));
    });
  },
  loginSilently: data => {
    return new Promise((resolve, reject) => {
      dispatch(loginSilently(data, resolve, reject));
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OnboardingContainer);
