import React from 'react';
import { Header, FilterBar } from 'src/components/App';
import { Typography, Box, Tabs, Tab, Grid } from '@material-ui/core';
import {
  SchoolBilling,
  SchoolDetails,
  SchoolProfile,
  SchoolSubscriptions,
  SchoolLogo
} from '../../components/settings/schools/index';

function SchoolView() {
  const [activeTab, setActiveTab] = React.useState(0);
  return (
    <React.Fragment>
      <Header title="Schools Details" />
      <FilterBar>
        <Typography variant="h6">School Name</Typography>
      </FilterBar>

      <Tabs
        value={activeTab}
        indicatorColor="primary"
        textColor="primary"
        onChange={(e, val) => {
          setActiveTab(val);
        }}
      >
        <Tab label="Details" />
        <Tab label="Profile" />
        <Tab label="Billing Details" />
        <Tab label="Subscription" />
      </Tabs>

      <Box mt={2}>
        <Grid container>
          <Grid item xs={3}>
            <Box display="flex" mt={4} justifyContent="center">
              {/* <SchoolLogo /> */}
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box>
              {activeTab == 0 && <SchoolDetails />}
              {activeTab == 1 && <SchoolProfile />}
              {activeTab == 2 && <SchoolBilling />}
              {activeTab == 3 && <SchoolSubscriptions />}
            </Box>
          </Grid>
        </Grid>
      </Box>
    </React.Fragment>
  );
}

export default SchoolView;
