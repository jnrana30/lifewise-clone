import React from 'react';
import { connect } from 'react-redux';
import SchoolDetailsView from './SchoolDetailsView';

export const SchoolDetailsContainer = props => {
  return <SchoolDetailsView {...props} />;
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SchoolDetailsContainer);
