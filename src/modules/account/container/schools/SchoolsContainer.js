import React from 'react';
import { connect } from 'react-redux';
import SchoolsView from './SchoolsView';

export const SchoolsContainer = props => {
  return <SchoolsView {...props} />;
};

const mapStateToProps = state => ({});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(SchoolsContainer);
