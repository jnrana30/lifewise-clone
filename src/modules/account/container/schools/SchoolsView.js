import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';

import SchoolFilter from '../../components/settings/schools/SchoolFilter';
import SchoolList from '../../components/settings/schools/SchoolList';
import SchoolForm from '../../components/settings/schools/SchoolForm';

import { schools } from '../../api/data';

function SchoolView() {
  const [drawerOpen, setDrawerOpen] = React.useState(false);

  return (
    <React.Fragment>
      <Header title="Schools">
        <SchoolFilter setDrawerOpen={setDrawerOpen} />
      </Header>
      <SchoolList schools={schools} />

      <Drawer
        open={drawerOpen}
        title="Add School"
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="school-form"
      >
        <Typography variant="body1" color="textPrimary">
          <b>Enter school details.</b>
        </Typography>
        <SchoolForm />
      </Drawer>
    </React.Fragment>
  );
}

export default SchoolView;
