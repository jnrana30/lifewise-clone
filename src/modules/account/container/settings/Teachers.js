import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import TeachersFilter from '../../components/settings/teachers/TeachersFilter';
import TeachersList from '../../components/settings/teachers/TeachersList';
import TeachersForm from '../../components/settings/teachers/TeachersForm';
import EditTeacher from '../../components/settings/teachers/EditTeacher';

function Teachers({ teachers, ...props }) {
  const router = useRouter();
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [creatingUser, setCreatingUser] = React.useState(false);
  const [editingUser, setEditingUser] = React.useState(false);
  const [teacher, setTeacher] = React.useState({});
  const { data, loading } = teachers;
  let { filters, paging } = teachers;

  const isAdmin = props?.user?.profile?.role?.level > 5;

  React.useEffect(() => {
    console.log('router : ', router);
    if (router?.query?.invite && router?.query?.invite == 'true') {
      setDrawerOpen(true);
    }
  }, [router]);

  React.useEffect(() => {
    fetchTeachers();
  }, []);

  React.useEffect(() => {
    if (drawerOpen === false) {
      setTeacher({});
    }
  }, [drawerOpen]);

  const fetchTeachers = filter => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    props.fetchTeachers(filters, paging);
  };

  const setFilter = filter => {
    fetchTeachers(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchTeachers();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchTeachers();
  };

  const onEdit = async teacher => {
    await setTeacher(teacher);
    await setDrawerOpen(true);
  };

  const onDelete = async teacher => {
    try {
      if (teacher?.id !== '') {
        await props.deleteUser(teacher.id);
        fetchTeachers();
      }
    } catch (error) {
      console.log('error : ', error);
    }
  };

  const onTeachersSubmit = async data => {
    if (data.teachers.length > 0) {
      setCreatingUser(true);
      const promises = [];
      data.teachers.map(async user => {
        promises.push(await props.createUser(user));
      });
      await Promise.all(promises)
        .then(res => {
          setCreatingUser(false);
          fetchTeachers();
          setDrawerOpen(false);
        })
        .catch(err => {
          console.log(err);
          setCreatingUser(false);
        });
    }
  };

  const onEditSubmit = async (data, classes) => {
    try {
      setEditingUser(true);
      await props.patchUser(data, teacher.id);
      if (classes.add.length || classes.remove.length) {
        await props.updateTeacherClasses(teacher.id, classes);
      }
      fetchTeachers();
      setEditingUser(false);
      setDrawerOpen(false);
    } catch (error) {
      console.log('error : ', error);
      setEditingUser(false);
    }
  };

  return (
    <React.Fragment>
      <Header title="Teachers">
        <TeachersFilter
          setDrawerOpen={setDrawerOpen}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
          isAdmin={isAdmin}
        />
      </Header>
      <TeachersList
        teachers={data}
        loading={loading}
        paging={paging}
        onPaging={onPaging}
        onEdit={onEdit}
        onDelete={onDelete}
      />

      <Drawer
        open={drawerOpen}
        title={teacher?.id > 0 ? 'Edit teacher' : 'Invite a New Teacher'}
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="teachers-form"
        buttonLoading={creatingUser || editingUser}
      >
        <Typography variant="body1" color="textPrimary">
          {teacher?.id > 0 ? (
            <b>Edit teacher's details</b>
          ) : (
            <b>
              Inviting your colleagues to LifeWise is easy. Enter their school
              email address below, and we’ll send them an invitation with an
              introduction and instructions.
            </b>
          )}
        </Typography>
        {teacher?.id > 0 ? (
          <EditTeacher
            onSubmit={onEditSubmit}
            roleTypes={props.roleTypes}
            teacher={teacher}
          />
        ) : (
          <TeachersForm
            onSubmit={onTeachersSubmit}
            roleTypes={props.roleTypes}
          />
        )}
      </Drawer>
    </React.Fragment>
  );
}

export default Teachers;
