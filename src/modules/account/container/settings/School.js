import React from 'react';

import SchoolDetailsContainer from '../../../school/container/schools/SchoolDetailsContainer';

function School() {
  return <SchoolDetailsContainer />;
}

export default School;
