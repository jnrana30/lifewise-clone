import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';

import ClassFilter from '../../components/settings/classes/ClassFilter';
import ClassList from '../../components/settings/classes/ClassList';
import ClassForm from '../../components/settings/classes/ClassForm';
import EditClassForm from '../../components/settings/classes/EditClassForm';

function Classes({ classes, ...props }) {
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [isLoading, setIsLoading] = React.useState(false);
  const [editClass, setEditClass] = React.useState({});

  const { data, loading } = classes;
  let { filters, paging } = classes;

  React.useEffect(() => {
    fetchClasses({
      year: new Date().getFullYear().toString(),
      status: 'active'
    });
  }, []);

  const fetchClasses = filter => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    props.fetchClasses(filters, paging);
  };

  const setFilter = filter => {
    fetchClasses(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchClasses();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchClasses();
  };

  const onCreateClasses = async data => {
    setIsLoading(true);
    try {
      const schoolId = data?.school?.id
        ? data?.school?.id
        : props?.mySchool?.id;
      await props.createClasses(schoolId, data);
      setIsLoading(false);
      fetchClasses();
      setDrawerOpen(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  const onEditClass = async data => {
    setIsLoading(true);
    try {
      const schoolId =
        editClass?.schoolId && editClass?.schoolId !== ''
          ? editClass?.schoolId
          : props?.mySchool?.id;
      const res = await props.patchClass(schoolId, editClass.id, data);
      setIsLoading(false);
      fetchClasses();
      setDrawerOpen(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  const onEdit = data => {
    setEditClass(data);
    setDrawerOpen(true);
  };

  return (
    <React.Fragment>
      <Header title="Classes">
        <ClassFilter
          setDrawerOpen={setDrawerOpen}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
        />
      </Header>

      <ClassList
        classes={data}
        loading={loading}
        paging={paging}
        onPaging={onPaging}
        onEdit={onEdit}
      />

      <Drawer
        open={drawerOpen}
        title={editClass.id ? 'Edit Class' : 'Add Classes'}
        onClose={() => {
          setEditClass({});
          setDrawerOpen(false);
        }}
        form="class-form"
        buttonLoading={isLoading}
      >
        <Typography variant="body1" color="textPrimary">
          {editClass.id ? (
            <b>Update class information below.</b>
          ) : (
            <b>Add class name and associated year group.</b>
          )}
        </Typography>
        {editClass.id ? (
          <EditClassForm
            editClass={editClass}
            schoolYears={props.schoolYears}
            onSubmit={onEditClass}
          />
        ) : (
          <ClassForm
            schoolYears={props.schoolYears}
            onSubmit={onCreateClasses}
          />
        )}
      </Drawer>
    </React.Fragment>
  );
}

export default Classes;
