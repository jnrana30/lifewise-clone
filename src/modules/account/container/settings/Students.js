import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';
import _ from 'lodash';
import { getRole } from 'src/utils/helpers';

import StudentsFilter from '../../components/settings/students/StudentsFilter';
import StudentsList from '../../components/settings/students/StudentsList';
import StudentsForm from '../../components/settings/students/StudentsForm';
import EditStudent from '../../components/settings/students/EditStudent';

import {
  addStudentsToClass,
  getStudentClasses,
  addClassToStudents,
} from '../../../school/api/schoolApis';

function Students({ students, ...props }) {
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [student, setStudent] = React.useState({});
  const [formLoading, setFormLoading] = React.useState(false);
  const { data, loading } = students;
  let { filters, paging } = students;

  React.useEffect(() => {
    // const role = getRole();
    // console.log(role);
    fetchStudents();
  }, []);

  React.useEffect(() => {
    if (!drawerOpen) {
      setStudent({});
    }
  }, [drawerOpen]);

  const fetchStudents = filter => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    props.fetchStudents(filters, paging);
  };

  const setFilter = filter => {
    fetchStudents(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchStudents();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchStudents();
  };

  const onStudentEdit = async data => {
    try {
      setFormLoading(true);
      const studentClass = data.class;
      data = _.omit(data, 'class');
      await props.patchUser(data, student.id);
      if (studentClass) {
        await addClassToStudents(student.classes.map(c => c.id), studentClass.id, student.id);
      }
      fetchStudents();
      setFormLoading(false);
      setDrawerOpen(false);
    } catch (error) {
      console.log('error : ', error);
      setFormLoading(false);
    }
  };

  const onSubmit = async data => {
    console.log(data);
    try {
      setFormLoading(true);
      const schoolId =
        data?.school?.id && data?.school?.id !== ''
          ? data.school.id
          : props?.mySchool?.id;

      if (data.students && data.students.length) {
        for (const student of data.students) {
          await createStudent(student, schoolId);
        }
      }
      fetchStudents();
      setFormLoading(false);
      setDrawerOpen(false);
    } catch (error) {
      console.log(error);
      setFormLoading(false);
    }
  };

  const createStudent = async (student, schoolId) => {
    try {
      let studentClass = student.class;
      student = _.omit(student, 'class');
      student.schoolId = schoolId;
      student.role = 5;
      let user = await props.createUser(student);
      let studentId = user?.profile?.id;
      await addStudentsToClass(schoolId, studentClass.id, [
        studentId
      ]);
    } catch (error) {
      console.log(error);
      return error
    }
  };

  const onEdit = async data => {
    await setStudent(data);
    const schoolId = data?.schoolId ? data?.schoolId : props?.mySchool?.id;
    getStudentClasses(data.id, schoolId)
      .then(res => {
        data.classes = res?.classes ? res?.classes : [];
        setDrawerOpen(true);
      })
      .catch(err => {
        data.classes = [];
        setDrawerOpen(true);
      });
  };

  return (
    <React.Fragment>
      <Header title="Students">
        <StudentsFilter
          setDrawerOpen={setDrawerOpen}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
        />
      </Header>

      <StudentsList
        students={data}
        loading={loading}
        paging={paging}
        onPaging={onPaging}
        onEdit={onEdit}
      />
      <Drawer
        open={drawerOpen}
        title={student?.id ? 'Edit student' : 'Add new  student'}
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="student-form"
        buttonLoading={formLoading}
      >
        <Typography variant="body1" color="textPrimary">
          {student?.id ? (
            <b>Edit student details.</b>
          ) : (
            <b>Enter student details below to add a new student.</b>
          )}
        </Typography>
        {student?.id ? (
          <EditStudent
            onSubmit={onStudentEdit}
            student={student}
            schoolId={props?.mySchool?.id}
          />
        ) : (
          <StudentsForm onSubmit={onSubmit} schoolId={props?.mySchool?.id} />
        )}
      </Drawer>
    </React.Fragment>
  );
}

export default Students;
