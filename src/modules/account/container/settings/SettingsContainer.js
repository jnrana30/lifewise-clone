import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';

import Classes from './Classes';
import School from './School';
import Students from './Students';
import Teachers from './Teachers';

import {
  fetchTeachers,
  fetchStudents,
  createUser,
  patchUser,
  deleteUser,
  updateTeacherClasses
} from '../../actions/accountActions';

import {
  createClasses,
  fetchClasses,
  patchClass
} from '../../../school/actions/schoolActions';

export const SettingsContainer = props => {
  const router = useRouter();

  const Component = {
    teachers: (
      <Teachers
        teachers={props.teachers}
        fetchTeachers={props.fetchTeachers}
        createUser={props.createUser}
        patchUser={props.patchUser}
        roles={props.roles}
        roleTypes={props.roleTypes}
        deleteUser={props.deleteUser}
        updateTeacherClasses={props.updateTeacherClasses}
        user={props.user}
      />
    ),
    classes: (
      <Classes
        classes={props.classes}
        fetchClasses={props.fetchClasses}
        createClasses={props.createClasses}
        patchClass={props.patchClass}
        mySchool={props.mySchool}
        schoolYears={props.schoolYears}
      />
    ),
    students: (
      <Students
        students={props.students}
        fetchStudents={props.fetchStudents}
        createUser={props.createUser}
        patchUser={props.patchUser}
        mySchool={props.mySchool}
      />
    ),
    school: <School />
  };

  return <React.Fragment>{Component[router.query.page]}</React.Fragment>;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  teachers: state.account.teachers,
  students: state.account.students,
  roles: state.app.roles,
  roleTypes: state.app.roleTypes,
  mySchool: state.school.mySchool,
  schoolYears: state.app.schoolYears,
  classes: state.school.classes
});

const mapDispatchToProps = dispatch => ({
  fetchTeachers: (filter, paging) => dispatch(fetchTeachers(filter, paging)),
  fetchClasses: (filter, paging) => dispatch(fetchClasses(filter, paging)),
  fetchStudents: (filter, paging) => dispatch(fetchStudents(filter, paging)),
  createUser: data => {
    return new Promise((resolve, reject) => {
      dispatch(createUser(data, resolve, reject));
    });
  },
  createClasses: (schoolId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(createClasses(schoolId, data, resolve, reject));
    });
  },
  patchClass: (schoolId, classId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(patchClass(schoolId, classId, data, resolve, reject));
    });
  },
  patchUser: (data, userId) => {
    return new Promise((resolve, reject) => {
      dispatch(patchUser(data, userId, resolve, reject));
    });
  },
  deleteUser: userId => {
    return new Promise((resolve, reject) => {
      dispatch(deleteUser(userId, resolve, reject));
    });
  },
  updateTeacherClasses: (teacherId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(updateTeacherClasses(teacherId, data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsContainer);
