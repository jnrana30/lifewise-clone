import api from 'src/api/index';
import { convertObjectToQuerystring } from 'src/utils/helpers';
const _ = require('lodash');

export const patchProfile = data => {
  return api('/auth/profile', data, 'PATCH');
};

export const patchUser = (data, userId) => {
  return api(`/auth/profile?id=${userId}`, data, 'PATCH');
};

export const deleteUser = userId => {
  return api(
    `/auth/profile?id=${userId}`,
    {
      deleted: true
    },
    'PATCH'
  );
};

export const createUser = data => {
  return api(`/auth/create`, data, 'POST');
};

export const fetchUsers = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/auth/users?${filter}`, null, 'GET');
};

export const resendInvite = userId => {
  return api('/auth/resend-invite', { id: userId }, 'POST');
};

export const updateTeacherClasses = (teacherId, data) => {
  return api(`/schools/teacher/${teacherId}/classes`, data, 'PUT');
};
