import React, { useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Box,
  Typography,
  Chip,
  Avatar
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ReorderIcon from '@material-ui/icons/Reorder';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import {
  Form,
  Button,
  Dialog,
  Spinner,
  ContextMenu
} from 'src/components/shared';
import * as Yup from 'yup';
import images from 'src/config/images';
import { fetchLessons } from '../../api/coursesApis';
import s3Helper from 'src/utils/s3Helper';

const useStyles = makeStyles(theme => ({
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7)
  }
}));

const LessonBuilder = ({
  lessons,
  addLesson,
  updateLessonConfig,
  removeLesson
}) => {
  const classes = useStyles();
  const [localItems, setLocalItems] = useState([]);
  const [formLoading, setFormLoading] = useState(false);
  const [gridLoading, setGridLoading] = React.useState(0);

  React.useEffect(() => {
    if (lessons && typeof lessons !== 'undefined') {
      setLocalLessons(lessons);
    }
  }, [lessons]);

  const setLocalLessons = lessons => {
    let lessonsData = Object.values(lessons);
    lessonsData = lessonsData
      .map(item => ({
        ...item,
        displayOrder: item?.displayOrder ? item?.displayOrder : 0
      }))
      .sort((a, b) => {
        return a.displayOrder - b.displayOrder;
      });

    setLocalItems(lessonsData);
  };

  const searchLessons = async search => {
    const filter = {
      lessonName: search
    };
    const res = await fetchLessons(filter);
    return res?.lessons ? res?.lessons : [];
  };

  const [loading, setLoading] = useState(false);
  const [dialogOpen, setDialogOpen] = useState(false);

  const handleDragEnd = (result, provided) => {
    if (!result.destination) {
      return;
    }
    if (result.destination.index === result.source.index) {
      return;
    }
    const temp = [...localItems];
    const s = temp[result.source?.index];
    const d = temp[result.destination?.index];
    let newData = [];
    if (result.destination?.index < result.source.index) {
      newData = [
        ...temp.slice(0, result.destination?.index),
        s,
        ...temp.slice(result.destination?.index, result.source.index),
        ...temp.slice(result.source.index + 1)
      ];
    } else {
      newData = [
        ...temp.slice(0, result.source?.index),
        ...temp.slice(result.source?.index + 1, result.destination.index + 1),
        s,
        ...temp.slice(result.destination.index + 1)
      ];
    }
    setLocalItems(newData);
    updateOrder(newData);
  };

  const updateOrder = async lessons => {
    if (lessons && lessons.length) {
      await updateLessonConfig(lessons);
    }
  };

  const onDelete = async (lessonId, index) => {
    try {
      await removeLesson(lessonId);
      setLocalItems(localItems.filter(item => item.lessonId !== lessonId));
    } catch (error) {
      console.log('error');
      console.log(error);
    }
  };

  return (
    <React.Fragment>
      <TableContainer component={Paper}>
        <Table className="paddedTable" style={{ overflow: 'auto' }}>
          <colgroup>
            <col style={{ width: '5%' }} />
            <col style={{ width: '30%' }} />
            <col style={{ width: '40%' }} />
            <col style={{ width: '25%' }} />
          </colgroup>
          <TableHead>
            <TableRow>
              <TableCell align="left">
                <Button
                  iconButton={true}
                  onClick={() => {
                    setDialogOpen(true);
                  }}
                  style={{ padding: 0 }}
                >
                  <AddIcon />
                </Button>
              </TableCell>
              <TableCell>Lesson</TableCell>
              <TableCell>Description</TableCell>
              <TableCell>Tags</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          {Object.keys(localItems).length ? (
            <DragDropContext onDragEnd={handleDragEnd}>
              <Droppable droppableId="droppable" direction="vertical">
                {droppableProvided => (
                  <TableBody
                    ref={droppableProvided.innerRef}
                    {...droppableProvided.droppableProps}
                  >
                    {/* {Object.entries(localItems).map((lesson, index) => { */}
                    {localItems.map((lesson, index) => {
                      // const key = lesson[0];
                      // lesson = lesson[1];
                      const description = lesson?.content?.content?.data
                        ?.description
                        ? lesson?.content?.content?.data?.description
                        : '';
                      const icon = lesson?.content?.content?.data?.icon
                        ? s3Helper.getFileUrl(
                            lesson?.content?.content?.data?.icon
                          )
                        : images.app.lessonPlaceholder;
                      return (
                        <Draggable
                          key={lesson.id}
                          draggableId={lesson.id}
                          index={index}
                        >
                          {(draggableProvided, snapshot) => {
                            return (
                              <TableRow
                                ref={draggableProvided.innerRef}
                                {...draggableProvided.draggableProps}
                                style={{
                                  ...draggableProvided.draggableProps.style,
                                  background: snapshot.isDragging
                                    ? 'rgba(245,245,245, 0.75)'
                                    : 'none'
                                }}
                              >
                                <TableCell align="left">
                                  {gridLoading ? (
                                    <Spinner color="primary" />
                                  ) : (
                                    <div {...draggableProvided.dragHandleProps}>
                                      <ReorderIcon />
                                    </div>
                                  )}
                                </TableCell>
                                <TableCell>
                                  <Box display="flex" alignItems="center">
                                    <Avatar
                                      className={classes.avatar}
                                      src={icon}
                                      size={36}
                                    />
                                    <Box ml={2}>
                                      <Typography
                                        variant="body1"
                                        color="secondary"
                                      >
                                        <b>{lesson?.content?.lessonName}</b>
                                      </Typography>
                                    </Box>
                                  </Box>
                                </TableCell>
                                <TableCell>
                                  <Typography
                                    variant="body2"
                                    color="textPrimary"
                                  >
                                    {description}
                                  </Typography>
                                </TableCell>
                                <TableCell>
                                  {lesson?.content?.content?.tags?.length && (
                                    <div className={classes.tagsContainer}>
                                      {lesson?.content?.content?.tags.map(
                                        (tag, tagIndex) => {
                                          return (
                                            <Chip
                                              variant="outlined"
                                              key={`courseList-tags-${tagIndex}`}
                                              label={tag.tagName}
                                              style={{
                                                marginLeft: 4,
                                                marginBottom: 4
                                              }}
                                            />
                                          );
                                        }
                                      )}
                                    </div>
                                  )}
                                </TableCell>
                                <TableCell
                                  width="5%"
                                  className="flex-justify-content-end"
                                >
                                  <ContextMenu
                                    options={[
                                      {
                                        label: 'Remove',
                                        icon: <DeleteIcon />,
                                        confirm: true,
                                        confirmMessage: `Are you sure you want to remove ${lesson?.content?.lessonName} from the course?`,
                                        onClick: () => {
                                          onDelete(lesson.lessonId, index);
                                        }
                                      }
                                    ]}
                                  />
                                </TableCell>
                              </TableRow>
                            );
                          }}
                        </Draggable>
                      );
                    })}
                    {droppableProvided.placeholder}
                  </TableBody>
                )}
              </Droppable>
            </DragDropContext>
          ) : (
            <TableBody>
              <TableRow>
                <TableCell colSpan={4} align="center">
                  <Typography color="textSecondary">
                    No lessons found
                  </Typography>
                </TableCell>
              </TableRow>
            </TableBody>
          )}
        </Table>
      </TableContainer>
      <Dialog
        open={dialogOpen}
        title="Add a Lesson to the course."
        onClose={() => {
          setDialogOpen(false);
        }}
      >
        <Form
          initialValues={{
            lesson: {}
          }}
          validationSchema={Yup.object().shape({
            lesson: Yup.object()
              .nullable(false)
              .required('Please select a Lesson!')
              .test(
                'lesson-empty-check',
                'Please select a Lesson!',
                async lesson => {
                  return lesson?.id && lesson?.id !== '';
                }
              )
          })}
          onSubmit={async (values, form) => {
            try {
              setFormLoading(true);
              const response = await addLesson(values, {
                displayOrder: localItems.length + 1
              });
              // console.log('response : ', response);
              // setLocalLessons(response?.courseLessons);
              setFormLoading(false);
              setDialogOpen(false);
            } catch (error) {
              console.log('error : ', error);
              setFormLoading(false);
            }
          }}
        >
          {props => {
            return (
              <form
                onSubmit={e => {
                  e.preventDefault();
                  props.submitForm();
                  return false;
                }}
                id="course-form"
                noValidate
              >
                <Form.Field.AutoComplete
                  fullWidth
                  showAvatar={true}
                  options={[{}]}
                  remoteMethod={searchLessons}
                  variant="standard"
                  name="lesson"
                  label="Select a lesson"
                  optLabel="lessonName"
                  optValue="id"
                />

                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    fullWidth
                    loading={formLoading}
                    mt={2}
                  >
                    Submit
                  </Button>
                </Box>
              </form>
            );
          }}
        </Form>
      </Dialog>
    </React.Fragment>
  );
};

export default LessonBuilder;
