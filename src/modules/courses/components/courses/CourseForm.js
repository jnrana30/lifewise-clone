import React from 'react';
import { Box, Grid } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { courseStatusOptions, courseVisibilityOptions } from 'src/config';
import { getCategories, getTagsByType } from '../../api/coursesApis';

function CourseForm({ course, onSubmit, schoolYears }) {
  const [loading, setLoading] = React.useState(false);
  const isEdit = course && course?.id !== '';

  const searchTags = async (search, type) => {
    const filter = {
      search
    };
    const res = await getTagsByType(type, filter);
    return res?.tags ? res?.tags : [];
  };

  return (
    <Form
      initialValues={{
        data: {
          icon: course?.data?.icon ? course?.data?.icon : '',
          background: course?.data?.background ? course?.data?.background : '',
          description: course?.data?.description
            ? course?.data?.description
            : ''
        },
        courseName: course?.courseName ? course?.courseName : '',
        courseStatus: course?.courseStatus ? course?.courseStatus : 'active',
        courseVisibility: course?.courseVisibility
          ? course?.courseVisibility
          : 'public',
        yearGroup: course?.tags
          ? course?.tags.filter(tag => tag.tagType === 'yearGroup')
          : [],
        curriculum: course?.tags
          ? course?.tags.filter(tag => tag.tagType === 'curriculum')
          : []
      }}
      enableReinitialize={true}
      validationSchema={Yup.object().shape({
        courseName: Yup.string().required('Course name is required!'),
        data: Yup.object().shape({
          description: Yup.string().required('Please enter course description!')
        })
        // category: Yup.object().required('Please choose a category!')
      })}
      onSubmit={async (values, form) => {
        let data = { ...values };
        data.tags = [
          ...(data?.yearGroup?.length
            ? data?.yearGroup.map(tag => tag.id)
            : []),
          ...(data?.curriculum?.length
            ? data?.curriculum.map(tag => tag.id)
            : [])
        ];
        data.yearGroups = data?.yearGroup?.length
          ? data?.yearGroup.map(tag => tag.id)
          : [];
        data = _.omit(data, ['curriculum', 'yearGroup']);
        if (
          data.data.icon &&
          typeof data.data.icon === 'object' &&
          typeof data.data.icon?.key !== 'undefined' &&
          data.data.icon?.key !== ''
        ) {
          data.data.icon = data.data.icon.key;
        }
        if (
          data.data.background &&
          typeof data.data.background === 'object' &&
          typeof data.data.background?.key !== 'undefined' &&
          data.data.background?.key !== ''
        ) {
          data.data.background = data.data.background.key;
        }

        setLoading(true);
        onSubmit(data);
        setLoading(false);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="course-form"
            noValidate
          >
            <Grid container>
              {isEdit && (
                <Grid item xs={12} md={3}>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    mt={4}
                  >
                    <Form.Field.Avatar
                      name="data.icon"
                      label="Choose Avatar"
                      folder="courses/icons"
                    />
                  </Box>
                </Grid>
              )}
              <Grid item xs={12} md={isEdit ? 6 : 12}>
                <Form.Field.Input
                  fullWidth
                  name="courseName"
                  label="Course Name"
                />
                <Form.Field.Input
                  fullWidth
                  multiline
                  name="data.description"
                  label="Course Description"
                />
                {isEdit && (
                  <React.Fragment>
                    <Form.Field.File
                      name="data.background"
                      folder="courses/backgrounds"
                      label="Background Image"
                    />
                    <Form.Field.Select
                      options={courseStatusOptions}
                      fullWidth
                      variant="standard"
                      name="courseStatus"
                      label="Status"
                    />
                    <Form.Field.Select
                      options={courseVisibilityOptions}
                      fullWidth
                      variant="standard"
                      name="courseVisibility"
                      label="Course Visibility"
                    />
                  </React.Fragment>
                )}
                <Form.Field.AutoComplete
                  multiple={true}
                  fullWidth
                  showAvatar={false}
                  options={[]}
                  variant="standard"
                  remoteMethod={val => searchTags(val, 'yearGroup')}
                  name="yearGroup"
                  label="Year Group"
                  optLabel="tagName"
                  optValue="id"
                />
                <Form.Field.AutoComplete
                  multiple={true}
                  fullWidth
                  options={[]}
                  remoteMethod={val => searchTags(val, 'curriculum')}
                  variant="standard"
                  name="curriculum"
                  label="Curriculum"
                  optLabel="tagName"
                  optValue="id"
                />
                {isEdit && (
                  <React.Fragment>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        loading={loading}
                        mt={2}
                      >
                        Submit
                      </Button>
                    </Box>
                  </React.Fragment>
                )}
              </Grid>
            </Grid>
          </form>
        );
      }}
    </Form>
  );
}

export default CourseForm;
