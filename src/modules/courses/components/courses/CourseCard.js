import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  CardActions,
  IconButton,
  Typography,
  Chip,
  Menu,
  MenuItem,
  Fade,
  Box,
  Tooltip,
} from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
// import Image from 'next/image'
import { useRouter } from 'next/router';
import s3Helper from 'src/utils/s3Helper';
import images from 'src/config/images';
import {
  CardGrid,
  CardMediaGrid
} from '../../../../components/shared/CardGrid';
import { Button, Avatar, ContextMenu } from 'src/components/shared';
import { Tag } from 'src/components/App';

const useStyles = makeStyles(theme => ({
  expand: {
    marginLeft: 'auto'
  },
  tagsContainer: {
    '& > *': {
      margin: theme.spacing(0.5)
    }
  },
  courseTitleWrap: {
    position: 'absolute',
    top: 14,
    padding: '10px 20px',
    height: 'calc(100% - 14px)',
    width: '100%',
    background: 'linear-gradient(0deg, #00000080, #00000000)'
  },
  cardContent: {
    paddingBottom: 0,
  },
  cardContentSubHeading: {
    lineHeight: 1.2,
    fontSize: 14,
    display: '-webkit-box',
    '-webkit-line-clamp': 2,
    '-webkit-box-orient': 'vertical',
    overflow: 'hidden',
    color: '#3A3A3A',
  },
  courseIconWrap: {
    minWidth: 80,
    minHeight: 80,
    height: 80,
    width: 80,
    borderRadius: 80,
    backgroundColor: '#ffffff'
  },
  courseIcon: {
    width: 80,
    height: 80,
    borderRadius: 80
  },
  courseTitle: {
    color: '#ffffff'
  },
  chipRoot: {
    fontSize: 12,
    height: 29,
    '& .MuiChip-label': {
      paddingLeft: 8,
      paddingRight: 8
    }
  },
  cardActions: {
    paddingTop: 0,
    paddingBottom: 0,
    display: 'flex',
    justifyContent: 'space-between'
  },
}));

export default function CourseCard({ course, addToClass, ...props }) {
  const classes = useStyles();
  const router = useRouter();
  const [anchorEl, setAnchorEl] = useState(null);
  const [courseTags, setCourseTags] = useState([]);
  const [sellAllTags, setSellAllTags] = useState(false);
  const [nonSelectedTag, setNonSelectedTag] = useState([]);
  const open = Boolean(anchorEl);

  useEffect(() => {
    if (course?.tags.length > 3) {
      setCourseTags(course?.tags.slice(0, 2));
      setNonSelectedTag(
        course?.tags.slice(2)
      );
    } else {
      setCourseTags(course?.tags);
    }
  }, [course.tags]);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleSeeMoreClick = event => {
    setSellAllTags(true);
    setCourseTags(course?.tags);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const icon =
    course?.data?.icon && course?.data?.icon !== ''
      ? s3Helper.getFileUrl(course?.data?.icon)
      : images.app.coursePlaceholder;

  const background =
    course?.data?.background && course?.data?.background !== ''
      ? s3Helper.getFileUrl(course?.data?.background)
      : images.app.courseBanner1;

  return (
    <CardGrid>
      <CardActionArea
        onClick={() => {
          router.push(`/courses/${course.id}`);
        }}
      >
        <CardMediaGrid
          image={icon}
          title={course?.courseName}
          style={{ backgroundSize: 'contain' }}
        />
        {/* <Box
          display="flex"
          alignItems="center"
          className={classes.courseTitleWrap}
        >
          <Box flex={1} display="flex" flexGrow={1}>
            <Typography variant="h6" className={classes.courseTitle}>
              <b>{course?.courseName}</b>
            </Typography>
          </Box>
        </Box> */}
      </CardActionArea>
      <CardContent className={classes.cardContent}>
        <Tooltip title={course?.data?.description || ''} arrow>
          <Typography variant="body2" component="p" className={classes.cardContentSubHeading}>
            {course?.data?.description}
          </Typography>
        </Tooltip>
      </CardContent>

      <CardActions className={classes.cardActions} disableSpacing>
        <div>
          {Boolean(courseTags.length) && (
            <div className={classes.tagsContainer}>
              {courseTags.map((tag, index) => {
                return (
                  <Tag
                    tag={tag}
                    key={`lesson-${props.lessonIndex}-tags-${index}`}
                  />
                );
              })}
              {!sellAllTags && course?.tags.length > 3 && (
                <Tag
                  tag={{
                    name: `+${course?.tags.length - 2}`,
                  }}
                  tootTipMessage={nonSelectedTag
                    .map(t => t.tagName)
                    .join(', ')}
                />
              )}
            </div>
          )}
        </div>
        <ContextMenu
          options={[
            {
              label: 'Add to class',
              onClick: () => {
                addToClass(course);
              }
            },
            {
              label: 'View Course',
              onClick: () => {
                router.push(`/courses/${course.id}`);
              }
            }
          ]}
        />
      </CardActions>
    </CardGrid>
  );
}
