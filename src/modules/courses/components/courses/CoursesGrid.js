import React from 'react';
import { Grid, Box, Paper } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import CourseCard from './CourseCard';
import { ContentCardContainer } from './styles';
import { GridContainer } from '../../../../components/shared/CardGrid';
import {
  CardSkeleton,
  CardEmpty
} from '../../../../components/shared/CardGrid';
import _ from 'lodash';

function CoursesGrid({ courses, paging, loading, onPaging, addToClass }) {
  const pages =
    paging.records > 0 ? Math.ceil(paging.records / paging.perPage) : 0;

  return (
    <ContentCardContainer>
      <Grid container spacing={2}>
        {loading ? (
          <React.Fragment>
            {_.times(paging.perPage, i => (
              <GridContainer
                key={`skeleton-lesson-card-${i}`}
                item
                xs={12}
                s={12}
                sm={6}
                lg={3}
                xl={3}
                md={3}
              >
                <CardSkeleton key={`lessons-loader-${i}`} />
              </GridContainer>
            ))}
          </React.Fragment>
        ) : (
          <React.Fragment>
            {courses && courses.length > 0 ? (
              <React.Fragment>
                {courses.map((course, index) => {
                  return (
                    <GridContainer
                      key={`course-${index}`}
                      item
                      xs={12}
                      s={12}
                      sm={6}
                      lg={3}
                      xl={3}
                      md={3}
                    >
                      <CourseCard
                        courseIndex={index}
                        course={course}
                        addToClass={addToClass}
                      />
                    </GridContainer>
                  );
                })}
              </React.Fragment>
            ) : (
              <CardEmpty title="No courses found!" />
            )}
          </React.Fragment>
        )}
      </Grid>
      <Grid container>
        <Box m="auto" mt={4}>
          <Pagination
            count={pages}
            page={paging.page}
            shape="rounded"
            onChange={(e, val) => {
              onPaging(val - 1);
            }}
          />
        </Box>
      </Grid>
    </ContentCardContainer>
  );
}

export default CoursesGrid;
