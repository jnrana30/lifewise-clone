import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Box,
  Chip,
  Avatar
} from '@material-ui/core/';
import _ from 'lodash';
import { ContextMenu } from 'src/components/shared';
import { Tag } from 'src/components/App';
import Skeleton from '@material-ui/lab/Skeleton';
import Link from 'next/link';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import images from 'src/config/images';
import { useStyles } from './styles';
import s3Helper from 'src/utils/s3Helper.js';
import { ContentContainer } from '../../../../components/shared/Form/File/styles';
import { useRouter } from 'next/router';

function CoursesList({ courses, loading, paging, onPaging, onEdit, ...props }) {
  const classes = useStyles();
  const router = useRouter();
  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  const previewCourse = course => {
    router.push(`/courses/${course.id}`);
  };

  return (
    <ContentContainer>
      <Paper>
        <TableContainer>
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>Course</TableCell>
                <TableCell>Tags</TableCell>
                <TableCell>Status</TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {loading ? (
                <TableRow>
                  <TableCell colSpan={4}>
                    <Skeleton animation="wave" />
                  </TableCell>
                </TableRow>
              ) : (
                <React.Fragment>
                  {courses && courses.length > 0 ? (
                    <React.Fragment>
                      {courses.map((course, index) => {
                        return (
                          <TableRow key={`courselist-${index}`}>
                            <TableCell>
                              <Box display="flex" alignItems="center">
                                <Box
                                  className={classes.courseIconWrap}
                                  display="flex"
                                  alignItems="center"
                                  justifyContent="center"
                                >
                                  {course?.data?.icon &&
                                  course?.data?.icon !== '' ? (
                                    <img
                                      src={s3Helper.getFileUrl(
                                        course?.data?.icon
                                      )}
                                      className={classes.courseIcon}
                                    />
                                  ) : (
                                    <img
                                      src={images.app.coursePlaceholder}
                                      className={classes.courseIcon}
                                    />
                                  )}
                                </Box>
                                <Box>
                                  <Link href={`/courses/admin/${course?.id}`}>
                                    <Typography
                                      variant="body1"
                                      color="secondary"
                                    >
                                      {course.courseName}
                                    </Typography>
                                  </Link>
                                  <Typography variant="body2">
                                    {course.desc}
                                  </Typography>
                                </Box>
                              </Box>
                            </TableCell>
                            <TableCell>
                              <div className={classes.tagsContainer}>
                                {course.tags.map((tag, index) => {
                                  return <Tag tag={tag} />;
                                })}
                              </div>
                            </TableCell>
                            <TableCell>
                              {course?.courseStatus ? (
                                <Chip
                                  label={_.capitalize(course?.courseStatus)}
                                  color={
                                    course?.courseStatus === 'active'
                                      ? 'primary'
                                      : 'default'
                                  }
                                />
                              ) : (
                                <>-</>
                              )}
                            </TableCell>
                            <TableCell width="5%">
                              <ContextMenu
                                options={[
                                  {
                                    label: 'Edit',
                                    icon: <EditIcon />,
                                    onClick: () => {
                                      onEdit(course);
                                    }
                                  },
                                  {
                                    label: 'Preview course',
                                    icon: <VisibilityIcon />,
                                    onClick: () => {
                                      previewCourse(course);
                                    }
                                  }
                                ]}
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </React.Fragment>
                  ) : (
                    <TableRow>
                      <TableCell colSpan={4} align="center">
                        <Typography color="textSecondary">
                          No courses found
                        </Typography>
                      </TableCell>
                    </TableRow>
                  )}
                </React.Fragment>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={paging?.records ? paging.records : 0}
          rowsPerPage={paging.perPage}
          page={paging.page - 1}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </ContentContainer>
  );
}

export default CoursesList;
