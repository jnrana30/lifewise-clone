import React from 'react';
import {
  FilterBar,
  FilterPicker,
  FilterSearch,
  AccessControl,
  FilterDrawer,
} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './styles';
import { courseStatusOptions, courseVisibilityOptions } from 'src/config';
import useViewport from '../../../../utils/ViewPort';

function CoursesFilter({
  filters,
  setFilter,
  unsetFilter,
  isAdmin,
  userProfile,
  schoolYears,
  ...props
}) {
  const [yearFilter, setYearFilter] = React.useState(false);
  const [tagFilter, setTagFilter] = React.useState(false);
  const { isMobileView } = useViewport();
  const classes = useStyles();

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={filters['courseName'] ? filters['courseName'] : ''}
      onChange={val => {
        setFilter({
          courseName: val
        });
      }}
      onClear={() => {
        unsetFilter('courseName');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
      <AccessControl level={6}>
        <FilterPicker
          title="Status"
          options={courseStatusOptions}
          onChange={val => {
            setFilter({
              status: val.value
            });
          }}
          selected={filters['status'] ? filters['status'] : ''}
          onClear={() => {
            unsetFilter('status');
          }}
          ml={2}
        />
        <FilterPicker
          title="Visibility"
          options={courseVisibilityOptions}
          onChange={val => {
            setFilter({
              visibility: val.value
            });
          }}
          selected={filters['visibility'] ? filters['visibility'] : ''}
          onClear={() => {
            unsetFilter('visibility');
          }}
          ml={2}
        />
      </AccessControl>

      {!isAdmin && (
        <FilterPicker
          title="Year Group"
          options={schoolYears}
          onChange={val => {
            setFilter({
              year: val.id
            });
          }}
          selected={
            filters['year']
              ? schoolYears.filter(yr => yr.id === filters['year'])[0]
              : ''
          }
          onClear={() => {
            unsetFilter('year');
          }}
          icon={<CalendarTodayIcon />}
          optLabel="yearName"
          optValue="id"
          ml={2}
          mr={1}
        />
      )}
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView && <Box display="flex">{filterOptions}</Box>}
        {isMobileView &&
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        }
        {userProfile?.role?.level > 5 && (
          <Button
            className={classes.addButton}
            variant="contained"
            color="primary"
            aria-controls="simple-menu"
            onClick={() => {
              props.setDrawerOpen(true);
            }}
            startIcon={<AddIcon />}
          >
            Add Course
          </Button>
        )}
      </Box>
    </FilterBar>
  );
}

export default CoursesFilter;
