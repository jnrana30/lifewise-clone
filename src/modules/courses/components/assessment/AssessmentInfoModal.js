import React from 'react';
import PropTypes from 'prop-types';
import { Drawer, Dialog, Button } from 'src/components/shared';
import { Box, Typography } from '@material-ui/core';
import { AssessmentColorIndicator } from './style';

function AssessmentInfoModal({ isVisible, onClose }) {
  return (
    <Dialog
      open={isVisible}
      onClose={onClose}
      title="Assessment Statuses Explained"
    >
      <Box mb={4}>
        <Box display="flex" alignItems="center">
          <AssessmentColorIndicator color="red" />
          <Typography variant="body1">
            <b>
              Red is used to indicate a student who requires support to
              understand
            </b>
          </Typography>
        </Box>
        <ul>
          <li>Student has struggled with the content in this lesson</li>
          <li>Student can define some of the keywords and key facts</li>
        </ul>
      </Box>

      <Box mb={4}>
        <Box display="flex" alignItems="center">
          <AssessmentColorIndicator color="amber" />
          <Typography variant="body1" style={{ width: '90%' }}>
            <b>
              Amber is used to indicate a student who is confidently developing
              their understanding
            </b>
          </Typography>
        </Box>
        <ul>
          <li>
            Student has understand most of the content of this lesson but needs
            more practice
          </li>
          <li>Student has answered most questions correctly </li>
        </ul>
      </Box>

      <Box>
        <Box display="flex" alignItems="center">
          <AssessmentColorIndicator color="green" />
          <Typography variant="body1">
            <b>
              Green is used to indicate a student who is very confident in their
              understanding
            </b>
          </Typography>
        </Box>
        <ul>
          <li>Student has understood all of the content in this lesson</li>
          <li>Student is confident to explain this to someone else</li>
        </ul>
      </Box>
    </Dialog>
  );
}

AssessmentInfoModal.propTypes = {
  isVisible: PropTypes.bool,
  onClose: PropTypes.func
};

AssessmentInfoModal.defaultProps = {
  isVisible: false,
  onClose: () => {}
};

export default AssessmentInfoModal;
