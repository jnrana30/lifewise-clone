import React from 'react';
import { FilterBar, FilterPicker } from 'src/components/App';
import { Box, Typography } from '@material-ui/core';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import InfoIcon from '@material-ui/icons/Info';
import { useStyles, FilterConrtainer } from './style';
import { getAcademicYearOption } from 'src/config';
const years = getAcademicYearOption();
import Link from 'next/link';

function AssessmentsFilter({
  student,
  filters,
  setFilter,
  unsetFilter,
  ...props
}) {
  const [acaYear, setAcaYear] = React.useState('');
  const classes = useStyles();

  return (
    <FilterConrtainer>
      <Box display="flex" justifyContent="space-between" mb={2}>
        <Link href="/assessments">
          <Box display="flex" alignItems="center" style={{ cursor: 'pointer' }}>
            <ChevronLeftIcon fontSize="small" /> Back to assessments homepage
          </Box>
        </Link>

        <Box
          display="flex"
          alignItems="center"
          style={{ cursor: 'pointer' }}
          onClick={() => {
            props.setInfoModal(true);
          }}
        >
          Assessment statuses explained!{' '}
          <InfoIcon fontSize="small" style={{ marginLeft: 8 }} />
        </Box>
      </Box>
      <Box display="flex" justifyContent="space-between">
        <Box display="flex" alignItems="center">
          <Typography variant="h6">
            {student.firstName} {student.lastName}
          </Typography>
          <FilterPicker
            title="Academic Year"
            options={years}
            onChange={val => {
              setFilter({
                year: val.value.split('/')[0]
              });
            }}
            selected={
              filters['year']
                ? years.filter(
                    year => year.value.split('/')[0] === filters['year']
                  )[0]
                : ''
            }
            onClear={() => {
              unsetFilter('year');
            }}
            ml={2}
          />
        </Box>
        {/* <Box display="flex">
          <Button
            className={classes.addButton}
            variant="contained"
            color="primary"
            aria-controls="simple-menu"
            onClick={() => {
              props.setDrawerOpen(true);
            }}
            startIcon={<AddIcon />}
          >
            New assessment
          </Button>
        </Box> */}
      </Box>
    </FilterConrtainer>
  );
}

export default AssessmentsFilter;
