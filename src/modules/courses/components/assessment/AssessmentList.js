import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableHead,
  TableRow,
  Paper,
  Typography
} from '@material-ui/core/';
import moment from 'moment';
import Skeleton from '@material-ui/lab/Skeleton';
import {
  ReviewContainer,
  ExcellentContainer,
  UnderstoodContainer,
  DevelopmentContainer
} from './style';
import { ContentContainer } from '../../../../components/shared/Form/File/styles';

function AssessmentList({ assessments, loading, paging, onPaging, ...props }) {
  return (
    <ContentContainer>
      <TableContainer component={Paper}>
        <Table className="paddedTable">
          <TableHead>
            <TableRow>
              <TableCell width="20%">Lesson Name</TableCell>
              <TableCell width="8%" align="center">
                Support Required
              </TableCell>
              <TableCell width="8%" align="center">
                Understood
              </TableCell>
              <TableCell width="8%" align="center">
                Good
              </TableCell>
              <TableCell width="50%" align="center">
                Comments
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={8}>
                  <Skeleton animation="wave" />
                </TableCell>
              </TableRow>
            ) : (
              <React.Fragment>
                {assessments && assessments.length > 0 ? (
                  <React.Fragment>
                    {assessments.map((assessmentItem, index) => {
                      return (
                        <TableRow key={`assessmentsList-${index}`}>
                          <TableCell width="20%">
                            <Typography variant="body1" color="secondary">
                              {assessmentItem.lessonName}
                            </Typography>
                            <Typography variant="body2">
                              Completed{' : '}
                              {moment(assessmentItem.assessedOn).format('l')}
                            </Typography>
                          </TableCell>
                          <TableCell width="24%" align="center" colSpan={3}>
                            <ReviewContainer
                              isChecked={assessmentItem.rating == 3}
                            >
                              <DevelopmentContainer
                                isChecked={assessmentItem.rating == 1}
                                onClick={() => {
                                  assessmentItem.rating = 1;
                                  props.updateStudentAssessment(assessmentItem);
                                }}
                              />
                              <UnderstoodContainer
                                isChecked={assessmentItem.rating == 2}
                                onClick={() => {
                                  assessmentItem.rating = 2;
                                  props.updateStudentAssessment(assessmentItem);
                                }}
                              />
                              <ExcellentContainer
                                isChecked={assessmentItem.rating == 3}
                                onClick={() => {
                                  assessmentItem.rating = 3;
                                  props.updateStudentAssessment(assessmentItem);
                                }}
                              />
                            </ReviewContainer>
                          </TableCell>
                          <TableCell width="50%">
                            <Typography variant="body2" color="textPrimary">
                              {assessmentItem.comments}
                            </Typography>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </React.Fragment>
                ) : (
                  <TableRow>
                    <TableCell colSpan={8} align="center">
                      <Typography color="textSecondary">
                        No assessments found
                      </Typography>
                    </TableCell>
                  </TableRow>
                )}
              </React.Fragment>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </ContentContainer>
  );
}

export default AssessmentList;
