import React from 'react';
import { Typography } from '@material-ui/core';
import * as Yup from 'yup';

import { Form } from 'src/components/shared';
import { lessons, classes } from "../../../../courses/api/data";

function LessonForm({ isEdit, onSubmit }) {
  const [loading, setLoading] = React.useState(false);

  return (
    <Form
      initialValues={{
        lesson: '',
        class: '',
      }}
      validationSchema={Yup.object().shape({
        lesson: Yup.string().required('Lesson is required!'),
        class: Yup.string().required('Class is required!')
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
        onSubmit();
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="assessment-form"
            noValidate
          >
            <Typography variant="body1" color="textPrimary">
              <b>Choose lesson and your class.</b>
            </Typography>
            <Form.Field.Select
              options={lessons}
              fullWidth
              variant="standard"
              name="lesson"
              label="Lesson"
              optLabel="title"
              optValue="id"
            />
            <Form.Field.Select
              fullWidth
              options={classes}
              variant="standard"
              name="class"
              label="Class"
              optLabel="label"
              optValue="value"
            />
          </form>
        );
      }}
    </Form>
  );
}

export default LessonForm;
