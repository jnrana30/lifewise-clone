import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Box,
  Chip,
  Avatar
} from '@material-ui/core/';
import { Formik, FieldArray } from 'formik';
import { Form, Button } from 'src/components/shared';
import {
  ReviewContainer,
  ExcellentContainer,
  UnderstoodContainer,
  DevelopmentContainer
} from './style';

function Assessment({ assessment, onSubmit, ...props }) {
  return (
    <Formik
      initialValues={{ assessment: assessment, students: [] }}
      enableReinitialize={true}
      onSubmit={async values => {
        console.log('values');
        console.log(values);
        onSubmit();
      }}
    >
      {({ values, setFieldValue, ...props }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            props.submitForm();
            return false;
          }}
          id="assessment-form"
          noValidate
        >
          <FieldArray
            name="assessment"
            render={arrayHelpers => (
              <React.Fragment>
                <Box
                  mt={2}
                  mb={2}
                  display="flex"
                  alignItems="flex-end"
                  justifyContent="space-between"
                >
                  <Box display="flex" flexGrow={2}>
                    <Typography variant="body1" color="textPrimary">
                      <b>Provide student assessment by clicking on the relevant box.</b>
                    </Typography>
                  </Box>
                </Box>
                <TableContainer>
                  <Table className="noBorders">
                    <TableHead>
                      <TableRow>
                        <TableCell>Student</TableCell>
                        <TableCell align="center">Excellent</TableCell>
                        <TableCell align="center">Understood</TableCell>
                        <TableCell align="center">Development</TableCell>
                        <TableCell align="center">Comments</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {values.assessment &&
                      values.assessment.length > 0 &&
                      values.assessment.map((assessmentItem, index) => {
                        return (
                          <TableRow key={`student-assessment-${index}`}>
                            <TableCell width="25%">
                              <Typography variant="body1" color="secondary">
                                {assessmentItem.student}
                              </Typography>
                            </TableCell>
                            <TableCell width="35%" align="center" colSpan={3}>
                              <ReviewContainer>
                                <ExcellentContainer
                                  isChecked={
                                    assessmentItem.score == 'excellent'
                                  }
                                  onClick={() => {
                                    assessmentItem.score = 'excellent';
                                    arrayHelpers.replace(
                                      index,
                                      assessmentItem
                                    );
                                  }}
                                />
                                <UnderstoodContainer
                                  isChecked={
                                    assessmentItem.score === 'understood'
                                  }
                                  onClick={() => {
                                    assessmentItem.score = 'understood';
                                    arrayHelpers.replace(
                                      index,
                                      assessmentItem
                                    );
                                  }}
                                />
                                <DevelopmentContainer
                                  isChecked={
                                    assessmentItem.score === 'development'
                                  }
                                  onClick={() => {
                                    assessmentItem.score = 'development';
                                    arrayHelpers.replace(
                                      index,
                                      assessmentItem
                                    );
                                  }}
                                />
                              </ReviewContainer>
                            </TableCell>
                            <TableCell width="40%" align="center">
                              <Form.Field.Input
                                fullWidth
                                multiline
                                variant="outlined"
                                name={`assessment[${index}].comment`}
                                label=""
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </TableBody>
                  </Table>
                </TableContainer>
              </React.Fragment>
            )}
          />
        </form>
      )}
    </Formik>
  );
}

export default Assessment;
