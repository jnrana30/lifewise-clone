import styled, { css } from 'styled-components';

export const ReviewContainer = styled.div`
  display: flex;
  flex: 1;
  background-color: #dddddd;
  height: 54px;
  border-radius: 4px;
  margin-top: 8px;
`;

export const ExcellentContainer = styled.div`
  display: flex;
  flex: 1;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  cursor: pointer;
  ${props =>
  props.isChecked &&
  css`
      background-color: #42b752;
    `}
`;
export const UnderstoodContainer = styled.div`
  display: flex;
  flex: 1;
  cursor: pointer;
  border-left: 1px solid #999999;
  border-right: 1px solid #999999;
  ${props =>
  props.isChecked &&
  css`
      background-color: #fab03c;
    `}
`;
export const DevelopmentContainer = styled.div`
  display: flex;
  flex: 1;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  cursor: pointer;
  ${props =>
  props.isChecked &&
  css`
      background-color: #ff3b36;
    `}
`;
