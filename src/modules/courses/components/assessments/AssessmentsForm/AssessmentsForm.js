import React from 'react';
import { Box } from '@material-ui/core';

import Stepper from '../../../../../components/shared/Stepper';
import Lesson from './Lesson';
import Assessment from './Assessment';
import { assessmentForm } from "../../../../courses/api/data";

function AssessmentsForm() {
  const [activeStep, setActiveStep] = React.useState(0);

  const steps = ['Lesson', 'Assessment'];

  const onSubmit = () => {
    setActiveStep(activeStep + 1);
  };

  return (
    <div>
      <Stepper steps={steps} activeStep={activeStep} />
      <Box mt={4}>
        {activeStep === 0 && <Lesson onSubmit={onSubmit} />}
        {activeStep === 1 && (
          <Assessment assessment={assessmentForm} onSubmit={onSubmit} />
        )}
      </Box>
    </div>
  );
}

export default AssessmentsForm;
