import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableHead,
  TableRow,
  Paper,
  Typography
} from '@material-ui/core/';
import Skeleton from '@material-ui/lab/Skeleton';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import Link from 'next/link';
import EditIcon from '@material-ui/icons/Edit';
import { ContextMenu } from 'src/components/shared';
import { ContentContainer } from '../../../../components/shared/Form/File/styles';

export const useStyles = makeStyles(theme => ({
  numberBold: {
    fontWeight: 500
  }
}));

function AssessmentsList({ assessments, loading, paging, onPaging, ...props }) {
  const router = useRouter();

  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };
  return (
    <ContentContainer>
      <Paper>
        <TableContainer>
          <Table aria-label="customized table">
            <TableHead>
              <TableRow>
                <TableCell>Student Name</TableCell>
                <TableCell>Class name</TableCell>
                <TableCell>Trending score</TableCell>
                <TableCell align="center">Developing</TableCell>
                <TableCell align="center">Understood</TableCell>
                <TableCell align="center">Good</TableCell>
                <TableCell align="center">Comments</TableCell>
                <TableCell />
              </TableRow>
            </TableHead>
            <TableBody>
              {loading ? (
                <TableRow>
                  <TableCell colSpan={8}>
                    <Skeleton animation="wave" />
                  </TableCell>
                </TableRow>
              ) : (
                <React.Fragment>
                  {assessments && assessments.length > 0 ? (
                    <React.Fragment>
                      {assessments.map((assessment, index) => {
                        return (
                          <TableRow key={`assessmentsList-${index}`}>
                            <TableCell>
                              <Link href={`/assessments/${assessment.id}`}>
                                <Typography variant="body1" color="secondary">
                                  {assessment.firstName} {assessment.lastName}
                                </Typography>
                              </Link>
                            </TableCell>

                            <TableCell>
                              <Link href={`/assessments/${assessment.id}`}>
                                <Typography variant="body2" color="textPrimary">
                                  {assessment.classes &&
                                  assessment.classes.length
                                    ? assessment.classes[0].className
                                    : '-'}
                                </Typography>
                              </Link>
                            </TableCell>
                            <TableCell>
                              <Link href={`/assessments/${assessment.id}`}>
                                <Typography variant="body2" color="textPrimary">
                                  {assessment.assessmentAggregates &&
                                  assessment.assessmentAggregates?.trending &&
                                  assessment.assessmentAggregates?.trending > 0
                                    ? assessment.assessmentAggregates
                                        ?.trending == 1
                                      ? 'Developing'
                                      : assessment.assessmentAggregates
                                          ?.trending == 2
                                      ? 'Understood'
                                      : 'Good'
                                    : '-'}
                                </Typography>
                              </Link>
                            </TableCell>
                            <TableCell width="12%" align="center">
                              <Typography
                                variant="body1"
                                color="textPrimary"
                                className={classes.numberBold}
                              >
                                {assessment.assessmentAggregates?.totals &&
                                assessment.assessmentAggregates?.totals[1] &&
                                assessment.assessmentAggregates?.totals[1] > 0
                                  ? assessment.assessmentAggregates?.totals[1]
                                  : 0}
                              </Typography>
                            </TableCell>
                            <TableCell width="12%" align="center">
                              <Typography
                                variant="body1"
                                color="textPrimary"
                                className={classes.numberBold}
                              >
                                {assessment.assessmentAggregates?.totals &&
                                assessment.assessmentAggregates?.totals[2] &&
                                assessment.assessmentAggregates?.totals[2] > 0
                                  ? assessment.assessmentAggregates?.totals[2]
                                  : 0}
                              </Typography>
                            </TableCell>
                            <TableCell width="12%" align="center">
                              <Typography
                                variant="body1"
                                color="textPrimary"
                                className={classes.numberBold}
                              >
                                {assessment.assessmentAggregates?.totals &&
                                assessment.assessmentAggregates?.totals[3] &&
                                assessment.assessmentAggregates?.totals[3] > 0
                                  ? assessment.assessmentAggregates?.totals[3]
                                  : 0}
                              </Typography>
                            </TableCell>
                            <TableCell width="12%" align="center">
                              <Typography
                                variant="body1"
                                color="textPrimary"
                                className={classes.numberBold}
                              >
                                {assessment.assessmentAggregates.comments}
                              </Typography>
                            </TableCell>
                            <TableCell width="5%">
                              <ContextMenu
                                options={[
                                  {
                                    label: 'Edit',
                                    icon: <EditIcon />,
                                    onClick: () => {
                                      router.push(
                                        `/assessments/${assessment.id}`
                                      );
                                    }
                                  }
                                ]}
                              />
                            </TableCell>
                          </TableRow>
                        );
                      })}
                    </React.Fragment>
                  ) : (
                    <TableRow>
                      <TableCell colSpan={8} align="center">
                        <Typography color="textSecondary">
                          No assessments found
                        </Typography>
                      </TableCell>
                    </TableRow>
                  )}
                </React.Fragment>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          component="div"
          count={paging?.records ? paging.records : 0}
          rowsPerPage={paging.perPage}
          page={paging.page - 1}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </ContentContainer>
  );
}

export default AssessmentsList;
