import React from 'react';
import {
  FilterBar,
  FilterDrawer,
  FilterPicker,
  FilterSearch
} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';
import useViewport from '../../../../utils/ViewPort';
import { getTeacherClasses } from '../../../school/api/schoolApis';

import { getAcademicYearOption } from 'src/config';
const years = getAcademicYearOption();

function AssessmentsFilter({ filters, setFilter, unsetFilter, ...props }) {
  const classes = useStyles();
  const { isMobileView } = useViewport();
  const [isMount, setIsMount] = React.useState(true);
  const [classFilter, setClassFilter] = React.useState({});

  React.useEffect(() => {
    if (isMount) {
      setIsMount(false);
      return;
    }
    if (classFilter && classFilter?.id && classFilter?.id !== '') {
      setFilter({ classId: classFilter.id });
    } else {
      unsetFilter('classId');
    }
  }, [classFilter]);

  const getClasses = async search => {
    let filter = {};
    if (search && search !== '') {
      filter.search = search;
    }
    const res = await getTeacherClasses(filter);
    return res?.classes ? res?.classes : [];
  };

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={filters['search'] ? filters['search'] : ''}
      onChange={val => {
        setFilter({
          search: val
        });
      }}
      onClear={() => {
        unsetFilter('search');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}
      <FilterPicker
        title="Class"
        options={[]}
        onChange={val => {
          setClassFilter(val);
        }}
        remoteMethod={val => getClasses(val)}
        selected={classFilter && classFilter?.id ? classFilter : ''}
        onClear={() => {
          setClassFilter({});
        }}
        optValue="id"
        optLabel="className"
        ml={2}
        mr={1}
      />
      <FilterPicker
        title="Academic Year"
        options={years}
        onChange={val => {
          setFilter({
            year: val.value.split('/')[0]
          });
        }}
        selected={
          filters['year']
            ? years.filter(
                year => year.value.split('/')[0] === filters['year']
              )[0]
            : ''
        }
        onClear={() => {
          unsetFilter('year');
        }}
        ml={2}
      />
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView && <Box display="flex">{filterOptions}</Box>}
        {isMobileView && (
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        )}
        <Box display="flex">
          <Button
            className={classes.addButton}
            variant="contained"
            color="primary"
            aria-controls="simple-menu"
            onClick={() => {
              props.setDrawerOpen(true);
            }}
            startIcon={<AddIcon />}
          >
            New assessment
          </Button>
        </Box>
      </Box>
    </FilterBar>
  );
}

export default AssessmentsFilter;
