import React, { useEffect, useState } from 'react';
import { Grid } from '@material-ui/core';
import { fetchResources } from 'src/modules/courses/api/coursesApis';
import { Form } from 'src/components/shared';

import ResourceCard from 'src/modules/app/components/resources/ResourceCard';
import { CardSkeleton } from 'src/components/shared/CardGrid';

function Share({ onSubmit }) {
  const [certificates, setCertificates] = useState([]);
  const [loading, setLoading] = useState([]);
  useEffect(() => {
    const filters = {
      resourceStatus: 'published',
      type: 'certificate'
    };
    setLoading(true);
    fetchResources(filters)
      .then(res => {
        setLoading(false);
        setCertificates(res.resources);
      })
      .catch(err => {
        setLoading(false);
        setCertificates([]);
      });
  }, []);

  return (
    <React.Fragment>
      <Grid container spacing={2}>
        {loading ? (
          <Grid item lg={6} md={6} sm={6}>
            <CardSkeleton />
          </Grid>
        ) : (
          <React.Fragment>
            {certificates.map(certificate => {
              return (
                <Grid
                  item
                  lg={6}
                  md={6}
                  sm={6}
                  key={`lesson-cert-${certificate.id}`}
                >
                  <ResourceCard resource={certificate} onOpen={() => {}} />
                </Grid>
              );
            })}
          </React.Fragment>
        )}
      </Grid>

      <Form
        initialValues={{}}
        onSubmit={async values => {
          onSubmit();
        }}
      >
        {props => {
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                props.submitForm();
                return false;
              }}
              id="lesson-complete-form"
              noValidate
            ></form>
          );
        }}
      </Form>
    </React.Fragment>
  );
}

export default Share;
