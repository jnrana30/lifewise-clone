import React from 'react';
import { Typography } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { fetchLessons, getLesson } from 'src/modules/courses/api/coursesApis';
import { fetchClasses } from 'src/modules/school/api/schoolApis';
import _ from 'lodash';

function LessonForm({ onSubmit, selectedClass, lesson }) {
  const formRef = React.createRef();
  const [loading, setLoading] = React.useState(false);
  const [defaultClass, setDefaultClass] = React.useState(false);

  const getClasses = async (search, index) => {
    let filter = {
      search,
      status: 'active',
      year: new Date().getFullYear().toString()
    };
    const res = await fetchClasses(filter);
    return res?.classes ? res?.classes : [];
  };

  const getLessons = async search => {
    let filter = {
      search,
      perPage: 10,
      page: 1
    };
    const res = await fetchLessons(filter);
    return res?.lessons ? res?.lessons : [];
  };

  return (
    <Form
      initialValues={{
        lessonId: lesson,
        classId: selectedClass
      }}
      validationSchema={Yup.object().shape({
        lessonId: Yup.object().required('Lesson is required!'),
        classId: Yup.object().required('Class is required!')
      })}
      onSubmit={async (values, form) => {
        let data = { ...values };
        // data.lessonId = data.lessonId?.id;
        data.status = 'completed';
        // const classId = data.classId?.id;
        // const students = data.classId?.students;
        // data = _.omit(data, ['classId']);
        onSubmit(data);
      }}
      innerRef={formRef}
      enableReinitialize={true}
    >
      {({ values, ...props }) => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="lesson-complete-form"
            noValidate
          >
            <Typography variant="body1" color="textPrimary">
              <b>
                Please check and confirm the Lesson you’d like to complete an
                Assessment for.
              </b>
            </Typography>
            <Form.Field.AutoComplete
              multiple={false}
              fullWidth
              showAvatar={false}
              options={[lesson]}
              variant="standard"
              remoteMethod={getLessons}
              name="lessonId"
              label="Choose Lesson"
              optLabel="lessonName"
              optValue="id"
            />
            <Form.Field.AutoComplete
              multiple={false}
              fullWidth
              showAvatar={false}
              options={[selectedClass]}
              variant="standard"
              remoteMethod={getClasses}
              name="classId"
              label="Choose Class"
              optLabel="className"
              optValue="id"
              disabled={!values?.lessonId?.id}
            />
          </form>
        );
      }}
    </Form>
  );
}

export default LessonForm;
