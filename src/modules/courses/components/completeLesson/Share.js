import React from 'react';
import { Box, Typography, Grid } from '@material-ui/core';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import toast from 'src/utils/toast';
import { Form, Button } from 'src/components/shared';

function Share({ lesson, onSubmit }) {
  // const parentLink = if (data?.parentLink && data?.parentLink !== '') {
  //   actions.push({
  //     title: 'Share with parents',
  //     action: 'share'
  //   });
  // }

  const parentLink = lesson?.content?.data?.parentLink
    ? lesson?.content?.data?.parentLink
    : '';

  return (
    <Grid container>
      <Grid item lg={1} md={1} sm={12}></Grid>
      <Grid item lg={12} md={12} sm={12}>
        <Form
          initialValues={{
            link: parentLink
          }}
          onSubmit={async values => {
            console.log('values');
            console.log(values);
            onSubmit();
          }}
          enableReinitialize={true}
        >
          {props => {
            return (
              <form
                onSubmit={e => {
                  e.preventDefault();
                  props.submitForm();
                  return false;
                }}
                id="lesson-complete-form"
                noValidate
              >
                <Box mb={2}>
                  <Typography variant="h6" color="textPrimary">
                    Congratulations on completing your lesson!
                  </Typography>
                </Box>
                <Typography variant="body1" color="">
                  Why not create a seamless learning experience by sharing
                  information about this lesson with your student’s parents. To
                  do this, copy and paste the link into your preferred
                  communication tool.
                </Typography>
                <Form.Field.Input
                  name="link"
                  disabled={true}
                  fullWidth
                  style={{ color: 'red' }}
                />
                <Box mt={2} display="flex" justifyContent="space-between">
                  <Button
                    variant="outlined"
                    color="primary"
                    mt={2}
                    onClick={() => {
                      navigator.clipboard.writeText(parentLink);
                      toast.success('Link copied successfully!');
                    }}
                  >
                    Copy to clipboard
                  </Button>
                  <Button
                    color="secondary"
                    endIcon={<OpenInNewIcon />}
                    onClick={() => {
                      window.open(parentLink, '_blank');
                    }}
                  >
                    Preview Parent Page
                  </Button>
                </Box>
              </form>
            );
          }}
        </Form>
      </Grid>
    </Grid>
  );
}

export default Share;
