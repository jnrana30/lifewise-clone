import { makeStyles } from '@material-ui/core/styles';
import styled, { css } from 'styled-components';

export const ReviewContainer = styled.div`
  display: flex;
  flex: 1;
  background-color: #dddddd;
  height: 24px;
  border-radius: 0px;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  margin-top: 0px;
  position: relative;
  &:after {
    content: '';
    position: absolute;
    height: 0;
    width: 0;
    left: 100%;
    top: 0;
    border: 12px solid transparent;
    border-left: 12px solid #42b752;
    z-index: 100;
    ${props =>
      props.isChecked
        ? css`
            border-left: 12px solid #42b752;
          `
        : css`
            border-left: 12px solid #dddddd;
          `}
  }
  &:before {
    content: '';
    display: block;
    position: absolute;
    bottom: 0px;
    width: 0;
    height: 0;
    border: 12px solid transparent;
    border-left-color: black;
    right: -24px;
    z-index: 99;
  }
`;

export const ExcellentContainer = styled.div`
  display: flex;
  flex: 1;
  border-top-left-radius: 0px;
  border-bottom-left-radius: 0px;
  border: 0.5px solid #99999975;
  border-left-width: 0;
  border-right-width: 0;
  ${props =>
    props.isChecked &&
    css`
      background-color: #42b752;
    `}
`;
export const UnderstoodContainer = styled.div`
  display: flex;
  flex: 1;
  border: 0.5px solid #99999975;
  ${props =>
    props.isChecked &&
    css`
      background-color: #fab03c;
    `}
`;
export const DevelopmentContainer = styled.div`
  display: flex;
  flex: 1;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  border: 0.5px solid #99999975;
  border-right-width: 0;
  ${props =>
    props.isChecked &&
    css`
      background-color: #ff3b36;
    `}
`;

export const SchoolLogoContainer = styled.div`
  display: flex;
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const SchoolLogoImage = styled.img`
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const Counter = styled.div`
  display: flex;
  font-size: 20px;
  align-items: center;
  justify-content: center;
  font-weight: 500;
  margin-top: 26px;
`;

export const useStyles = makeStyles(theme => ({
  buttonRoot: {
    display: 'flex',
    justifyContent: 'space-around',
    minWidth: 180,
    borderRadius: 30,
    backgroundColor: '#ffffff',
    marginRight: 10
  },
  root: {
    padding: '0',
    display: 'flex',
    alignItems: 'center',
    borderRadius: 40,
    borderColor: 'rgba(43, 107, 0, 0.5)',
    marginRight: 10
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    height: 28,
    margin: 4
  },
  addButton: {
    borderRadius: 4,
    padding: '00px 20px',
    height: 36,
    minHeight: 36
  }
}));
