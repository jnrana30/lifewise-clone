import React from 'react';
import {
  Box,
  Typography,
  Grid
} from '@material-ui/core';
import Rating from '@material-ui/lab/Rating';
import { Form, Button } from 'src/components/shared';

function RatingComponent({ onSubmit }) {
  return (
    <Grid container>
      <Grid item lg={1} md={1} sm={12}></Grid>
      <Grid item lg={12} md={12} sm={12}>
        <Form
          initialValues={{
            rating: 0,
            message: ''
          }}
          onSubmit={async (values, form) => {
            onSubmit(values);
          }}
        >
          {({ values, ...props }) => {
            return (
              <form
                onSubmit={e => {
                  e.preventDefault();
                  props.submitForm();
                  return false;
                }}
                id="lesson-complete-form"
                noValidate
              >
                <Typography variant="body1" color="textPrimary" paragraph>
                  <b>Lesson Feedback</b>
                </Typography>
                <Rating
                  name="size-large"
                  defaultValue={0}
                  size="large"
                  onChange={(event, val) => {
                    props.setFieldValue('rating', val);
                  }}
                />
                <Form.Field.Input
                  name="message"
                  multiline
                  variant="outlined"
                  fullWidth
                  placeholder="Please provide any feedback on the lesson to help us improve our services and content."
                  inputProps={{
                    style: {
                      height: 100
                    }
                  }}
                />
              </form>
            );
          }}
        </Form>
      </Grid>
    </Grid>
  );
}

export default RatingComponent;
