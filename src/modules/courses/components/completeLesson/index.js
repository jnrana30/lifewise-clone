import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Box, Typography } from '@material-ui/core';
import Stepper from 'src/components/shared/Stepper';
import { Drawer } from 'src/components/shared';

import Lesson from './Lesson';
import Assessment from './Assessment';
import Share from './Share';
import Rating from './Rating';
import Certificates from './Certificates';
import AssessmentInfoModal from 'src/modules/courses/components/assessment/AssessmentInfoModal';

import {
  completeLesson,
  submitAssessment
} from 'src/modules/courses/actions/coursesActions';
import { courseLessonFeedback } from 'src/modules/courses/api/coursesApis';
import toast from 'src/utils/toast';

function CompleteLesson({
  action,
  lesson,
  courseId,
  onClose,
  selectedClass,
  ...props
}) {
  const [activeStep, setActiveStep] = React.useState(0);
  const [isLoading, setIsLoading] = React.useState(false);
  const [steps, setSteps] = React.useState([]);
  const [title, setTitle] = React.useState('');
  const [studentIDs, setStudentIDs] = React.useState([]);
  const [infoModal, setInfoModal] = React.useState(false);

  const [asmtLesson, setAsmtLesson] = React.useState({});
  const [asmtClass, setAsmtClass] = React.useState({});

  React.useEffect(() => {
    setAsmtLesson(lesson);
    setAsmtClass(selectedClass);
  }, [lesson, selectedClass]);

  React.useEffect(() => {
    if (action === '' || action == null) return false;
    switch (action) {
      case 'completeLesson':
        setSteps(['Lesson', 'Assessment']);
        setTitle('Complete lesson');
        break;

      case 'assessments':
        setSteps(['Lesson', 'Assessment']);
        setTitle('Create new Assessment');
        break;

      case 'feedback':
        setSteps(['Feedback']);
        setTitle('Feedback');
        break;

      case 'share':
        setSteps(['Share with Parent']);
        setTitle('Share with Parent');
        break;

      case 'certificates':
        setSteps(['Lesson Certificates']);
        setTitle('Lesson Certificates');
        break;

      default:
        setSteps(['Lesson', 'Share with Parent', 'Feedback']);
        break;
    }
  }, [action]);

  const onSubmit = () => {
    handleModalClose();
  };

  const handleModalClose = () => {
    if (activeStep == steps.length - 1) {
      if (action == 'completeLesson' || action == 'assessments') {
        setSteps(['Share with Parent']);
        setTitle('Share with Parent');
        setActiveStep(0);
      } else {
        onClose();
      }
    } else {
      setActiveStep(activeStep + 1);
    }
  };

  let step = steps[activeStep];
  step = step?.replace(/\s+/g, '-').toLowerCase();

  if (!step) return null;

  const onCompleteLesson = async data => {
    /**
     * NOTE : disabled sending the courseId.
     * as we need to mark the lesson completed against all courses for the selected class
     * */
    // if (courseId && courseId !== '') {
    //   data.courseId = courseId;
    // }
    try {
      let payload = { ...data };
      setIsLoading(true);
      await setAsmtClass(data.classId);
      await setAsmtLesson(data.lessonId);
      payload.lessonId = payload.lessonId?.id;
      payload = _.omit(payload, ['classId']);
      const res = await props.completeLesson(
        data.classId.id,
        courseId,
        payload
      );
      setIsLoading(false);
      setStudentIDs(data.classId?.students);
      handleModalClose();
    } catch (error) {
      console.log('error: ', error);
      setIsLoading(false);
    }
  };

  const onFeedback = data => {
    setIsLoading(true);
    data.lessonId = asmtLesson.id;
    courseLessonFeedback(data)
      .then(res => {
        toast.success('Feedback submitted successfully!');
        handleModalClose();
        setIsLoading(false);
      })
      .catch(err => {
        toast.error('Error submitting your feedback! Please try again.');
        setIsLoading(false);
      });
  };

  const onAssessmentSubmit = data => {
    try {
      setIsLoading(true);
      props.submitAssessment(asmtClass?.id, asmtLesson?.id, data.students);
      toast.success('Assessment saved successfully!');
      handleModalClose();
      setIsLoading(false);
    } catch (error) {
      toast.error('Error saving your assessment! Please try again.');
      setIsLoading(false);
    }
  };

  return (
    <Drawer
      open={action && action !== ''}
      title={title}
      onClose={onClose}
      fullWidth={true}
      form="lesson-complete-form"
      buttonText="Continue"
      buttonLoading={isLoading}
    >
      <div>
        {steps.length > 1 && <Stepper steps={steps} activeStep={activeStep} />}
        <Box mt={step === 'lesson-certificates' ? 0 : 4}>
          {step == 'lesson' && (
            <Lesson
              lesson={asmtLesson}
              selectedClass={asmtClass}
              onSubmit={onCompleteLesson}
            />
          )}
          {step == 'assessment' && (
            <Assessment
              studentIDs={studentIDs}
              lesson={asmtLesson}
              onSubmit={onAssessmentSubmit}
              setInfoModal={setInfoModal}
            />
          )}
          {step === 'share-with-parent' && (
            <Share lesson={asmtLesson} onSubmit={onSubmit} />
          )}
          {step === 'feedback' && <Rating onSubmit={onFeedback} />}
          {step === 'lesson-certificates' && (
            <>
              <Typography
                variant="body1"
                color="textPrimary"
                gutterBottom
                paragraph
              >
                <b>Click certificate to download and give to the children.</b>
              </Typography>
              <Certificates onSubmit={onSubmit} />
            </>
          )}
        </Box>
      </div>
      <AssessmentInfoModal
        isVisible={infoModal}
        onClose={() => setInfoModal(false)}
      />
    </Drawer>
  );
}

CompleteLesson.propTypes = {
  courseId: PropTypes.string,
  selectedClass: PropTypes.object
};

CompleteLesson.defaultProps = {
  courseId: '',
  selectedClass: {}
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  completeLesson: (classId, courseId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(completeLesson(classId, courseId, data, resolve, reject));
    });
  },
  submitAssessment: (classId, lessonId, students) => {
    return new Promise((resolve, reject) => {
      dispatch(submitAssessment(classId, lessonId, students, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CompleteLesson);
