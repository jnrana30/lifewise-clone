import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
  Box
} from '@material-ui/core/';
import { Formik, FieldArray } from 'formik';
import { Form, Button } from 'src/components/shared';
import {
  ReviewContainer,
  ExcellentContainer,
  UnderstoodContainer,
  DevelopmentContainer
} from './style';
import InfoIcon from '@material-ui/icons/Info';
import { fetchUsers } from 'src/modules/account/api/accountApis';

function Assessment({ studentIDs, onSubmit, lesson, ...props }) {
  const [students, setStudents] = React.useState([]);

  React.useEffect(() => {
    if (studentIDs && studentIDs.length > 0) {
      const filters = {
        // ids: `[${studentIDs.map(id => id)}]`,
        ids: `[${studentIDs.join(',')}]`,
        propagate: false
      };
      fetchUsers(filters)
        .then(res => {
          if (res && res.profiles && res.profiles.length) {
            const data = res.profiles.map(student => ({
              studentId: student.id,
              studentName: `${student.firstName} ${student.lastName}`,
              rating: 2,
              comment: ''
            }));
            setStudents(data);
          } else {
            setStudents([]);
          }
        })
        .catch(err => {
          console.log('err : ', err);
          setStudents([]);
        });
    }
  }, [studentIDs]);

  return (
    <Formik
      initialValues={{ students: students }}
      enableReinitialize={true}
      onSubmit={async values => {
        onSubmit(values);
      }}
    >
      {({ values, setFieldValue, ...prop }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            prop.submitForm();
            return false;
          }}
          id="lesson-complete-form"
          noValidate
        >
          <FieldArray
            name="students"
            render={arrayHelpers => (
              <React.Fragment>
                <Box mt={2} mb={2}>
                  <Box display="flex" flexGrow={2} mb={2}>
                    <Typography variant="body1" color="textPrimary">
                      <b>
                        Complete the Learning Assessment status for each of your
                        students below.
                      </b>
                    </Typography>
                  </Box>

                  <Box
                    display="flex"
                    flexGrow={2}
                    justifyContent="space-between"
                  >
                    <Typography variant="h6" color="textPrimary">
                      <b>{lesson.lessonName}</b>
                    </Typography>

                    <Box
                      display="flex"
                      alignItems="center"
                      style={{ cursor: 'pointer' }}
                      onClick={() => {
                        props.setInfoModal(true);
                      }}
                    >
                      Assessment statuses explained!{' '}
                      <InfoIcon fontSize="small" style={{ marginLeft: 8 }} />
                    </Box>
                  </Box>
                </Box>
                <TableContainer>
                  <Table className="noBordersBack table-header-rotated">
                    <TableHead>
                      <TableRow>
                        <TableCell width="22%">Student</TableCell>
                        <TableCell
                          width="10%"
                          align="center"
                          className="rotate-45"
                        >
                          <div>
                            <span>
                              Support
                              <br />
                              <br />
                              Required
                            </span>
                          </div>
                        </TableCell>
                        <TableCell
                          width="10%"
                          align="center"
                          className="rotate-45"
                        >
                          <div>
                            <span>Understood</span>
                          </div>
                        </TableCell>
                        <TableCell
                          width="10%"
                          align="center"
                          className="rotate-45"
                        >
                          <div>
                            <span>Good</span>
                          </div>
                        </TableCell>
                        <TableCell align="center">Comments</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {values.students &&
                        values.students.length > 0 &&
                        values.students.map((studentsItem, index) => {
                          return (
                            <TableRow key={`student-students-${index}`}>
                              <TableCell width="22%">
                                <Typography variant="body1" color="secondary">
                                  {studentsItem.studentName}
                                </Typography>
                              </TableCell>
                              <TableCell width="30%" align="center" colSpan={3}>
                                <ReviewContainer
                                  isChecked={studentsItem.rating == 3}
                                >
                                  <DevelopmentContainer
                                    isChecked={studentsItem.rating == 1}
                                    onClick={() => {
                                      studentsItem.rating = 1;
                                      arrayHelpers.replace(index, studentsItem);
                                    }}
                                  />

                                  <UnderstoodContainer
                                    isChecked={studentsItem.rating == 2}
                                    onClick={() => {
                                      studentsItem.rating = 2;
                                      arrayHelpers.replace(index, studentsItem);
                                    }}
                                  />
                                  <ExcellentContainer
                                    isChecked={studentsItem.rating == 3}
                                    onClick={() => {
                                      studentsItem.rating = 3;
                                      arrayHelpers.replace(index, studentsItem);
                                    }}
                                  />
                                </ReviewContainer>
                              </TableCell>
                              <TableCell width="50%" align="center">
                                <Form.Field.Input
                                  fullWidth
                                  multiline
                                  name={`students[${index}].comment`}
                                  label=""
                                  placeholder="Please enter you comment"
                                />
                              </TableCell>
                            </TableRow>
                          );
                        })}
                    </TableBody>
                  </Table>
                </TableContainer>
              </React.Fragment>
            )}
          />
        </form>
      )}
    </Formik>
  );
}

export default Assessment;
