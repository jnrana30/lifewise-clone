import React from 'react';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Card,
  Box,
  Typography,
  Avatar,
} from '@material-ui/core';

function LessonActions(props) {
  const lessonActionList = [
    {
      label: 'Download Activity Workbook',
    },
    {
      label: 'Start lesson',
    },
    {
      label: 'Add to class',
    },
    {
      label: 'Download Lesson Plan',
    },
    {
      label: 'Share with Parents',
    },
    {
      label: 'Mark as complete',
    },
    {
      label: 'Children Certificates',
    },
    {
      label: 'Download Activity Workbook',
    },
    {
      label: 'Feedback',
    },
  ];

  return (
    <Box>
      <Card>
        <Box p={2} pt={4}>
          <Typography variant="h6">Action & Download</Typography>
          <List>
            {lessonActionList.map((lesson, index) =>
              <ListItem button key={`lesson-${lesson.label}-${index}`}>
                <ListItemIcon>
                  <Avatar alt={lesson.label} src="/static/images/avatar/1.jpg" />
                </ListItemIcon>
                <ListItemText primary={lesson.label} />
              </ListItem>
            )}
          </List>
        </Box>
      </Card>
    </Box>
  );
}

export default LessonActions;
