import React from 'react';
import { SchoolLogoContainer, SchoolLogoImage } from './styles';
import images from 'src/config/images';

function LessonIcon() {
  return (
    <SchoolLogoContainer>
      <SchoolLogoImage src={images.app.courseBanner} />
    </SchoolLogoContainer>
  );
}

export default LessonIcon;
