import React from 'react';
import { Box } from '@material-ui/core';
import { useRouter } from 'next/router';
import { actionStyles, LessonHeaderContainer } from './styles';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Chip } from '../../../../../components/shared';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';

const useStyles = makeStyles(theme => ({
  chipRoot: {
    backgroundColor: '#FFFFFF',
  },
}));

function LessonHeader({ image, title }) {
  const router = useRouter();
  const classes = useStyles();
  const actionClasses = actionStyles();

  const onPlay = () => {
    router.push(`${router.asPath}/play`);
  };

  const chipListData = [
    {
      id: 1,
      label: 'Year 7',
    },
    {
      id: 2,
      label: 'Help',
    },
    {
      id: 3,
      label: 'Health',
    },
  ];

  return (
    <LessonHeaderContainer image={image}>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <h1>Fire Safety</h1>
        <Button
          className={actionClasses.button}
          variant="contained"
          size="large"
          startIcon={<PlayCircleOutlineIcon />}
          onClick={onPlay}
        >
          Play Lesson
        </Button>
      </Box>
      <div>
        {chipListData.map(cList =>
          <Chip label={cList.label} className={classes.chipRoot} />
        )}
      </div>
    </LessonHeaderContainer>
  );
}

export default LessonHeader;
