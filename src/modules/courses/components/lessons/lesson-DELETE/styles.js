import styled, { css } from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';

export const SchoolLogoContainer = styled.div`
  display: flex;
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const SchoolLogoImage = styled.img`
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const actionStyles = makeStyles(theme => ({
  button: {
    borderRadius: 40,
    backgroundColor: '#F6AE32',
    color: '#ffffff',
    height: 54,
    fontSize: 20,
    '&:hover': {
      backgroundColor: '#f2a218'
    },
    '&:active': {
      backgroundColor: '#f2a218'
    }
  }
}));

export const LessonHeaderContainer = styled.div`
  margin: -30px -24px;
  background-color: #f2f2f2;
  padding: 24px;
  margin-bottom: 30px;
  height: 200px;
  display: flex;
  flex-direction: column;
  ${props =>
    css`
      background-image: url('${props.image}');
      background-size: cover;
      background-position: center;
    `}
  h1 {
    color: #ffffff;
    font-size: 40px;
  }
`;
