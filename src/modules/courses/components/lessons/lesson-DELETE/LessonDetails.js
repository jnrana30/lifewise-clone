import React from 'react';
import { Card, CardContent, Typography, Box } from '@material-ui/core';

function LessonDetails() {
  return (
    <Card>
      <CardContent>
        <Box p={2}>
          <Box mb={2}>
            <Typography variant="h5" component="h3">
              About the lesson
            </Typography>
          </Box>
          <Typography variant="body1">
            Salutantibus vitae elit libero, a pharetra augue. Mercedem aut
            nummos unde unde extricat, amaras. Contra legem facit qui id facit
            quod lex prohibet. Qui ipsorum lingua Celtae, nostra Galli
            appellantur. A communi observantia non est recedendum.
          </Typography>

          <Box mb={2} mt={3}>
            <Typography variant="h5" component="h3">
              What the children will learn
            </Typography>
          </Box>
          <Typography variant="body1">
            Salutantibus vitae elit libero, a pharetra augue. Mercedem aut
            nummos unde unde extricat, amaras. Contra legem facit qui id facit
            quod lex prohibet. Qui ipsorum lingua Celtae, nostra Galli
            appellantur. A communi observantia non est recedendum.
          </Typography>
        </Box>
      </CardContent>
    </Card>
  );
}

export default LessonDetails;
