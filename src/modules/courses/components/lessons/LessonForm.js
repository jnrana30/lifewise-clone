import React from 'react';
import { Box, Grid } from '@material-ui/core';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { getCategories, getTagsByType } from '../../api/coursesApis';
import _ from 'lodash';

function LessonForm({ lesson, schoolYears, onSubmit }) {
  const [loading, setLoading] = React.useState(false);
  const [category, setCategory] = React.useState({});
  const isEdit = lesson && lesson?.id !== '';

  const searchTags = async (search, type) => {
    const filter = {
      search
    };
    const res = await getTagsByType(type, filter);
    return res?.tags ? res?.tags : [];
  };

  return (
    <Form
      initialValues={{
        lessonName: lesson?.lessonName ? lesson?.lessonName : '',
        data: {
          icon: lesson?.data?.icon ? lesson?.data?.icon : '',
          background: lesson?.data?.background ? lesson?.data?.background : '',
          description: lesson?.data?.description
            ? lesson?.data?.description
            : ''
        },
        status: lesson?.status ? lesson?.status : 'active',
        yearGroup: lesson?.tags
          ? lesson?.tags.filter(tag => tag.tagType === 'yearGroup')
          : [],
        category: lesson?.tags
          ? lesson?.tags.filter(tag => tag.tagType === 'category')[0]
            ? lesson?.tags.filter(tag => tag.tagType === 'category')[0]
            : {}
          : {},
        topics: lesson?.tags
          ? lesson?.tags.filter(tag => tag.tagType === 'topic')
          : [],
        curriculum: lesson?.tags
          ? lesson?.tags.filter(tag => tag.tagType === 'curriculum')
          : []
      }}
      enableReinitialize={true}
      validationSchema={Yup.object().shape({
        lessonName: Yup.string().required('Lesson Name is required!'),
        data: Yup.object().shape({
          description: Yup.string().required('Please enter lesson description!')
        })
      })}
      onSubmit={async (values, form) => {
        let data = { ...values };
        data.tags = [
          ...(data?.yearGroup?.length
            ? data?.yearGroup.map(tag => tag.id)
            : []),
          ...(data?.topics?.length ? data?.topics.map(tag => tag.id) : []),
          ...(data?.curriculum?.length
            ? data?.curriculum.map(tag => tag.id)
            : []),
          data?.category && data?.category?.id
        ];
        data.yearGroups = data?.yearGroup?.length
          ? data?.yearGroup.map(tag => tag.id)
          : [];
        data = _.omit(data, ['category', 'curriculum', 'topics', 'yearGroup']);

        if (
          data.data.icon &&
          typeof data.data.icon === 'object' &&
          typeof data.data.icon?.key !== 'undefined' &&
          data.data.icon?.key !== ''
        ) {
          data.data.icon = data.data.icon.key;
        }
        if (
          data.data.background &&
          typeof data.data.background === 'object' &&
          typeof data.data.background?.key !== 'undefined' &&
          data.data.background?.key !== ''
        ) {
          data.data.background = data.data.background.key;
        }

        // if (data.yearGroups && data.yearGroups.length) {
        //   data.yearGroups = data.yearGroups.map(group => group.id);
        // }

        setLoading(true);
        onSubmit(data);
        setLoading(false);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="lesson-form"
            noValidate
          >
            <Grid container>
              {isEdit && (
                <Grid item xs={12} md={3}>
                  <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="center"
                    mt={4}
                  >
                    <Form.Field.Avatar
                      name="data.icon"
                      label="Choose Avatar"
                      folder="lessons/icons"
                    />
                  </Box>
                </Grid>
              )}
              <Grid item xs={12} md={isEdit ? 6 : 12}>
                <Form.Field.Input
                  fullWidth
                  name="lessonName"
                  label="Lesson Name"
                />
                <Form.Field.Input
                  fullWidth
                  multiline
                  name="data.description"
                  label="Lesson Description"
                />
                <Form.Field.File
                  fullWidth
                  folder="lessons/backgrounds"
                  name="data.background"
                  label="Background Image"
                />
                <Form.Field.AutoComplete
                  multiple={true}
                  fullWidth
                  showAvatar={false}
                  options={[]}
                  variant="standard"
                  remoteMethod={val => searchTags(val, 'yearGroup')}
                  name="yearGroup"
                  label="Year Group"
                  optLabel="tagName"
                  optValue="id"
                />
                <Form.Field.AutoComplete
                  multiple={false}
                  fullWidth
                  showAvatar={false}
                  options={[category]}
                  variant="standard"
                  remoteMethod={val => searchTags(val, 'category')}
                  name="category"
                  label="Choose Category"
                  optLabel="tagName"
                  optValue="id"
                />
                <Form.Field.AutoComplete
                  multiple={true}
                  fullWidth
                  showAvatar={false}
                  options={lesson?.tags ? lesson?.tags : []}
                  variant="standard"
                  remoteMethod={val => searchTags(val, 'topic')}
                  name="topics"
                  label="Choose Topics"
                  optLabel="tagName"
                  optValue="id"
                />
                <Form.Field.AutoComplete
                  multiple={true}
                  fullWidth
                  options={[]}
                  remoteMethod={val => searchTags(val, 'curriculum')}
                  variant="standard"
                  name="curriculum"
                  label="Curriculum"
                  optLabel="tagName"
                  optValue="id"
                />
                {isEdit && (
                  <React.Fragment>
                    <Box mt={2}>
                      <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                        loading={loading}
                        mt={2}
                      >
                        Submit
                      </Button>
                    </Box>
                  </React.Fragment>
                )}
              </Grid>
            </Grid>
          </form>
        );
      }}
    </Form>
  );
}

export default LessonForm;
