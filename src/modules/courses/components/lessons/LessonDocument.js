import React from 'react';
import { Typography, Box, Divider, Tabs, Tab, Paper } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';
import * as Yup from 'yup';
import ActivityLog from '../documents/ActivityLog';
import DocumentList from '../documents/DocumentList';
import _ from 'lodash';
import s3Helper from 'src/utils/s3Helper';

function LessonDocument({
  docType,
  onSubmit,
  onClose,
  lesson,
  lessonId,
  lessonData,
  versions
}) {
  const [loading, setLoading] = React.useState(false);
  const [tab, setTab] = React.useState(0);

  // const disableUpload =
  //   docType == 'articulateLink' && lessonData?.data?.articulateLinkLoading
  //     ? lessonData?.data?.articulateLinkLoading
  //     : false;

  const disableUpload = false;

  const uniqueData = [];
  const docVersions = [];
  if (versions) {
    Object.keys(versions).map(key => {
      if (
        versions[key].data[docType] &&
        versions[key].data[docType] !== '' &&
        uniqueData.indexOf(versions[key].data[docType]) < 0
      ) {
        uniqueData.push(versions[key].data[docType]);
        docVersions.push({
          key: versions[key].data[docType],
          link:
            docType != 'parentLink'
              ? s3Helper.getFileUrl(versions[key].data[docType])
              : versions[key].data[docType],
          createdOn: versions[key].createdOn,
          active: versions[key].data[docType] == lessonData?.data[docType]
        });
      }
    });
  }

  const files = [];
  const documentContent = () => {
    switch (docType) {
      case 'articulateLink':
        return {
          label: 'Main Articulate',
          description: 'Upload & publish your new classroom lesson',
          type: 'File',
          key: 'articulateLink',
          fileName: lessonId ? lessonId : '',
          folder: 'lessons/articulate',
          accept: '.zip,.rar,.7zip'
        };
        break;
      case 'remoteArticulateLink':
        return {
          label: 'Remote Articulate',
          description: 'Upload & publish your remote lesson',
          type: 'File',
          key: 'remoteArticulateLink',
          fileName: '',
          folder: 'lessons/content',
          accept: '.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf'
        };
        break;
      case 'lessonPlan':
        return {
          label: 'Lesson Plan',
          description: 'Upload & publish your lesson plan',
          type: 'File',
          key: 'lessonPlan',
          fileName: '',
          folder: 'lessons/content',
          accept: '.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf'
        };
        break;
      case 'activityWorkbook':
        return {
          label: 'Activity Workbook',
          description: 'Upload & manage the lesson activity workbook.',
          type: 'File',
          key: 'activityWorkbook',
          fileName: '',
          folder: 'lessons/content',
          accept: '.xlsx,.xls,image/*,.doc, .docx,.ppt, .pptx,.txt,.pdf'
        };
        break;
      case 'parentLink':
        return {
          label: 'Parent Link',
          description: 'Update the parent link.',
          type: 'Input',
          key: 'parentLink'
        };
        break;
    }
  };

  const defaultData = () => {
    switch (docType) {
      case 'articulateLink':
        return {
          articulateLink: ''
        };
        break;
      case 'remoteArticulateLink':
        return {
          remoteArticulateLink: ''
        };
        break;
      case 'activityWorkbook':
        return {
          activityWorkbook: ''
        };
        break;
      case 'lessonPlan':
        return {
          lessonPlan: ''
        };
        break;
      case 'parentLink':
        return {
          parentLink: lessonData?.data?.parentLink
            ? lessonData?.data?.parentLink
            : ''
        };
        break;
    }
  };

  const onChange = key => {
    onSubmit({
      data: {
        [docType]: key
      }
    });
  };

  const onUploadStart = () => {
    if (docType == 'articulateLink') {
      const data = {
        data: {
          articulateLinkLoading: true
        }
      };
      onSubmit(data);
    }
  };

  return (
    <div>
      <Typography variant="body1" color="textPrimary">
        <b>{documentContent()?.description}</b>
      </Typography>

      <Form
        initialValues={{
          data: defaultData()
        }}
        enableReinitialize={true}
        validationSchema={Yup.object().shape({
          data: Yup.object()
            .nullable(false)
            .required('This is a required field!')
            .test(
              'document-empty-check',
              'This is a required field!',
              async data => {
                if (documentContent().type === 'File') {
                  return (
                    data[documentContent().key] &&
                    !_.isEmpty(data[documentContent().key])
                  );
                } else {
                  return (
                    data[documentContent().key] &&
                    data[documentContent().key] !== ''
                  );
                }
              }
            )
        })}
        onSubmit={async values => {
          if (documentContent()?.type === 'File') {
            if (documentContent()?.key === 'articulateLink') {
              values.data = _.omit(values.data, ['articulateLink']);
              values.data.articulateLinkLoading = true;
              values.status = 'draft';
              // NO NEED TO SAVE DATA WHEN ARTICULATE AS WE ARE DOING THAT NOW ON UPLOAD START...
              onClose();
            } else {
              values.data[documentContent()?.key] =
                values.data[documentContent()?.key].key;
              onSubmit(values);
              onClose();
            }
          } else {
            onSubmit(values);
            onClose();
          }
        }}
      >
        {props => {
          return (
            <form
              onSubmit={e => {
                e.preventDefault();
                props.submitForm();
                return false;
              }}
              id="document-form"
              noValidate
            >
              {documentContent()?.type === 'File' ? (
                <React.Fragment>
                  {!disableUpload && (
                    <Form.Field.File
                      fullWidth
                      folder={documentContent()?.folder}
                      fileName={documentContent()?.fileName}
                      name={`data.${documentContent()?.key}`}
                      label={documentContent()?.label}
                      accept={documentContent()?.accept}
                      onUploadStart={onUploadStart}
                    />
                  )}
                </React.Fragment>
              ) : (
                <Form.Field.Input
                  fullWidth
                  name={`data.${documentContent()?.key}`}
                  label={documentContent()?.label}
                />
              )}
            </form>
          );
        }}
      </Form>
      <Box mt={4} mb={2}>
        <Paper>
          <Tabs
            value={tab}
            textColor="primary"
            onChange={(e, val) => {
              setTab(val);
            }}
            TabIndicatorProps={{
              style: {
                backgroundColor: '#ff9800'
              }
            }}
          >
            <Tab label="Version control" />
            <Tab label="Activity" />
          </Tabs>
        </Paper>
      </Box>

      {tab === 0 && <DocumentList data={docVersions} onChange={onChange} />}
      {tab === 1 && <ActivityLog />}
    </div>
  );
}

export default LessonDocument;
