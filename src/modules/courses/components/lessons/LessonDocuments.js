import React from 'react';
import {
  Box,
  Card,
  Divider,
  CardHeader,
  CardContent,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Grid,
  TextField
} from '@material-ui/core';
import { Button, Spinner } from 'src/components/shared';
import EditIcon from '@material-ui/icons/Edit';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import { withStyles } from '@material-ui/core/styles';
import s3Helper from 'src/utils/s3Helper';
import toast from 'src/utils/toast';

const styles = {
  cardContent: {
    padding: '16px 0px'
  }
};

const documents = [
  {
    type: 'File',
    title: 'Main Articulate',
    key: 'articulateLink',
    is: 'File'
  },
  {
    type: 'File',
    title: 'Remote Articulate',
    key: 'remoteArticulateLink'
  },
  {
    type: 'File',
    title: 'Activity Workbook ',
    key: 'activityWorkbook'
  },
  {
    type: 'Input',
    title: 'Parent Link',
    key: 'parentLink'
  },
  {
    type: 'File',
    title: 'Lesson Plan',
    key: 'lessonPlan'
  }
];

function LessonDocuments({ onEdit, lesson, versions }) {
  const classes = withStyles(styles);
  return (
    <React.Fragment>
      <Grid container spacing={2}>
        {documents.map((document, index) => {
          let link = lesson?.data[document.key];
          if (link && link !== '' && document.type === 'File') {
            link = s3Helper.getFileUrl(link);
          }
          const articulateLoading =
            document.key == 'articulateLink' &&
            lesson?.data?.articulateLinkLoading
              ? lesson?.data?.articulateLinkLoading
              : false;

          // let articulateLoading = false;
          // if (
          //   document.key == 'articulateLink' &&
          //   versions &&
          //   Object.keys(versions).length
          // ) {
          //   articulateLoading =
          //     Object.keys(versions).filter(
          //       versionId =>
          //         versions[versionId]?.data?.articulateLinkLoading === true
          //     ).length > 0;
          // }
          return (
            <Grid
              item
              lg={6}
              md={6}
              sm={12}
              key={`Lesson-documents-${document.key}-${index}`}
            >
              <Card>
                <CardHeader title={document.title} />
                <Divider />
                {articulateLoading ? (
                  <Box mt={2} mb={2}>
                    <List component="div" disablePadding dense>
                      <ListItem>
                        <ListItemText primary="Articulate zip file is being processed. Please check again in few mins." />
                        <ListItemSecondaryAction>
                          <Button iconButton={true} onClick={() => {}}>
                            <Spinner />
                          </Button>
                          <Button
                            iconButton={true}
                            onClick={() => {
                              onEdit(document.key);
                            }}
                          >
                            <EditIcon />
                          </Button>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </List>
                  </Box>
                ) : (
                  <Box mt={2} mb={2}>
                    {/* <TextField value={link} disabled={true} fullWidth /> */}
                    <List component="div" disablePadding dense>
                      <ListItem>
                        {link && link !== '' ? (
                          <TextField value={link} disabled={true} />
                        ) : (
                          <ListItemText primary="Link not set." />
                        )}
                        <ListItemSecondaryAction>
                          {/* {articulateLoading && (
                            <Button iconButton={true} onClick={() => {}}>
                              <Spinner />
                            </Button>
                          )} */}
                          {link && link !== '' && (
                            <Button
                              iconButton={true}
                              onClick={() => {
                                navigator.clipboard.writeText(link);
                                toast.success('Link copied to your clipboard!');
                              }}
                            >
                              <FileCopyIcon />
                            </Button>
                          )}
                          <Button
                            iconButton={true}
                            onClick={() => {
                              onEdit(document.key);
                            }}
                          >
                            <EditIcon />
                          </Button>
                        </ListItemSecondaryAction>
                      </ListItem>
                    </List>
                  </Box>
                )}
              </Card>
            </Grid>
          );
        })}
      </Grid>
    </React.Fragment>
  );
}

export default LessonDocuments;
