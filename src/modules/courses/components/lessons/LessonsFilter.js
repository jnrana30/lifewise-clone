import React from 'react';
import {
  FilterBar,
  FilterPicker,
  FilterSearch,
  FilterDrawer,
  AccessControl
} from 'src/components/App';
import { Box } from '@material-ui/core';
import { Button } from 'src/components/shared';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ViewListIcon from '@material-ui/icons/ViewList';
import AddIcon from '@material-ui/icons/Add';
import { useStyles } from './style';
import { lessonStatusOptions } from 'src/config';
import { getCategories, getTagsByType } from '../../api/coursesApis';
import useViewport from '../../../../utils/ViewPort';

function LessonsFilter({
  filters,
  setFilter,
  unsetFilter,
  viewType,
  setViewType,
  isAdmin,
  classesOption,
  yearGroupOption,
  categoryOption,
  topicsOption,
  userProfile,
  isAssemblies,
  ...props
}) {
  const classes = useStyles();
  const [status, setStatus] = React.useState(false);
  const [tags, setTags] = React.useState([]);
  const [isMount, setIsMount] = React.useState(true);
  const { isMobileView } = useViewport();

  React.useEffect(() => {
    if (filters.tags && filters.tags.length) {
      setTags(filters.tags);
    }
  }, []);

  React.useEffect(() => {
    if (isMount) {
      setIsMount(false);
      return;
    }
    if (tags && tags.length > 0) {
      // setFilter({ tags: `[${tags.map(tag => '"' + tag.id + '"')}]` });
      setFilter({ tags: tags });
    } else {
      unsetFilter('tags');
    }
  }, [tags]);

  const searchTags = async (search, type) => {
    const filter = {
      search
    };
    const res = await getTagsByType(type, filter);
    return res?.tags ? res?.tags : [];
  };

  const filterSearchOptions = (
    <FilterSearch
      placeholder="Search"
      value={filters['lessonName'] ? filters['lessonName'] : ''}
      onChange={val => {
        setFilter({
          lessonName: val
        });
      }}
      onClear={() => {
        unsetFilter('lessonName');
      }}
    />
  );

  const filterOptions = (
    <React.Fragment>
      {!isMobileView && filterSearchOptions}

      {!isAssemblies && (
        <React.Fragment>
          <FilterPicker
            title="Year Group"
            options={[]}
            onChange={val => {
              setTags([...tags, val]);
            }}
            remoteMethod={val => searchTags(val, 'yearGroup')}
            selected={
              tags.filter(tag => tag.tagType === 'yearGroup')[0]
                ? tags.filter(tag => tag.tagType === 'yearGroup')[0]
                : ''
            }
            onClear={() => {
              setTags(tags.filter(tag => tag.tagType !== 'yearGroup'));
            }}
            optValue="id"
            optLabel="tagName"
            ml={2}
            mr={1}
          />
          <FilterPicker
            title="Category"
            options={[]}
            onChange={val => {
              setTags([...tags, val]);
            }}
            remoteMethod={val => searchTags(val, 'category')}
            selected={
              tags.filter(tag => tag.tagType === 'category')[0]
                ? tags.filter(tag => tag.tagType === 'category')[0]
                : ''
            }
            onClear={() => {
              setTags(tags.filter(tag => tag.tagType !== 'category'));
            }}
            optValue="id"
            optLabel="tagName"
            ml={2}
            mr={1}
          />
          <FilterPicker
            title="Topics"
            options={[]}
            onChange={val => {
              setTags([...tags, val]);
            }}
            remoteMethod={val => searchTags(val, 'topic')}
            selected={
              tags.filter(tag => tag.tagType === 'topic')[0]
                ? tags.filter(tag => tag.tagType === 'topic')[0]
                : ''
            }
            onClear={() => {
              setTags(tags.filter(tag => tag.tagType !== 'topic'));
            }}
            optValue="id"
            optLabel="tagName"
            ml={2}
            mr={1}
          />
          <AccessControl level={6}>
            <FilterPicker
              title="Status"
              options={lessonStatusOptions}
              onChange={val => {
                setFilter({
                  status: val.value
                });
              }}
              selected={filters['status'] ? filters['status'] : ''}
              onClear={() => {
                unsetFilter('status');
              }}
              ml={2}
            />
          </AccessControl>
        </React.Fragment>
      )}
    </React.Fragment>
  );

  return (
    <FilterBar>
      <Box display="flex" justifyContent="space-between">
        {!isMobileView && <Box display="flex">{filterOptions}</Box>}
        {isMobileView && (
          <Box display="flex">
            <FilterDrawer
              title="Filters"
              ml={2}
              mr={1}
              filterOptions={filterOptions}
              searchBoxFilter={filterSearchOptions}
            />
          </Box>
        )}

        <Box display="flex">
          {!isAdmin && !isMobileView && (
            <React.Fragment>
              <Button
                className={classes.iconButton}
                iconButton={true}
                onClick={() => {
                  setViewType('grid');
                }}
                color={viewType === 'grid' ? 'primary' : 'default'}
              >
                <ViewModuleIcon />
              </Button>
              <Button
                className={classes.iconButton}
                iconButton={true}
                onClick={() => {
                  setViewType('list');
                }}
                color={viewType === 'list' ? 'primary' : 'default'}
              >
                <ViewListIcon />
              </Button>
            </React.Fragment>
          )}
          {userProfile?.role?.level > 5 && (
            <Box display="flex" ml={1}>
              <Button
                className={classes.addButton}
                variant="contained"
                color="primary"
                aria-controls="simple-menu"
                onClick={() => {
                  props.setDrawerOpen(true);
                }}
                startIcon={<AddIcon />}
              >
                Add Lesson
              </Button>
            </Box>
          )}
        </Box>
      </Box>
    </FilterBar>
  );
}

export default LessonsFilter;
