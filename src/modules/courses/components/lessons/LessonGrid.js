import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Grid, Box, Typography } from '@material-ui/core';
import { Drawer } from 'src/components/shared';
import LessonCard from './LessonCard';
import Pagination from '@material-ui/lab/Pagination';
import Link from 'next/link';
import _ from 'lodash';
import { makeStyles } from '@material-ui/core/styles';
import AddLessonToClassForm from '../lesson/AddLessonToClassForm';
import CompleteLesson from '../completeLesson';

import { assignLessonsToClass } from '../../../../modules/school/api/schoolApis';
import toast from 'src/utils/toast';

import { ContentCardContainer } from 'src/components/shared/Form/File/styles';
import {
  CardSkeleton,
  CardEmpty,
  GridContainer,
  ViewMoreCard
} from 'src/components/shared/CardGrid';

const useStyles = makeStyles(theme => ({
  gridRoot: {
    width: '100%',
    height: 'auto',
    [theme.breakpoints.down(1615)]: {
      maxWidth: 336
    },
    [theme.breakpoints.up(1615)]: {
      maxWidth: 376
    },
    [theme.breakpoints.down(600)]: {
      maxWidth: '100%'
    }
  }
}));

function LessonGrid({
  lessons,
  onComplete,
  paging,
  loading,
  onPaging,
  showPaging,
  showViewMore,
  showMoreText,
  courseId,
  selectedClass,
  toggleShowMore,
  ...props
}) {
  const classes = useStyles();

  const isAdmin = props.user?.profile?.role?.level > 5;
  const [formLoading, setFormLoading] = React.useState(false);
  const [addToClassLesson, setAddToClassLesson] = React.useState({});

  const [action, setAction] = React.useState('');
  const [actionLesson, setActionLesson] = React.useState({});

  const pages =
    paging.records > 0 ? Math.ceil(paging.records / paging.perPage) : 0;

  const onAddToClass = async classes => {
    setFormLoading(true);
    const promises = [];
    if (classes.add && classes.add.length > 0) {
      classes.add.map(async classId => {
        promises.push(
          await assignLessonsToClass(
            props.mySchool.id,
            classId,
            [addToClassLesson.id],
            'add'
          )
        );
      });
    }
    if (classes.remove && classes.remove.length > 0) {
      classes.remove.map(async classId => {
        promises.push(
          await assignLessonsToClass(
            props.mySchool.id,
            classId,
            [addToClassLesson.id],
            'remove'
          )
        );
      });
    }
    await Promise.all(promises)
      .then(res => {
        toast.success('Course assigned successfully.');
        setFormLoading(false);
        setAddToClassLesson({});
      })
      .catch(err => {
        console.log(err);
        setFormLoading(false);
        toast.success(
          'An error occurred while adding class to the course. Please try again.'
        );
      });
  };

  const addToClass = lesson => {
    setAddToClassLesson(lesson);
  };

  const onLessonAction = (action, lesson) => {
    setAction(action);
    setActionLesson(lesson);
  };

  return (
    <React.Fragment>
      <ContentCardContainer>
        <Grid container spacing={2}>
          {loading ? (
            <React.Fragment>
              {_.times(paging.perPage, i => (
                <GridContainer
                  key={`skeleton-lesson-card-${i}`}
                  item
                  xs={12}
                  s={12}
                  sm={6}
                  lg={3}
                  xl={3}
                  md={3}
                >
                  <CardSkeleton key={`lessons-loader-${i}`} />
                </GridContainer>
              ))}
            </React.Fragment>
          ) : (
            <React.Fragment>
              {lessons && lessons.length > 0 ? (
                <React.Fragment>
                  {(lessons || []).map((lesson, index) => {
                    return (
                      <Grid
                        key={`lesson-${index}`}
                        className={`${classes.gridRoot} ${index ? '' : props.showJoyRide ? 'joy-ride-lesson-click' : ''}`}
                        item
                        xs={12}
                        s={12}
                        sm={6}
                        lg={3}
                        xl={3}
                        md={3}
                      >
                        <LessonCard
                          lessonIndex={index}
                          onComplete={onComplete}
                          lesson={lesson}
                          isAdmin={isAdmin}
                          addToClass={addToClass}
                          onLessonAction={onLessonAction}
                        />
                      </Grid>
                    );
                  })}
                  {showViewMore && (
                    <Grid
                      className={classes.gridRoot}
                      item
                      xs={12}
                      s={12}
                      sm={6}
                      lg={3}
                      xl={3}
                      md={3}
                    >
                      <ViewMoreCard onClick={toggleShowMore}>
                        <Typography variant="subtitle2" color="textSecondary">
                          {showMoreText}
                        </Typography>
                      </ViewMoreCard>
                    </Grid>
                  )}
                </React.Fragment>
              ) : (
                <CardEmpty title="No lessons found!" />
              )}
            </React.Fragment>
          )}
        </Grid>
        {showPaging && (
          <Grid container>
            <Box m="auto" mt={4}>
              <Pagination
                count={pages}
                page={paging.page}
                shape="rounded"
                onChange={(e, val) => {
                  onPaging(val - 1);
                }}
              />
            </Box>
          </Grid>
        )}
      </ContentCardContainer>
      <Drawer
        open={!!addToClassLesson?.id}
        title="Add lessons to class"
        onClose={() => {
          setAddToClassLesson({});
        }}
        form="lesson-add-to-class-form"
        buttonLoading={formLoading}
      >
        <AddLessonToClassForm
          onSubmit={onAddToClass}
          lesson={addToClassLesson}
        />
      </Drawer>
      {action && action !== '' && actionLesson?.id && (
        <CompleteLesson
          action={action}
          lesson={actionLesson}
          courseId={courseId}
          selectedClass={selectedClass}
          onClose={() => {
            setAction('');
            setActionLesson({});
          }}
        />
      )}
      {/* <Drawer
        open={completeLesson}
        title="Complete Lesson"
        onClose={() => {
          setCompleteLesson(false);
        }}
        fullWidth={true}
        form="lesson-complete-form"
        buttonText="Continue"
      >
        
      </Drawer> */}
    </React.Fragment>
  );
}

LessonGrid.propTypes = {
  showPaging: PropTypes.bool,
  showViewMore: PropTypes.bool,
  showMoreText: PropTypes.string,
  toggleShowMore: PropTypes.func,
  courseId: PropTypes.string,
  selectedClass: PropTypes.object
};

LessonGrid.defaultProps = {
  showPaging: true,
  showViewMore: false,
  showMoreText: '',
  toggleShowMore: () => {},
  courseId: '',
  selectedClass: {}
};

const mapStateToProps = state => ({
  user: state.auth.user,
  mySchool: state.school.mySchool
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(LessonGrid);
// export default LessonGrid;
