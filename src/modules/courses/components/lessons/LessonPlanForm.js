import React from 'react';
import {
  Box,
  Card,
  CardHeader,
  CardContent,
  Typography,
  Grid
} from '@material-ui/core';
import { Form, Button } from 'src/components/shared';
import EditIcon from '@material-ui/icons/Edit';
import { Formik, FieldArray } from 'formik';
import { lessonContentOptions } from 'src/config';
import * as Showdown from 'showdown';
import { FixedButton } from './style';

const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true
});

function LessonPlanForm({ lesson, onSubmit }) {
  const [loading, setLoading] = React.useState(false);
  // const { content } = lesson;
  const content = lesson?.content ? lesson?.content : [];
  const tags = lesson?.tags ? lesson?.tags : [];

  let contentOptions = lessonContentOptions;
  if (content && content.length) {
    contentOptions = lessonContentOptions.map(itm => ({
      ...itm,
      ...content.find(item => item.type === itm.type)
    }));
  }
  if (tags.length && tags.find(t =>
    t.tagType === 'curriculum' && t.tagNameIndex === "assemblies")
  ) {
    contentOptions = lessonContentOptions.filter(lc => lc.type === 'introduction').map(itm => ({
      ...itm,
      ...content.find(item => item.type === 'introduction')
    }));
  }

  return (
    <Formik
      initialValues={{ content: contentOptions }}
      enableReinitialize={true}
      onSubmit={async values => {
        const data = { ...values };
        if (data.content && data.content.length) {
          data.content = data.content.map(item => ({
            type: item.type,
            content: item.content,
            data: item.data
          }));
        }
        setLoading(true);
        onSubmit(data);
        setLoading(false);
      }}
    >
      {({ values, ...props }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            props.submitForm();
            return false;
          }}
          noValidate
        >
          <FieldArray
            name="content"
            render={arrayHelpers => (
              <Grid container spacing={2}>
                {values.content &&
                  values.content.length > 0 &&
                  values.content.map((lessonPlan, index) => (
                    <React.Fragment key={`lessonPlan-${index}`}>
                      <Grid item lg={6} md={6} sm={12}>
                        <Card>
                          <CardHeader
                            subheader={lessonPlan.title}
                            action={
                              <Button
                                iconButton={true}
                                onClick={() => {
                                  lessonPlan.edit = !lessonPlan.edit;
                                  arrayHelpers.replace(index, lessonPlan);
                                }}
                              >
                                <EditIcon />
                              </Button>
                            }
                          />
                          <CardContent>
                            <Box display="flex">
                              {lessonPlan.edit === true ? (
                                <Form.Field.Markdown
                                  fullWidth
                                  multiline
                                  name={`content[${index}].data`}
                                  label={lessonPlan.title}
                                />
                              ) : (
                                <React.Fragment>
                                  {lessonPlan.data && lessonPlan.data !== '' ? (
                                    <div
                                      className="text-container"
                                      dangerouslySetInnerHTML={{
                                        __html: converter.makeHtml(
                                          lessonPlan.data
                                        )
                                      }}
                                    />
                                  ) : (
                                    <Typography
                                      variant="body2"
                                      color="textSecondary"
                                      onClick={() => {
                                        lessonPlan.edit = !lessonPlan.edit;
                                        arrayHelpers.replace(index, lessonPlan);
                                      }}
                                    >
                                      Click here to set the value
                                    </Typography>
                                  )}
                                </React.Fragment>
                              )}
                            </Box>
                          </CardContent>
                        </Card>
                      </Grid>
                    </React.Fragment>
                  ))}
              </Grid>
            )}
          />
          <Box mt={2}>
            <FixedButton loading={loading}>Save</FixedButton>
          </Box>
        </form>
      )}
    </Formik>
  );
}

export default LessonPlanForm;
