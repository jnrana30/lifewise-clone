import React from 'react';
import PropTypes from 'prop-types';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Box,
  Avatar,
  Badge
} from '@material-ui/core/';
import Skeleton from '@material-ui/lab/Skeleton';
import { Tag } from 'src/components/App';
import { makeStyles } from '@material-ui/core/styles';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { Button, Chip, ContextMenu } from 'src/components/shared';
import Link from 'next/link';
import images from 'src/config/images';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityIcon from '@material-ui/icons/Visibility';
import s3Helper from 'src/utils/s3Helper';
import { useRouter } from 'next/router';

const useStyles = makeStyles(theme => ({
  paperWrapper: {
    maxWidth: 1440,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  avatar: {
    width: theme.spacing(19),
    height: 'auto',
    borderRadius: 4
  },
  tagsContainer: {
    // marginLeft: '-4px',
    // position: 'absolute',
    // bottom: 20
  },
  checkBadge: {
    top: '4%',
    right: '4%'
  },
  tableImageCol: {
    width: theme.spacing(19)
  },
  tableActionCol: {
    maxWidth: 200,
    width: 200
  },
  contentCol: {
    position: 'relative',
    verticalAlign: 'unset'
  },
  lessonDesc: {
    width: 550
  },
  contextMenu: {
    '& .MuiIconButton-root': {
      padding: 0
    }
  }
}));

function LessonList({
  lessons,
  isAdmin,
  loading,
  paging,
  onPaging,
  onEdit,
  showPaging,
  rowsPerPageOptions,
  ...props
}) {
  const classes = useStyles();
  const router = useRouter();

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  return (
    <Paper className={classes.paperWrapper}>
      <TableContainer>
        <Table aria-label="customized table" className="paddedTable">
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={4}>
                  <Skeleton animation="wave" />
                </TableCell>
              </TableRow>
            ) : (
              <React.Fragment>
                {lessons && lessons.length > 0 ? (
                  <React.Fragment>
                    {lessons.map((lesson, index) => {
                      const icon =
                        lesson?.content?.data?.icon &&
                        lesson?.content?.data?.icon !== ''
                          ? s3Helper.getFileUrl(lesson?.content?.data?.icon)
                          : images.app.lessonPlaceholder;

                      // const background =
                      //   lesson?.content?.data?.background &&
                      //   lesson?.content?.data?.background !== ''
                      //     ? s3Helper.getFileUrl(
                      //         lesson?.content?.data?.background
                      //       )
                      //     : images.app.lessonPlaceholder;

                      const description =
                        lesson?.content?.data?.description &&
                        lesson?.content?.data?.description !== ''
                          ? lesson?.content?.data?.description
                          : '';

                      const isCompleted =
                        lesson?.status && lesson?.status === 'completed';

                      return (
                        <TableRow key={`lesson-list-${index}`}>
                          <TableCell className={classes.tableImageCol}>
                            <Box display="flex" alignItems="center">
                              <Link
                                href={
                                  isAdmin
                                    ? `/lessons/admin/${lesson.id}`
                                    : `/lessons/${lesson.id}`
                                }
                              >
                                {isCompleted ? (
                                  <Badge
                                    overlap="circle"
                                    anchorOrigin={{
                                      vertical: 'top',
                                      horizontal: 'right'
                                    }}
                                    classes={{
                                      badge: classes.checkBadge
                                    }}
                                    badgeContent={
                                      <CheckCircleIcon color="primary" />
                                    }
                                  >
                                    <Avatar
                                      className={classes.avatar}
                                      src={icon}
                                      size={36}
                                    />
                                  </Badge>
                                ) : (
                                  <Avatar
                                    className={classes.avatar}
                                    src={icon}
                                    size={36}
                                  />
                                )}

                                {/* </Badge> */}
                              </Link>
                            </Box>
                          </TableCell>
                          <TableCell className={classes.contentCol}>
                            <Box mb={2}>
                              <Link
                                href={
                                  isAdmin
                                    ? `/lessons/admin/${lesson.id}`
                                    : `/lessons/${lesson.id}`
                                }
                              >
                                <Typography variant="body1">
                                  <b>{lesson.lessonName}</b>
                                </Typography>
                              </Link>
                              <Typography
                                variant="body2"
                                color="textSecondary"
                                className={classes.lessonDesc}
                              >
                                {description}
                              </Typography>
                            </Box>
                            {lesson?.content?.tags?.length && (
                              <div className={classes.tagsContainer}>
                                {lesson?.content?.tags?.map((tag, tagIndex) => {
                                  return (
                                    <Tag
                                      tag={tag}
                                      key={`lesson-${lesson.id}-tag-${tagIndex}`}
                                    />
                                  );
                                })}
                              </div>
                            )}
                          </TableCell>

                          <TableCell
                            size={'small'}
                            className={classes.tableActionCol}
                            align="right"
                          >
                            <Box
                              display="flex"
                              alignItems="center"
                              justifyContent="flex-end"
                            >
                              <Link
                                href={
                                  isAdmin
                                    ? `/lessons/admin/${lesson.id}`
                                    : `/lessons/${lesson.id}`
                                }
                              >
                                <Button variant="outlined" color="primary">
                                  View lesson
                                </Button>
                              </Link>
                              {isAdmin && (
                                <ContextMenu
                                  options={[
                                    {
                                      label: 'Edit',
                                      icon: <EditIcon />,
                                      onClick: () => {
                                        onEdit(lesson);
                                      }
                                    },
                                    {
                                      label: 'Preview',
                                      icon: <VisibilityIcon />,
                                      onClick: () => {
                                        window.open(lesson.id);
                                      }
                                    }
                                  ]}
                                />
                              )}
                            </Box>
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </React.Fragment>
                ) : (
                  <TableRow>
                    <TableCell colSpan={4} align="center">
                      <Typography color="textSecondary">
                        No lessons found
                      </Typography>
                    </TableCell>
                  </TableRow>
                )}
              </React.Fragment>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      {showPaging && (
        <TablePagination
          component="div"
          count={paging?.records ? paging.records : 0}
          rowsPerPage={paging.perPage}
          page={paging.page - 1}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
          rowsPerPageOptions={rowsPerPageOptions}
        />
      )}
    </Paper>
  );
}

LessonList.propTypes = {
  showPaging: PropTypes.bool,
  rowsPerPageOptions: PropTypes.array
};

LessonList.defaultProps = {
  showPaging: true,
  rowsPerPageOptions: [10, 25, 50, 100]
};

export default LessonList;
