import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  CardActions,
  Typography,
  Box,
  Badge,
  Tooltip
} from '@material-ui/core';
import { ContextMenu } from 'src/components/shared';
import { useRouter } from 'next/router';
import images from 'src/config/images';
import { CardGrid } from '../../../../components/shared/CardGrid';
import s3Helper from 'src/utils/s3Helper';
import { isColorLightOrDark, getLessonActions } from 'src/utils/helpers';
import { Tag } from 'src/components/App';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  cardRoot: {
    position: 'relative',
    width: '100%',
    height: '100%',
    [theme.breakpoints.down(1615)]: {
      maxWidth: 320
    },
    [theme.breakpoints.down(600)]: {
      maxWidth: '100%'
    },
    [theme.breakpoints.up(1615)]: {
      maxWidth: 360
    }
  },
  cardHeadingEllipsis: {
    overflow: 'hidden',
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    fontWeight: 700,
    [theme.breakpoints.down(1615)]: {
      maxWidth: 'calc(100% - 80px)'
    },
    [theme.breakpoints.down(600)]: {
      maxWidth: '100%'
    },
    [theme.breakpoints.up(1615)]: {
      maxWidth: 336
    }
  },
  media: {
    paddingTop: '50%'
  },
  expand: {
    marginLeft: 'auto'
  },
  tagsContainer: {
    '& > *': {
      margin: theme.spacing(0.25),
      '& .MuiChip-label': {
        color: '#0000008a',
        fontSize: 10
      }
    }
  },
  tag: {},
  lessonCategory: {
    width: 'auto',
    display: 'inline-block',
    marginTop: 6,
    top: 14,
    color: '#2B6B00',
    fontSize: 14
  },
  cardActions: {
    paddingTop: 0,
    paddingBottom: 0,
    display: 'flex',
    justifyContent: 'space-between'
  },
  mainCardContent: {
    paddingBottom: 0,
    padding: '17px 12px 12px 12px'
  },
  cardContentHeading: {
    position: 'relative',
    '& .MuiTypography-root': {
      position: 'absolute',
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      width: '100%',
      lineHeight: 1.1,
      textOverflow: 'ellipsis'
    }
  },
  cardContentSubHeading: {
    lineHeight: 1.1,
    fontSize: 14,
    display: '-webkit-box',
    '-webkit-line-clamp': 2,
    '-webkit-box-orient': 'vertical',
    overflow: 'hidden'
  },
  checkBadge: {
    top: '4%',
    right: '4%'
  },
  checkBadgeRoot: {
    position: 'absolute',
    top: 25,
    right: 25
  },
  grayScale: {
    filter: 'grayscale(100%)',
    opacity: 0.6
  }
}));

export default function LessonCard({ lesson, isAdmin, ...props }) {
  const classes = useStyles();
  const router = useRouter();
  const [anchorEl, setAnchorEl] = useState(null);
  const [lessonTags, setLessonTags] = useState([]);
  const [nonSelectedTag, setNonSelectedTag] = useState([]);
  const open = Boolean(anchorEl);

  useEffect(() => {
    if (
      lesson?.content?.tags &&
      lesson?.content?.tags.filter(t => t.tagType !== 'category').length > 3
    ) {
      setLessonTags(
        lesson?.content?.tags.filter(t => t.tagType !== 'category').slice(0, 2)
      );
      setNonSelectedTag(
        lesson?.content?.tags.filter(t => t.tagType !== 'category').slice(2)
      );
    } else {
      setLessonTags(lesson?.content?.tags || []);
    }
  }, [lesson?.content?.tags]);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const icon =
    lesson?.content?.data?.icon && lesson?.content?.data?.icon !== ''
      ? s3Helper.getFileUrl(lesson?.content?.data?.icon)
      : images.app.lessonPlaceholder;

  const description =
    lesson?.content?.data?.description &&
    lesson?.content?.data?.description !== ''
      ? lesson?.content?.data?.description
      : '';

  const category = lesson?.content?.tags
    ? lesson?.content?.tags.filter(tag => tag.tagType == 'category')[0]
    : {};

  const lessonAction = (action, lesson) => {
    console.log('action : ', action);
    console.log('lesson : ', lesson);
  };

  const actions = getLessonActions(lesson?.content?.data);

  const isCompleted = lesson?.status && lesson?.status === 'completed';

  return (
    <CardGrid className={classes.cardRoot}>
      <CardActionArea
        onClick={() => {
          router.push(`/lessons/${lesson.id}`);
        }}
      >
        <CardMedia
          className={clsx({
            [classes.media]: true,
            [classes.grayScale]: isCompleted
          })}
          image={icon}
          title={lesson.lessonName}
        />
      </CardActionArea>
      {isCompleted && (
        <Badge
          overlap="circle"
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right'
          }}
          className={classes.checkBadgeRoot}
          classes={{
            badge: classes.checkBadge
          }}
          badgeContent={<CheckCircleIcon fontSize="large" color="primary" />}
        ></Badge>
      )}
      <CardContent
        className={clsx({
          [classes.mainCardContent]: true,
          [classes.grayScale]: isCompleted
        })}
      >
        <Typography
          variant="body2"
          component="p"
          className={classes.cardContentSubHeading}
        >
          <b>{description}</b>
        </Typography>

        {category && category?.tagName && (
          <Typography variant="body2" className={classes.lessonCategory}>
            {category?.tagName}
          </Typography>
        )}
      </CardContent>

      <CardActions className={classes.cardActions} disableSpacing>
        <div>
          {Boolean(lesson?.content?.tags?.length) && (
            <div className={classes.tagsContainer}>
              {lessonTags.map((tag, index) => {
                if (tag.tagType === 'category') return null;
                return (
                  <Tag
                    tag={tag}
                    size={'small'}
                    className={classes.tag}
                    key={`lesson-${props.lessonIndex}-tags-${index}`}
                  />
                );
              })}
              {lesson?.content?.tags.filter(t => t.tagType !== 'category')
                .length > 3 && (
                <span>
                  <Tag
                    tag={{
                      tagName: `+${
                        lesson?.content?.tags.filter(
                          t => t.tagType !== 'category'
                        ).length - 2
                      }`
                    }}
                    size={'small'}
                    className={classes.tag}
                    tootTipMessage={nonSelectedTag
                      .map(t => t.tagName)
                      .join(', ')}
                  />
                </span>
              )}
            </div>
          )}
        </div>

        <ContextMenu
          options={[
            ...(!isAdmin
              ? [
                  ...(actions &&
                    actions.length &&
                    actions.map(action => ({
                      label: action.title,
                      onClick: () => {
                        if (action.action && action.action !== '') {
                          props.onLessonAction(action.action, lesson);
                        } else if (action.href && action.href !== '') {
                          window.open(action.href, '_blank');
                        }
                      }
                    }))),
                  {
                    label: 'Add to class',
                    onClick: () => {
                      props.addToClass(lesson);
                    }
                  }
                ]
              : [])
          ]}
        />
      </CardActions>
    </CardGrid>
  );
}
