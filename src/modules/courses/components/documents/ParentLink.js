import React from 'react';
import { Typography, Box, Divider, Tabs, Tab, Paper } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';
import * as Yup from 'yup';
import ActivityLog from './ActivityLog';
import DocumentList from './DocumentList';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

function ParentLink() {
  const [loading, setLoading] = React.useState(false);
  const [tab, setTab] = React.useState(0);

  const links = [
    {
      name: 'http://google.com/demo',
      date: '2021/01/01',
      isLink: true
    },
    {
      name: 'http://google.com/hello-world',
      date: '2021/05/21',
      isLink: true
    }
  ];

  return (
    <div>
      <Box display="flex" justifyContent="space-between" alignItems="center">
        <Typography variant="body1" color="textPrimary">
          <b>Update the parent link.</b>
        </Typography>
        <Button iconButton={true} onClick={() => {}} style={{ padding: 0 }}>
          <OpenInNewIcon />
        </Button>
      </Box>
      <Box mt={2} mb={0}>
        <Form
          initialValues={{
            parentLink: 'https://lifewise.co.uk/parents-link'
          }}
          validationSchema={Yup.object().shape({
            parentLink: Yup.string().required('Parent Link is required!')
          })}
          onSubmit={async (values, form) => {
            console.log('values : ', values);
          }}
        >
          {props => {
            return (
              <form
                onSubmit={e => {
                  e.preventDefault();
                  props.submitForm();
                  return false;
                }}
                id="lesson-form"
                noValidate
              >
                <Form.Field.Input
                  fullWidth
                  variant="outlined"
                  name="parentLink"
                  label="Parent Link"
                />
              </form>
            );
          }}
        </Form>
      </Box>
      <Box mt={2} mb={2}>
        <Paper>
          <Tabs
            value={tab}
            textColor="primary"
            onChange={(e, val) => {
              setTab(val);
            }}
            TabIndicatorProps={{
              style: {
                backgroundColor: '#ff9800'
              }
            }}
          >
            {/* <Tab label="Version control" /> */}
            <Tab label="Activity" />
          </Tabs>
        </Paper>
      </Box>

      {/* {tab === 0 && <DocumentList data={links} />} */}
      {/* {tab === 1 && <ActivityLog />} */}
      <ActivityLog />
    </div>
  );
}

export default ParentLink;
