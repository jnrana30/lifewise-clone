import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  ListItemSecondaryAction,
  Switch,
  Divider
} from '@material-ui/core';
import images from 'src/config/images';
import { Button } from 'src/components/shared';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import DeleteIcon from '@material-ui/icons/Delete';
import OpenInNewIcon from '@material-ui/icons/OpenInNew';

import moment from 'moment';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  FileIcon: {
    maxWidth: 36
  },
  docItem: {
    paddingLeft: theme.spacing(0)
  }
}));

export default function DocumentList({ data, onChange }) {
  const classes = useStyles();
  return (
    <List disablePadding>
      {data.map((item, index) => {
        return (
          <React.Fragment key={`document-list-${index}`}>
            <ListItem dense className={classes.docItem}>
              <ListItemIcon>
                <Switch
                  color="secondary"
                  checked={item.active}
                  disabled={item.active}
                  onChange={(event, val) => {
                    onChange(item.key);
                  }}
                />
              </ListItemIcon>
              <ListItemText
                primary={typeof item.key === 'string' ? item.key : ''}
                secondary={`${moment(item.createdOn).fromNow()}`}
              />
              <ListItemSecondaryAction>
                <Button
                  iconButton={true}
                  onClick={() => {
                    window.open(item.link, '_blank');
                  }}
                >
                  <OpenInNewIcon />
                </Button>
              </ListItemSecondaryAction>
            </ListItem>
            <Divider />
          </React.Fragment>
        );
      })}
    </List>
  );
}
