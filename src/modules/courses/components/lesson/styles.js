import styled, { css } from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';

export const ContentContainer = styled.div`
  width: 100%;
  max-width: 1500px;
  display: block;
  box-sizing: border-box;
  margin-left: auto;
  margin-right: auto;
  z-index: 1;
`;

export const SchoolLogoContainer = styled.div`
  display: flex;
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const SchoolLogoImage = styled.img`
  width: 140px;
  height: 140px;
  border-radius: 140px;
`;

export const actionStyles = makeStyles(theme => ({
  button: {
    borderRadius: theme.spacing(5),
    backgroundColor: '#F6AE32',
    color: '#ffffff',
    height: theme.spacing(5.5),
    padding: theme.spacing(0),
    fontSize: 18,
    maxWidth: 200,
    [theme.breakpoints.down('xs')]: {
      fontSize: 12
    },
    '& .MuiButton-label': {
      textTransform: 'capitalize'
    },
    '&:hover': {
      backgroundColor: '#f2a218'
    },
    '&:active': {
      backgroundColor: '#f2a218'
    }
  }
}));

export const LessonHeaderContainer = styled.div`
  margin: -32px -25px;
  background-color: #f2f2f2;
  padding: 24px;
  margin-bottom: 30px;
  min-height: 200px;
  display: flex;
  flex-direction: column;
  position: relative;
  border-bottom: 28px solid #F6B036;
  ${props =>
    css`
      background-image: url('${props.image}');
      background-size: cover;
      background-position: center;
    `}
  h1 {
    font-size: 40px;
  }
`;

export const HeaderOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #FFFFFF;
  z-index: 0;
`;

export const LessonActionImage = styled.div`
  ${props =>
    css`
      background-image: url('${props.image}');
    `}
  display: block;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  padding-top: 50%;
  margin: -12px -6px 16px -6px;
`;
