import React, { useEffect, useRef, useState } from 'react';
import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Card,
  Box,
  Typography,
  Avatar,
  Link
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from 'src/components/shared';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import s3Helper from 'src/utils/s3Helper';
import { isColorLightOrDark, getLessonActions } from 'src/utils/helpers';

import { actionStyles, LessonActionImage } from './styles';
import images from 'src/config/images';

const useStyles = makeStyles(theme => ({
  lessonActionBox: {
    top: -170,
    width: 'calc(100% - 24px)',
    height: 'max-content',
    transform: 'translate3d(0, 0, 0)',
    willChange: 'position, transform',
    position: 'absolute',
    overflow: 'auto',
    background: '#fff',
    borderRadius: theme.spacing(0.5),
    padding: `${theme.spacing(1.5)}px 6px`,
    boxShadow:
      '0px 2px 1px -1px rgb(0 0 0 / 20%), 0px 1px 1px 0px rgb(0 0 0 / 14%), 0px 1px 3px 0px rgb(0 0 0 / 12%)',
    [theme.breakpoints.down('sm')]: {
      width: '100%',
      height: '100%',
      position: 'unset'
    }
  },
  actionImage: {
    display: 'block',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center'
  },
  buttonBottomListItem: {
    textAlign: 'center',
    marginTop: theme.spacing(1.5),
    paddingTop: 0,
    paddingBottom: 0
  },
  cardWrapper: {
    boxShadow: 'unset'
  },
  listItem: {
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1)
  },
  listItemIcon: {
    paddingLeft: 0,
    paddingRight: 0,
    minWidth: theme.spacing(4)
  },
  avatarImg: {
    width: theme.spacing(3),
    height: theme.spacing(3)
  }
}));

function LessonActions({ lesson, lessonData, ...props }) {
  const classes = useStyles();
  const classesAction = actionStyles();
  const lessonBox = useRef();

  useEffect(() => {
    const mainTag = document.getElementsByTagName('main')[0];
    mainTag &&
      mainTag.addEventListener('scroll', event => {
        if (event.target.scrollTop > 32) {
          lessonBox.current.style.top = `${
            event.target.scrollTop - (170 + 32)
          }px`;
        } else {
          lessonBox.current.style.top = '-170px';
        }
      });
    return () => {
      if (mainTag) {
        mainTag.removeEventListener('scroll', () => {});
      }
    };
  }, []);

  const data = lessonData?.data ? lessonData?.data : {};
  const actionList = getLessonActions(data);
  let activityLink = '';
  if (
    (data?.articulateLink &&
      data?.articulateLink !== '' &&
      !data?.articulateLinkLoading) ||
    data?.articulateLinkLoading === false
  ) {
    activityLink = s3Helper.getFileUrl(data?.articulateLink);
  }

  // const background =
  //   data?.background && data?.background !== ''
  //     ? s3Helper.getFileUrl(data?.background)
  //     : images.app.lessonPlaceholder;

  const icon =
    data?.icon && data?.icon !== ''
      ? s3Helper.getFileUrl(data?.icon)
      : images.app.lessonPlaceholder;

  // console.log('lesson : ', lesson);

  return (
    <Box className={classes.lessonActionBox} ref={lessonBox}>
      <LessonActionImage image={icon} />
      {activityLink && activityLink !== '' && (
        <Box
          mr={'16%'}
          ml={'16%'}
          textAlign={'center'}
          className="joy-ride-play-lesson"
        >
          <Button
            className={classesAction.button}
            variant="contained"
            size="large"
            startIcon={<PlayCircleOutlineIcon style={{ fontSize: 32 }} />}
            fullWidth={true}
            target="_blank"
            href={activityLink}
          >
            Play
          </Button>
          <ListItem
            button
            className={classes.buttonBottomListItem}
            onClick={() => {
              props.onLessonAction('completeLesson', lesson);
            }}
          >
            <ListItemText primary={'Mark as complete'} />
          </ListItem>
        </Box>
      )}

      <Card className={classes.cardWrapper}>
        <Box p={2}>
          <Typography variant="h6">Actions & Downloads</Typography>
          <List>
            {actionList.map((action, index) => {
              if (index <= 1) return null;
              return (
                <ListItem
                  className={classes.listItem}
                  button
                  key={`lesson-${action.title}-${index}`}
                  onClick={() => {
                    if (action.action && action.action !== '') {
                      props.onLessonAction(action.action, lesson);
                    } else if (action.href && action.href !== '') {
                      window.open(action.href, '_blank');
                    }
                  }}
                >
                  <ListItemIcon className={classes.listItemIcon}>
                    <Avatar
                      alt={action.title}
                      src={action.icon}
                      className={classes.avatarImg}
                      variant="rounded"
                      style={action.icon ? { borderRadius: 0 } : {}}
                    />
                  </ListItemIcon>
                  <ListItemText primary={action.title} />
                </ListItem>
              );
            })}
          </List>
        </Box>
      </Card>
    </Box>
  );
}

export default LessonActions;
