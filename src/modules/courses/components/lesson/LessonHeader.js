import React from 'react';
import { Box, Typography, Grid } from '@material-ui/core';
import { useRouter } from 'next/router';
import {
  actionStyles,
  LessonHeaderContainer,
  HeaderOverlay,
  ContentContainer
} from './styles';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Chip } from 'src/components/shared';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
import s3Helper from 'src/utils/s3Helper';
import images from 'src/config/images';

const useStyles = makeStyles(theme => ({
  chipRoot: {},
  lessonHeaderSubTitle: {
    fontSize: 24,
    marginBottom: 10,
  },
}));

function LessonHeader({ image, title, lessonData }) {
  const router = useRouter();
  const classes = useStyles();
  const actionClasses = actionStyles();

  const onPlay = () => {
    router.push(`${router.asPath}/play`);
  };

  const background =
    lessonData?.data?.background && lessonData?.data?.background !== ''
      ? s3Helper.getFileUrl(lessonData?.data?.background)
      : '';

  return (
    <LessonHeaderContainer image={background}>
      <HeaderOverlay />
      <ContentContainer>
        <Grid container>
          <Grid item md={8} xs={12}>
            <Box>
              <Typography variant="h1" style={{ marginBottom: 6 }}>
                <b>{lessonData?.lessonName}</b>
              </Typography>
              <Typography
                variant="body1"
                className={classes.lessonHeaderSubTitle}
              >
                {lessonData?.data?.description}
              </Typography>
            </Box>
            {lessonData?.tags && (
              <div>
                {lessonData?.tags.map((tag, key) => (
                  <Chip
                    label={tag.tagName}
                    key={key + 'lesson-header' + tag.id}
                    className={classes.chipRoot}
                  />
                ))}
              </div>
            )}
          </Grid>
        </Grid>
      </ContentContainer>
    </LessonHeaderContainer>
  );
}

export default LessonHeader;
