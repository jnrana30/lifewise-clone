import React from 'react';
import {
  Card,
  CardContent,
  CardHeader,
  Typography,
  Box
} from '@material-ui/core';
import * as Showdown from 'showdown';
import { makeStyles } from '@material-ui/core/styles';

const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true
});

const useStyles = makeStyles(theme => ({
  lessonPlan: {
    padding: `${theme.spacing(1.5)}px ${theme.spacing(2)}px 0 ${theme.spacing(
      2
    )}px`,
    '& .MuiTypography-root': {
      fontSize: '28px'
    }
  },
  lessonPlanSubTitle: {
    fontSize: 18
  }
}));

function LessonDetails({ lessonData }) {
  const classes = useStyles();
  let content = [...(lessonData?.content || [])];

  const introductionIndex = lessonData?.content.findIndex(
    c => c.type === 'introduction'
  );

  if (content && introductionIndex && introductionIndex !== -1) {
    const newContentList = [
      ...content.slice(0, introductionIndex),
      ...content.slice(introductionIndex + 1)
    ];
    content = [lessonData?.content[introductionIndex], ...newContentList];
  }

  return (
    <Card>
      <CardContent>
        <Box className={classes.lessonPlan}>
          <Typography variant="h5" component="h3">
            Plan
          </Typography>
        </Box>
        {content.length > 0 && (
          <Box p={2}>
            {content.map((item, index) => {
              if (!item.data || item.data === null || item.data === '')
                return null;
              let title = item.type.replace(/([A-Z])/g, ' $1');
              title = title.charAt(0).toUpperCase() + title.slice(1);
              return (
                <Box mb={4} key={`lesson-info-content-${index}`}>
                  <Box mb={2}>
                    <Typography
                      variant="body1"
                      component="h3"
                      className={classes.lessonPlanSubTitle}
                    >
                      <b>{title}</b>
                    </Typography>
                  </Box>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: converter.makeHtml(item.data)
                    }}
                  />
                </Box>
              );
            })}
          </Box>
        )}
      </CardContent>
    </Card>
  );
}

export default LessonDetails;
