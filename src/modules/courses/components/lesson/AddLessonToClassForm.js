import React, { useEffect, useState } from 'react';
import {
  Box,
  Grid,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Typography
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { Formik, FieldArray } from 'formik';
import _ from 'lodash';
import { fetchClasses } from '../../../../modules/school/api/schoolApis';

const defaultClasses = [
  {
    id: {}
  }
];

export default function AddToClassForm({ onSubmit, lesson }) {
  const [existingClasses, setExistingClasses] = useState([]);

  useEffect(() => {
    fetchClasses({ lessonId: lesson.id })
      .then(res => {
        const data = res.classes.map(obj => ({ ...obj, checked: true }));
        setExistingClasses(data);
        // setExistingClasses([]);
      })
      .catch(err => {
        setExistingClasses([]);
      });
  }, []);

  const getClasses = async (search, index) => {
    let filter = {
      search,
      status: 'active'
    };
    const res = await fetchClasses(filter);
    return res?.classes ? res?.classes : [];
  };

  return (
    <Formik
      initialValues={{ classes: defaultClasses, existing: existingClasses }}
      onSubmit={async values => {
        const data = {
          add: values.classes.map(classItem => classItem?.id?.id),
          remove: values.existing
            .filter(item => !item.checked)
            .map(classItem => classItem?.id)
        };
        onSubmit(data);
      }}
      enableReinitialize={true}
    >
      {({ values, ...prop }) => (
        <form
          onSubmit={e => {
            e.preventDefault();
            prop.submitForm();
            return false;
          }}
          id="lesson-add-to-class-form"
          noValidate
        >
          <FieldArray
            name="classes"
            render={arrayHelpers => (
              <div>
                {values.existing && values.existing.length > 0 && (
                  <Box mb={2}>
                    <List dense>
                      <Typography variant="body1" color="textPrimary">
                        <b>Manage existing classes.</b>
                      </Typography>
                      {values.existing.map((item, index) => (
                        <React.Fragment key={`existing-class-${index}`}>
                          <ListItem>
                            <Form.Field.Checkbox
                              name={`existing[${index}].checked`}
                              label={item.className}
                              // onChange={(event) => {
                              //   handleCheckChange(event);
                              // }}
                            />
                          </ListItem>
                        </React.Fragment>
                      ))}
                    </List>
                  </Box>
                )}

                <Typography variant="body1" color="textPrimary">
                  <b>Add new classes to the lesson.</b>
                </Typography>

                {values.classes &&
                  values.classes.length > 0 &&
                  values.classes.map((obj, index) => (
                    <React.Fragment key={`class-${index}-${obj.day}`}>
                      <Grid container spacing={2} alignItems="flex-end">
                        <Grid item xs={10} ml={0}>
                          <Form.Field.AutoComplete
                            multiple={false}
                            fullWidth
                            showAvatar={false}
                            options={[{}]}
                            variant="standard"
                            remoteMethod={value => getClasses(value, index)}
                            name={`classes[${index}].id`}
                            label="Choose Class"
                            optLabel="className"
                            optValue="id"
                          />
                        </Grid>
                        <Grid item xs={1} ml={2}>
                          {values.classes.length > 1 && (
                            <Button
                              iconButton={true}
                              onClick={() => {
                                arrayHelpers.remove(index);
                              }}
                            >
                              <CloseIcon />
                            </Button>
                          )}
                        </Grid>
                      </Grid>
                    </React.Fragment>
                  ))}
                <Box my={2}>
                  <Button
                    color="primary"
                    onClick={() => {
                      arrayHelpers.push(defaultClasses[0]);
                    }}
                  >
                    + Add new class
                  </Button>
                </Box>
              </div>
            )}
          />
        </form>
      )}
    </Formik>
  );
}
