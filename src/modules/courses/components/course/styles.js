import styled, { css } from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  media: {
    paddingTop: '40%'
  },
  expand: {
    marginLeft: 'auto'
  },
  tagsContainer: {
    '& > *': {
      margin: theme.spacing(0.5)
    }
  },
  courseTitleWrap: {
    position: 'absolute',
    top: 14,
    padding: '10px 20px',
    height: 'calc(100% - 28px)'
  },
  courseIconWrap: {
    minWidth: 80,
    minHeight: 80,
    height: 80,
    width: 80,
    borderRadius: 80,
    backgroundColor: '#ffffff'
  },
  courseIcon: {
    maxWidth: 50,
    maxHeight: 50
  },
  courseTitle: {
    color: '#ffffff'
  }
}));

export const actionStyles = makeStyles(theme => ({
  button: {
    borderRadius: 40,
    backgroundColor: '#F6AE32',
    color: '#ffffff',
    height: 54,
    fontSize: 20,
    marginBottom: 40,
    '&:hover': {
      backgroundColor: '#f2a218'
    },
    '&:active': {
      backgroundColor: '#f2a218'
    }
  }
}));

export const CourseIcon = styled.div`
  display: block;
  background-repeat: no-repeat;
  padding-top: 50%;
  width: 100%;
  border-radius: 4px;
  ${props =>
    css`
      background-image: url('${props.icon}');
      background-size: cover;
      background-position: center center;
    `}
`;

export const CourseHeaderContainer = styled.div`
  margin: -32px -25px;
  background-color: #f2f2f2;
  padding: 24px;
  margin-bottom: 30px;
  display: flex;
  flex-direction: column;
  position: relative;
  ${props =>
    css`
      background-image: url('${props.image}');
      background-size: cover;
      background-position: center;
    `}
  h1 {
    color: #ffffff;
    font-size: 40px;
    margin: 0;
  }
  p {
    color: #ffffff;
  }
`;
export const ContentContainer = styled.div`
  width: 100%;
  max-width: 1500px;
  display: block;
  box-sizing: border-box;
  margin-left: auto;
  margin-right: auto;
  z-index: 1;
`;
export const HeaderOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #00000085;
  z-index: 0;
`;
