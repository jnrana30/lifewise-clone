import React from 'react';
import { FilterBar, FilterPicker, FilterSearch } from 'src/components/App';
import { Box } from '@material-ui/core';
import { getCategories, getTagsByType } from '../../api/coursesApis';

function CourseDetailFilter({ filters, setFilter, unsetFilter }) {
  const [category, setCategory] = React.useState('');
  const [topic, setTopic] = React.useState('');
  const [search, setSearch] = React.useState('');

  const [tags, setTags] = React.useState([]);
  const [isMount, setIsMount] = React.useState(true);

  React.useEffect(() => {
    if (isMount) {
      setIsMount(false);
      return;
    }
    if (tags && tags.length > 0) {
      setFilter({ tags: `[${tags.map(tag => '"' + tag.id + '"')}]` });
    } else {
      unsetFilter('tags');
    }
  }, [tags]);

  const searchTags = async (search, type) => {
    const filter = {
      search
    };
    const res = await getTagsByType(type, filter);
    return res?.tags ? res?.tags : [];
  };

  return (
    <FilterBar nonStickyHeader>
      <Box display="flex" justifyContent="space-between">
        <Box display="flex">
          <FilterSearch
            placeholder="Search lessons"
            value={filters['lessonName'] ? filters['lessonName'] : ''}
            onChange={val => {
              setFilter({
                lessonName: val
              });
            }}
            onClear={() => {
              unsetFilter('lessonName');
            }}
          />
          <FilterPicker
            title="Category"
            options={[]}
            onChange={val => {
              setTags([...tags, val]);
            }}
            remoteMethod={val => searchTags(val, 'category')}
            selected={
              tags.filter(tag => tag.tagType === 'category')[0]
                ? tags.filter(tag => tag.tagType === 'category')[0]
                : ''
            }
            onClear={() => {
              setTags(tags.filter(tag => tag.tagType !== 'category'));
            }}
            optValue="id"
            optLabel="tagName"
            ml={2}
            mr={1}
          />
          <FilterPicker
            title="Topics"
            options={[]}
            onChange={val => {
              setTags([...tags, val]);
            }}
            remoteMethod={val => searchTags(val, 'topic')}
            selected={
              tags.filter(tag => tag.tagType === 'topic')[0]
                ? tags.filter(tag => tag.tagType === 'topic')[0]
                : ''
            }
            onClear={() => {
              setTags(tags.filter(tag => tag.tagType !== 'topic'));
            }}
            optValue="id"
            optLabel="tagName"
            ml={2}
            mr={1}
          />
        </Box>
        <Box display="flex" />
      </Box>
    </FilterBar>
  );
}

export default CourseDetailFilter;
