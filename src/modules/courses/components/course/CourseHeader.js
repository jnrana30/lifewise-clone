import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography, Chip } from '@material-ui/core';
import images from 'src/config/images';
import s3Helper from 'src/utils/s3Helper';

import {
  CourseHeaderContainer,
  CourseIcon,
  HeaderOverlay,
  ContentContainer
} from './styles';

function CourseHeader({ course }) {
  const icon =
    course?.data?.icon && course?.data?.icon !== ''
      ? s3Helper.getFileUrl(course?.data?.icon)
      : images.app.coursePlaceholder;

  const background =
    course?.data?.background && course?.data?.background !== ''
      ? s3Helper.getFileUrl(course?.data?.background)
      : images.app.courseBanner1;

  return (
    <CourseHeaderContainer image={background}>
      <HeaderOverlay />
      <ContentContainer>
        <Box display="flex" p={0.5}>
          <Box
            width={0.2}
            display="flex"
            alignItems="center"
            justifyContent="center"
            flexWrap={'wrap'}
            mr={3}
            mt={1}
          >
            <CourseIcon icon={icon} />
          </Box>
          <Box>
            <h1>{course?.courseName}</h1>
            {course?.data?.description && course?.data?.description != '' && (
              <Typography variant="body1">
                {course?.data?.description}
              </Typography>
            )}
            <Box mt={2} display="flex" alignItems="center">
              {course?.tags && (
                <div>
                  {course?.tags.map((tag, key) => (
                    <Chip
                      label={tag.tagName}
                      style={{ marginRight: 6 }}
                      key={key + 'lesson-header' + tag.id}
                    />
                  ))}
                </div>
              )}
            </Box>
          </Box>
        </Box>
      </ContentContainer>
    </CourseHeaderContainer>
  );
}

export default CourseHeader;
