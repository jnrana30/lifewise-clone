import * as types from './coursesTypes';

export const fetchCourses = (filters, paging) => ({
  type: types.FETCH_COURSES,
  filters,
  paging
});

export const setCourses = data => ({ type: types.SET_COURSES, data });
export const setCourse = data => ({ type: types.SET_COURSE, data });
export const getCourse = courseId => ({ type: types.GET_COURSE, courseId });
export const createCourse = (data, resolve, reject) => ({
  type: types.CREATE_COURSE,
  data,
  resolve,
  reject
});
export const updateCourse = (data, courseId, resolve, reject) => ({
  type: types.UPDATE_COURSE,
  data,
  courseId,
  resolve,
  reject
});
export const fetchCourseLessons = (filters, paging) => ({
  type: types.FETCH_COURSE_LESSONS,
  filters,
  paging
});
export const setCourseLessons = data => ({
  type: types.SET_COURSE_LESSONS,
  data
});

export const fetchLessons = (filters, paging, isAssemblies) => ({
  type: types.FETCH_LESSONS,
  filters,
  paging,
  isAssemblies
});

export const resetLessonsFilter = () => ({
  type: types.RESET_LESSONS_FILTER
});

export const setLesson = data => ({ type: types.SET_LESSON, data });
export const setLessons = data => ({ type: types.SET_LESSONS, data });
export const getLesson = lessonId => ({ type: types.GET_LESSON, lessonId });
export const createLesson = (data, resolve, reject) => ({
  type: types.CREATE_LESSON,
  data,
  resolve,
  reject
});
export const updateLesson = (data, lessonId, resolve, reject) => ({
  type: types.UPDATE_LESSON,
  data,
  lessonId,
  resolve,
  reject
});

export const assignLessonToCourse = (
  courseId,
  lessonId,
  data,
  resolve,
  reject
) => ({
  type: types.ASSIGN_LESSON_TO_COURSE,
  courseId,
  lessonId,
  data,
  resolve,
  reject
});

export const fetchResources = (filters, paging) => ({
  type: types.FETCH_RESOURCES,
  filters,
  paging
});
export const setResources = data => ({ type: types.SET_RESOURCES, data });
export const getResource = resourceId => ({
  type: types.GET_RESOURCE,
  resourceId
});
export const setResource = data => ({ type: types.SET_RESOURCE, data });
export const createResource = (data, resolve, reject) => ({
  type: types.CREATE_RESOURCE,
  data,
  resolve,
  reject
});
export const updateResource = (resourceId, data, resolve, reject) => ({
  type: types.UPDATE_RESOURCE,
  resourceId,
  data,
  resolve,
  reject
});

export const setActionLesson = lesson => ({
  type: types.SET_ACTION_LESSON,
  lesson
});
export const completeLesson = (classId, courseId, data, resolve, reject) => ({
  type: types.COMPLETE_LESSON,
  classId,
  courseId,
  data,
  resolve,
  reject
});

// Assessment actions : START
export const submitAssessment = (
  classId,
  lessonId,
  students,
  resolve,
  reject
) => ({
  type: types.SUBMIT_ASSESSMENT,
  classId,
  lessonId,
  students,
  resolve,
  reject
});

export const fetchAssessments = (filters, paging) => ({
  type: types.FETCH_ASSESSMENTS,
  filters,
  paging
});
export const setAssessments = data => ({ type: types.SET_ASSESSMENTS, data });

export const fetchStudentAssessments = (studentId, filters, paging) => ({
  type: types.FETCH_STUDENT_ASSESSMENTS,
  studentId,
  filters,
  paging
});
export const setStudentAssessments = data => ({
  type: types.SET_STUDENT_ASSESSMENTS,
  data
});

export const updateStudentAssessment = assessment => ({
  type: types.UPDATE_STUDENT_ASSESSMENTS,
  assessment
});
// Assessment actions : END

export const clearCourse = () => ({ type: types.CLEAR_COURSE });
export const clearLesson = () => ({ type: types.CLEAR_LESSON });
