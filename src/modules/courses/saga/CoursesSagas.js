import { takeLatest, all, fork, put, call, select } from 'redux-saga/effects';
import * as coursesTypes from '../actions/coursesTypes';
import * as coursesActions from '../actions/coursesActions';
import * as coursesApis from '../api/coursesApis';
import { classParticipation } from 'src/modules/school/api/schoolApis';
import {
  updateHomeClass,
  setHomeClass
} from 'src/modules/app/actions/appActions';
import toast from 'src/utils/toast';
import _ from 'lodash';

export const getMySchoolId = state => state.school.mySchool?.id;
export const getHomeClass = state => state.app?.home?.classItem;
export const getStudentAssessments = state => state.courses?.studentAssessments;

function* fetchCourses({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const data = yield call(coursesApis.fetchCourses, filter);
    yield put(coursesActions.setCourses(data));
  } catch (error) {
    yield put(coursesActions.setCourses([]));
  }
}

function* assignLessonToCourse({ courseId, lessonId, data, resolve, reject }) {
  try {
    const res = yield call(
      coursesApis.assignLessonToCourse,
      courseId,
      lessonId,
      data
    );
    // yield put(coursesActions.setCourse(res));
    toast.success('Lesson added to course!');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* fetchCourseLessons({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const data = yield call(coursesApis.fetchLessons, filter);
    yield put(coursesActions.setCourseLessons(data));
  } catch (error) {
    yield put(coursesActions.setCourseLessons([]));
  }
}

function* fetchLessons({ filters, paging, isAssemblies }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    if (isAssemblies === true) {
      filter.tags = [
        {
          id: 'a26eedca-cc0c-4224-b5b8-97e742061f7e',
          tagName: 'Assemblies',
          tagType: 'curriculum'
        }
      ];
    }
    const data = yield call(coursesApis.fetchLessons, filter);
    yield put(coursesActions.setLessons(data));
  } catch (error) {
    yield put(coursesActions.setLessons([]));
  }
}

function* createCourse({ data, resolve, reject }) {
  try {
    const res = yield call(coursesApis.createCourse, data);
    toast.success('Course created successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* getCourse({ courseId }) {
  try {
    const res = yield call(coursesApis.getCourse, courseId);
    yield put(coursesActions.setCourse(res));
  } catch (error) {}
}

function* updateCourse({ data, courseId, resolve, reject }) {
  try {
    const res = yield call(coursesApis.updateCourse, data, courseId);
    yield put(coursesActions.setCourse(res));
    toast.success('Course updated successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* createLesson({ data, resolve, reject }) {
  try {
    const res = yield call(coursesApis.createLesson, data);
    toast.success('Lesson created successfully!');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* updateLesson({ data, lessonId, resolve, reject }) {
  try {
    if (!data.content) {
      data.content = [];
    }
    const res = yield call(coursesApis.updateLesson, data, lessonId);
    toast.success('Lesson updated successfully!');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* getLesson({ lessonId }) {
  try {
    const data = {
      lesson: yield call(coursesApis.getLesson, lessonId),
      lessonData: yield call(coursesApis.getLessonContent, lessonId),
      versions: yield call(coursesApis.getLessonContentAll, lessonId)
    };
    yield put(coursesActions.setLesson(data));
  } catch (error) {
    toast.error(error?.error);
  }
}

function* fetchResources({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const data = yield call(coursesApis.fetchResources, filter);
    yield put(coursesActions.setResources(data));
  } catch (error) {
    yield put(coursesActions.setResources({ resources: [], records: 0 }));
  }
}

function* createResource({ data, resolve, reject }) {
  try {
    const res = yield call(coursesApis.createResource, data);
    toast.success('Resource created successfully!');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* updateResource({ resourceId, data, resolve, reject }) {
  try {
    const res = yield call(coursesApis.updateResource, resourceId, data);
    toast.success('Resource updated successfully!');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* completeLesson({ classId, courseId, data, resolve, reject }) {
  try {
    const schoolId = yield select(getMySchoolId);
    let res;
    if (courseId) {
      res = yield call(classParticipation, schoolId, classId, {
        ...(data || {}),
        courseId
      });
    } else {
      res = yield call(classParticipation, schoolId, classId, data);
    }
    const homeClass = yield select(getHomeClass);
    if (homeClass.id === res.id) {
      yield put(
        updateHomeClass({
          lessons: res.lessons,
          courseId,
          coursesLesson: res.courses[courseId]
            ? res.courses[courseId].lessons[data.lessonId]
            : null
        })
      );
    } else {
      // yield put(setHomeClass(res));
    }
    resolve(res);
  } catch (error) {
    toast.error(
      'Invalid class selected! Please check if you have assigned your course or lesson with the class.'
    );
    reject(error);
  }
}

function* submitAssessment({ classId, lessonId, students, resolve, reject }) {
  try {
    const res = yield call(
      coursesApis.submitAssessment,
      classId,
      lessonId,
      students
    );
    resolve(res);
  } catch (error) {
    reject(error);
  }
}

function* fetchAssessments({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const data = yield call(coursesApis.fetchAssessments, filter);
    yield put(coursesActions.setAssessments(data));
  } catch (error) {
    yield put(coursesActions.setAssessments([]));
  }
}

function* fetchStudentAssessments({ studentId, filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const data = yield call(
      coursesApis.fetchStudentAssessments,
      studentId,
      filter
    );
    yield put(coursesActions.setStudentAssessments(data));
  } catch (error) {
    yield put(coursesActions.setStudentAssessments([]));
  }
}

function* updateStudentAssessments({ assessment }) {
  try {
    const data = {
      lessonId: assessment.lessonId,
      students: [
        {
          assessmentId: assessment.id,
          studentId: assessment.studentId,
          rating: assessment.rating,
          comment: assessment.comments
        }
      ]
    };
    const res = yield call(
      coursesApis.updateAssessment,
      assessment.classId,
      data
    );
    console.log('res : ', res);
    let studentAssessments = yield select(getStudentAssessments);
    const updateIndex = studentAssessments.data.findIndex(
      x => x.id === assessment.id
    );
    studentAssessments.data[updateIndex] = {
      ...assessment,
      ...res[0]
    };
    const updatedData = {
      assessments: studentAssessments.data,
      records: studentAssessments.paging.records
    };
    console.log('studentAssessments : ', updatedData);
    yield put(coursesActions.setStudentAssessments(updatedData));
  } catch (error) {
    console.log('error : ', error);
  }
}

export function* watchSagas() {
  yield takeLatest(coursesTypes.FETCH_COURSE_LESSONS, fetchCourseLessons);

  yield takeLatest(coursesTypes.FETCH_COURSES, fetchCourses);
  yield takeLatest(coursesTypes.CREATE_COURSE, createCourse);
  yield takeLatest(coursesTypes.GET_COURSE, getCourse);
  yield takeLatest(coursesTypes.UPDATE_COURSE, updateCourse);
  yield takeLatest(coursesTypes.ASSIGN_LESSON_TO_COURSE, assignLessonToCourse);

  yield takeLatest(coursesTypes.FETCH_LESSONS, fetchLessons);
  yield takeLatest(coursesTypes.GET_LESSON, getLesson);
  yield takeLatest(coursesTypes.CREATE_LESSON, createLesson);
  yield takeLatest(coursesTypes.UPDATE_LESSON, updateLesson);

  yield takeLatest(coursesTypes.FETCH_RESOURCES, fetchResources);
  yield takeLatest(coursesTypes.CREATE_RESOURCE, createResource);
  yield takeLatest(coursesTypes.UPDATE_RESOURCE, updateResource);

  yield takeLatest(coursesTypes.COMPLETE_LESSON, completeLesson);

  yield takeLatest(coursesTypes.SUBMIT_ASSESSMENT, submitAssessment);
  yield takeLatest(coursesTypes.FETCH_ASSESSMENTS, fetchAssessments);

  yield takeLatest(
    coursesTypes.FETCH_STUDENT_ASSESSMENTS,
    fetchStudentAssessments
  );
  yield takeLatest(
    coursesTypes.UPDATE_STUDENT_ASSESSMENTS,
    updateStudentAssessments
  );
}

export default function* runSagas() {
  yield all([fork(watchSagas)]);
}
