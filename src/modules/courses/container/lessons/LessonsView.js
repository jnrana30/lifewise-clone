import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import LessonsFilter from '../../components/lessons/LessonsFilter';
import LessonGrid from '../../components/lessons/LessonGrid';
import LessonList from '../../components/lessons/LessonList';
import LessonForm from '../../components/lessons/LessonForm';
import CompleteLesson from '../../components/completeLesson';
import { getAcademicYearOption } from '../../../../config';

const yearOptions = getAcademicYearOption();

function LessonsView({ lessons, user, ...props }) {
  const router = useRouter();
  const [viewType, setViewType] = React.useState('grid');
  const [formLoading, setFormLoading] = React.useState(false);
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [completeLesson, setCompleteLesson] = React.useState(false);

  const isAdmin = user?.profile?.role?.level > 5;
  const isAssemblies = !isAdmin && router.route == '/courses/assemblies';

  const { data, loading } = lessons;
  let { filters, paging } = lessons;

  React.useEffect(() => {
    fetchLessons(
      {},
      {
        perPage: isAdmin ? 100 : 12,
        ...(!isAdmin && { status: 'active' })
      }
    );
    // return () => {
    //   props.resetLessonsFilter();
    // };
  }, []);

  const fetchLessons = (filter, pagingData) => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    if (typeof filter !== 'pagingData' && !_.isEmpty(pagingData)) {
      paging = { ...paging, ...pagingData };
    }
    props.fetchLessons(filters, paging, isAssemblies);
  };

  const setFilter = filter => {
    fetchLessons(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchLessons();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    // filters
    fetchLessons();
  };

  const onComplete = val => {
    setCompleteLesson(true);
  };

  const onEdit = lesson => {
    router.push(`/lessons/admin/${lesson.id}`);
  };

  const onSubmit = async data => {
    setFormLoading(true);
    data.content = [];
    const res = await props.createLesson(data);
    setFormLoading(false);
    setDrawerOpen(false);
    // fetchLessons();
    router.push(`/lessons/admin/${res.id}`);
  };

  return (
    <React.Fragment>
      <Header title={isAssemblies ? 'Assemblies' : 'Lessons Library'}>
        <LessonsFilter
          viewType={viewType}
          setViewType={setViewType}
          setDrawerOpen={setDrawerOpen}
          isAdmin={isAdmin}
          userProfile={user?.profile}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          yearGroupOption={yearOptions}
          filters={filters}
          isAssemblies={isAssemblies}
        />
      </Header>
      {/* if (viewType == 'grid') { */}
      {viewType === 'grid' && !isAdmin ? (
        <LessonGrid
          lessons={data}
          onComplete={onComplete}
          loading={loading}
          paging={paging}
          onPaging={onPaging}
        />
      ) : (
        <LessonList
          lessons={data}
          isAdmin={isAdmin}
          loading={loading}
          paging={paging}
          onPaging={onPaging}
          onEdit={onEdit}
        />
      )}

      <Drawer
        open={drawerOpen}
        title="Add Lesson"
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="lesson-form"
        buttonLoading={formLoading}
      >
        <Typography variant="body1" color="textPrimary">
          <b>Add new lesson to the platform.</b>
        </Typography>
        <LessonForm schoolYears={props.schoolYears} onSubmit={onSubmit} />
      </Drawer>

      <Drawer
        open={completeLesson}
        title="Complete Lesson"
        onClose={() => {
          setCompleteLesson(false);
        }}
        fullWidth={true}
        form="lesson-complete-form"
        buttonText="Continue"
      >
        <CompleteLesson />
      </Drawer>
    </React.Fragment>
  );
}

export default LessonsView;
