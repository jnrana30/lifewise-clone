import React from 'react';
import { connect } from 'react-redux';
import LessonsView from './LessonsView';
import {
  fetchLessons,
  createLesson,
  resetLessonsFilter
} from '../../actions/coursesActions';

export const LessonsContainer = props => {
  return <LessonsView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  lessons: state.courses.lessons,
  schoolYears: state.app.schoolYears
});

const mapDispatchToProps = dispatch => ({
  fetchLessons: (filter, paging, isAssemblies) =>
    dispatch(fetchLessons(filter, paging, isAssemblies)),
  resetLessonsFilter: () => dispatch(resetLessonsFilter()),
  createLesson: data => {
    return new Promise((resolve, reject) => {
      dispatch(createLesson(data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(LessonsContainer);
