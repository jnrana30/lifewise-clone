import React from 'react';
import { connect } from 'react-redux';
import AssessmentView from './AssessmentView';
import {
  fetchStudentAssessments,
  updateStudentAssessment
} from '../../actions/coursesActions';

export const AssessmentContainer = props => {
  return <AssessmentView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  studentAssessments: state.courses.studentAssessments
});

const mapDispatchToProps = dispatch => ({
  fetchStudentAssessments: (studentId, filters, paging) =>
    dispatch(fetchStudentAssessments(studentId, filters, paging)),
  updateStudentAssessment: assessment =>
    dispatch(updateStudentAssessment(assessment))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AssessmentContainer);
