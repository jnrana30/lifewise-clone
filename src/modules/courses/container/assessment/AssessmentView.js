import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import AssessmentFilter from '../../components/assessment/AssessmentFilter';
import AssessmentList from '../../components/assessment/AssessmentList';
import AssessmentForm from '../../components/assessment/AssessmentForm';
import AssessmentInfoModal from '../../components/assessment/AssessmentInfoModal';

import { assessment } from '../../api/data';
import { fetchUsers } from 'src/modules/account/api/accountApis';

function AssessmentView({ studentAssessments, ...props }) {
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [infoModal, setInfoModal] = React.useState(false);
  const router = useRouter();
  const [student, setStudent] = React.useState({});

  const { data, loading } = studentAssessments;
  let { filters, paging } = studentAssessments;

  React.useEffect(() => {
    if (router?.query?.id && router?.query?.id !== '') {
      fetchStudentAssessments();
      const filters = {
        ids: `[${router?.query?.id}]`,
        propagate: false
      };
      fetchUsers(filters)
        .then(res => {
          if (res?.profiles[0]) {
            setStudent(res?.profiles[0]);
          } else {
            setStudent({});
          }
        })
        .catch(err => {
          setStudent({});
        });
    }
  }, [router]);

  const fetchStudentAssessments = (filter, pagingData) => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    if (typeof filter !== 'pagingData' && !_.isEmpty(pagingData)) {
      paging = { ...paging, ...pagingData };
    }
    const studentId = router?.query?.id;
    props.fetchStudentAssessments(studentId, filters, paging);
  };

  const setFilter = filter => {
    fetchStudentAssessments(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchStudentAssessments();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchStudentAssessments();
  };

  return (
    <React.Fragment>
      <Header title="Assessment">
        <div />
      </Header>

      <AssessmentFilter
        student={student}
        setDrawerOpen={setDrawerOpen}
        setFilter={setFilter}
        unsetFilter={unsetFilter}
        filters={filters}
        setInfoModal={setInfoModal}
      />
      <AssessmentList
        assessment={assessment}
        assessments={data}
        loading={loading}
        paging={paging}
        onPaging={onPaging}
        updateStudentAssessment={props.updateStudentAssessment}
      />

      <Drawer
        open={drawerOpen}
        title="Create new Assessment"
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="assessment-form"
      >
        <Typography variant="body1" color="textPrimary">
          <b>Create new assessment.</b>
        </Typography>
        <AssessmentForm />
      </Drawer>
      <AssessmentInfoModal
        isVisible={infoModal}
        onClose={() => setInfoModal(false)}
      />
    </React.Fragment>
  );
}

export default AssessmentView;
