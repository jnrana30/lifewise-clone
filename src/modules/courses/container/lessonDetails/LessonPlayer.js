import React from 'react';
import { Header } from 'src/components/App';

function LessonPlayer() {
  return (
    <React.Fragment>
      <Header title="Lesson Player" />
    </React.Fragment>
  );
}

export default LessonPlayer;
