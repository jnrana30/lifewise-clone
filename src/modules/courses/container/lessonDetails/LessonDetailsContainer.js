import React from 'react';
import { connect } from 'react-redux';
import LessonDetailsView from './LessonDetailsView';
import { getLesson } from '../../actions/coursesActions';
import { useRouter } from 'next/router';

export const LessonDetailsContainer = props => {
  const router = useRouter();

  const fetchLesson = () => {
    if (router.query.id) {
      props.getLesson(router.query.id);
    }
  };

  React.useEffect(() => {
    fetchLesson();
  }, [router]);

  return <LessonDetailsView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  lessonObj: state.courses.lesson,
  schoolYears: state.app.schoolYears
});

const mapDispatchToProps = dispatch => ({
  getLesson: lessonId => dispatch(getLesson(lessonId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LessonDetailsContainer);
