import React from 'react';
import { connect } from 'react-redux';
import LessonDetailsAdmin from './LessonDetailsAdmin';
import {
  getLesson,
  updateLesson,
  clearLesson
} from '../../actions/coursesActions';
import { useRouter } from 'next/router';
export const LessonAdminContainer = props => {
  const router = useRouter();

  const fetchLesson = () => {
    if (router.query.id) {
      props.getLesson(router.query.id);
    }
  };

  React.useEffect(() => {
    return () => {
      props.clearLesson();
    };
  }, []);

  React.useEffect(() => {
    fetchLesson();
  }, [router]);

  return (
    <LessonDetailsAdmin
      {...props}
      lessonId={router.query.id}
      fetchLesson={fetchLesson}
    />
  );
};

const mapStateToProps = state => ({
  user: state.auth.user,
  lessonObj: state.courses.lesson,
  schoolYears: state.app.schoolYears
});

const mapDispatchToProps = dispatch => ({
  getLesson: lessonId => dispatch(getLesson(lessonId)),
  clearLesson: () => dispatch(clearLesson()),
  updateLesson: (data, lessonId) => {
    return new Promise((resolve, reject) => {
      dispatch(updateLesson(data, lessonId, resolve, reject));
    });
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LessonAdminContainer);
