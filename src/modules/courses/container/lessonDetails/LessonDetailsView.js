import React from 'react';
import { Header } from 'src/components/App';
import { Grid } from '@material-ui/core';
import {
  LessonDetails,
  LessonHeader,
  LessonActions
} from '../../components/lesson';
import { ContentContainer } from '../../../../components/shared/Form/File/styles';

import CompleteLesson from 'src/modules/courses/components/completeLesson';

function LessonDetailsView({ lessonObj }) {
  const [action, setAction] = React.useState('');
  const [actionLesson, setActionLesson] = React.useState({});

  const { lesson, lessonData } = lessonObj;

  const onLessonAction = (action, lesson) => {
    setAction(action);
    setActionLesson(lesson);
  };

  return (
    <React.Fragment>
      <Header title="Details" />
      <LessonHeader
        image="https://media-exp3.licdn.com/dms/image/C4D1BAQGeH4zbMzYUnA/company-background_10000/0/1573566485283?e=2159024400&v=beta&t=zT1XbbWWEzfaEHAyEFLbiIzArozFXoy9_zCM_ZXokHg"
        lessonData={lessonData}
      />
      <ContentContainer>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={12} lg={8} md={8}>
            <LessonDetails lessonData={lessonData} />
          </Grid>
          <Grid
            item
            xs={12}
            sm={12}
            lg={4}
            md={4}
            style={{ position: 'relative' }}
          >
            <LessonActions
              lesson={lesson}
              lessonData={lessonData}
              onLessonAction={onLessonAction}
            />
          </Grid>
        </Grid>
      </ContentContainer>
      <CompleteLesson
        action={action}
        lesson={actionLesson}
        onClose={() => {
          setAction('');
          setActionLesson({});
        }}
      />
    </React.Fragment>
  );
}

export default LessonDetailsView;
