import React from 'react';
import { Header, FilterBar } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography, Box, Grid, Switch } from '@material-ui/core';
import _ from 'lodash';
import { ContentContainer } from '../courseDetails/styles';

import LessonForm from '../../components/lessons/LessonForm';
import LessonPlanForm from '../../components/lessons/LessonPlanForm';
import LessonDocuments from '../../components/lessons/LessonDocuments';
import LessonDocument from '../../components/lessons/LessonDocument';

// import {
//   ActivityWorkbook,
//   RemoteArticulate,
//   MainArticulate,
//   ParentLink
// } from '../../components/documents';

export const LessonDetailsAdmin = ({ lessonObj, lessonId, ...props }) => {
  const [activeTab, setActiveTab] = React.useState(0);
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [formLoading, setFormLoading] = React.useState(false);
  const [updatingStatus, setUpdatingStatus] = React.useState(false);
  const [docType, setDocType] = React.useState('');
  const { lesson, lessonData, versions } = lessonObj;

  // console.log('lessonObj : ', lessonObj);

  const getDrawerTitle = () => {
    switch (docType) {
      case 'articulateLink':
        return 'Manage Main Articulate';
        break;
      case 'remoteArticulateLink':
        return 'Manage Remote Articulate';
        break;
      case 'activityWorkbook':
        return 'Manage Activity Workbook';
        break;
      case 'lessonPlan':
        return 'Lesson Plan';
        break;
      case 'parentLink':
        return 'Manage Parent link';
        break;
    }
  };

  const updateLesson = data => {
    if (data.data && !_.isEmpty(data.data)) {
      data.data = {
        ...lessonData?.data,
        ...data.data
      };
    }
    return props.updateLesson(data, lessonId);
  };

  const onDocumentEdit = async type => {
    await setDocType(type);
    await setDrawerOpen(true);
  };

  const onDocumentSubmit = async data => {
    setFormLoading(true);
    updateLesson(data);
    props.fetchLesson();
    setFormLoading(false);
    // await setDocType('');
    // await setDrawerOpen(false);
  };

  const onLessonUpdate = data => {
    updateLesson(data);
    props.fetchLesson();
  };

  // const formData = {
  //   id: lesson?.id,
  //   category: lesson?.category,
  //   ...lessonData
  // };
  const formData = {
    id: lessonId,
    category: lesson?.category,
    ...lessonData
  };

  // console.log('lesson : ', lesson);
  // console.log('lessonData : ', lessonData);

  const onStatusChange = async () => {
    setUpdatingStatus(true);
    const courseStatus =
      lessonData.versionStatus === 'active' ? 'draft' : 'active';
    updateLesson({ status: courseStatus });
    props.fetchLesson();
    setUpdatingStatus(false);
  };

  return (
    <React.Fragment>
      <Header title="Edit Lesson">
        <FilterBar
          tabs={['Details', 'Documents', 'Lesson Plan', 'Free access']}
          currentTab={activeTab}
          onTabChange={val => {
            setActiveTab(val);
          }}
        >
          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
          >
            <Typography variant="h6">{lesson?.lessonName}</Typography>
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
            >
              <Typography variant="body1">Status : </Typography>
              <Switch
                color="primary"
                checked={lessonData?.versionStatus === 'active'}
                disabled={updatingStatus}
                onChange={onStatusChange}
              />
            </Box>
          </Box>
        </FilterBar>
      </Header>
      <ContentContainer>
        <Box mt={2}>
          <Grid container>
            <Grid item xs={12}>
              <Box>
                {activeTab == 0 && (
                  <LessonForm
                    lesson={formData}
                    schoolYears={props.schoolYears}
                    onSubmit={onLessonUpdate}
                  />
                )}
                {activeTab == 1 && (
                  <LessonDocuments
                    lesson={lessonData}
                    onEdit={onDocumentEdit}
                    versions={versions}
                  />
                )}
                {activeTab == 2 && (
                  <LessonPlanForm
                    lesson={lessonData}
                    onSubmit={onLessonUpdate}
                  />
                )}
              </Box>
            </Grid>
          </Grid>
        </Box>
      </ContentContainer>
      <Drawer
        open={drawerOpen}
        title={getDrawerTitle()}
        onClose={() => {
          setDocType('');
          setDrawerOpen(false);
        }}
        form="document-form"
        buttonLoading={formLoading}
      >
        <LessonDocument
          docType={docType}
          lessonData={lessonData}
          onSubmit={onDocumentSubmit}
          lesson={lesson}
          lessonId={lessonId}
          versions={versions}
          onClose={() => {
            setDocType('');
            setDrawerOpen(false);
          }}
        />
      </Drawer>
    </React.Fragment>
  );
};

export default LessonDetailsAdmin;
