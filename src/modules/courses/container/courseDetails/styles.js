import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';

export const ContentContainer = styled.div`
  width: 100%;
  max-width: 1500px;
  display: block;
  box-sizing: border-box;
  margin-left: auto;
  margin-right: auto;
  margin-top: 100px;
`;

export const useStyles = makeStyles(theme => ({
  buttonRoot: {
    display: 'flex',
    justifyContent: 'space-around',
    minWidth: 180,
    borderRadius: 30,
    backgroundColor: '#ffffff'
  },
  root: {
    padding: '0',
    display: 'flex',
    alignItems: 'center',
    borderRadius: 40,
    borderColor: 'rgba(43, 107, 0, 0.5)'
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    height: 28,
    margin: 4
  }
}));
