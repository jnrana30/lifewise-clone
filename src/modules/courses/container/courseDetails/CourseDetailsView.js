import React from 'react';
import { Header } from 'src/components/App';
import { useRouter } from 'next/router';

import CourseHeader from '../../components/course/CourseHeader';
import CourseLessonFilter from '../../components/course/CourseLessonFilter';
import LessonGrid from '../../components/lessons/LessonGrid';
import { ContentContainer } from '../../components/course/styles';

function CourseDetailsView({ course, courseLessons, ...props }) {
  const { data, loading } = courseLessons;
  let { filters, paging } = courseLessons;
  const router = useRouter();

  React.useEffect(() => {
    fetchCourseLessons();
    return () => {
      props.clearCourse();
    };
  }, []);

  const fetchCourseLessons = (filter, pagingData) => {
    filters = {
      ...filters,
      courseId: router.query.id,
      status: 'active'
    };
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
    }
    if (typeof filter !== 'pagingData' && !_.isEmpty(pagingData)) {
      paging = { ...paging, ...pagingData };
    }
    props.fetchCourseLessons(filters, paging);
  };

  const setFilter = filter => {
    fetchCourseLessons(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchCourseLessons();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchCourseLessons();
  };

  const onComplete = val => {
    // setCompleteLesson(true);
  };

  return (
    <React.Fragment>
      <Header title="Course Detail" />
      <CourseHeader course={course} />
      <ContentContainer>
        <CourseLessonFilter
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
        />
      </ContentContainer>

      {/* <LessonGrid courses={courses} lessons={lessons} /> */}
      <LessonGrid
        courseId={course.id}
        lessons={data}
        onComplete={onComplete}
        loading={loading}
        paging={paging}
        onPaging={onPaging}
      />
    </React.Fragment>
  );
}

export default CourseDetailsView;
