import React from 'react';
import { connect } from 'react-redux';
import CourseAdminView from './CourseAdminView';
import {
  getCourse,
  updateCourse,
  assignLessonToCourse
} from '../../actions/coursesActions';
import { useRouter } from 'next/router';

export const CourseAdminContainer = props => {
  const router = useRouter();

  React.useEffect(() => {
    fetchCourse();
  }, [router]);

  const fetchCourse = () => {
    if (router.query.id) {
      props.getCourse(router.query.id);
    }
  };

  return <CourseAdminView {...props} fetchCourse={fetchCourse} />;
};

const mapStateToProps = state => ({
  course: state.courses.course,
  schoolYears: state.app.schoolYears
});

const mapDispatchToProps = dispatch => ({
  getCourse: courseId => dispatch(getCourse(courseId)),
  updateCourse: (data, courseId) => {
    return new Promise((resolve, reject) => {
      dispatch(updateCourse(data, courseId, resolve, reject));
    });
  },
  assignLessonToCourse: (courseId, lessonId, data) => {
    return new Promise((resolve, reject) => {
      dispatch(assignLessonToCourse(courseId, lessonId, data, resolve, reject));
    });
  }
  // updateCourse: (data, courseId) => dispatch(updateCourse(data, courseId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CourseAdminContainer);
