import React from 'react';
import { Header, FilterBar } from 'src/components/App';
import { Typography, Box, Grid } from '@material-ui/core';

import CourseForm from '../../components/courses/CourseForm';
import LessonBuilder from '../../components/courses/LessonBuilder';
import {
  courseLessonConfigure,
  assignLessonsToCourse,
  deleteLessonFromCourse
} from '../../api/coursesApis';
import { ContentContainer } from './styles';
import toast from 'src/utils/toast';
import { Pricing } from 'aws-sdk';

export const CourseAdminView = ({ course, ...props }) => {
  const [activeTab, setActiveTab] = React.useState(0);

  const onSubmit = async data => {
    await props.updateCourse(data, course.id);
    props.getCourse(course.id);
  };

  const addLesson = (lesson, data) => {
    return new Promise(async (resolve, reject) => {
      try {
        const res = await props.assignLessonToCourse(
          course.id,
          lesson.lesson.id,
          data
        );
        props.fetchCourse();
        resolve(res);
      } catch (err) {
        reject(err);
      }
    });
  };

  const updateLessonConfig = lessons => {
    return new Promise((resolve, reject) => {
      const data = {};
      lessons.map((lesson, index) => {
        data[lesson.lessonId] = {
          displayOrder: index
        };
      });
      assignLessonsToCourse(course.id, data)
        .then(res => {
          toast.success('Lesson order saved!');
          return resolve(true);
        })
        .catch(err => {
          console.log(err);
          return reject(err);
        });
    });
  };

  const removeLesson = lessonId => {
    return new Promise((resolve, reject) => {
      deleteLessonFromCourse(course.id, lessonId)
        .then(res => {
          toast.success('Lesson removed from the course!');
          return resolve(true);
        })
        .catch(err => {
          console.log(err);
          return reject(err);
        });
    });
    // deleteLessonFromCourse
  };

  const { courseLessons } = course;

  return (
    <React.Fragment>
      <Header title="Edit Course">
        <FilterBar
          tabs={['Details', 'Lesson Builder']}
          currentTab={activeTab}
          onTabChange={val => {
            setActiveTab(val);
          }}
        >
          <Typography variant="h6">{course.courseName}</Typography>
        </FilterBar>
      </Header>
      <ContentContainer>
        <Box mt={4}>
          <Grid container>
            <Grid item xs={12}>
              <Box>
                {activeTab == 0 && (
                  <CourseForm
                    course={course}
                    schoolYears={props.schoolYears}
                    onSubmit={onSubmit}
                  />
                )}
                {activeTab == 1 && (
                  <LessonBuilder
                    lessons={courseLessons}
                    addLesson={addLesson}
                    updateLessonConfig={updateLessonConfig}
                    removeLesson={removeLesson}
                  />
                )}
              </Box>
            </Grid>
          </Grid>
        </Box>
      </ContentContainer>
    </React.Fragment>
  );
};

export default CourseAdminView;
