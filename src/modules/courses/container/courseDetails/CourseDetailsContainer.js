import React from 'react';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import {
  getCourse,
  fetchCourseLessons,
  clearCourse
} from '../../actions/coursesActions';

import CourseDetailsView from './CourseDetailsView';

export const CourseDetailsContainer = props => {
  const router = useRouter();
  React.useEffect(() => {
    if (router.query.id) {
      props.getCourse(router.query.id);
    }
  }, [router]);

  return <CourseDetailsView {...props} />;
};

const mapStateToProps = state => ({
  course: state.courses.course,
  courseLessons: state.courses.courseLessons
});

const mapDispatchToProps = dispatch => ({
  clearCourse: () => dispatch(clearCourse()),
  getCourse: courseId => dispatch(getCourse(courseId)),
  fetchCourseLessons: (filter, paging) =>
    dispatch(fetchCourseLessons(filter, paging))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CourseDetailsContainer);
