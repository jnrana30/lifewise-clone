import React from 'react';
import { connect } from 'react-redux';
import AssessmentsView from './AssessmentsView';
import { fetchAssessments } from '../../actions/coursesActions';

export const AssessmentsContainer = props => {
  return <AssessmentsView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  courses: state.courses.assessments,
  assessments: state.courses.assessments
});

const mapDispatchToProps = dispatch => ({
  fetchAssessments: (filters, paging) =>
    dispatch(fetchAssessments(filters, paging))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AssessmentsContainer);
