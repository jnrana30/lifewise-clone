import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';

import AssessmentsFilter from '../../components/assessments/AssessmentsFilter';
import AssessmentsList from '../../components/assessments/AssessmentsList';
import AssessmentsForm from '../../components/assessments/AssessmentsForm/AssessmentsForm';

import CompleteLesson from '../../components/completeLesson';

// import { assessments } from '../../api/data';

function AssessmentsView({ assessments, ...props }) {
  const [drawerOpen, setDrawerOpen] = React.useState(false);

  const [action, setAction] = React.useState('');
  const [actionLesson, setActionLesson] = React.useState({});

  const { data, loading } = assessments;
  let { filters, paging } = assessments;

  React.useEffect(() => {
    fetchAssessments();
  }, []);

  React.useEffect(() => {
    if (drawerOpen == true) {
      setAction('assessments');
    } else {
      setAction('');
    }
  }, [drawerOpen]);

  const fetchAssessments = (filter, pagingData) => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    if (typeof filter !== 'pagingData' && !_.isEmpty(pagingData)) {
      paging = { ...paging, ...pagingData };
    }
    props.fetchAssessments(filters, paging);
  };

  const setFilter = filter => {
    fetchAssessments(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchAssessments();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchAssessments();
  };

  return (
    <React.Fragment>
      <Header title="Assessments">
        <AssessmentsFilter
          setDrawerOpen={setDrawerOpen}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
        />
      </Header>

      <AssessmentsList
        assessments={data}
        loading={loading}
        paging={paging}
        onPaging={onPaging}
      />

      {/* <Drawer
        open={drawerOpen}
        title="Create an assessment"
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="assessment-form"
      >
        <AssessmentsForm />
      </Drawer> */}

      {action && action !== '' && (
        <CompleteLesson
          action={action}
          lesson={actionLesson}
          courseId={''}
          selectedClass={{}}
          onClose={() => {
            fetchAssessments();
            setDrawerOpen(false);
          }}
        />
      )}
    </React.Fragment>
  );
}

export default AssessmentsView;
