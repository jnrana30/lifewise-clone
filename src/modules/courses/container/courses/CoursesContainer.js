import React from 'react';
import { connect } from 'react-redux';
import CoursesView from './CoursesView';
import { fetchCourses, createCourse } from '../../actions/coursesActions';

export const CoursesContainer = props => {
  return <CoursesView {...props} />;
};

const mapStateToProps = state => ({
  user: state.auth.user,
  courses: state.courses.courses,
  schoolYears: state.app.schoolYears,
  mySchool: state.school.mySchool
});

const mapDispatchToProps = dispatch => ({
  fetchCourses: (filter, paging) => dispatch(fetchCourses(filter, paging)),
  createCourse: data => {
    return new Promise((resolve, reject) => {
      dispatch(createCourse(data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CoursesContainer);
