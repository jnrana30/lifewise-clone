import React from 'react';
import { Header, AccessControl } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import CoursesFilter from '../../components/courses/CoursesFilter';
import CoursesGrid from '../../components/courses/CoursesGrid';
import CoursesList from '../../components/courses/CoursesList';
import CourseForm from '../../components/courses/CourseForm';
import AddToClassForm from '../../components/course/AddToClassForm';
import {
  assignCoursesToClass,
  fetchClasses
} from '../../../../modules/school/api/schoolApis';
import toast from '../../../../utils/toast';

function CoursesView({ courses, user, ...props }) {
  const router = useRouter();

  const isAdmin = user?.profile?.role?.level > 5;
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [formLoading, setFormLoading] = React.useState(false);
  const [course, setCourse] = React.useState({});
  const [addToClassCourse, setAddToClassCourse] = React.useState({});

  const { data, loading } = courses;
  let { filters, paging } = courses;

  // React.useEffect(() => {
  //   if (drawerOpen === false) {
  //     setCourse({});
  //   }
  // }, [drawerOpen]);

  React.useEffect(() => {
    fetchCourses(
      {},
      {
        perPage: isAdmin ? 100 : 12,
        status: !isAdmin ? 'active' : '',
      }
    );
  }, []);

  const fetchCourses = (filter, pagingData) => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    if (typeof filter !== 'pagingData' && !_.isEmpty(pagingData)) {
      paging = { ...paging, ...pagingData };
    }
    props.fetchCourses(filters, paging);
  };

  const setFilter = filter => {
    fetchCourses(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchCourses();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchCourses();
  };

  const onSubmit = async data => {
    setFormLoading(true);
    const res = await props.createCourse(data);
    setFormLoading(false);
    setDrawerOpen(false);
    router.push(`/courses/admin/${res.id}`);
  };

  const onEdit = course => {
    router.push(`/courses/admin/${course.id}`);
  };

  const onDelete = course => {
    console.log('course : ', course);
  };

  const addToClass = course => {
    setAddToClassCourse(course);
  };

  const onAddToClass = async classes => {
    // schoolId, classId, classes,
    setFormLoading(true);
    const promises = [];
    if (classes.add && classes.add.length > 0) {
      classes.add.map(async classId => {
        promises.push(
          await assignCoursesToClass(
            props.mySchool.id,
            classId,
            [addToClassCourse.id],
            'add'
          )
        );
      });
    }
    if (classes.remove && classes.remove.length > 0) {
      classes.remove.map(async classId => {
        promises.push(
          await assignCoursesToClass(
            props.mySchool.id,
            classId,
            [addToClassCourse.id],
            'remove'
          )
        );
      });
    }
    await Promise.all(promises)
      .then(res => {
        toast.success('Course assigned successfully.');
        setFormLoading(false);
        setAddToClassCourse({});
      })
      .catch(err => {
        console.log(err);
        setFormLoading(false);
        toast.success(
          'An error occurred while adding class to the course. Please try again.'
        );
      });
  };

  return (
    <React.Fragment>
      <Header title="Courses">
        <CoursesFilter
          setDrawerOpen={setDrawerOpen}
          isAdmin={isAdmin}
          userProfile={user?.profile}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
          schoolYears={props.schoolYears}
        />
      </Header>

      {isAdmin ? (
        <CoursesList
          courses={data}
          loading={loading}
          paging={paging}
          onPaging={onPaging}
          onEdit={onEdit}
          onDelete={onDelete}
        />
      ) : (
        <CoursesGrid
          courses={data}
          loading={loading}
          paging={paging}
          onPaging={onPaging}
          addToClass={addToClass}
        />
      )}
      <Drawer
        open={drawerOpen}
        title={course?.id ? 'Edit course' : 'Add Course'}
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="course-form"
        buttonLoading={formLoading}
      >
        <Typography variant="body1" color="textPrimary">
          <b>Add new course to the platform.</b>
        </Typography>
        <CourseForm onSubmit={onSubmit} schoolYears={props.schoolYears} />
      </Drawer>

      <Drawer
        open={!!addToClassCourse?.id}
        title="Add course to class"
        onClose={() => {
          setAddToClassCourse({});
        }}
        form="course-add-to-class-form"
        buttonLoading={formLoading}
      >
        {/* <CourseForm onSubmit={onSubmit} schoolYears={props.schoolYears} /> */}
        <AddToClassForm onSubmit={onAddToClass} course={addToClassCourse} />
      </Drawer>
    </React.Fragment>
  );
}

export default CoursesView;
