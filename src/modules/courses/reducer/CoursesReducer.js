import * as types from '../actions/coursesTypes';

const initialState = {
  courses: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0
    },
    filters: {}
  },
  lessons: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0
    },
    filters: {}
  },
  courseLessons: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 12,
      records: 0
    },
    filters: {}
  },
  course: {},
  lesson: {},
  loading: {
    lesson: false,
    course: false
  },
  resources: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 25,
      records: 0
    },
    filters: {}
  },
  assessments: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 25,
      records: 0
    },
    filters: {}
  },
  studentAssessments: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 25,
      records: 0
    },
    filters: {}
  },
  actionLesson: {}
};

const CoursesReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.CLEAR_LESSON:
      return {
        ...state,
        lesson: {},
        loading: {
          ...state.loading,
          lesson: false
        }
      };

    case types.CLEAR_COURSE:
      return {
        ...state,
        course: {},
        courseLessons: {
          loading: false,
          data: [],
          paging: {
            page: 1,
            perPage: 12,
            records: 0
          },
          filters: {}
        }
      };

    case types.SET_ACTION_LESSON:
      return {
        ...state,
        actionLesson: action.lesson
      };

    case types.GET_LESSON:
      return {
        ...state,
        lesson: {},
        loading: {
          ...state.loading,
          lesson: true
        }
      };

    case types.SET_COURSE:
      return {
        ...state,
        course: action.data
      };

    case types.SET_LESSON:
      return {
        ...state,
        lesson: action.data,
        loading: {
          ...state.loading,
          lesson: false
        }
      };

    case types.FETCH_COURSES:
      return {
        ...state,
        courses: {
          ...state.courses,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.courses.paging,
            ...action.paging
          }
        }
      };

    case types.SET_COURSES:
      return {
        ...state,
        courses: {
          ...state.courses,
          loading: false,
          data: action.data.courses,
          paging: {
            ...state.courses.paging,
            records: action.data.records
          }
        }
      };

    case types.FETCH_LESSONS:
      return {
        ...state,
        lessons: {
          ...state.lessons,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.lessons.paging,
            ...action.paging
          }
        }
      };

    case types.RESET_LESSONS_FILTER:
      return {
        ...state,
        lessons: {
          ...state.lessons,
          filters: {}
        }
      };

    case types.SET_LESSONS:
      return {
        ...state,
        lessons: {
          ...state.lessons,
          loading: false,
          data: action.data.lessons,
          paging: {
            ...state.lessons.paging,
            records: action.data.records
          }
        }
      };

    case types.FETCH_COURSE_LESSONS:
      return {
        ...state,
        courseLessons: {
          ...state.courseLessons,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.courseLessons.paging,
            ...action.paging
          }
        }
      };

    case types.SET_COURSE_LESSONS:
      return {
        ...state,
        courseLessons: {
          ...state.courseLessons,
          loading: false,
          data: action.data.lessons,
          paging: {
            ...state.courseLessons.paging,
            records: action.data.records
          }
        }
      };

    case types.FETCH_RESOURCES:
      return {
        ...state,
        resources: {
          ...state.resources,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.resources.paging,
            ...action.paging
          }
        }
      };

    case types.SET_RESOURCES:
      return {
        ...state,
        resources: {
          ...state.resources,
          loading: false,
          data: action.data.resources,
          paging: {
            ...state.resources.paging,
            records: action.data.records
          }
        }
      };

    case types.FETCH_ASSESSMENTS:
      return {
        ...state,
        assessments: {
          ...state.assessments,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.assessments.paging,
            ...action.paging
          }
        }
      };

    case types.SET_ASSESSMENTS:
      return {
        ...state,
        assessments: {
          ...state.assessments,
          loading: false,
          data: action.data.assessments,
          paging: {
            ...state.assessments.paging,
            records: action.data.records
          }
        }
      };

    case types.FETCH_STUDENT_ASSESSMENTS:
      return {
        ...state,
        studentAssessments: {
          ...state.studentAssessments,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.studentAssessments.paging,
            ...action.paging
          }
        }
      };

    case types.SET_STUDENT_ASSESSMENTS:
      return {
        ...state,
        studentAssessments: {
          ...state.studentAssessments,
          loading: false,
          data: action.data.assessments,
          paging: {
            ...state.studentAssessments.paging,
            records: action.data.records
          }
        }
      };

    default:
      return state;
  }
};
export default CoursesReducer;
