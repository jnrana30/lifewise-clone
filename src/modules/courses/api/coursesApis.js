import api from 'src/api/index';
import { convertObjectToQuerystring } from 'src/utils/helpers';
const _ = require('lodash');

/* COURSES APIs START */
export const fetchCourses = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/courses/search?${filter}`, null, 'GET');
};
export const getCourse = courseId => {
  return api(`/courses/${courseId}`, null, 'GET');
};
export const createCourse = data => {
  return api(`/courses`, data, 'POST');
};
export const updateCourse = (data, courseId) => {
  return api(`/courses/${courseId}`, data, 'PATCH');
};
export const assignLessonToCourse = (courseId, lessonId, data) => {
  return api(`/courses/${courseId}/lesson/${lessonId}/assign`, data, 'PATCH');
};
/* COURSES APIs END */

/* LESSONS APIs START */
export const getLessons = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/courses/lessons?${filter}`, null, 'GET');
};
export const fetchLessons = filters => {
  let filter = '';
  if (filters.tags && !_.isEmpty(filters.tags)) {
    filters.tags = `[${filters.tags.map(tag => '"' + tag.id + '"')}]`;
  }
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/courses/lessons/search?${filter}`, null, 'GET');
};
export const getLesson = lessonId => {
  return api(`/courses/lesson/${lessonId}`, null, 'GET');
};
export const getLessonContent = lessonId => {
  return api(`/courses/lesson/${lessonId}/content`, null, 'GET');
};
export const getLessonContentAll = lessonId => {
  return api(`/courses/lesson/${lessonId}/content/all`, null, 'GET');
};
export const createLesson = data => {
  return api(`/courses/lesson`, data, 'POST');
};
export const updateLesson = (data, lessonId) => {
  return api(`/courses/lesson/${lessonId}`, data, 'PATCH');
};
/* LESSONS APIs END */

export const getCategories = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/courses/lessons/categories?${filter}`, null, 'GET');
};
export const getCategory = categoryId => {
  return api(`/courses/lessons/category/${categoryId}`, null, 'GET');
};

export const getTags = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/courses/tags?${filter}`, null, 'GET');
};
export const getTagsByType = (type, filters) => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/courses/tags/${type}?${filter}`, null, 'GET');
};

export const courseLessonConfigure = (courseId, lessonId, data) => {
  return api(`/courses/${courseId}/lesson/${lessonId}/assign`, data, 'PATCH');
};
export const assignLessonsToCourse = (courseId, data) => {
  return api(`/courses/${courseId}/lesson/assign`, data, 'PATCH');
};

export const deleteLessonFromCourse = (courseId, lessonId) => {
  return api(`/courses/${courseId}/lesson/${lessonId}/assign`, {}, 'DELETE');
};

// RESOURCES API START
export const fetchResources = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/courses/resources?${filter}`, null, 'GET');
};
export const getResource = resourceId => {
  return api(`/courses/resources/${resourceId}`, null, 'GET');
};
export const createResource = data => {
  return api(`/courses/resources`, data, 'POST');
};
export const updateResource = (resourceId, data) => {
  return api(`/courses/resource/${resourceId}`, data, 'PATCH');
};
// RESOURCES API END

export const courseLessonFeedback = data => {
  return api(`/courses/lesson/feedback`, data, 'PUT');
};

export const submitAssessment = (classId, lessonId, students) => {
  const payload = {
    lessonId,
    students
  };
  return api(`/assessments/submit/${classId}`, payload, 'PUT');
};

export const fetchAssessments = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/assessments?${filter}`, null, 'GET');
};

export const fetchStudentAssessments = (studentId, filters) => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/assessments/student/${studentId}?${filter}`, null, 'GET');
};

export const updateAssessment = (classId, data) => {
  return api(`/assessments/update/${classId}`, data, 'PATCH');
};
