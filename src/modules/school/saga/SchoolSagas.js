import { takeLatest, all, fork, put, call, select } from 'redux-saga/effects';
import * as schoolTypes from '../actions/schoolTypes';
import * as schoolActions from '../actions/schoolActions';
import * as schoolApis from '../api/schoolApis';
import toast from 'src/utils/toast';
import _ from 'lodash';
import * as packagesApis from '../../packages/api/packagesApis';

export const getTeachers = state => state.account.teachers;
export const getUserId = state => state.auth.user.userId;

function* fetchSchools({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const users = yield call(schoolApis.fetchSchools, filter);
    yield put(schoolActions.setSchools(users));
  } catch (error) {
    yield put(schoolActions.setSchools([]));
  }
}

function* fetchSchoolDetail({ filters }) {
  try {
    const schoolData = yield call(schoolApis.fetchSchoolDetail, filters);

    yield put(schoolActions.setSchoolDetail(schoolData));
  } catch (error) {
    yield put(schoolActions.setSchoolDetail({}));
  }
}

function* fetchSchoolSubscriptions({ filters }) {
  try {
    const schoolSubscriptionsData = yield call(
      schoolApis.fetchSchoolSubscriptions,
      filters
    );

    yield put(schoolActions.setSchoolSubscriptions(schoolSubscriptionsData));
  } catch (error) {
    yield put(schoolActions.setSchoolSubscriptions([]));
  }
}

function* fetchSchoolSubscriptionsType({ filters }) {
  try {
    const schoolSubscriptionsData = yield call(
      schoolApis.setTagsBySubscriptionsType
    );
    yield put(
      schoolActions.setSchoolSubscriptionsType(schoolSubscriptionsData)
    );
  } catch (error) {
    yield put(schoolActions.setSchoolSubscriptionsType([]));
  }
}

function* fetchMySchool() {
  try {
    const school = yield call(schoolApis.fetchMySchool);
    yield put(schoolActions.setMySchool(school));
  } catch (error) {
    yield put(schoolActions.setMySchool({}));
  }
}

function* postSchool({ data, resolve, reject }) {
  try {
    const res = yield call(schoolApis.postSchool, data);
    toast.success('School created successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* patchSchool({ schoolId, data, resolve, reject }) {
  try {
    const res = yield call(schoolApis.patchSchool, schoolId, data);
    toast.success('School detail update successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* createSchoolClasses({ schoolId, data, resolve, reject }) {
  try {
    const res = yield call(schoolApis.createClasses, schoolId, data);
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* fetchClasses({ filters, paging }) {
  try {
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const classes = yield call(schoolApis.fetchClasses, filter);
    yield put(schoolActions.setClasses(classes));
  } catch (error) {
    console.log('error : ', error);
    yield put(schoolActions.setClasses([]));
  }
}

function* patchClass({ schoolId, classId, data, resolve, reject }) {
  try {
    const res = yield call(schoolApis.patchClass, schoolId, classId, data);
    resolve(res);
  } catch (error) {
    reject(error);
  }
}

function* addSubscriptionToSchool({ data, resolve, reject }) {
  try {
    const res = yield call(schoolApis.addSubscription, data);
    toast.success('Subscription add successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* updateSubscriptionToSchool({
  data,
  subscriptionId,
  resolve,
  reject
}) {
  try {
    const res = yield call(schoolApis.updateSubscription, data, subscriptionId);

    toast.success('Subscription updated successfully');
    resolve(res);
  } catch (error) {
    toast.error(error?.error);
    reject(error);
  }
}

function* fetchMyClasses({ filters, paging }) {
  try {
    const userId = yield select(getUserId);
    let filter = {
      ...paging,
      ...filters
    };
    filter = _.omit(filter, ['records']);
    const classes = yield call(schoolApis.getTeacherClasses, filter);
    yield put(schoolActions.setMyClasses(classes));
  } catch (error) {
    console.log('error : ', error);
    yield put(schoolActions.setMyClasses([]));
  }
}

export function* watchSagas() {
  yield takeLatest(schoolTypes.FETCH_SCHOOLS, fetchSchools);
  yield takeLatest(schoolTypes.FETCH_SCHOOL_DETAILS, fetchSchoolDetail);
  yield takeLatest(schoolTypes.FETCH_CLASSES, fetchClasses);
  yield takeLatest(schoolTypes.FETCH_MY_SCHOOL, fetchMySchool);
  yield takeLatest(
    schoolTypes.FETCH_SCHOOL_SUBSCRIPTIONS,
    fetchSchoolSubscriptions
  );
  yield takeLatest(
    schoolTypes.FETCH_SCHOOL_SUBSCRIPTIONS_TYPE,
    fetchSchoolSubscriptionsType
  );
  yield takeLatest(schoolTypes.POST_SCHOOL, postSchool);
  yield takeLatest(schoolTypes.PATCH_SCHOOL, patchSchool);
  yield takeLatest(schoolTypes.PATCH_CLASS, patchClass);
  yield takeLatest(
    schoolTypes.ADD_SUBSCRIPTION_TO_SCHOOL,
    addSubscriptionToSchool
  );
  yield takeLatest(
    schoolTypes.PATCH_SUBSCRIPTION_TO_SCHOOL,
    updateSubscriptionToSchool
  );
  yield takeLatest(schoolTypes.CREATE_SCHOOL_CLASSES, createSchoolClasses);

  yield takeLatest(schoolTypes.FETCH_MY_CLASSES, fetchMyClasses);
}

export default function* runSagas() {
  yield all([fork(watchSagas)]);
}
