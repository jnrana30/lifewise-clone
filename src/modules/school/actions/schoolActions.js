import * as types from './schoolTypes';

export const fetchSchools = (filters, paging) => ({
  type: types.FETCH_SCHOOLS,
  filters,
  paging
});
export const setSchools = data => ({ type: types.SET_SCHOOLS, data });

export const fetchSchoolDetail = (filters, paging) => ({
  type: types.FETCH_SCHOOL_DETAILS,
  filters,
  paging
});

export const fetchSchoolSubscriptions = (filters, paging) => ({
  type: types.FETCH_SCHOOL_SUBSCRIPTIONS,
  filters,
  paging
});

export const setSchoolSubscriptions = data => ({
  type: types.SET_SCHOOL_SUBSCRIPTIONS,
  data
});

export const fetchSchoolSubscriptionsType = () => ({
  type: types.FETCH_SCHOOL_SUBSCRIPTIONS_TYPE
});

export const setSchoolSubscriptionsType = data => ({
  type: types.SET_SCHOOL_SUBSCRIPTIONS_TYPE,
  data
});

export const setSchoolDetail = data => ({
  type: types.SET_SCHOOL_DETAILS,
  data
});

export const createSchool = (data, resolve, reject) => ({
  type: types.POST_SCHOOL,
  data,
  resolve,
  reject
});

export const updateSchool = (schoolId, data, resolve, reject) => ({
  type: types.PATCH_SCHOOL,
  schoolId,
  data,
  resolve,
  reject
});

export const createClasses = (schoolId, data, resolve, reject) => ({
  type: types.CREATE_SCHOOL_CLASSES,
  schoolId,
  data,
  resolve,
  reject
});

export const patchClass = (schoolId, classId, data, resolve, reject) => ({
  type: types.PATCH_CLASS,
  schoolId,
  classId,
  data,
  resolve,
  reject
});

export const addSubscriptionToSchool = (data, resolve, reject) => ({
  type: types.ADD_SUBSCRIPTION_TO_SCHOOL,
  data,
  resolve,
  reject
});

export const updateSubscriptionToSchool = (
  subscriptionId,
  data,
  resolve,
  reject
) => ({
  type: types.PATCH_SUBSCRIPTION_TO_SCHOOL,
  subscriptionId,
  data,
  resolve,
  reject
});

export const getMySchool = () => ({ type: types.FETCH_MY_SCHOOL });
export const setMySchool = data => ({ type: types.SET_MY_SCHOOL, data });

export const fetchClasses = (filters, paging) => ({
  type: types.FETCH_CLASSES,
  filters,
  paging
});
export const setClasses = data => ({ type: types.SET_CLASSES, data });

export const fetchMyClasses = (filters, paging) => ({
  type: types.FETCH_MY_CLASSES,
  filters,
  paging
});

export const setMyClasses = data => ({
  type: types.SET_MY_CLASSES,
  data
});
