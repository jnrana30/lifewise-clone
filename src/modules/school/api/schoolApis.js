import api from 'src/api/index';
import { convertObjectToQuerystring } from 'src/utils/helpers';
const _ = require('lodash');

export const fetchSchools = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`/schools/search?${filter}`, null, 'GET');
};

export const searchSchools = (filters, details = false) => {
  return new Promise((resolve, reject) => {
    let filter = '';
    if (!_.isEmpty(filters)) {
      filter = `${convertObjectToQuerystring(filters)}`;
    }
    api(`/schools/search?${filter}`, null, 'GET')
      .then(res => {
        if (res.schools && res.schools.length > 0) {
          let results = res.schools.map(school => ({
            id: school.id,
            schoolName: `${school.schoolName}${
              school?.schoolAddresses && school?.schoolAddresses[0]?.city
                ? ' (' + school?.schoolAddresses[0]?.city + ')'
                : ''
            }`,
            ...(details && { ...school })
          }));
          resolve(results);
        } else {
          resolve([]);
        }
      })
      .catch(err => {
        console.log('err : ', err);
        resolve([]);
        // reject(err);
      });
  });
};

export const fetchSchoolDetail = id => {
  return api(`/schools?id=${id}`, null, 'GET');
};

export const fetchMySchool = () => {
  return api('/schools', null, 'GET');
};

export const fetchSchoolSubscriptions = id => {
  return api(`/subscriptions?schoolId=${id}`, null, 'GET');
};

export const setTagsBySubscriptionsType = () => {
  return api(`/subscriptions/types`, null, 'GET');
};

export const fetchSchoolById = id => {
  return api(`/schools?id=${id}`, null, 'GET');
};

// export const addStudentsToClass = (schoolId, classId, students) => {
//   return api(
//     `/schools/${schoolId}/classes/${classId}/students`,
//     students,
//     'PUT'
//   );
// };

export const patchSchool = (schoolId, data) => {
  return api(`/schools/${schoolId}`, data, 'PATCH');
};

export const postSchool = data => {
  return api('/schools', data, 'POST');
};

export const createClasses = (schoolId, data) => {
  return api(`/schools/${schoolId}/classes/multiple`, data, 'POST');
};

export const patchClass = (schoolId, classId, data) => {
  return api(`/schools/${schoolId}/classes/${classId}`, data, 'PATCH');
};

export const getTeacherClasses = (filters, teacherId) => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  if (teacherId && teacherId !== '') {
    filter += `&teacherId=${teacherId}`;
  }
  return api(`schools/teacher/classes?${filter}`, null, 'GET');
};

export const getStudentClasses = (studentId, schoolId) => {
  return api(
    `schools/student/classes?studentId=${studentId}&schoolId=${schoolId}`,
    null,
    'GET'
  );
};

export const fetchClasses = filters => {
  let filter = '';
  if (!_.isEmpty(filters)) {
    filter = `${convertObjectToQuerystring(filters)}`;
  }
  return api(`schools/classes?${filter}`, null, 'GET');
};

export const getSchoolClasses = schoolId => {
  return api(`schools/classes?schoolId=${schoolId}`, null, 'GET');
};

export const addStudentsToClass = (schoolId, classId, students) => {
  const data = {
    students,
    mode: 'add'
  };
  return api(`schools/${schoolId}/classes/${classId}/students`, data, 'PUT');
};

export const addClassToStudents = (oldClassId, classId, student) => {
  const data = {
    add: [classId],
    remove: oldClassId
  };
  return api(`schools/student/${student}/classes`, data, 'PUT');
};

export const removeStudentsFromClass = (schoolId, classId, students) => {
  const data = {
    students,
    mode: 'remove'
  };
  return api(`schools/${schoolId}/classes/${classId}/students`, data, 'PUT');
};

export const replaceStudentsToClass = (schoolId, classId, students) => {
  const data = {
    students,
    mode: 'replace'
  };
  return api(`schools/${schoolId}/classes/${classId}/students`, data, 'PUT');
};

export const assignCoursesToClass = (
  schoolId,
  classId,
  courses,
  mode = 'add'
) => {
  const data = {
    courses,
    mode
  };
  return api(`schools/${schoolId}/classes/${classId}/courses`, data, 'PUT');
};

export const assignLessonsToClass = (
  schoolId,
  classId,
  lessons,
  mode = 'add'
) => {
  const data = {
    lessons,
    mode
  };
  return api(`schools/${schoolId}/classes/${classId}/lessons`, data, 'PUT');
};

export const addSubscription = data => {
  return api(`/subscriptions`, data, 'POST');
};

export const updateSubscription = (data, subscriptionId) => {
  return api(`/subscriptions/${subscriptionId}`, data, 'PATCH');
};

export const classParticipation = (schoolId, classId, data) => {
  return api(
    `/schools/${schoolId}/classes/${classId}/participation`,
    data,
    'PATCH'
  );
};
