function createTeacher(id, name, email, lastActivity, status) {
  return { id, name, email, lastActivity, status };
}

function createEmployees(id, name, email, role, lastActivity, status) {
  return { id, name, email, role, lastActivity, status };
}

function createStudent(id, name, year, className, academicYear, status) {
  return { id, name, year, className, academicYear, status };
}

function createClass(id, name, year, noStudents, academicYear, status) {
  return { id, name, year, noStudents, academicYear, status };
}

function createSchool(id, name, address, noTeachers, status) {
  return {
    id: Math.random().toString(36).substr(2),
    name,
    address,
    noTeachers,
    status
  };
}

export const schools = [
  createSchool('', 'School Demo 01', 'London, UK', 10, true),
  createSchool('', 'School Demo 02', 'Bristol, UK', 8, false),
  createSchool('', 'School Demo 03', 'Cardiff, UK', 9, true)
];

export const teachers = [
  createTeacher('', 'John Doe', 'john@doe.com', '2021/06/01', true),
  createTeacher('', 'Jane Doe', 'Jane@doe.com', '2021/07/11', false),
  createTeacher('', 'Demo User', 'demo@demo.com', '2021/04/30', true)
];

export const employees = [
  createEmployees(
    '',
    'John Doe',
    'john@doe.com',
    'Principal',
    '2021/06/01',
    true
  ),
  createEmployees(
    '',
    'Jane Doe',
    'Jane@doe.com',
    'Counselor',
    '2021/07/11',
    false
  ),
  createEmployees(
    '',
    'Demo User',
    'demo@demo.com',
    'Counselor',
    '2021/04/30',
    true
  )
];

export const students = [
  createStudent('', 'John Doe', '7', 'Class 1 A', '2021', true),
  createStudent('', 'Jane Doe', '6', 'Class 2 A', '2020', true),
  createStudent('', 'Demo Demo', '8', 'Class 1 A', '2021', false)
];

export const classes = [
  createClass('', 'Class 1 A', '7', '227', '2021', true),
  createClass('', 'Class 1 B', '6', '217', '2020', true),
  createClass('', 'Class 2 A', '8', '254', '2021', false),
  createClass('', 'Class 2 B', '8', '220', '2020', true)
];
