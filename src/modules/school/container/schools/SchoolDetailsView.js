import React, { useEffect, useState } from 'react';
import { Header, FilterBar } from 'src/components/App';
import { Typography, Box, Grid, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import {
  SchoolBilling,
  SchoolDetails,
  SchoolDomains,
  SchoolSubscriptions,
  AddSchoolSubscription
} from '../../components/schools/index';
import { ContentContainer } from '../../../../components/shared/Form/File/styles';
import PackageForm from '../../../packages/components/PackageForm';
import { Drawer } from '../../../../components/shared';

const useStyles = makeStyles(theme => ({
  tabFiltrationWrapper: {
    '& .MuiTabs-flexContainer': {
      justifyContent: 'center'
    },
    '& .MuiAppBar-root': {
      backgroundColor: 'transparent !important'
    }
  },
  schoolTabContentContainer: {
    '& .MuiGrid-container': {
      justifyContent: 'center'
    }
  },
  schoolTabFormWrapper: {
    display: 'flex',
    background: '#ffffff',
    borderRadius: 4,
    padding: '50px 60px',
    maxWidth: 700
  },
  schoolSubscriptionsTabFormWrapper: {
    padding: 0
  }
}));

function SchoolView(props) {
  const [activeTab, setActiveTab] = useState(0);
  const [schoolId, setSchoolId] = useState();
  const [drawerOpenSubscription, setDrawerOpenSubscription] = useState(false);
  const [addSubscriptionLoading, setAddSubscriptionLoading] = useState(false);
  const [selectedSubscription, setSelectedSubscription] = useState();

  const { data: schoolDetail, loading } = props.schoolDetail;

  const classes = useStyles();

  useEffect(() => {
    const newSchoolId = props?.mySchool?.id
      ? props?.mySchool?.id
      : window.history.state.as.split('/')[2];
    setSchoolId(newSchoolId);
    newSchoolId && props.fetchSchoolDetail(newSchoolId);
    newSchoolId && props.fetchSchoolSubscriptions(newSchoolId);
    props.fetchSchoolSubscriptionsType();
  }, [props?.mySchool?.id]);

  const onSubmitForm = async payload => {
    const res = await props.patchSchool(payload, schoolId);
    schoolId && props.fetchSchoolDetail(schoolId);
    return res;
  };

  const handleAddSubscriptions = () => {
    setDrawerOpenSubscription(true);
  };

  const handleEditClick = data => {
    setSelectedSubscription(data);
    setDrawerOpenSubscription(true);
  };

  const onSubmitToSubscription = async data => {
    setAddSubscriptionLoading(true);
    if (data.id) {
      await props.patchSubscriptionToSchool(data, data.id);
    } else {
      await props.addSubscriptionToSchool(data);
    }
    schoolId && props.fetchSchoolSubscriptions(schoolId);
    setSelectedSubscription(null);
    setDrawerOpenSubscription(false);
    setAddSubscriptionLoading(false);
  };

  const schoolDetailTabs = [
    {
      name: 'schoolDetails',
      component: (
        <SchoolDetails schoolDetail={schoolDetail} submitForm={onSubmitForm} />
      ),
      width: 12
    },
    {
      name: 'schoolBilling',
      component: (
        <SchoolBilling schoolDetail={schoolDetail} submitForm={onSubmitForm} />
      ),
      width: 12
    },
    {
      name: 'domains',
      component: (
        <SchoolDomains schoolDetail={schoolDetail} submitForm={onSubmitForm} />
      ),
      width: 12
    },
    {
      name: 'schoolSubscriptions',
      component: (
        <SchoolSubscriptions
          schoolSubscriptions={props.schoolSubscriptions}
          addSubscriptions={handleAddSubscriptions}
          onEdit={handleEditClick}
        />
      ),
      width: 12
    }
  ];

  const schoolTabs = ['Details', 'Billing Details', 'Domains', 'Subscriptions'];

  if (!props.user?.profile?.role?.level > 4) {
    schoolDetailTabs.splice(2, 1);
    schoolTabs.splice(2, 1);
  }

  const schoolDetailTab = schoolDetailTabs[activeTab];

  return (
    <React.Fragment>
      <Header title="Schools Details">
        <FilterBar
          tabs={schoolTabs}
          className={classes.tabFiltrationWrapper}
          currentTab={activeTab}
          onTabChange={val => {
            setActiveTab(val);
          }}
          showBarContent={false}
        >
          <Typography variant="h6">{schoolDetail?.schoolName}</Typography>
        </FilterBar>
      </Header>

      <ContentContainer>
        <Box className={classes.schoolTabContentContainer}>
          <Grid container>
            {!loading && schoolDetailTab && (
              <Grid
                item
                xs={9}
                justify={'center'}
                component={Paper}
                className={
                  schoolDetailTabs.findIndex(
                    tab => tab.name === 'schoolSubscriptions'
                  ) === activeTab
                    ? `${classes.schoolSubscriptionsTabFormWrapper} ${classes.schoolTabFormWrapper}`
                    : classes.schoolTabFormWrapper
                }
              >
                <Grid item xs={schoolDetailTab.width}>
                  <Box>{schoolDetailTab.component}</Box>
                </Grid>
              </Grid>
            )}
          </Grid>
        </Box>

        <Drawer
          open={drawerOpenSubscription}
          title={
            selectedSubscription?.id
              ? 'Edit Subscriptions'
              : 'Add Subscriptions'
          }
          onClose={() => {
            setDrawerOpenSubscription(false);
            setSelectedSubscription(null);
          }}
          form="school-Subscription-form"
          buttonLoading={addSubscriptionLoading}
        >
          <Typography variant="body1" color="textPrimary">
            <b>
              {selectedSubscription?.id
                ? 'Edit subscriptions'
                : 'Add new subscriptions'}
            </b>
          </Typography>
          <AddSchoolSubscription
            schoolSubscriptionsType={props.schoolSubscriptionsType}
            schoolDetail={schoolDetail}
            subscriptionData={selectedSubscription}
            onSubmit={onSubmitToSubscription}
          />
        </Drawer>
      </ContentContainer>
    </React.Fragment>
  );
}

export default SchoolView;
