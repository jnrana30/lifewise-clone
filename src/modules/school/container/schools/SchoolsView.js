import React from 'react';
import { Header } from 'src/components/App';
import { Drawer } from 'src/components/shared';
import { Typography } from '@material-ui/core';
import Head from 'next/head';
import { useRouter } from 'next/router';

import SchoolFilter from '../../components/schools/SchoolFilter';
import SchoolList from '../../components/schools/SchoolList';
import SchoolForm from '../../components/schools/SchoolForm';
import { schools } from '../../api/data';
import _ from 'lodash';

function SchoolView({ schools, schoolSubscriptionsType, ...props }) {
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [formLoading, setFormLoading] = React.useState(false);
  const { data, loading } = schools;
  let { filters, paging } = schools;
  const router = useRouter();

  React.useEffect(() => {
    fetchSchools();
    props.fetchSchoolSubscriptionsType();
  }, []);

  const fetchSchools = filter => {
    if (typeof filter !== 'undefined' && !_.isEmpty(filter)) {
      filters = { ...filters, ...filter };
      paging = { ...paging, page: 1 };
    }
    props.fetchSchools(filters, paging);
  };

  const setFilter = filter => {
    fetchSchools(filter);
  };

  const unsetFilter = filter => {
    filters = _.omit(filters, [filter]);
    fetchSchools();
  };

  const onPaging = (page, perPage = 0) => {
    if (perPage > 0) {
      paging.perPage = perPage;
    }
    paging.page = page + 1;
    fetchSchools();
  };

  const onSubmit = async (data, subscriptionFormData) => {
    setFormLoading(true);
    try {
      const emailDomains = data.emailDomains;
      data = _.omit(data, ['emailDomains']);
      const res = await props.createSchool(data);
      await props.patchSchool({ emailDomains }, res.id);
      // fetchSchools();
      await props.addSubscriptionToSchool({
        ...subscriptionFormData,
        schoolId: res.id,
      });
      setFormLoading(false);
      setDrawerOpen(false);
      router.push(`/schools/${res.id}`);
    } catch (error) {
      setFormLoading(false);
    }
  };

  return (
    <React.Fragment>
      {/* <Head>
        <script
          src={`https://maps.googleapis.com/maps/api/js?key=${process.env.GOOGLE_MAPS_API_KEY}&libraries=places`}
        ></script>
      </Head> */}
      <Header title="Schools">
        <SchoolFilter
          setDrawerOpen={setDrawerOpen}
          setFilter={setFilter}
          unsetFilter={unsetFilter}
          filters={filters}
        />
      </Header>

      <SchoolList
        schools={data}
        loading={loading}
        paging={paging}
        onPaging={onPaging}
      />

      <Drawer
        open={drawerOpen}
        title="Add School"
        onClose={() => {
          setDrawerOpen(false);
        }}
        form="school-form"
        buttonLoading={formLoading}
      >
        <Typography variant="body1" color="textPrimary">
          <b>Enter school details.</b>
        </Typography>
        <SchoolForm
          onSubmit={onSubmit}
          subscriptionsTagType={[ ...(schoolSubscriptionsType?.data || []) ]}
          getSubscriptionsTag={props.fetchSchoolSubscriptionsType}
        />
      </Drawer>
    </React.Fragment>
  );
}

export default SchoolView;
