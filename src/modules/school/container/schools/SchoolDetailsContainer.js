import React from 'react';
import { connect } from 'react-redux';
import SchoolDetailsView from './SchoolDetailsView';
import {
  fetchSchoolDetail,
  getMySchool,
  updateSchool,
  fetchSchoolSubscriptions,
  fetchSchoolSubscriptionsType,
  addSubscriptionToSchool,
  updateSubscriptionToSchool,
} from '../../actions/schoolActions';
import {createPackage, patchPackage} from "../../../packages/actions/packagesActions";

export const SchoolDetailsContainer = props => {
  return <SchoolDetailsView {...props} />;
};

const mapStateToProps = state => ({
  schoolDetail: state.school.schoolDetail,
  mySchool: state.school.mySchool,
  schoolSubscriptions: state.school.schoolSubscriptions,
  schoolSubscriptionsType: state.school.schoolSubscriptionsType,
  user: state.auth.user,
});

const mapDispatchToProps = dispatch => ({
  fetchSchoolDetail: (id) => dispatch(fetchSchoolDetail(id)),
  fetchSchoolSubscriptions: (id) => dispatch(fetchSchoolSubscriptions(id)),
  fetchSchoolSubscriptionsType: () => dispatch(fetchSchoolSubscriptionsType()),
  patchSchool: (data, schoolId) => {
    return new Promise((resolve, reject) => {
      dispatch(updateSchool(schoolId, data, resolve, reject));
    });
  },
  addSubscriptionToSchool: data => {
    return new Promise((resolve, reject) => {
      dispatch(addSubscriptionToSchool(data, resolve, reject));
    });
  },
  patchSubscriptionToSchool: (data, subscriptionId) => {
    return new Promise((resolve, reject) => {
      dispatch(updateSubscriptionToSchool(subscriptionId, data, resolve, reject));
    });
  },
  getMySchool: () => dispatch(getMySchool()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SchoolDetailsContainer);
