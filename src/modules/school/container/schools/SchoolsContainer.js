import React from 'react';
import { connect } from 'react-redux';
import SchoolsView from './SchoolsView';
import {
  fetchSchools,
  createSchool,
  updateSchool,
  fetchSchoolSubscriptionsType,
  addSubscriptionToSchool,
} from '../../actions/schoolActions';

export const SchoolsContainer = props => {
  return <SchoolsView {...props} />;
};

const mapStateToProps = state => ({
  schools: state.school.schools,
  schoolSubscriptionsType: state.school.schoolSubscriptionsType,
});

const mapDispatchToProps = dispatch => ({
  fetchSchools: (filter, paging) => dispatch(fetchSchools(filter, paging)),
  fetchSchoolSubscriptionsType: () => dispatch(fetchSchoolSubscriptionsType()),
  addSubscriptionToSchool: (data) => {
    return new Promise((resolve, reject) => {
      dispatch(addSubscriptionToSchool(data, resolve, reject));
    });
  },
  createSchool: data => {
    return new Promise((resolve, reject) => {
      dispatch(createSchool(data, resolve, reject));
    });
  },
  patchSchool: (data, schoolId) => {
    return new Promise((resolve, reject) => {
      dispatch(updateSchool(schoolId, data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(SchoolsContainer);
