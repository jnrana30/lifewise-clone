import React, { useEffect, useState } from 'react';
import moment from 'moment';
import * as Yup from 'yup';
import { sortBy } from 'lodash';
import { Grid, Typography, Box } from '@material-ui/core';

import { Form } from '../../../../components/shared';
import { schoolSubscriptionStatusOptions } from '../../../../config';
import { AccessControl } from '../../../../components/App';

export default function AddSchoolSubscription({ onSubmit, schoolSubscriptionsType, schoolDetail, subscriptionData }) {
  const { data: subscriptionsTagType } = schoolSubscriptionsType;

  const [formData, setFormData] = useState({});

  useEffect(() => {
    if (subscriptionData?.id) {
      setFormData({
        ...formData,
        subscriptionStatus: subscriptionData?.subscriptionStatus || '',
        subscriptionTypes: subscriptionData?.subscriptionTypes || [],
        startDate: subscriptionData?.startDate ? moment(subscriptionData?.startDate).format() : moment().format(),
        endDate: subscriptionData?.endDate ? moment(subscriptionData?.endDate).format() : moment().format(),
        price: subscriptionData?.price || '',
      })
    }
  }, [subscriptionData]);

  const preventDefault = (event) => event.preventDefault();

  const handleClickAddSchoolSubscription = (event) => {
    preventDefault(event)
    // addSubscriptions()
  };

  const searchTags = async (search) => {
    if (search) {
      return subscriptionsTagType.filter(type => type.typeName.toLocaleLowerCase().includes(search.toLocaleLowerCase()));
    } else {
      return subscriptionsTagType;
    }
  };

  const getMomentUnit = (unitValue) => {
    let unit = '';
    switch (unitValue) {
      case 'w': {
        unit = 'days';
       break
      }
      case 'm': {
        unit = 'months';
       break
      }
      case 'y': {
        unit = 'years';
       break
      }
      default : {
        unit = 'days';
        break
      }
    }
    return unit;
  };

  const handleSubscriptionTypesChange = (selectedData) => {
    if (!selectedData.length) return;
    if (subscriptionData?.id) return;
    let subscriptionTimePeriod;
    if (selectedData.find(e => e.timePeriod?.includes('w'))) {
      subscriptionTimePeriod = selectedData.filter(e => e.timePeriod?.includes('w'));
    } else {
      subscriptionTimePeriod = selectedData.filter(e => !e.timePeriod?.includes('w'));
    }
    const dateRange = sortBy(subscriptionTimePeriod, ['timePeriod'])[0];
    const dateRangeUnit = dateRange.timePeriod || '';
    if (selectedData.find(e => e.timePeriod?.includes('w'))) {
      setFormData({
        ...formData,
        subscriptionTypes: selectedData,
        endDate: dateRangeUnit ? moment().add(
          dateRangeUnit.substring(0, dateRange.timePeriod?.length - 1) * 7,
          getMomentUnit(dateRangeUnit[dateRange?.timePeriod.length - 1])).format() :
          moment().format()
      });
    } else {
      setFormData({
        ...formData,
        subscriptionTypes: selectedData,
        endDate: dateRangeUnit ? moment().add(
          dateRangeUnit.substring(0, dateRange?.timePeriod.length - 1),
          getMomentUnit(dateRangeUnit[dateRange?.timePeriod.length - 1])).format() :
          moment().format()
      });
    }
  };

  const handleChange = (name) => (value) => {
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleSubmit = (data) => {
    if (!data.subscriptionTypes.length) {
      return
    }
    const payload = {
      ...data,
      id: subscriptionData?.id,
      schoolId: schoolDetail.id,
      startDate: data.startDate ? moment(data.startDate).format('YYYY-MM-DD') : '',
      endDate: data.endDate ? moment(data.endDate).format('YYYY-MM-DD') : '',
      subscriptionTypes: data.subscriptionTypes ? data.subscriptionTypes.map(s => s.id) : [],
    };

    onSubmit(payload);
  };

  return (
    <Form
      initialValues={{
        subscriptionStatus: formData?.subscriptionStatus || '',
        subscriptionTypes: formData?.subscriptionTypes || [],
        price: formData?.price || '',
        startDate: formData?.startDate ? moment(formData?.startDate).format() : moment().format(),
        endDate: formData?.endDate ? moment(formData?.endDate).format() : moment().format(),
      }}
      enableReinitialize={true}
      validationSchema={Yup.object().shape({
        subscriptionStatus: Yup.string().required('Subscription status is required!'),
        subscriptionTypes: Yup.array().min(1, 'Subscription type is required!'),
        price: Yup.string().required('Price is required!'),
      })}
      onSubmit={async (values) => {
        let data = { ...values };
        handleSubmit(data);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="school-Subscription-form"
            noValidate
          >
            <Grid container>
              <Grid item xs={12} md={12}>
                <AccessControl level={6}>
                  <Box mt={0.5}>
                    <Typography
                      variant="body2"
                      gutterBottom
                    >
                      <b>School Name:</b> <br/> {schoolDetail.schoolName}
                    </Typography>
                  </Box>
                </AccessControl>
                <Form.Field.Select
                  options={schoolSubscriptionStatusOptions}
                  fullWidth
                  variant="standard"
                  name="subscriptionStatus"
                  label="Subscription Status"
                  optLabel="label"
                  optValue="value"
                  onChange={handleChange('subscriptionStatus')}
                />
                <Form.Field.AutoComplete
                  multiple={true}
                  fullWidth
                  showAvatar={false}
                  options={[]}
                  variant="standard"
                  remoteMethod={val => searchTags(val)}
                  name="subscriptionTypes"
                  label="Subscription Type"
                  optLabel="typeName"
                  optValue="id"
                  onChange={handleSubscriptionTypesChange}
                />
                <Form.Field.Input
                  fullWidth
                  name="price"
                  label="Price"
                  type="number"
                  onChange={handleChange('price')}
                />
                <Form.Field.DatePicker
                  name="startDate"
                  fullWidth
                  label="Start Date"
                />
                <Form.Field.DatePicker
                  name="endDate"
                  fullWidth
                  label="End Date"
                />
              </Grid>
            </Grid>
          </form>
        );
      }}
    </Form>
  );
}
