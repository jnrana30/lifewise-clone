import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Chip,
  Box,
  Typography,
  Link,
} from '@material-ui/core';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import { ContextMenu } from "../../../../components/shared";
import EditIcon from "@material-ui/icons/Edit";
import _ from "lodash";
import {AccessControl} from "../../../../components/App";
import Skeleton from "@material-ui/lab/Skeleton";

const useStyles = makeStyles(theme => ({
  schoolSubscriptionDesc: {
    maxWidth: 420,
  },
  tableRow: {
    minHeight: 150,
  },
  tableActionCol: {
    maxWidth: 200,
    width: 200,
  },
}));

export default function SchoolSubscriptions({ schoolSubscriptions, addSubscriptions, onEdit }) {
  const classes = useStyles();
  const { loading, data } = schoolSubscriptions;

  const preventDefault = (event) => event.preventDefault();

  const handleClickAddSubscriptions = (event) => {
    preventDefault(event);
    addSubscriptions()
  };

  return (
    <>
      <AccessControl level={6}>
        <Box m={2} mb={1}>
          <Link href="#" onClick={handleClickAddSubscriptions}>
            Add subscription to school
          </Link>
        </Box>
      </AccessControl>
      <TableContainer>
        <Table aria-label="customized table" className="paddedTable">
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={2}>
                  <Skeleton animation="wave"/>
                </TableCell>
              </TableRow>
            ) : (
              <React.Fragment>
                {data && data.length > 0 ? (
                  <React.Fragment>
                    {(data || []).map(row => (
                      <TableRow key={row.subscription} className={classes.tableRow}>
                        <TableCell>
                          <Box mb={2}>
                            <Typography variant="body1" gutterBottom>
                              <b>{(row.subscriptionTypes || []).map(st => st.typeName).join(', ')}</b>
                            </Typography>
                            <Typography
                              variant="body2"
                              color="textSecondary"
                              className={classes.schoolSubscriptionDesc}
                            >
                              Price: £{row.price || ''} billed monthly,{' '}
                              Ends: {moment(row.endDate).format('MMM DD YYYY')}
                            </Typography>
                          </Box>
                        </TableCell>
                        <TableCell
                          className={classes.tableActionCol}
                          align="left"
                        >
                          <Box
                            display="flex"
                            alignItems="center"
                            justifyContent="flex-end"
                          >
                            <Chip
                              label={_.capitalize(row.subscriptionStatus)}
                              color={'primary'}
                            />
                            <AccessControl level={6}>
                              <ContextMenu
                                options={[
                                  {
                                    label: 'Edit',
                                    icon: <EditIcon/>,
                                    onClick: () => {
                                      onEdit(row)
                                    }
                                  }
                                ]}
                              />
                            </AccessControl>
                          </Box>
                        </TableCell>
                      </TableRow>
                    ))}
                  </React.Fragment>
                ) : (
                  <TableRow>
                    <TableCell colSpan={2} align="center">
                      <Typography color="textSecondary">
                        No Subscription found
                      </Typography>
                    </TableCell>
                  </TableRow>
                )}
              </React.Fragment>
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
