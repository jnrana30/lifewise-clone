import React, { useState } from 'react';
import moment from 'moment';
import * as Yup from 'yup';
import { Box, Grid, FormControl } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';
import { sortBy, isEmpty, omit } from 'lodash';
import { FieldArray } from 'formik';
import CloseIcon from '@material-ui/icons/Close';
import { FieldSet, Legend } from './style';
import { schoolSourceTypes } from 'src/config';
import { getMomentUnit } from 'src/utils/helpers';

function StudentsForm({ getSubscriptionsTag, subscriptionsTagType = [], ...props }) {
  const subscriptionsTag = [ ...subscriptionsTagType ];
  const [subscriptionFormData, setSubscriptionFormData] = useState({
    subscriptionStatus: 'active',
  });

  const handleSubscriptionTypesChange = (selectedData) => {
    if (selectedData.length) {
      let subscriptionTimePeriod;
      if (selectedData.find(e => e.timePeriod?.includes('w'))) {
        subscriptionTimePeriod = selectedData.filter(e => e.timePeriod?.includes('w'));
      } else {
        subscriptionTimePeriod = selectedData.filter(e => !e.timePeriod?.includes('w'));
      }

      const dateRange = sortBy(subscriptionTimePeriod, ['timePeriod'])[0];
      const dateRangeUnit = dateRange.timePeriod || '';
      if (selectedData.find(e => e.timePeriod?.includes('w'))) {
        setSubscriptionFormData({
          ...subscriptionFormData,
          subscriptionTypes: selectedData.map(s => s.id),
          price: dateRange.defaultPrice,
          startDate: moment().format(),
          endDate: dateRangeUnit ? moment().add(
            dateRangeUnit.substring(0, dateRange.timePeriod?.length - 1) * 7,
            getMomentUnit(dateRangeUnit[dateRange?.timePeriod.length - 1])).format() :
            moment().format()
        });
      } else {
        setSubscriptionFormData({
          ...subscriptionFormData,
          subscriptionTypes: selectedData.map(s => s.id),
          price: dateRange.defaultPrice,
          startDate: moment().format(),
          endDate: dateRangeUnit ? moment().add(
            dateRangeUnit.substring(0, dateRange?.timePeriod.length - 1),
            getMomentUnit(dateRangeUnit[dateRange?.timePeriod.length - 1])).format() :
            moment().format()
        });
      }
    } else {
      setSubscriptionFormData({});
    }
  };

  const searchTags = async (search) => {
    if (search) {
      return subscriptionsTag.filter(type => type.typeName.toLocaleLowerCase().includes(search.toLocaleLowerCase()));
    } else {
      return subscriptionsTag;
    }
  };

  return (
    <Form
      initialValues={{
        schoolName: '',
        location: '',
        website: '',
        domains: [''],
        details: {
          source: ''
        }
      }}
      validationSchema={Yup.object().shape({
        schoolName: Yup.string().required('School Name is required!'),
        location: Yup.object().required('Location is required!'),
        website: Yup.string()
          .matches(
            /((https?):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/,
            'Enter correct url!'
          )
          .required('Location is required!'),
        domains: Yup.array().of(
          Yup.object().shape({
            domain: Yup.string()
              .required('Domain is a required!')
              .nullable(true)
          })
        )
      })}
      onSubmit={async (values, form) => {
        let data = { ...values };
        if (data.location && !isEmpty(data.location)) {
          data.schoolAddresses = [data.location];
        }
        data.emailDomains = (data.domains || []).map(d => d.domain);
        data = omit(data, ['location', 'domains', 'subscription']);
        // console.log('Data  - subscriptionFormData', data, subscriptionFormData);
        props.onSubmit(data, subscriptionFormData);
      }}
    >
      {({ values, ...props }) => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="school-form"
            noValidate
          >
            <Form.Field.Input fullWidth name="schoolName" label="School Name" />
            <Form.Field.LocationAutoComplete
              fullWidth
              name="location"
              label="Location"
            />
            <Form.Field.Input fullWidth name="website" label="Web Address" />

            <Form.Field.AutoComplete
              multiple={true}
              fullWidth
              showAvatar={false}
              options={[]}
              variant="standard"
              remoteMethod={val => searchTags(val)}
              name="subscription"
              label="Subscription"
              optLabel="typeName"
              optValue="id"
              onChange={handleSubscriptionTypesChange}
            />

            <FieldArray
              name="domains"
              render={arrayHelpers => (
                <div>
                  {values.domains &&
                    values.domains.length > 0 &&
                    values.domains.map((obj, index) => (
                      <React.Fragment key={`class-${index}-${obj.day}`}>
                        <Grid container spacing={2} alignItems="flex-end">
                          <Grid item xs={11}>
                            <Form.Field.Input
                              fullWidth
                              variant="standard"
                              name={`domains[${index}].domain`}
                              label="Enter email domain"
                            />
                          </Grid>
                          <Grid item xs={1}>
                            {values.domains.length > 1 && (
                              <Button
                                iconButton={true}
                                onClick={() => {
                                  arrayHelpers.remove(index);
                                }}
                              >
                                <CloseIcon />
                              </Button>
                            )}
                          </Grid>
                        </Grid>
                      </React.Fragment>
                    ))}
                  <Box my={2}>
                    <Button
                      color="primary"
                      onClick={() => {
                        arrayHelpers.push('');
                      }}
                    >
                      + Domain
                    </Button>
                  </Box>
                </div>
              )}
            />
            <Form.Field.Select
              options={schoolSourceTypes}
              fullWidth
              name="details.source"
              label="Source"
            />
          </form>
        );
      }}
    </Form>
  );
}

export default StudentsForm;
