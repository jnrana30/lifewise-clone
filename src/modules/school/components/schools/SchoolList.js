import React from 'react';
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TablePagination,
  Paper,
  Typography,
  Box,
  Chip,
  Avatar
} from '@material-ui/core/';
import { capitalize } from 'lodash';
import Skeleton from '@material-ui/lab/Skeleton';
import {Button, ContextMenu} from 'src/components/shared';
import Link from 'next/link';
import { useRouter } from 'next/router';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import EditIcon from '@material-ui/icons/Edit';

function SchoolList({ schools, loading, paging, onPaging }) {
  const router = useRouter();

  const handleChangePage = (event, newPage) => {
    onPaging(newPage);
  };

  const handleChangeRowsPerPage = event => {
    onPaging(0, event.target.value);
  };

  return (
    <Paper>
      <TableContainer>
        <Table aria-label="customized table">
          <TableHead>
            <TableRow>
              <TableCell>School Name</TableCell>
              <TableCell>City</TableCell>
              <TableCell>No. of Teachers</TableCell>
              <TableCell>Status</TableCell>
              <TableCell></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {loading ? (
              <TableRow>
                <TableCell colSpan={5}>
                  <Skeleton animation="wave" />
                </TableCell>
              </TableRow>
            ) : (
              <React.Fragment>
                {schools && schools.length > 0 ? (
                  <React.Fragment>
                    {schools.map((school, index) => {
                      return (
                        <TableRow key={`courselist-${index}`}>
                          <TableCell>
                            <Box display="flex" alignItems="center">
                              <Box mr={2}>
                                <Avatar>{school.schoolName.charAt(0)}</Avatar>
                              </Box>
                              <Link href={`/schools/${school.id}`}>
                                <Typography variant="body1" color="secondary">
                                  {school.schoolName}
                                </Typography>
                              </Link>
                            </Box>
                          </TableCell>
                          <TableCell>
                            <Typography variant="body2" color="textPrimary">
                              {school?.schoolAddresses
                                ? school?.schoolAddresses[0].city
                                  ? school?.schoolAddresses[0].city
                                  : '-'
                                : '-'}
                            </Typography>
                          </TableCell>
                          <TableCell width="15%">
                            <Typography variant="body2" color="textPrimary">
                              {/* {school.noTeachers} */}
                            </Typography>
                          </TableCell>
                          <TableCell width="10%">
                            <Chip
                              label={capitalize(school.schoolStatus)}
                              color={
                                school.schoolStatus ? 'primary' : 'default'
                              }
                            />
                          </TableCell>
                          <TableCell width="5%" className="flex-justify-content-end">
                            <ContextMenu
                              options={[
                                {
                                  label: 'Edit',
                                  icon: <EditIcon />,
                                  onClick: () => {
                                    router.push(`/schools/${school.id}`);
                                  }
                                }
                              ]}
                            />

                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </React.Fragment>
                ) : (
                  <TableRow>
                    <TableCell colSpan={6} align="center">
                      <Typography color="textSecondary">
                        No data found
                      </Typography>
                    </TableCell>
                  </TableRow>
                )}
              </React.Fragment>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        component="div"
        count={paging?.records ? paging.records : 0}
        rowsPerPage={paging.perPage}
        page={paging.page - 1}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
    </Paper>
  );
}

export default SchoolList;
