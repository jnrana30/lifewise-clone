import React, { useState } from 'react';
import * as Yup from 'yup';
import { Box, Grid } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';
import _ from 'lodash';

function StudentsDetails(props) {
  const { schoolDetail, submitForm } = props;
  const [isLoading, setIsLoading] = useState(false);
  const schoolAddress = (schoolDetail?.schoolAddresses || []).find(
    e => !e.isBilling
  );

  const handleSubmit = async formValue => {
    const schoolBillingIndex = (schoolDetail?.schoolAddresses || []).findIndex(
      e => !e.isBilling
    );
    let payload = {};
    if (schoolBillingIndex === -1) {
      payload = {
        ...schoolDetail,
        schoolName: formValue.schoolName,
        details: {
          ...schoolDetail.details,
          ...formValue.details
        },
        website: (formValue.webAddress || '').toLocaleLowerCase(),
        schoolAddresses: [
          ...(schoolDetail.schoolAddresses || []),
          {
            isBilling: false,
            address1: formValue.address1,
            address2: formValue.address2,
            city: formValue.city,
            region: formValue.region,
            country: formValue.country,
            postcode: formValue.postcode
          }
        ]
      };
    } else {
      payload = {
        ...schoolDetail,
        schoolName: formValue.schoolName,
        details: {
          ...schoolDetail.details,
          ...formValue.details
        },
        website: (formValue.webAddress || '').toLocaleLowerCase(),
        schoolAddresses: [
          ...(schoolDetail.schoolAddresses || []).splice(0, schoolBillingIndex),
          {
            ...schoolDetail.schoolAddresses[schoolBillingIndex],
            isBilling: false,
            address1: formValue.address1,
            address2: formValue.address2,
            city: formValue.city,
            region: formValue.region,
            country: formValue.country,
            postcode: formValue.postcode
          },
          ...(schoolDetail.schoolAddresses || []).splice(schoolBillingIndex + 1)
        ]
      };
    }

    payload = _.omit(payload, ['emailDomains']);
    setIsLoading(true);
    await submitForm(payload);
    setIsLoading(false);
  };

  return (
    <Form
      initialValues={{
        schoolName: schoolDetail.schoolName || '',
        address1: schoolAddress?.address1 || '',
        address2: schoolAddress?.address2 || '',
        webAddress: schoolDetail?.website || '',
        city: schoolAddress?.city || '',
        region: schoolAddress?.region || '',
        country: schoolAddress?.country || '',
        postcode: schoolAddress?.postcode || '',
        details: {
          icon: schoolDetail?.details?.icon ? schoolDetail?.details?.icon : ''
        }
      }}
      validationSchema={Yup.object().shape({
        schoolName: Yup.string()
          .required('School Name is required!')
          .nullable(true),
        webAddress: Yup.string()
          .required('Location is required!')
          .nullable(true),
        city: Yup.string().required('City is required!'),
        region: Yup.string().required('Region is required!'),
        country: Yup.string().required('Country is required!'),
        postcode: Yup.string().required('Post Code is required!')
      })}
      enableReinitialize={true}
      onSubmit={async (values, form) => {
        const data = { ...values };
        if (
          data.details?.icon &&
          !_.isEmpty(data.details?.icon) &&
          data.details?.icon?.key
        ) {
          data.details.icon = data.details.icon.key;
        }
        handleSubmit(data);
      }}
    >
      {({ values, ...formProp }) => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              formProp.submitForm();
              return false;
            }}
            id="school-form"
            noValidate
          >
            <Grid container spacing={2}>
              <Grid item xs={12} md={9}>
                <Form.Field.Input
                  fullWidth
                  name="schoolName"
                  label="School Name"
                />
                <Form.Field.Input
                  fullWidth
                  name="webAddress"
                  label="School Website"
                />
                <Form.Field.Input
                  fullWidth
                  name="address1"
                  label="Address Line 1"
                />
                <Form.Field.Input
                  fullWidth
                  name="address2"
                  label="Address Line 2"
                />
                <Form.Field.Input fullWidth name="city" label="City" />
                <Form.Field.Input fullWidth name="region" label="Region" />
                <Form.Field.Input fullWidth name="country" label="Country" />
                <Form.Field.Input fullWidth name="postcode" label="Post code" />
                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    fullWidth
                    loading={isLoading}
                  >
                    Submit
                  </Button>
                </Box>
              </Grid>
              <Grid item xs={12} md={3}>
                <Box mt={2}>
                  <Form.Field.Avatar
                    name="details.icon"
                    label="Choose Avatar"
                    folder="schools/icons"
                  />
                </Box>
              </Grid>
            </Grid>
          </form>
        );
      }}
    </Form>
  );
}

export default StudentsDetails;
