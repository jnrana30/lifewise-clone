import React, { useEffect, useState } from 'react';
import * as Yup from 'yup';
import { Box } from '@material-ui/core';
import { Form, Button } from 'src/components/shared';
import _ from 'lodash';

function StudentsBilling(props) {
  const { schoolDetail, submitForm } = props;

  const [isLoading, setIsLoading] = useState(false);
  const [copyAddress, setCopyAddress] = useState(true);
  const [schoolBillingAddress, setSchoolBillingAddress] = useState(
    (schoolDetail?.schoolAddresses || []).find(e => e.isBilling) || {}
  );

  useEffect(() => {
    const schoolBaseAddress = (schoolDetail.schoolAddresses || []).find(
      sAddress => !sAddress.isBilling
    );
    setSchoolBillingAddress({
      ...schoolBillingAddress,
      address1: schoolBaseAddress?.address1 || '',
      address2: schoolBaseAddress?.address2 || '',
      city: schoolBaseAddress?.city || '',
      region: schoolBaseAddress?.region || '',
      postcode: schoolBaseAddress?.postcode || '',
      country: schoolBaseAddress?.country || ''
    });
  }, []);

  const handleSubmit = async formValue => {
    const schoolBillingIndex = (schoolDetail?.schoolAddresses || []).findIndex(
      e => e.isBilling
    );
    let payload = {};
    if (schoolBillingIndex === -1) {
      payload = {
        ...schoolDetail,
        schoolAddresses: [
          ...(schoolDetail.schoolAddresses || []),
          {
            ...formValue,
            isBilling: true
          }
        ]
      };
    } else {
      payload = {
        ...schoolDetail,
        schoolAddresses: [
          ...schoolDetail.schoolAddresses.splice(0, schoolBillingIndex),
          {
            ...schoolDetail.schoolAddresses[schoolBillingIndex],
            ...formValue,
            isBilling: true
          },
          ...schoolDetail.schoolAddresses.splice(schoolBillingIndex + 1)
        ]
      };
    }

    payload = _.omit(payload, ['emailDomains']);
    setIsLoading(true);
    await submitForm(payload);
    setIsLoading(false);
  };

  const handleCheckChange = event => {
    const schoolBaseAddress = (schoolDetail.schoolAddresses || []).find(
      sAddress => !sAddress.isBilling
    );
    const schoolBillingAddressData = (schoolDetail.schoolAddresses || []).find(
      sAddress => sAddress.isBilling
    );
    setCopyAddress(event);
    if (event) {
      setSchoolBillingAddress({
        ...schoolBillingAddress,
        address1: schoolBaseAddress?.address1 || '',
        address2: schoolBaseAddress?.address2 || '',
        city: schoolBaseAddress?.city || '',
        region: schoolBaseAddress?.region || '',
        postcode: schoolBaseAddress?.postcode || '',
        country: schoolBaseAddress?.country || ''
      });
    } else {
      setSchoolBillingAddress({
        ...schoolBillingAddress,
        address1: schoolBillingAddressData?.address1 || '',
        address2: schoolBillingAddressData?.address2 || '',
        city: schoolBillingAddressData?.city || '',
        region: schoolBillingAddressData?.region || '',
        postcode: schoolBillingAddressData?.postcode || '',
        country: schoolBillingAddressData?.country || ''
      });
    }
  };

  return (
    <Form
      enableReinitialize={true}
      initialValues={{
        copyAddress: copyAddress,
        companyName: schoolBillingAddress?.companyName || '',
        address1: schoolBillingAddress?.address1 || '',
        address2: schoolBillingAddress?.address2 || '',
        city: schoolBillingAddress?.city || '',
        region: schoolBillingAddress?.region || '',
        country: schoolBillingAddress?.country || '',
        postcode: schoolBillingAddress?.postcode || '',
        vat: schoolBillingAddress?.vat || '',
        invoiceName: schoolBillingAddress?.invoiceName || '',
        invoiceEmail: schoolBillingAddress?.invoiceEmail || ''
      }}
      validationSchema={Yup.object().shape({
        copyAddress: Yup.boolean(),
        companyName: Yup.string().required('Company name is required!'),
        address1: Yup.string().when('copyAddress', {
          is: false,
          then: Yup.string().required('Address 1 is required!')
        }),
        address2: Yup.string().when('copyAddress', {
          is: false,
          then: Yup.string().required('Address 2 is required!')
        }),
        city: Yup.string().when('copyAddress', {
          is: false,
          then: Yup.string().required('City is required!')
        }),
        region: Yup.string().when('copyAddress', {
          is: false,
          then: Yup.string().required('Region is required!')
        }),
        country: Yup.string().when('copyAddress', {
          is: false,
          then: Yup.string().required('Country is required!')
        }),
        postcode: Yup.string().when('copyAddress', {
          is: false,
          then: Yup.string().required('Post Code is required!')
        }),
        vat: Yup.string().required('VAT number is required!'),
        invoiceName: Yup.string().required('Invoice Contact Name is required!'),
        invoiceEmail: Yup.string().required('Invoice Email is required!')
      })}
      onSubmit={async (values, form) => {
        console.log('values : ', values);
        // this function will execute after all validation goes right
        handleSubmit(values);
      }}
    >
      {props => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              props.submitForm();
              return false;
            }}
            id="school-form"
            noValidate
          >
            <Form.Field.Checkbox
              name="copyAddress"
              label="Billing address is same as school"
              onChange={event => {
                handleCheckChange(event);
              }}
            />
            <Form.Field.Input
              fullWidth
              name="companyName"
              label="Company Name"
            />
            <Form.Field.Input
              fullWidth
              name="address1"
              label="Address Line 1"
              style={copyAddress ? { display: 'none' } : {}}
            />
            <Form.Field.Input
              fullWidth
              name="address2"
              label="Address Line 2"
              style={copyAddress ? { display: 'none' } : {}}
            />
            <Form.Field.Input
              fullWidth
              name="city"
              label="City"
              style={copyAddress ? { display: 'none' } : {}}
            />
            <Form.Field.Input
              fullWidth
              name="region"
              label="Region"
              style={copyAddress ? { display: 'none' } : {}}
            />
            <Form.Field.Input
              fullWidth
              name="country"
              label="Country"
              style={copyAddress ? { display: 'none' } : {}}
            />
            <Form.Field.Input
              fullWidth
              name="postcode"
              label="Post code"
              style={copyAddress ? { display: 'none' } : {}}
            />
            <Form.Field.Input fullWidth name="vat" label="VAT Number" />
            <Form.Field.Input
              fullWidth
              name="invoiceName"
              label="Invoice Contact Name"
            />
            <Form.Field.Input
              fullWidth
              name="invoiceEmail"
              label="Invoice Email"
            />
            <Box mt={2}>
              <Button
                variant="contained"
                color="primary"
                type="submit"
                fullWidth
                loading={isLoading}
              >
                Submit
              </Button>
            </Box>
          </form>
        );
      }}
    </Form>
  );
}

export default StudentsBilling;
