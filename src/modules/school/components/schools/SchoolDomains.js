import React, { useState } from 'react';
import { Box, Grid, Typography, FormHelperText } from '@material-ui/core';
import * as Yup from 'yup';
import { FieldArray } from 'formik';

import { Button, Form } from '../../../../components/shared';
import { Counter } from '../../../account/components/settings/teachers/style';
import toast from 'src/utils/toast';

import CloseIcon from '@material-ui/icons/Close';

const SchoolDomains = props => {
  const { schoolDetail, submitForm } = props;
  const [isLoading, setIsLoading] = useState(false);
  const [invalidDomains, setInvalidDomains] = useState([]);

  const handleSubmit = async formValue => {
    setInvalidDomains([]);
    const payload = {
      ...schoolDetail,
      emailDomains: (formValue.domains || []).map(d => d.domain)
    };
    setIsLoading(true);
    try {
      const res = await submitForm(payload);
      setIsLoading(false);
    } catch (error) {
      toast.error(
        'Error ocurred! Please remove the invalid domain(s) from the list.'
      );
      setInvalidDomains(error?.invalidDomains ? error?.invalidDomains : []);
      setIsLoading(false);
    }
  };

  return (
    <Form
      initialValues={{
        domains:
          schoolDetail?.emailDomains && schoolDetail?.emailDomains.length > 0
            ? schoolDetail?.emailDomains
            : []
      }}
      enableReinitialize={true}
      validationSchema={Yup.object().shape({
        domains: Yup.array().of(
          Yup.object().shape({
            domain: Yup.string()
              .required('Domain is a required!')
              .nullable(true)
          })
        )
      })}
      onSubmit={async (values, form) => {
        const data = { ...values };
        handleSubmit(data);
      }}
    >
      {({ values, ...formProp }) => {
        return (
          <form
            onSubmit={e => {
              e.preventDefault();
              formProp.submitForm();
              return false;
            }}
            id="school-form-domains"
            noValidate
          >
            <Grid item xs={12} md={12}>
              <Typography paragraph>
                Users with approved email domains can join with or without an
                invitation. They will get access to the platform following email
                verification.
              </Typography>
              <Grid container alignItems="flex-start">
                <Grid item xs={12} md={12}>
                  <FieldArray
                    name="domains"
                    render={arrayHelpers => (
                      <div>
                        {values.domains &&
                          values.domains.length > 0 &&
                          values.domains.map((obj, index) => (
                            <Box mb={1}>
                              <Grid container spacing={0} alignItems="flex-end">
                                <Grid item xs={1}>
                                  <Counter>{index + 1}.</Counter>
                                </Grid>
                                <Grid
                                  item
                                  xs={10}
                                  spacing={2}
                                  justifyContent="flex-start"
                                >
                                  <Form.Field.Input
                                    fullWidth
                                    variant="standard"
                                    name={`domains[${index}].domain`}
                                    label="Enter approved domain"
                                  />
                                  {invalidDomains.indexOf(obj.domain) >= 0 && (
                                    <FormHelperText
                                      error={true}
                                      style={{ marginTop: 0 }}
                                    >
                                      Invalid domain!
                                    </FormHelperText>
                                  )}
                                </Grid>
                                <Grid item xs={1}>
                                  {values.domains.length > 1 && (
                                    <Button
                                      iconButton={true}
                                      onClick={() => {
                                        arrayHelpers.remove(index);
                                      }}
                                    >
                                      <CloseIcon />
                                    </Button>
                                  )}
                                </Grid>
                              </Grid>
                            </Box>
                          ))}
                        <Box my={2}>
                          <Button
                            color="primary"
                            onClick={() => {
                              arrayHelpers.push({
                                domain: ''
                              });
                            }}
                            style={{ padding: '6px 20px' }}
                          >
                            + Add Domain
                          </Button>
                        </Box>
                        <Box mt={2}>
                          <Button
                            variant="contained"
                            color="primary"
                            type="submit"
                            fullWidth
                            loading={isLoading}
                          >
                            Submit
                          </Button>
                        </Box>
                      </div>
                    )}
                  />
                </Grid>
              </Grid>
            </Grid>
          </form>
        );
      }}
    </Form>
  );
};

export default SchoolDomains;
