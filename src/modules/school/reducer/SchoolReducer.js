import * as types from '../actions/schoolTypes';

const initialState = {
  schools: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0
    },
    filters: {}
  },
  schoolSubscriptions: {
    loading: true,
    data: []
  },
  schoolSubscriptionsType: {
    loading: true,
    data: []
  },
  schoolDetail: {
    loading: true,
    data: {}
  },
  classes: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 100,
      records: 0
    },
    filters: {}
  },
  myClasses: {
    loading: false,
    data: [],
    paging: {
      page: 1,
      perPage: 10,
      records: 0
    },
    filters: {}
  },
  mySchool: {}
};

const SchoolReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_MY_SCHOOL:
      return {
        ...state,
        mySchool: action.data
      };

    case types.FETCH_SCHOOLS:
      return {
        ...state,
        schools: {
          ...state.schools,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.schools.paging,
            ...action.paging
          }
        }
      };

    case types.SET_SCHOOLS:
      return {
        ...state,
        schools: {
          ...state.schools,
          loading: false,
          data: action.data.schools,
          paging: {
            ...state.schools.paging,
            records: action.data.records
          }
        }
      };

    case types.FETCH_SCHOOL_DETAILS:
      return {
        ...state,
        schoolDetail: {
          ...state.schoolDetail,
          loading: true,
          data: {}
        }
      };

    case types.SET_SCHOOL_DETAILS:
      return {
        ...state,
        schoolDetail: {
          ...state.schoolDetail,
          loading: false,
          data: action.data
        }
      };

    case types.FETCH_CLASSES:
      return {
        ...state,
        classes: {
          ...state.classes,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.classes.paging,
            ...action.paging
          }
        }
      };

    case types.SET_CLASSES:
      return {
        ...state,
        classes: {
          ...state.classes,
          loading: false,
          data: action.data.classes,
          paging: {
            ...state.classes.paging,
            records: action.data.records
          }
        }
      };

    case types.FETCH_SCHOOL_SUBSCRIPTIONS:
      return {
        ...state,
        schoolSubscriptions: {
          ...state.schoolSubscriptions,
          loading: true
        }
      };

    case types.SET_SCHOOL_SUBSCRIPTIONS:
      return {
        ...state,
        schoolSubscriptions: {
          ...state.schoolSubscriptions,
          loading: false,
          data: action.data.subscriptions
        }
      };

    case types.FETCH_SCHOOL_SUBSCRIPTIONS_TYPE:
      return {
        ...state,
        schoolSubscriptionsType: {
          ...state.schoolSubscriptionsType,
          loading: true
        }
      };

    case types.SET_SCHOOL_SUBSCRIPTIONS_TYPE:
      return {
        ...state,
        schoolSubscriptionsType: {
          ...state.schoolSubscriptionsType,
          loading: false,
          data: action.data.types
        }
      };

    case types.FETCH_MY_CLASSES:
      return {
        ...state,
        myClasses: {
          ...state.myClasses,
          loading: true,
          filters: action.filters,
          paging: {
            ...state.myClasses.paging,
            ...action.paging
          }
        }
      };

    case types.SET_MY_CLASSES:
      return {
        ...state,
        myClasses: {
          ...state.myClasses,
          loading: false,
          data: action.data.classes,
          paging: {
            ...state.myClasses.paging,
            records: action.data.records
          }
        }
      };

    default:
      return state;
  }
};
export default SchoolReducer;
