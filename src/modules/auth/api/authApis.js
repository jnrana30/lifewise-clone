import api from 'src/api/index';

export const loginUser = data => {
  return api('/auth/login', data, 'post');
};

export const registerUser = data => {
  return api('/auth/register', data, 'post');
};

export const getProfile = () => {
  return api('/auth/profile', null, 'get');
};

export const verifyUser = (code, userId) => {
  const data = {
    code,
    id: userId
  };
  return api('/auth/verify-user', data, 'post');
};

export const checkVerificationStatus = (code, userId) => {
  return api(
    `/auth/verification-status?code=${code}&id=${userId}`,
    null,
    'get'
  );
};

export const checkUsername = username => {
  return api(`/auth/check-username?username=${username}`, null, 'get');
};

export const forgotPassword = data => {
  return api(`/auth/reset-password`, data, 'post');
};

export const checkPasswordToken = (token, id) => {
  return api(`/auth/check-reset?token=${token}&id=${id}`, null, 'get');
};

export const resetPassword = data => {
  return api(`/auth/change-password`, data, 'post');
};

export const getInvitedUser = code => {
  return api(`/auth/retrieve-invite?code=${code}`, null, 'get');
};

export const completeInvite = (data, code) => {
  return api(`/auth/complete-invite?code=${code}`, data, 'POST');
};

export const renewSubscription = (data, code) => {
  return api(`/auth/renew`, { mode: 'extend_subscription' }, 'POST');
};
