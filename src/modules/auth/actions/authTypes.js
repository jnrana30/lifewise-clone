export const LOGIN = 'LOGIN';
export const LOGIN_SILENTLY = 'LOGIN_SILENTLY';
export const REGISTER = 'REGISTER';
export const RESTORE_SESSION = 'RESTORE_SESSION';

export const VERIFY_ACCOUNT = 'VERIFY_ACCOUNT';
export const SET_AUTH = 'SET_AUTH';
export const COMPLETE_INVITE = 'COMPLETE_INVITE';

export const LOGOUT = 'LOGOUT';
export const LOGOUT_SILENTLY = 'LOGOUT_SILENTLY';

export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const SET_PROFILE = 'SET_PROFILE';
export const RENEW_SUBSCRIPTION = 'RENEW_SUBSCRIPTION';
