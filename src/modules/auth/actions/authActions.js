import * as authTypes from './authTypes';

export const login = (data, resolve, reject) => ({
  type: authTypes.LOGIN,
  data,
  resolve,
  reject
});

export const loginSilently = (data, resolve, reject) => ({
  type: authTypes.LOGIN_SILENTLY,
  data,
  resolve,
  reject
});

export const register = (data, resolve, reject) => ({
  type: authTypes.REGISTER,
  data,
  resolve,
  reject
});
export const restoreSession = token => ({
  type: authTypes.RESTORE_SESSION,
  token
});
export const setAuth = data => ({ type: authTypes.SET_AUTH, data });
export const setProfile = profile => ({ type: authTypes.SET_PROFILE, profile });

export const completeInvite = (data, code, resolve, reject) => ({
  type: authTypes.COMPLETE_INVITE,
  data,
  code,
  resolve,
  reject
});

export const logout = () => ({ type: authTypes.LOGOUT });

export const logoutSilently = () => ({ type: authTypes.LOGOUT_SILENTLY });

export const updateProfile = (data, resolve, reject) => ({
  type: authTypes.UPDATE_PROFILE,
  data,
  resolve,
  reject
});

export const verifyAccount = (code, userId, resolve, reject) => ({
  type: authTypes.VERIFY_ACCOUNT,
  code,
  userId,
  resolve,
  reject
});

export const renewSubscription = (resolve, reject) => ({
  type: authTypes.RENEW_SUBSCRIPTION,
  resolve,
  reject
});
