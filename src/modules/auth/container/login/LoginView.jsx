import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography } from '@material-ui/core';
import Link from 'next/link';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import _ from 'lodash';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%' // Fix IE 11 issue.
  },
  heading: {
    marginTop: 16,
    fontWeight: '500'
  },
  footer: {
    display: 'flex',
    justifyContent: 'space-between'
  },
  footerContent: {
    color: '#0000008a',
    fontSize: 12,
    fontWeight: 400,
    '& span': {
      fontWeight: 400,
      fontSize: 12,
      cursor: 'pointer'
    }
  },
  forgotPassword: {
    fontSize: 12,
    fontWeight: 400,
    cursor: 'pointer'
  }
}));

export default function LoginView(props) {
  const classes = useStyles();
  const [loading, setLoading] = React.useState(false);

  return (
    <React.Fragment>
      <Typography component="h1" variant="h5" className={classes.heading}>
        Log in
      </Typography>
      <Form
        initialValues={{
          username: '',
          password: ''
        }}
        validationSchema={Yup.object().shape({
          username: Yup.string().required('Username is required!'),
          password: Yup.string().required('Password is required!')
        })}
        onSubmit={async (values, form) => {
          setLoading(true);
          const data = { ...values };
          data.username = data.username.trim();
          try {
            await props.login(data);
          } catch (error) {}
          setLoading(false);
          form.prevent;
        }}
      >
        {props => {
          return (
            <form
              className={classes.form}
              onSubmit={e => {
                e.preventDefault();
                props.submitForm();
                return false;
              }}
              noValidate
            >
              <Form.Field.Input
                fullWidth
                variant="outlined"
                name="username"
                label="Email"
                disabled={loading}
              />
              <Form.Field.Input
                fullWidth
                variant="outlined"
                type="password"
                name="password"
                label="Password"
                disabled={loading}
              />
              <Box mt={2}>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  loading={loading}
                  fullWidth
                  mt={2}
                >
                  log in
                </Button>
              </Box>

              <Box mt={3} className={classes.footer}>
                <Link href="/auth/forgotPassword">
                  <Typography
                    color="primary"
                    className={classes.forgotPassword}
                  >
                    Forgot password?
                  </Typography>
                </Link>
                <Typography className={classes.footerContent}>
                  Don't have an account?{' '}
                  <Link href="/auth/register">
                    <Typography variant="inherit" color="primary">
                      Sign up
                    </Typography>
                  </Link>
                </Typography>
              </Box>
            </form>
          );
        }}
      </Form>
    </React.Fragment>
  );
}
