import React from 'react';
import { connect } from 'react-redux';
import LoginView from './LoginView';
import * as authActions from '../../actions/authActions';

export const LoginContainer = props => {
  return <LoginView {...props} />;
};

const mapStateToProps = state => ({
  posts: state.app.posts,
  loader: state.app.loader
});

const mapDispatchToProps = dispatch => ({
  login: data => {
    return new Promise((resolve, reject) => {
      dispatch(authActions.login(data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
