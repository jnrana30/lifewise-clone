import React from 'react';
import { useRouter } from 'next/router';
import * as Yup from 'yup';
import _ from 'lodash';

import { green, grey } from '@material-ui/core/colors';
import { Form, Button } from 'src/components/shared';
import { Box, Typography } from '@material-ui/core';
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import toast from 'src/utils/toast';

import { checkPasswordToken, resetPassword } from '../../api/authApis';
import { makeStyles } from '@material-ui/core/styles';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%', // Fix IE 11 issue.
    '& .Mui-error.MuiFormHelperText-filled': {
      display: 'none'
    }
  },
  heading: {
    marginTop: 16,
    fontWeight: '500'
  },
  validationMessageListMain: {
    paddingBottom: 0
  }
}));

export default function LoginView() {
  const [loading, setLoading] = React.useState(false);
  const [verifyingToken, setVerifyingToken] = React.useState('demo@demo.cc');
  const router = useRouter();
  const classes = useStyles();

  const lowerCaseRegex = new RegExp('^(?=.*[a-z])');
  const upperCaseRegex = new RegExp('^(?=.*[A-Z])');
  const numberRegex = new RegExp('^(?=.*[0-9])');
  // const specialCharRegex = new RegExp('^(?=.*[!@#$%^&*])');
  const specialCharRegex = new RegExp(/[!"#$%&'()*+,-./:;<=>?@[\\\]^_`{|}~]/);
  const eightChatRegex = new RegExp('^(?=.{8,})');

  React.useEffect(() => {
    let { token, id } = router?.query;
    if (!id || !token) {
      // router.replace('/auth/forgotPassword');
    } else {
      try {
        checkPasswordToken(token, parseInt(id))
          .then(res => {})
          .catch(err => {
            toast.error('Invalid password token! Please try again.');
            // router.replace('/auth/forgotPassword');
          });
      } catch (error) {}
    }
  }, [router]);

  return (
    <React.Fragment>
      <React.Fragment>
        <Box mb={2}>
          <Typography
            align="center"
            component="h1"
            variant="h5"
            gutterBottom
            className={classes.heading}
          >
            Choose your new password
          </Typography>
          <Typography align="center" color="textSecondary">
            Hint - make sure it's memorable and secure!
          </Typography>
        </Box>
        <Form
          initialValues={{
            password: '',
            confPassword: ''
          }}
          validationSchema={Yup.object().shape({
            password: Yup.string()
              .required('Password is required!')
              .min(8, 'Password must be at least 8 characters long!'),
            confPassword: Yup.string()
              .required('Confirm Password is required!')
              .oneOf([Yup.ref('password'), null], 'Passwords must match')
            // .matches(
            //   /^(?=.*[A-Za-z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/,
            //   'Password must contain 8 Characters, One Uppercase, One Lowercase, One Number and one Special Character'
            // )
          })}
          onSubmit={async (values, form) => {
            const { token, id } = router?.query;
            setLoading(true);
            values.confirmPassword = values.password;
            values.token = token;
            values.userId = parseInt(id);
            await resetPassword(values)
              .then(res => {
                setLoading(false);
                toast.success('Password reset successfully.');
                router.replace('/auth/login');
              })
              .catch(err => {
                setLoading(false);
                toast.error('An error occurred! Please try again');
              });
            form.prevent;
          }}
        >
          {props => {
            return (
              <form
                className={classes.form}
                onSubmit={e => {
                  e.preventDefault();
                  props.submitForm();
                  return false;
                }}
                noValidate
              >
                <Form.Field.Input
                  fullWidth
                  variant="outlined"
                  name="password"
                  type="password"
                  label="Enter your new password"
                />

                <Form.Field.Input
                  fullWidth
                  variant="outlined"
                  type="password"
                  name="confPassword"
                  label="Confirm Password"
                />

                {/* <List className={classes.validationMessageListMain}>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: eightChatRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="At least 8 characters long" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: lowerCaseRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="One lowercase character" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: upperCaseRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="One uppercase character" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: numberRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="One numeric character" />
                  </ListItem>
                  <ListItem>
                    <ListItemIcon>
                      <CheckCircleOutlineIcon
                        style={{
                          color: specialCharRegex.test(props.values.password)
                            ? green[900]
                            : grey[300]
                        }}
                      />
                    </ListItemIcon>
                    <ListItemText primary="One special character" />
                  </ListItem>
                </List> */}

                <Box mt={2}>
                  <Button
                    variant="contained"
                    color="primary"
                    type="submit"
                    fullWidth
                    disabled={!_.isEmpty(props.errors)}
                    mt={2}
                    loading={loading}
                  >
                    Save
                  </Button>
                </Box>
              </form>
            );
          }}
        </Form>
      </React.Fragment>
    </React.Fragment>
  );
}
