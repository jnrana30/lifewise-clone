import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography } from '@material-ui/core';
import Link from 'next/link';
import Router from 'next/router';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import toast from 'src/utils/toast';
import { forgotPassword } from '../../api/authApis';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%' // Fix IE 11 issue.
  },
  heading: {
    marginTop: 16,
    fontWeight: '500'
  },
  messageHeading: {
    marginTop: 16,
    marginBottom: theme.spacing(3),
    fontWeight: '500'
  },
  mailSendSubHeader: {
    marginBottom: theme.spacing(3)
  },
  mailSendContent: {
    marginBottom: theme.spacing(3)
  },
  footerEmailSend: {
    display: 'flex',
    justifyContent: 'center',
    fontWeight: 400,
    '& p': {
      fontWeight: 400,
      marginLeft: 4
    }
  }
}));

export default function LoginView() {
  const [MailSent, setMailSent] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [email, setEmail] = React.useState('');

  const classes = useStyles();

  return (
    <React.Fragment>
      {!MailSent ? (
        <React.Fragment>
          <Box>
            <Typography
              align="center"
              component="h1"
              variant="h5"
              gutterBottom
              className={classes.heading}
            >
              Reset your password
            </Typography>
            <Typography align="center" color="textSecondary">
              Please enter the email address you'd like your password reset
              information sent to.
            </Typography>
          </Box>
          <Form
            initialValues={{
              workEmail: ''
            }}
            validationSchema={Yup.object().shape({
              workEmail: Yup.string()
                .email('Please enter a valid email address.')
                .required('Email is required!')
            })}
            onSubmit={async (values, form) => {
              setLoading(true);
              try {
                const forgotResp = await forgotPassword(values);
                if (forgotResp?.redirect) {
                  Router.replace(forgotResp?.redirect);
                } else {
                  setLoading(false);
                  setMailSent(true);
                  setEmail(values.workEmail);
                }
              } catch (error) {
                toast.error('An error occurred! Please try again.');
                setLoading(false);
              }
              form.prevent;
            }}
          >
            {props => {
              return (
                <form
                  className={classes.form}
                  onSubmit={e => {
                    e.preventDefault();
                    props.submitForm();
                    return false;
                  }}
                  noValidate
                >
                  <Form.Field.Input
                    fullWidth
                    variant="outlined"
                    name="workEmail"
                    label="Email Address"
                  />
                  <Box mt={2}>
                    <Button
                      variant="contained"
                      color="primary"
                      type="submit"
                      fullWidth
                      loading={loading}
                      mt={2}
                    >
                      Reset your password
                    </Button>
                  </Box>
                </form>
              );
            }}
          </Form>
        </React.Fragment>
      ) : (
        <Box mt={5}>
          <Typography
            align="center"
            component="h1"
            variant="h5"
            gutterBottom
            className={classes.messageHeading}
            paragraph
          >
            Help is on the way !
          </Typography>
          <Typography
            align="center"
            gutterBottom
            paragraph
            className={classes.mailSendSubHeader}
          >
            We're checking our records to find <b>LifeWise Account</b> for{' '}
            <Typography color="primary" display="inline">
              {email}
            </Typography>
          </Typography>
          <Typography
            align="center"
            gutterBottom
            paragraph
            className={classes.mailSendContent}
          >
            If we find a match, you'll get an email with further instructions.
            If you don't hear from us in the next <b>15 minutes</b>, please
            double check that you entered the correct email address and check
            your spam folder.
            {/*If you don't hear from us in the <b>next 15 mins</b>, please double*/}
            {/*check that you entered the correct email address and check your spam*/}
            {/*folder.*/}
          </Typography>

          <Typography
            color="textSecondary"
            gutterBottom
            paragraph
            className={classes.footerEmailSend}
          >
            Go back to{' '}
            <Link href="/auth/login">
              <Typography color="primary">lifewise.co.uk/login</Typography>
            </Link>
          </Typography>
        </Box>
      )}
    </React.Fragment>
  );
}
