import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Box, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import * as authActions from '../../actions/authActions';
import { Spinner } from 'src/components/shared';
import { useRouter } from 'next/router';

const useStyles = makeStyles(theme => ({
  mainBox: {
    textAlign: 'center'
  },
  messageHeading: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(3),
    fontWeight: '500'
  },
  mailSendSubHeader: {
    marginBottom: theme.spacing(3)
  }
}));

export const ConfirmAccount = props => {
  const classes = useStyles();
  const router = useRouter();

  useEffect(() => {
    const { code, id } = router?.query;
    if (code !== '' && id) {
      props.verifyAccount(code, id);
    }
  }, [router]);

  return (
    <React.Fragment>
      <Box mt={5} className={classes.mainBox}>
        <Typography
          align="center"
          component="h1"
          variant="h5"
          gutterBottom
          className={classes.messageHeading}
          paragraph
        >
          Verifying your account
        </Typography>
        <Box mb={2}>
          <Spinner />
        </Box>
        <Typography
          align="center"
          gutterBottom
          paragraph
          className={classes.mailSendSubHeader}
        >
          Please wait while we verify your account! You'll be redirected
          automatically on successful verification.
        </Typography>
      </Box>
    </React.Fragment>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  verifyAccount: (code, userId) => {
    return new Promise((resolve, reject) => {
      dispatch(authActions.verifyAccount(code, userId, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmAccount);
