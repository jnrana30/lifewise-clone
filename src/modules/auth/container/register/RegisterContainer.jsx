import React from 'react';
import { connect } from 'react-redux';
import RegisterView from './RegisterView';
import * as authActions from '../../actions/authActions';
import { patchProfile } from 'src/modules/account/actions/accountActions';

export const RegisterContainer = props => {
  return <RegisterView {...props} />;
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  register: data => {
    return new Promise((resolve, reject) => {
      dispatch(authActions.register(data, resolve, reject));
    });
  },
  completeInvite: (data, code) => {
    return new Promise((resolve, reject) => {
      dispatch(authActions.completeInvite(data, code, resolve, reject));
    });
  },
  login: data => {
    return new Promise((resolve, reject) => {
      dispatch(authActions.login(data, resolve, reject));
    });
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContainer);
