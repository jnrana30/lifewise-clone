import React, { useRef } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Typography } from '@material-ui/core';
import Link from 'next/link';
import * as Yup from 'yup';
import { Form, Button } from 'src/components/shared';
import { useRouter } from 'next/router';
import { getInvitedUser } from '../../api/authApis';
import { cacheTest, checkEmailUnique } from 'src/utils/validation';

const useStyles = makeStyles(theme => ({
  form: {
    width: '100%'
  },
  termsPrivacyWrapper: {
    display: 'flex',
    margin: '8px 0',
    '& .MuiCheckbox-root': {
      marginLeft: '-11px'
    },
    '& .MuiTypography-root': {
      fontSize: 14
    }
  },
  heading: {
    marginTop: 16,
    fontWeight: '500'
  },
  footer: {
    display: 'flex',
    justifyContent: 'flex-end'
  },
  footerContent: {
    color: '#0000008a',
    fontSize: 12,
    fontWeight: 400,
    '& span': {
      fontWeight: 400,
      fontSize: 12,
      cursor: 'pointer'
    }
  }
}));

export default function RegisterView(props) {
  const classes = useStyles();
  const Router = useRouter();
  const [invitedUser, setInvitedUser] = React.useState({});
  const [checkingUsername, setCheckingUsername] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    if (Router?.query?.code) {
      setCheckingUsername(true);
      getInvitedUser(Router?.query?.code)
        .then(res => {
          setCheckingUsername(false);
          setInvitedUser(res.profile);
        })
        .catch(err => {
          setCheckingUsername(false);
          setInvitedUser({});
        });
    }
  }, [Router]);

  const emailUniqueTest = useRef(cacheTest(checkEmailUnique));

  return (
    <React.Fragment>
      {invitedUser.workEmail ? (
        <>
          <Typography component="h1" variant="h5" className={classes.heading}>
            You’re about to enter the new platform
          </Typography>
          <Typography variant="body1" className={classes.heading}>
            To ensure your security, please choose a new password.
          </Typography>
        </>
      ) : (
        <Typography component="h1" variant="h5" className={classes.heading}>
          Sign up
        </Typography>
      )}

      <Form
        initialValues={{
          email: invitedUser.workEmail ? invitedUser.workEmail : '',
          password: '',
          confPassword: '',
          terms: false
        }}
        enableReinitialize={true}
        validateOnBlur={true}
        validateOnChange={false}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .required('Email is required!')
            .email('Please enter a valid email address.')
            .test(
              'username-backend-validation',
              'This email is already taken!',
              async (value, { createError }) => {
                if (value === invitedUser?.workEmail) {
                  return true;
                } else {
                  return emailUniqueTest.current(value, {
                    createError,
                    formRegistration: true
                  });
                }
              }
            ),
          password: Yup.string()
            .required('Password is required!')
            .min(8, 'Password must be at least 8 characters long!'),
          // .matches(
          //   /^(?=.*[A-Za-z])(?=.*\d)(?=.*[!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~])[A-Za-z\d!"#$%&'()*+,-./:;<=>?@[\]^_`{|}~]{8,}$/,
          //   'Password must contain 8 Characters, One Uppercase, One Lowercase, One Number and one Special Character'
          // ),
          confPassword: Yup.string().oneOf(
            [Yup.ref('password'), null],
            'Passwords must match'
          ),
          terms: Yup.boolean().oneOf(
            [true],
            'Please accept terms & conditions.'
          )
        })}
        onSubmit={async (values, form) => {
          setLoading(true);
          if (invitedUser.id && invitedUser.id > 0) {
            try {
              const data = {
                login: {
                  password: values.password,
                  confPassword: values.confPassword
                }
              };
              await props.completeInvite(data, Router?.query?.code);
              await setLoading(false);
            } catch (error) {
              setLoading(false);
            }
          } else {
            const data = {
              workEmail: values.email,
              login: {
                username: values.email,
                ...values
              }
            };
            try {
              await props.register(data);
              setLoading(false);
            } catch (error) {
              setLoading(false);
            }
          }
          form.prevent;
        }}
      >
        {props => {
          return (
            <form
              className={classes.form}
              onSubmit={e => {
                e.preventDefault();
                props.submitForm();
                return false;
              }}
              noValidate
            >
              <Form.Field.Input
                fullWidth
                variant="outlined"
                name="email"
                label="Email Address"
                disabled={
                  (invitedUser.workEmail && invitedUser.workEmail !== '') ||
                  loading
                }
              />
              <Form.Field.Input
                fullWidth
                variant="outlined"
                type="password"
                name="password"
                label="Password"
                disabled={loading}
              />
              <Form.Field.Input
                fullWidth
                variant="outlined"
                type="password"
                name="confPassword"
                label="Confirm Password"
                disabled={loading}
              />
              <div className={classes.termsPrivacyWrapper}>
                <Form.Field.Checkbox name="terms" label="" />
                <Typography>
                  By continuing, you're agreeing to the LifeWise Customer{' '}
                  <a
                    href="https://lifewise.co.uk/service-agreement/"
                    target="_blank"
                  >
                    Terms of service
                  </a>{' '}
                  and{' '}
                  <a
                    href="https://lifewise.co.uk/privacy-policy/"
                    target="_blank"
                  >
                    Privacy policy
                  </a>
                </Typography>
              </div>

              <Box mt={2}>
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  fullWidth
                  loading={loading || checkingUsername}
                  mt={2}
                >
                  CONFIRM
                </Button>
              </Box>

              <Box mt={3} className={classes.footer}>
                <Typography className={classes.footerContent}>
                  Already have an account?{' '}
                  <Link href="/auth/login">
                    <Typography variant="inherit" color="primary">
                      Log in
                    </Typography>
                  </Link>
                </Typography>
              </Box>
            </form>
          );
        }}
      </Form>
    </React.Fragment>
  );
}
