import { takeLatest, all, fork, put, call, delay } from 'redux-saga/effects';
import * as authTypes from '../actions/authTypes';
import * as authActions from '../actions/authActions';
import {
  setAppLoading,
  fetchRoles,
  fetchSchoolYears,
  fetchHomeDefaultClass,
  clearHomeClass
} from 'src/modules/app/actions/appActions';
import { getMySchool } from 'src/modules/school/actions/schoolActions';
import Router from 'next/router';
import {
  setRegisterData,
  setUserToken,
  getUserToken,
  getUserData,
  unsetUserData,
  unsetUserToken
} from 'src/utils/storageUtils';
import * as authApis from '../api/authApis';
import { authPages } from 'src/config';
import toast from 'src/utils/toast';

function* loginUser({ data, resolve, reject }) {
  try {
    const login = yield call(authApis.loginUser, data);
    if (login?.redirect) {
      yield call(Router.replace, login?.redirect);
      yield call(resolve, true);
    } else if (
      typeof login.access_token !== 'undefined' &&
      login.access_token !== ''
    ) {
      yield call(setUserToken, login?.access_token);
      if (login?.status === 'new') {
        if (login?.profile?.role?.level && login?.profile?.role?.level <= 5) {
          yield put(getMySchool());
        }
        yield put(fetchRoles());
        yield put(authActions.setAuth(login));
        yield call(setRegisterData, { ...login });
        yield call(Router.replace, '/auth/onboarding');
        toast.success('Please complete your profile.');
        yield call(resolve, true);
      } else {
        toast.success('Login successful!');
        yield call(resolve, true);
        yield put(authActions.restoreSession());
      }
      // yield call(Router.replace, '/');
    } else {
      toast.error('Invalid login details. Please try again.');
      yield call(reject, false);
    }
  } catch (error) {
    toast.error(
      error?.message == 'Unauthorized'
        ? 'Email or password is not correct'
        : error?.message
    );
    yield call(reject, false);
  }
}

function* loginSilently({ data, resolve, reject }) {
  try {
    const login = yield call(authApis.loginUser, data);
    if (
      typeof login.access_token !== 'undefined' &&
      login.access_token !== ''
    ) {
      yield call(setUserToken, login?.access_token);
      yield put(getMySchool());
      resolve(login);
    } else {
      yield call(reject, false);
    }
  } catch (error) {
    console.log(error);
    yield call(reject, false);
  }
}

function* registerUser({ data, resolve, reject }) {
  try {
    const res = yield call(authApis.registerUser, data);
    yield call(setUserToken, res?.access_token);
    if (res?.profile?.role?.level && res?.profile?.role?.level <= 5) {
      yield put(getMySchool());
    }
    yield put(fetchRoles());
    yield put(fetchSchoolYears());
    yield put(authActions.setAuth(res));
    yield call(setRegisterData, {
      ...res,
      ...data
    });
    yield call(setRegisterData, {
      ...res,
      ...data
    });
    yield call(Router.replace, '/auth/onboarding');
    resolve(true);
  } catch (error) {
    console.log('error : ', error);
    toast.error(error?.message);
    reject(error);
  }
}

function* verifyAccount({ code, userId, resolve, reject }) {
  try {
    const res = yield call(authApis.verifyUser, code, userId);

    yield call(setRegisterData, {
      ...res
    });
    yield call(setUserToken, res?.access_token);
    if (res?.profile?.role?.level && res?.profile?.role?.level <= 5) {
      yield put(getMySchool());
    }
    yield put(fetchRoles());
    yield put(fetchSchoolYears());
    yield put(authActions.setAuth(res));
    resolve(true);
    Router.push({
      pathname: '/auth/onboarding',
      query: { step: 'class-details' }
    });
  } catch (error) {
    console.log('error : ', error);
    toast.error(error?.error);
    reject(error);
  }
}

function* completeInvite({ data, code, resolve, reject }) {
  try {
    const res = yield call(authApis.completeInvite, data, code);
    yield call(setUserToken, res?.access_token);
    yield call(setRegisterData, res);
    yield put(authActions.setAuth(res));
    yield put(fetchRoles());
    yield put(fetchSchoolYears());
    if (res?.profile?.role?.level <= 5) {
      yield call(Router.replace, '/auth/onboarding');
      yield put(getMySchool());
    } else {
      yield call(Router.replace, '/');
    }
    resolve(res);
  } catch (error) {
    console.log('error : ', error);
    toast.error(error?.error);
    reject(error);
  }
}

function* logoutUser() {
  try {
    yield call(unsetUserData);
    yield call(unsetUserToken);
    yield put(clearHomeClass());
    yield call(Router.replace, '/auth/login');
  } catch (error) {
    console.log('error : ', error);
  }
}

function* logoutSilently() {
  try {
    yield call(unsetUserData);
    yield call(unsetUserToken);
  } catch (error) {
    console.log('error : ', error);
  }
}

function* restoreSession() {
  try {
    yield put(setAppLoading(true));
    const token = yield call(getUserToken);
    if (token) {
      const profile = yield call(authApis.getProfile);
      if (profile?.profile?.role?.level && profile?.profile?.role?.level <= 5) {
        yield put(getMySchool());
        yield put(fetchHomeDefaultClass());
      }
      yield put(fetchRoles());
      yield put(fetchSchoolYears());
      yield put(authActions.setAuth(profile));
      if (
        authPages.includes(Router.pathname) &&
        Router.pathname != '/auth/onboarding' &&
        Router.pathname != '/auth/verificationEmail' &&
        Router.pathname != '/auth/confirmAccount'
      ) {
        yield call(Router.replace, '/');
      }
    } else {
      yield call(unsetUserToken);
      if (!authPages.includes(Router.pathname)) {
        yield call(Router.replace, '/auth/login');
      }
    }
    yield put(setAppLoading(false));
  } catch (error) {
    console.log(error);
    yield call(unsetUserToken);
    if (!authPages.includes(Router.pathname)) {
      yield call(Router.replace, '/auth/login');
    }
    yield put(setAppLoading(false));
  }
}

function* renewSubscription({ resolve, reject }) {
  try {
    const res = yield call(authApis.renewSubscription);
    yield put(authActions.setProfile(res.profile));
    resolve(true);
  } catch (error) {
    reject(error);
  }
}

export function* watchSagas() {
  yield takeLatest(authTypes.LOGIN, loginUser);
  yield takeLatest(authTypes.LOGIN_SILENTLY, loginSilently);
  yield takeLatest(authTypes.REGISTER, registerUser);
  yield takeLatest(authTypes.LOGOUT, logoutUser);
  yield takeLatest(authTypes.LOGOUT_SILENTLY, logoutSilently);
  yield takeLatest(authTypes.RESTORE_SESSION, restoreSession);
  yield takeLatest(authTypes.COMPLETE_INVITE, completeInvite);
  yield takeLatest(authTypes.VERIFY_ACCOUNT, verifyAccount);
  yield takeLatest(authTypes.RENEW_SUBSCRIPTION, renewSubscription);
}

export default function* runSagas() {
  yield all([fork(watchSagas)]);
}
