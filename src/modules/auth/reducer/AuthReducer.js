import * as types from '../actions/authTypes';

const initialState = {
  user: {},
  token: '',
  isLoggedIn: false,
  isAdmin: false
};

const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_PROFILE:
      return {
        ...state,
        user: {
          ...state.user,
          profile: action.profile
        }
      };

    case types.SET_AUTH:
      return {
        ...state,
        user: action.data,
        isLoggedIn: true,
        isAdmin: action.data?.profile?.role.level >= 5
      };

    default:
      return state;
  }
};
export default AuthReducer;
