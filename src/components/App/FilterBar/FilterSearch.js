import React from 'react';
import PropTypes from 'prop-types';
import { Box, Paper, IconButton, InputBase } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import CloseIcon from '@material-ui/icons/Close';
import { useStyles } from './style';

function FilterSearch({ ml, mr, placeholder, onChange, value, onClear }) {
  const classes = useStyles();

  const onChangeVal = event => {
    onChange(event.target.value);
  };

  const onClick = () => {
    if (value && value !== '') {
      onClear();
    }
  };

  return (
    <Box ml={ml} mr={mr} display="flex">
      <Paper
        component="form"
        variant="outlined"
        className={classes.inputWrap}
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <InputBase
          className={classes.input}
          placeholder={placeholder}
          inputProps={{ 'aria-label': placeholder }}
          onChange={onChangeVal}
          value={value}
        />
        <IconButton
          className={classes.iconButton}
          aria-label="search"
          onClick={onClick}
        >
          {value && value !== '' ? <CloseIcon /> : <SearchIcon />}
        </IconButton>
      </Paper>
    </Box>
  );
}

FilterSearch.propTypes = {
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  onClear: PropTypes.func,
  ml: PropTypes.number,
  mr: PropTypes.number,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.bool])
};

FilterSearch.defaultProps = {
  placeholder: undefined,
  onChange: () => {},
  onClear: () => {},
  ml: 0,
  mr: 0,
  value: {}
};

export default FilterSearch;
