import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  Menu,
  MenuItem,
  Fade,
  Box,
  Typography,
  TextField,
  CircularProgress
} from '@material-ui/core';
import _, { debounce, uniqueId } from 'lodash';

import { Button } from 'src/components/shared';

import CloseIcon from '@material-ui/icons/Close';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import FilterListIcon from '@material-ui/icons/FilterList';

import { useStyles } from './style';

function FilterPicker({
  title,
  options,
  icon,
  onChange,
  onClear,
  ml,
  mr,
  optLabel,
  optValue,
  selected,
  remoteMethod,
  cancellable,
  searchable,
  className = '',
  ...props
}) {
  const id = uniqueId('filter-picker-');
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [input, setInput] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const [remoteOptions, setRemoteOptions] = React.useState([]);
  const [isMount, setIsMount] = React.useState(true);

  const isSelected = selected;
  const hasRemoteMethod =
    typeof remoteMethod !== 'undefined' && typeof remoteMethod === 'function';

  const handleClick = event => {
    if (!isSelected || !cancellable) {
      setAnchorEl(event.currentTarget);
      if (hasRemoteMethod) {
        fetchRemoteData('');
      }
    } else {
      onClear();
      handleClose();
    }
  };

  React.useEffect(() => {
    if (isMount) {
      setIsMount(false);
      return;
    }
    if (input && input !== '') {
      fetchRemoteData(input);
    }
  }, [input]);

  const onType = event => {
    setInput(event?.target?.value);
  };

  const fetchRemoteData = async val => {
    setRemoteOptions([]);
    setLoading(true);
    const data = await getRemoteData(val);
    setRemoteOptions(data);
    setLoading(false);
  };

  const getRemoteData = async val => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await remoteMethod(val);
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
  };

  const handleClose = () => {
    setAnchorEl(null);
    setInput('');
    setRemoteOptions([]);
  };

  const onSelect = val => {
    onChange(val);
    handleClose();
  };

  const onClearInput = () => {
    setInput('');
    fetchRemoteData('');
  };

  const allOptions = [...options, ...remoteOptions];

  return (
    <Box
      ml={ml}
      mr={mr}
      display="flex"
      className={`${className} ${classes.filterPickerWrapper}`}
    >
      <Button
        className={classes.buttonRoot}
        variant={isSelected ? 'contained' : 'outlined'}
        color={isSelected ? 'primary' : 'default'}
        size="small"
        aria-controls={id}
        onClick={handleClick}
        startIcon={icon ? icon : null}
        endIcon={
          isSelected && cancellable ? (
            <CloseIcon />
          ) : hasRemoteMethod ? (
            <FilterListIcon />
          ) : (
            <FilterListIcon />
            // <KeyboardArrowDownIcon />
          )
        }
        classes={{
          containedPrimary: classes.selectButtonContainedPrimary,
          outlined: classes.buttonOutlined
        }}
        {...props}
      >
        {isSelected
          ? selected[optLabel]
            ? selected[optLabel]
            : selected
          : title}
      </Button>
      <Menu
        id={id}
        anchorEl={anchorEl}
        getContentAnchorEl={null}
        keepMounted
        TransitionComponent={Fade}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center'
        }}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        className={hasRemoteMethod ? classes.filterMenu : ''}
      >
        {hasRemoteMethod && searchable && (
          <MenuItem>
            <TextField
              placeholder="Search..."
              variant="outlined"
              size="small"
              onChange={onType}
              value={input}
              className={classes.filterMenuSearch}
              InputProps={{
                endAdornment: (
                  <div>
                    {loading ? (
                      <CircularProgress color="inherit" size={20} />
                    ) : input && input !== '' ? (
                      <Button iconButton={true} onClick={onClearInput}>
                        <CloseIcon />
                      </Button>
                    ) : null}
                  </div>
                )
              }}
            />
          </MenuItem>
        )}
        {loading ? (
          <MenuItem>
            <Box alignItems="center">
              <CircularProgress color="inherit" size={20} />
              <span style={{ paddingLeft: 8 }}>Loading</span>
            </Box>
          </MenuItem>
        ) : (
          <div>
            {allOptions.length > 0 ? (
              <div>
                {allOptions.map((option, index) => (
                  <MenuItem
                    key={`${id}-option-${index}`}
                    value={option[optValue]}
                    onClick={() => onSelect(option)}
                  >
                    {option[optLabel]}
                  </MenuItem>
                ))}
              </div>
            ) : (
              <MenuItem disabled>
                <Typography>No options.</Typography>
              </MenuItem>
            )}
          </div>
        )}
      </Menu>
    </Box>
  );
}

FilterPicker.propTypes = {
  title: PropTypes.string,
  options: PropTypes.array,
  icon: PropTypes.object,
  onChange: PropTypes.func,
  onClear: PropTypes.func,
  remoteMethod: PropTypes.func,
  ml: PropTypes.number,
  mr: PropTypes.number,
  optLabel: PropTypes.string,
  optValue: PropTypes.string,
  selected: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.string,
    PropTypes.bool
  ]),
  cancellable: PropTypes.bool,
  searchable: PropTypes.bool
};

FilterPicker.defaultProps = {
  title: undefined,
  options: [],
  icon: undefined,
  onChange: () => {},
  onClear: () => {},
  ml: 0,
  mr: 0,
  optLabel: 'label',
  optValue: 'value',
  selected: {},
  cancellable: true,
  searchable: false
};

export default FilterPicker;
