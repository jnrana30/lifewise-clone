import React, { useState } from 'react';
import {
  Box,
  Drawer,
  Paper,
} from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { Button } from 'src/components/shared';
import { useStyles } from './style';
import AddIcon from "@material-ui/icons/Add";

function FilterDrawer({
  title,
  ml,
  mr,
  filterOptions,
  searchBoxFilter,
  ...props
}) {
  const classes = useStyles();

  const [openDrawer, setOpenDrawer] = useState();

  const handleClick = event => {
    setOpenDrawer(true);
  };

  const toggleDrawer = (open) => (event) => {
    if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
      return;
    }

    setOpenDrawer(open)
  };

  return (
    <Box ml={0} mr={mr} display="flex">
      <Button
        className={`${classes.buttonFilter} ${classes.buttonRoot}`}
        variant={'outlined'}
        color={'default'}
        size="small"
        onClick={handleClick}
        endIcon={<KeyboardArrowDownIcon />}
        classes={{
          containedPrimary: classes.selectButtonContainedPrimary,
          outlined: classes.buttonOutlined,
        }}
        {...props}
      >
        {title}
      </Button>

      <Drawer
        anchor={'bottom'} open={openDrawer}
        classes={{
          paper: classes.drawerPaper,
        }}
        onClose={toggleDrawer(false)}
      >
        <Paper className={classes.drawerHeader}>
          {searchBoxFilter}
          <Box>
            <Button
              className={classes.addButton}
              color="primary"
              onClick={toggleDrawer(false)}
            >
              Close
            </Button>
          </Box>
        </Paper>

        <Box display="flex" mt={1.5} flexWrap={'wrap'} overflow="auto">
          {filterOptions}
        </Box>
      </Drawer>
    </Box>
  );
}

export default FilterDrawer;
