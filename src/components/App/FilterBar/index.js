import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { useSelector } from 'react-redux';
import { FilterBarContainer, FilterBarContent, FilterBarWrapper } from './style';
import { drawerCollapseWidth } from '../../../config/theme';

const useStyles = makeStyles(theme => ({
  "& .main-app-container": {
    backgroundColor: 'purple',
  }
}));

function FilterBar({ children, currentTab, onTabChange, tabs, className = '', showBarContent = true, nonStickyHeader = false, ...props }) {
  const classes = useStyles();
  const sidebarOpen = useSelector(state => state.app.sidebarOpen);

  return (
    <FilterBarWrapper nonStickyHeader={nonStickyHeader}>
      <FilterBarContainer
        className={clsx(
          className,
        )}
        nonStickyHeader={nonStickyHeader}
      >
        {showBarContent && <FilterBarContent>{children}</FilterBarContent>}
        {tabs.length > 0 && (
          <AppBar
            component="div"
            color="transparent"
            position="static"
            elevation={0}
            style={{
              backgroundColor: '#FFFFFF',
            }}
          >
            <Tabs
              value={currentTab}
              textColor="primary"
              onChange={(e, val) => {
                onTabChange(val);
              }}
              TabIndicatorProps={{
                style: {
                  backgroundColor: '#ff9800'
                }
              }}
            >
              {tabs.map((tab, index) => (
                <Tab
                  textColor="primary"
                  label={tab}
                  key={`FilterBar-tab-${index}`}
                />
              ))}
            </Tabs>
          </AppBar>
        )}
      </FilterBarContainer>
    </FilterBarWrapper>
  );
}

FilterBar.propTypes = {
  tabs: PropTypes.array,
  currentTab: PropTypes.number,
  onTabChange: PropTypes.func
};

FilterBar.defaultProps = {
  tabs: [],
  currentTab: 0,
  onTabChange: () => {}
};

export default FilterBar;
