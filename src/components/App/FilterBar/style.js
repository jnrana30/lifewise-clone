import styled, { css } from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';

export const FilterBarWrapper = styled.div``;

export const FilterBarContainer = styled.div`
  ${props =>
    props?.nonStickyHeader
      ? `
        margin: -30px -24px;
        background-color: #F7F7F7;
        margin-bottom: 30px;
        border-top: 1px solid rgba(0, 0, 0, 0.12);
      `
      : `
        background-color: #F7F7F7;
        border-top: 1px solid rgba(0, 0, 0, 0.12);
        width: 100%;
        transition: width 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 225ms cubic-bezier(0.4, 0, 0.6, 1) 0ms;
        border-left-width: 0;
        z-index: 1201;
      `}
`;

export const FilterBarContent = styled.div`
  padding: 14px 24px;
  @media (min-width: 600px) {
    overflow-x: auto;
  }
`;

export const useStyles = makeStyles(theme => ({
  filterMenu: {
    '&  li': {
      maxWidth: '320px',
      minWidth: '320px',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
      display: 'block'
    }
  },
  filterMenuSearch: {
    width: '100%'
  },
  filterPickerWrapper: {
    [theme.breakpoints.down(600)]: {
      marginBottom: theme.spacing(0.5),
      width: '50%',
      paddingRight: 8,
      marginLeft: 0,
      marginRight: 0
    }
  },
  buttonRoot: {
    [theme.breakpoints.down(600)]: {
      width: '100%'
    },
    padding: '0px 14px',
    borderRadius: 4,
    minHeight: 44,
    height: 36,
    minHeight: 36
  },
  inputWrap: {
    padding: '0',
    display: 'flex',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.23)',
    height: 36,
    [theme.breakpoints.down(600)]: {
      maxWidth: 250
    }
  },
  input: {
    marginLeft: theme.spacing(2),
    flex: 1
  },
  iconButton: {
    padding: 10
  },
  divider: {
    height: 28,
    margin: 4
  },
  selectButtonContainedPrimary: {
    backgroundColor: '#f6b036',
    '&:hover': {
      backgroundColor: '#ac7b25'
    }
  },
  buttonOutlined: {
    backgroundColor: '#ffffff'
  },
  drawerPaper: {
    borderTopLeftRadius: theme.spacing(1.5),
    borderTopRightRadius: theme.spacing(1.5),
    padding: '12px 12px 16px',
    height: '80%'
  },
  drawerHeader: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(1.5),
    marginLeft: -theme.spacing(1.5),
    marginRight: -theme.spacing(1.5),
    marginTop: -theme.spacing(1.5),
    borderRadius: 0,
    boxShadow: 'unset',
    borderBottom: '1px solid rgba(0,0,0,0.12)'
  },
  buttonFilter: {
    marginLeft: 0
  }
}));
