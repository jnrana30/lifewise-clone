import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export const AccessControl = ({
  user,
  level,
  levelGt,
  levelLt,
  permission,
  children
}) => {
  const checkRole = level => {
    return user?.profile?.role?.level && user?.profile?.role?.level >= level;
  };

  if (level && checkRole(level)) {
    return children;
  }
  if (
    levelGt &&
    user?.profile?.role?.level &&
    user?.profile?.role?.level >= levelGt
  ) {
    return children;
  }
  if (
    levelLt &&
    user?.profile?.role?.level &&
    user?.profile?.role?.level <= levelLt
  ) {
    return children;
  }
  return null;
};

AccessControl.propTypes = {
  levelGt: PropTypes.number,
  levelLt: PropTypes.number,
  level: PropTypes.number,
  permission: PropTypes.string,
  isAdmin: PropTypes.bool
};

AccessControl.defaultProps = {
  levelGt: undefined,
  levelLt: undefined,
  level: undefined,
  permission: undefined,
  isAdmin: false
};

const mapStateToProps = state => ({
  user: state.auth.user,
  roles: state.app.roles
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(AccessControl);
