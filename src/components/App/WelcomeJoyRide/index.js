import React, { useEffect, useState } from 'react';
import Joyride, { ACTIONS, EVENTS, STATUS } from 'react-joyride';
import { Typography } from '@material-ui/core';

const WelcomeJoyRide = ({ startJoyRide = false, setJoyRide }) => {
  const [run, setRun] = useState(false);
  const [stepIndex, setStepIndex] = useState(0);
  const [steps] = useState([
    {
      content: (
        <div>
          <Typography variant="body1">
            Navigate to the section you want from here.
          </Typography>
        </div>
      ),
      placement: 'right',
      spotlightPadding: 0,
      styles: {
        options: {
          zIndex: 10000,
        },
      },
      target: '.main__side-bar',
      title: 'Navigation',
    },
    {
      content: (
        <div>
          <Typography variant="body1">
            Select the class here to see the lessons linked with this class.
          </Typography>
        </div>
      ),
      placement: 'bottom',
      spotlightPadding: 0,
      styles: {
        options: {
          zIndex: 10000,
        },
      },
      target: '.home-page-filter',
      title: 'Classes',
      hideBackButton: true,
      hideCloseButton: true,
      spotlightClicks: true,
    },
    {
      content: (
        <div>
          <Typography variant="body1">
            Click anywhere on the card to open the lesson. Select the 3 green dots to see a list of download or actions against the lesson.
          </Typography>
        </div>
      ),
      floaterProps: {
        disableAnimation: true,
      },
      hideBackButton: true,
      placement: 'right',
      disableBeacon: true,
      disableOverlayClose: true,
      hideCloseButton: true,
      spotlightClicks: true,
      styles: {
        options: {
          zIndex: 11234,
        },
        spotlight: {
          top: '90px !important',
        }
      },
      target: '.joy-ride-lesson-click',
      title: 'Lesson Card',
    },
    // {
    //   content: (
    //     <div>
    //       <Typography variant="h5">
    //         Click on the lesson to play
    //       </Typography>
    //     </div>
    //   ),
    //   hideBackButton: true,
    //   hideCloseButton: true,
    //   styles: {
    //     options: {
    //       zIndex: 10000,
    //     },
    //   },
    //   target: '.joy-ride-play-lesson',
    //   title: 'Menu',
    // },
  ]);

  useEffect(() => {
    if (startJoyRide === true) {
      setRun(true);
      setStepIndex(0);
    }
  }, [startJoyRide]);

  const handleJoyrideCallback = (data) => {
    const { action, index, type, status } = data;

    if (STATUS.FINISHED === status) {
      setJoyRide(false);
    }

    if ([STATUS.FINISHED, STATUS.SKIPPED].includes(status)) {
      // Need to set our running state to false, so we can restart if we click start again.
      setRun(false);
      setStepIndex(0);
    } else if ([EVENTS.STEP_AFTER, EVENTS.TARGET_NOT_FOUND].includes(type)) {
      const newStepIndex = index + (action === ACTIONS.PREV ? -1 : 1);
      if (index === 0) {
        setTimeout(() => {
          setRun(true);
          setStepIndex(newStepIndex);
        }, 400);
      } else if (index === 1) {
        setRun(false);
        setStepIndex(newStepIndex);
        setTimeout(() => {
          if (document.getElementsByClassName('joy-ride-main-scroll').length) {
            setRun(true);
            setTimeout(() => {
              document.getElementsByClassName('joy-ride-main-scroll')[0].scrollTop = 50
            }, 400);
          } else {
            setJoyRide(false);
          }
        }, 500);
      } else if (index === 2) {
        setRun(false);
        setStepIndex(0);
      } else {
        setRun(false);
        setStepIndex(0);
      }
    }
  };

  return (
    <div>
      <Joyride
        continuous={true}
        run={run}
        steps={steps}
        stepIndex={stepIndex}
        scrollToFirstStep={true}
        showProgress={true}
        showSkipButton={false}
        styles={{
          buttonNext: {
            backgroundColor: '#F6B036',
          }
        }}
        callback={handleJoyrideCallback}
      />
    </div>
  )
};

export default WelcomeJoyRide;