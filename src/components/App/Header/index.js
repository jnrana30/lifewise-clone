import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import {
  toggleSidebar,
  toggleUpgradeModal
} from 'src/modules/app/actions/appActions';
import { logout } from 'src/modules/auth/actions/authActions';

import {
  AppBar,
  Toolbar,
  IconButton,
  Typography,
  Box,
  ButtonBase,
  Menu,
  MenuItem,
  ListItemIcon,
  Fade
} from '@material-ui/core';
import { Button, Avatar } from 'src/components/shared';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

import Search from './Search';
import { useRouter } from 'next/router';
import { drawerWidth, drawerCollapseWidth } from 'src/config/theme';
import clsx from 'clsx';
import { ConfirmDialog } from '../index';
import useViewport from '../../../utils/ViewPort';

const useStyles = makeStyles(theme => ({
  toolbar: {
    display: 'block',
    paddingRight: 24,
    justifyContent: 'space-between'
  },
  toolbarHeaderWrapper: {
    [theme.breakpoints.down(600)]: {
      minHeight: 48
    },
    [theme.breakpoints.up(600)]: {
      minHeight: 64
    }
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: '#ffffff',
    width: `calc(100% - ${drawerCollapseWidth}px)`,
    [theme.breakpoints.down('xs')]: {
      width: '100%'
    },
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    borderLeftWidth: 0,
    width: `calc(100% - ${drawerWidth}px)`,
    [theme.breakpoints.down('xs')]: {
      width: '100%'
    },
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  menuButton: {
    display: 'none',
    [theme.breakpoints.down('xs')]: {
      display: 'block'
    }
  },
  toolbarFilter: {
    [theme.breakpoints.up(600)]: {
      marginLeft: -theme.spacing(3),
      marginRight: -theme.spacing(3)
    },
    [theme.breakpoints.down(600)]: {
      marginLeft: -theme.spacing(2),
      marginRight: -theme.spacing(3)
    }
  },
  menuButtonHidden: { display: 'none' },
  buttonBase: { borderRadius: 36 },
  menuRoot: {
    '& li': {
      width: '180px'
    }
  }
}));

const Header = props => {
  const router = useRouter();
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState(null);
  const [confirmOpen, setConfirmOpen] = useState(false);
  const { isMobileView } = useViewport();
  const isUserMenuOpen = Boolean(anchorEl);

  const toggleUserMenu = event => {
    setAnchorEl(isUserMenuOpen ? null : event.currentTarget);
  };

  useEffect(() => {
    const appContainerMargin =
      document.getElementsByClassName('main-app-container')[0];
    if (!props.children && appContainerMargin) {
      appContainerMargin.style.marginTop = '67px';
    } else {
      appContainerMargin.style.marginTop = '';
    }
  }, []);

  const toggleDrawer = () => {
    props.toggleSidebar();
  };

  const navigateToProfile = () => {
    router.push('/profile');
    toggleUserMenu();
  };

  const onLogout = () => {
    toggleUserMenu();
    props.logout();
  };

  const handleUpgradeClick = event => {
    // setConfirmOpen(true);
    props.toggleUpgradeModal();
  };

  const onConfirm = () => {
    setConfirmOpen(false);
  };

  const { profile } = props.user;
  const showUpgradeButton = profile?.schoolSubscriptionType === 'trial';

  return (
    <React.Fragment>
      <AppBar
        position="absolute"
        className={clsx(
          classes.appBar,
          props.sidebarOpen && classes.appBarShift
        )}
        variant="outlined"
        color="default"
      >
        <Toolbar className={classes.toolbar}>
          <Box
            display="flex"
            justifyContent="space-between"
            width={1}
            className={classes.toolbarHeaderWrapper}
          >
            <Box display="flex" alignItems="center">
              <IconButton
                edge="start"
                color="inherit"
                aria-label="open drawer"
                onClick={toggleDrawer}
                className={clsx(classes.menuButton)}
              >
                <MenuIcon />
              </IconButton>

              <Typography
                component="h1"
                variant="h6"
                color="inherit"
                noWrap
                className={classes.title}
              >
                {props.title}
              </Typography>
            </Box>

            {/*<Search />*/}

            <Box display="flex" alignItems="center">
              {!isMobileView && showUpgradeButton && (
                <Box mr={2}>
                  <Button
                    variant="outlined"
                    color="primary"
                    onClick={handleUpgradeClick}
                  >
                    Upgrade
                  </Button>
                </Box>
              )}
              <Box mr={2}>
                {/* <Button
                  iconButton={true}
                  variant="outlined"
                  color="primary"
                  onClick={() => {}}
                >
                  <HelpOutlineIcon color="primary" />
                </Button> */}
                {/* <ButtonBase onClick={() => {}}>
                <Avatar alt="John Doe" src="" />
              </ButtonBase> */}

                <ButtonBase
                  className={classes.buttonBase}
                  onClick={toggleUserMenu}
                >
                  <Avatar name={profile?.firstName ? profile?.firstName : ''} />
                </ButtonBase>
                <Menu
                  id="fade-menu"
                  anchorEl={anchorEl}
                  getContentAnchorEl={null}
                  keepMounted
                  open={isUserMenuOpen}
                  onClose={toggleUserMenu}
                  TransitionComponent={Fade}
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center'
                  }}
                  transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                  }}
                  className={classes.menuRoot}
                >
                  <MenuItem onClick={navigateToProfile}>
                    <ListItemIcon>
                      <AccountCircleIcon />
                    </ListItemIcon>
                    Profile
                  </MenuItem>
                  <MenuItem onClick={onLogout}>
                    <ListItemIcon>
                      <ExitToAppIcon />
                    </ListItemIcon>
                    Logout
                  </MenuItem>
                </Menu>
              </Box>
            </Box>
          </Box>
          {props.children && (
            <Box className={classes.toolbarFilter}>{props.children}</Box>
          )}
        </Toolbar>

        <ConfirmDialog
          title="Are you sure?"
          open={confirmOpen}
          setOpen={setConfirmOpen}
          onConfirm={onConfirm}
        >
          Are you sure you want to upgrade?
        </ConfirmDialog>
      </AppBar>
    </React.Fragment>
  );
};

const mapStateToProps = state => ({
  sidebarOpen: state.app.sidebarOpen,
  user: state.auth.user
});

const mapDispatchToProps = dispatch => ({
  toggleSidebar: () => dispatch(toggleSidebar()),
  toggleUpgradeModal: () => dispatch(toggleUpgradeModal()),
  logout: () => dispatch(logout())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
