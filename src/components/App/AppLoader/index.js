import React from 'react';
import images from 'src/config/images.js';
import { LoaderContainer } from './style';
import { Box } from '@material-ui/core';
import Image from 'next/image';
import CircularProgress from '@material-ui/core/CircularProgress';

function AppLoader(props) {
  return (
    <div>
      <div className="wrapper">
        <div className="page-content">
          <section className="desktop-front-bg">
            <LoaderContainer>
              <Box mb={4}>
                <Image
                  src={images.app.logo}
                  alt="LifeWise Logo"
                  width={171}
                  height={40}
                />
              </Box>
              <CircularProgress color="secondary" />
            </LoaderContainer>
          </section>
        </div>
      </div>
    </div>
  );
}

export default AppLoader;
