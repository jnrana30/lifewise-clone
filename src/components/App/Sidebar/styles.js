import styled, { css } from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import { drawerWidth } from 'src/config/theme';

export const MenuImage = styled.img`
  width: 26px;
  height: 26px;
  ${props =>
    !props.open &&
    css`
      transform: rotate(180deg);
      -webkit-transform: rotate(180deg);
    `}
`;

export const useStyles = makeStyles(theme => ({
  toolbar: {
    paddingRight: 24
  },
  subMenus: { paddingLeft: 56 },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0px 4px',
    ...theme.mixins.toolbar
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing(0),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(7)
    }
  },
  listItemIcon: {
    minWidth: 40
  }
}));
