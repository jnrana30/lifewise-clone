import React from 'react';
import clsx from 'clsx';
import { Drawer, IconButton, Hidden } from '@material-ui/core';

import MenuIcon from '@material-ui/icons/Menu';
import SettingsIcon from '@material-ui/icons/Settings';
import MenuBookIcon from '@material-ui/icons/MenuBook';
import SchoolIcon from '@material-ui/icons/School';
import DashboardIcon from '@material-ui/icons/Dashboard';
import BusinessIcon from '@material-ui/icons/Business';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import AssessmentIcon from '@material-ui/icons/Assessment';
import SettingsApplicationsIcon from '@material-ui/icons/SettingsApplications';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import ChromeReaderModeIcon from '@material-ui/icons/ChromeReaderMode';
import LabelIcon from '@material-ui/icons/Label';
import AssistantIcon from '@material-ui/icons/Assistant';

import Image from 'next/image';
import images from 'src/config/images';
import { useRouter } from 'next/router';
import Menu from './Menu';

import { useStyles, MenuImage } from './styles';

const Sidebar = ({ sidebarOpen, ...props }) => {
  const [_document, set_document] = React.useState(null);

  React.useEffect(() => {
    set_document(document);
  }, []);
  const router = useRouter();

  const userLevel = props.user?.profile?.role?.level
    ? props.user?.profile?.role?.level
    : 0;

  const [menuItems, setMenuItems] = React.useState([
    {
      title: 'My Lessons',
      href: '/',
      icon: <DashboardIcon />,
      image: images.sidebar.MyLessons,
      access: userLevel > 0
    },
    {
      title: 'Lessons Library',
      href: '/lessons',
      icon: <MenuBookIcon />,
      image: images.sidebar.LessonsLibrary,
      access: userLevel > 0 && userLevel <= 5
    },
    {
      title: 'Lessons',
      href: '/lessons/admin',
      icon: <MenuBookIcon />,
      image: images.sidebar.LessonsLibrary,
      access: userLevel > 5
    },
    {
      title: 'Courses',
      href: '/courses',
      icon: <SchoolIcon />,
      image: images.sidebar.MyCourses,
      access: userLevel > 0 && userLevel <= 5
    },
    {
      title: 'Assemblies',
      href: '/courses/assemblies',
      icon: <AssistantIcon />,
      image: images.sidebar.Assemblies,
      access: userLevel > 0 && userLevel <= 5
    },
    {
      title: 'Courses',
      href: '/courses/admin',
      icon: <SchoolIcon />,
      image: images.sidebar.MyCourses,
      access: userLevel > 5
    },
    {
      title: userLevel > 5 ? 'Resources' : 'Planning Docs',
      href: '/resources',
      icon: <SettingsApplicationsIcon />,
      image: images.sidebar.PlanningDocs,
      access: userLevel > 0
    },
    {
      title: 'Assessments',
      href: '/assessments',
      icon: <AssessmentIcon />,
      image: images.sidebar.Assesments,
      access: userLevel > 0
    },
    {
      title: 'Settings',
      icon: <SettingsIcon />,
      image: images.sidebar.Settings,
      isOpen: false,
      children: [
        {
          title: 'Teachers',
          href: '/settings/teachers',
          icon: '',
          access: userLevel > 0
        },
        {
          title: 'Classes',
          href: '/settings/classes',
          icon: '',
          access: userLevel > 0
        },
        {
          title: 'Students',
          href: '/settings/students',
          icon: '',
          access: userLevel > 0
        },
        {
          title: 'School',
          href: '/settings/school',
          icon: '',
          access: userLevel > 4 && userLevel <= 5
        }
      ],
      access: userLevel >= 2
    },
    {
      title: 'Invite Colleagues',
      href: '/settings/teachers?invite=true',
      icon: <AccountTreeIcon />,
      image: images.sidebar.InviteColleague,
      access: userLevel > 0 && userLevel <= 5
    },
    {
      title: 'Subscription',
      href: '/subscriptions',
      icon: <AccountTreeIcon />,
      image: images.sidebar.Subscriptions,
      access: userLevel > 5
    },
    {
      title: 'Packages',
      href: '/packages',
      icon: <ChromeReaderModeIcon />,
      image: images.sidebar.Packages,
      access: userLevel > 5
    },
    {
      title: 'Schools',
      href: '/schools',
      icon: <BusinessIcon />,
      image: images.sidebar.Schools,
      access: userLevel > 5
    },
    {
      title: 'Employees',
      href: '/employees',
      icon: <AssignmentIndIcon />,
      image: images.sidebar.Employees,
      access: userLevel > 5
    },
    {
      title: 'Tags',
      href: '/tags',
      icon: <LabelIcon />,
      image: images.sidebar.Tags,
      access: userLevel > 5
    }
  ]);
  const classes = useStyles();

  React.useEffect(() => {
    if (sidebarOpen === true) return null;
    const menus = [...menuItems];
    menus.map(menu => {
      if (menu.children?.length && menu.isOpen === true) {
        menu.isOpen = false;
      }
    });
    setMenuItems(menus);
  }, [sidebarOpen]);

  const toggleSidebar = () => {
    props.toggleSidebar();
  };

  const onMenuClick = (menu, i) => {
    if (menu.children?.length) {
      const newMenuItems = [...menuItems];
      menu.isOpen = !menu.isOpen;
      newMenuItems[i] = menu;
      setMenuItems(newMenuItems);

      if (!sidebarOpen) {
        toggleSidebar();
      }
    }
    if (menu.href && menu.href !== '') {
      router.push(menu.href);
    }
  };

  return (
    <React.Fragment>
      <Hidden xsDown>
        <Drawer
          variant="permanent"
          classes={{
            paper: clsx(
              classes.drawerPaper,
              !sidebarOpen && classes.drawerPaperClose
            )
          }}
          open={sidebarOpen}
        >
          <div className={classes.toolbarIcon}>
            <Image
              src={images.app.logo}
              alt="LifeWise Logo"
              width={171}
              height={40}
            />
            <IconButton onClick={toggleSidebar}>
              {/* <MenuIcon /> */}
              <MenuImage src={images.sidebar.MenuIcon} open={sidebarOpen} />
            </IconButton>
          </div>
          <Menu menuItems={menuItems} onMenuClick={onMenuClick} />
        </Drawer>
      </Hidden>
      <Hidden smUp>
        <Drawer
          variant="temporary"
          classes={{
            paper: clsx(
              classes.drawerPaper,
              !sidebarOpen && classes.drawerPaperClose
            )
          }}
          open={sidebarOpen}
        >
          <div className={classes.toolbarIcon}>
            <Image
              src={images.app.logo}
              alt="LifeWise Logo"
              width={171}
              height={40}
            />
            <IconButton onClick={toggleSidebar}>
              <MenuIcon />
            </IconButton>
          </div>
          <Menu menuItems={menuItems} onMenuClick={onMenuClick} />
        </Drawer>
      </Hidden>
    </React.Fragment>
  );
};

export default Sidebar;
