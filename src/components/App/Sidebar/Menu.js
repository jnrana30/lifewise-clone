import React from 'react';
import {
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Box,
  Collapse,
  Hidden
} from '@material-ui/core';
import { useRouter } from 'next/router';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { useStyles } from './styles';

function Menu({ menuItems, onMenuClick }) {
  const router = useRouter();
  const classes = useStyles();
  return (
    <Box className="main__side-bar" style={{ marginTop: 10 }}>
      {menuItems.map((menu, index) => {
        const active = router.route === menu.href;
        if (menu.access === false) return null; // DISABLE MENU IF NO ACCESS.
        return (
          <React.Fragment key={`sidebar-menu-${index}`}>
            <ListItem
              button
              selected={active}
              onClick={() => onMenuClick(menu, index)}
            >
              {menu.icon && (
                <ListItemIcon className={classes.listItemIcon}>
                  {/* {menu.icon} */}
                  <img src={menu.image} width={26} height={26} />
                </ListItemIcon>
              )}
              <ListItemText primary={menu.title} />
              {menu.children?.length && (
                <React.Fragment>
                  {menu.isOpen ? <ExpandLess /> : <ExpandMore />}
                </React.Fragment>
              )}
            </ListItem>

            {menu.children?.length && (
              <Collapse in={menu.isOpen} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  {menu.children.map((childMenu, i) => {
                    if (childMenu.access === false) return null; // DISABLE MENU IF NO ACCESS.
                    return (
                      <ListItem
                        button
                        key={`sidebar-sub-menu-${i}`}
                        className={classes.subMenus}
                        onClick={() => onMenuClick(childMenu, i)}
                      >
                        {childMenu.icon && <ListItemIcon></ListItemIcon>}
                        <ListItemText
                          primary={childMenu.title}
                          primaryTypographyProps={{ variant: 'body2' }}
                        />
                      </ListItem>
                    );
                  })}
                </List>
              </Collapse>
            )}
          </React.Fragment>
        );
      })}
    </Box>
  );
}

export default Menu;
