import React, { useEffect, useState } from 'react';
import { Chip, Tooltip } from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  chipRoot: {
    fontSize: 12,
    height: 29,
    '& .MuiChip-label': {
      paddingLeft: 8,
      paddingRight: 8,
    },
  },
  label: {
    whiteSpace: 'nowrap',
    textOverflow: 'ellipsis',
    [theme.breakpoints.down('sm')]: {
      maxWidth: 50,
    },
    [theme.breakpoints.up('md')]: {
      maxWidth: 40,
    },
    [theme.breakpoints.up('lg')]: {
      maxWidth: 50,
    },
    [theme.breakpoints.up(1615)]: {
      maxWidth: 70,
    },
    [theme.breakpoints.up('xl')]: {
      maxWidth: 80,
    },
  },
}));

function Tag({ tag, tootTipMessage = '', className = '', ...props }) {
  const [textColor, setTextColor] = useState('');
  const classes = useStyles();

  useEffect(() => {
    let color = '#333333';
    if (tag?.color && tag?.color != '') {
      color = lightOrDark(tag?.color) == 'light' ? '#333333' : '#ffffff';
    }
    setTextColor(color);
  }, [tag]);

  const lightOrDark = color => {
    var r, g, b, hsp;
    if (color.match(/^rgb/)) {
      color = color.match(
        /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
      );
      r = color[1];
      g = color[2];
      b = color[3];
    } else {
      color = +(
        '0x' + color.slice(1).replace(color.length < 5 && /./g, '$&$&')
      );
      r = color >> 16;
      g = (color >> 8) & 255;
      b = color & 255;
    }
    hsp = Math.sqrt(0.299 * (r * r) + 0.587 * (g * g) + 0.114 * (b * b));
    if (hsp > 127.5) {
      return 'light';
    } else {
      return 'dark';
    }
  };

  return (
    <Tooltip arrow title={tootTipMessage || tag.tagName}>
      <Chip
        variant="outlined"
        label={tag.tagName}
        className={className}
        classes={{
          root: classes.chipRoot,
          label: classes.label,
        }}
        style={{
          backgroundColor: tag.color ? tag.color : '#ffffff',
          color: textColor,
          marginRight: 6
        }}
        {...props}
      />
    </Tooltip>
  );
}

export default Tag;
