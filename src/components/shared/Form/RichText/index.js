import React from 'react';
import PropTypes from 'prop-types';
import { Box, Typography } from '@material-ui/core';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import MUIRichTextEditor from 'mui-rte';
import { convertToRaw, convertFromHTML, convertFromRaw } from 'draft-js';

const defaultTheme = createMuiTheme();

Object.assign(defaultTheme, {
  overrides: {
    MUIRichTextEditor: {
      root: {
        backgroundColor: '#ffffff',
        border: '1px solid #dddddd',
        borderRadius: 4,
        '& pre': {
          color: '#212121'
        }
      },
      editor: {
        padding: '10px 20px',
        height: '200px',
        maxHeight: '200px',
        overflow: 'auto'
      },
      placeHolder: {
        paddingTop: 10,
        paddingLeft: 20,
        width: 'inherit',
        position: 'absolute',
        height: '200px',
        width: '100%'
      },
      anchorLink: {
        color: '#FFEB3B',
        textDecoration: 'underline'
      }
    }
  }
});

function RichText({ label, onChange, onBlur, ...props }) {
  const handleChange = event => {
    // onChange(event.target.value, event);

    const content = JSON.stringify(convertToRaw(event.getCurrentContent()));

    // console.log('content');
    // console.log(content);
  };

  const onSave = val => {
    // console.log('val');
    // console.log(val);
  };

  return (
    <Box mb={2}>
      {/* <label>
        <Typography variant="body2" color="textPrimary">
          {label}
        </Typography>
      </label> */}
      <MuiThemeProvider theme={defaultTheme}>
        <MUIRichTextEditor
          label={label}
          // controls={[
          //   'title',
          //   'bold',
          //   'italic',
          //   'underline',
          //   'numberList',
          //   'bulletList',
          //   'highlight'
          // ]}
          defaultValue={props.value}
          onChange={handleChange}
          onSave={onSave}
        />
      </MuiThemeProvider>
    </Box>
  );
}

RichText.propTypes = {};

export default RichText;
