import React from 'react';
import PropTypes from 'prop-types';
import { Box, Typography } from '@material-ui/core';
import ReactMde from 'react-mde';
import * as Showdown from 'showdown';
import 'react-mde/lib/styles/css/react-mde-all.css';

const converter = new Showdown.Converter({
  tables: true,
  simplifiedAutoLink: true,
  strikethrough: true,
  tasklists: true
});

function Markdown({ label, onChange, onBlur, value, ...props }) {
  const [selectedTab, setSelectedTab] = React.useState('write');

  const handleChange = data => {
    onChange(data, null);
  };

  // ToDo : Add file upload support later./
  // const save = async function* (data) {
  //   const wait = function (time) {
  //     return new Promise((a, r) => {
  //       setTimeout(() => a(), time);
  //     });
  //   };
  //   await wait(2000);
  //   yield 'https://picsum.photos/300';
  //   await wait(2000);
  //   return true;
  // };

  return (
    <Box mb={2}>
      <ReactMde
        value={value}
        onChange={handleChange}
        selectedTab={selectedTab}
        onTabChange={setSelectedTab}
        generateMarkdownPreview={markdown =>
          Promise.resolve(converter.makeHtml(markdown))
        }
        // paste={{
        //   saveImage: save
        // }}
      />
    </Box>
  );
}

Markdown.propTypes = {};

export default Markdown;
