import React from 'react';
import PropTypes from 'prop-types';

import {
  Radio,
  RadioGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  Box
} from '@material-ui/core';

function RadioComponent({
  label,
  options,
  labelPlacement,
  color,
  onChange,
  helperText,
  error,
  ...props
}) {
  const handleChange = event => {
    onChange(event.target.value);
  };
  return (
    <Box mt={2}>
      <FormControl component="fieldset">
        <Box mb={1}>
          <FormLabel component="legend">{label}</FormLabel>
        </Box>
        <RadioGroup
          aria-label="gender"
          name="gender1"
          value="female"
          onChange={handleChange}
          {...props}
          row
        >
          {Array.isArray(options) && options.length > 0 && (
            <React.Fragment>
              {options.map((option, index) => (
                <FormControlLabel
                  key={`${props.id}-${index}`}
                  value={option.value}
                  control={<Radio color={color} />}
                  label={option.label}
                  labelPlacement={labelPlacement}
                />
              ))}
            </React.Fragment>
          )}
        </RadioGroup>
      </FormControl>
    </Box>
  );
}

RadioComponent.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  labelPlacement: PropTypes.string,
  options: PropTypes.array,
  onChange: PropTypes.func,
  variant: PropTypes.oneOf(['standard', 'filled', 'outlined']),
  color: PropTypes.oneOf(['default', 'primary', 'secondary']),
  size: PropTypes.oneOf(['medium', 'small'])
};

RadioComponent.defaultProps = {
  className: undefined,
  label: undefined,
  labelPlacement: 'end',
  options: () => [],
  onChange: () => {},
  variant: 'standard',
  color: 'primary',
  size: 'medium'
};
export default RadioComponent;
