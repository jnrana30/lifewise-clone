import React from 'react';
import PropTypes from 'prop-types';
import { uniqueId } from 'lodash';

import Input from './Input';
import Select from './Select';
import Checkbox from './Checkbox';
import Radio from './Radio';
import AutoComplete from './AutoComplete';
import Textarea from './Textarea';
import DatePicker from './DatePicker';
import InputDebounced from './InputDebounced';
import OtpInput from './OtpInput';
import Phone from './Phone';
import File from './File';
import Avatar from './Avatar';
import RichText from './RichText';
import Switch from './Switch';
import LocationAutoComplete from './LocationAutoComplete';
import Markdown from './Markdown';
import ColorPicker from './ColorPicker';

const propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  tip: PropTypes.string,
  error: PropTypes.string,
  errorMsg: PropTypes.string,
  name: PropTypes.string
};

const defaultProps = {
  className: undefined,
  label: undefined,
  tip: undefined,
  error: undefined,
  errorMsg: undefined,
  name: undefined
};

const generateField = FormComponent => {
  const FieldComponent = ({
    className,
    tip,
    error,
    errorMsg,
    name,
    ...otherProps
  }) => {
    const fieldId = uniqueId('form-field-');

    let classes = 'from-field';
    const helperText = tip ? tip : error ? error : errorMsg ? errorMsg : false;
    const isError = !!(error || errorMsg);

    return (
      <FormComponent
        margin="normal"
        id={fieldId}
        name={name}
        helperText={helperText}
        error={isError}
        {...otherProps}
      />
    );
  };

  FieldComponent.propTypes = propTypes;
  FieldComponent.defaultProps = defaultProps;

  return FieldComponent;
};

export default {
  Input: generateField(Input),
  Select: generateField(Select),
  Checkbox: generateField(Checkbox),
  Radio: generateField(Radio),
  AutoComplete: generateField(AutoComplete),
  Textarea: generateField(Textarea),
  DatePicker: generateField(DatePicker),
  InputDebounced: generateField(InputDebounced),
  OtpInput: generateField(OtpInput),
  Phone: generateField(Phone),
  File: generateField(File),
  Avatar: generateField(Avatar),
  RichText: generateField(RichText),
  Switch: generateField(Switch),
  LocationAutoComplete: generateField(LocationAutoComplete),
  Markdown: generateField(Markdown),
  ColorPicker: generateField(ColorPicker)
};
