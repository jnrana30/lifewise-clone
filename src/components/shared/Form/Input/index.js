import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { IconButton } from '@material-ui/core';
import { InputAdornment, CircularProgress } from '@material-ui/core';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const propTypes = {
  className: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  icon: PropTypes.string,
  filter: PropTypes.instanceOf(RegExp),
  onChange: PropTypes.func,
  variant: PropTypes.oneOf(['standard', 'filled', 'outlined']),
  loading: PropTypes.bool
};

const defaultProps = {
  className: undefined,
  value: undefined,
  icon: undefined,
  filter: undefined,
  onChange: () => {},
  variant: 'standard',
  loading: false
};

const InputComponent = forwardRef(
  (
    {
      icon,
      className,
      filter,
      onChange,
      variant,
      helperText,
      error,
      id,
      loading,
      type,
      ...inputProps
    },
    ref
  ) => {
    const [showPassword, setShowPassword] = React.useState(false);
    const handleChange = event => {
      if (!filter || filter.test(event.target.value)) {
        onChange(event.target.value, event);
      }
    };

    const loader = {
      ...(loading && {
        endAdornment: (
          <InputAdornment position="end">
            <CircularProgress size={20} />
          </InputAdornment>
        )
      }),
      ...(type === 'password' && {
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={() => {
                setShowPassword(!showPassword);
              }}
            >
              {showPassword ? (
                <VisibilityOff size={20} />
              ) : (
                <Visibility size={20} />
              )}
            </IconButton>
          </InputAdornment>
        )
      })
    };

    return (
      <TextField
        {...inputProps}
        type={type === 'password' && showPassword ? 'text' : type}
        onChange={handleChange}
        ref={ref}
        variant={variant}
        helperText={helperText}
        error={error}
        id={id}
        InputProps={loader}
      />
    );
  }
);

InputComponent.propTypes = propTypes;
InputComponent.defaultProps = defaultProps;

export default InputComponent;
