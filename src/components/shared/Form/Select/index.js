import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';

const propTypes = {
  className: PropTypes.string,
  variant: PropTypes.oneOf(['standard', 'filled', 'outlined']),
  name: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.array,
    PropTypes.string,
    PropTypes.number
  ]),
  defaultValue: PropTypes.any,
  placeholder: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  multiple: PropTypes.bool,
  remoteMethod: PropTypes.func,
  fullWidth: PropTypes.bool,
  optLabel: PropTypes.string,
  optValue: PropTypes.string,
  showNone: PropTypes.bool
};

const defaultProps = {
  className: undefined,
  variant: 'standard',
  name: undefined,
  label: undefined,
  value: '',
  defaultValue: undefined,
  placeholder: 'Select',
  multiple: false,
  fullWidth: false,
  optLabel: 'label',
  optValue: 'value',
  showNone: true
};

const useStyles = makeStyles(theme => ({
  chips: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  chip: {
    margin: 2
  },
  noLabel: {
    marginTop: theme.spacing(3)
  }
}));

const InputSelect = ({
  className,
  variant,
  label,
  name,
  value,
  defaultValue,
  placeholder,
  options,
  onChange,
  multiple,
  fullWidth,
  helperText,
  margin,
  optLabel,
  optValue,
  showNone,
  ...props
}) => {
  const classes = useStyles();

  const handleChange = event => {
    onChange(event.target.value, event);
  };

  return (
    <FormControl fullWidth={fullWidth} margin={margin} error={props.error}>
      <InputLabel id={`${props.id}-label`}>{label}</InputLabel>
      <Select
        labelId={`${props.id}-label`}
        id={props.id}
        onChange={handleChange}
        variant={variant}
        name={name}
        label={label}
        defaultValue={value}
        value={value}
        {...props}
        name={props.name}
      >
        {showNone && <MenuItem value="">None</MenuItem>}

        {(() => {
          if (options && typeof options !== 'undefined' && options.length) {
            return options.map(option => {
              return (
                <MenuItem
                  disabled={option?.disabled ? option?.disabled : false}
                  key={option[optValue]}
                  value={option[optValue]}
                >
                  {option[optLabel]}
                </MenuItem>
              );
            });
          }
        })()}

        {/* {options && typeof options !== 'undefined' && options.length && (
          <React.Fragment>
            {options.map(option => {
              return (
                <MenuItem key={option[optValue]} value={option[optValue]}>
                  {option[optLabel]}
                </MenuItem>
              );
            })}
          </React.Fragment>
        )} */}
      </Select>
      {helperText && (
        <FormHelperText style={{ paddingLeft: 14 }}>
          {helperText}
        </FormHelperText>
      )}
    </FormControl>
  );
};

InputSelect.propTypes = propTypes;
InputSelect.defaultProps = defaultProps;

export default InputSelect;
