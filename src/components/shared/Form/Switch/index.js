import React from 'react';
import PropTypes from 'prop-types';

import {
  Switch,
  FormGroup,
  FormControlLabel,
  FormControl,
  FormLabel,
  Box
} from '@material-ui/core';

function SwitchComponent({
  label,
  labelPlacement,
  color,
  onChange,
  error,
  value,
  ...props
}) {
  const handleChange = event => {
    // console.log(event.target);
    onChange(!value);
  };

  return (
    <Box mt={2}>
      <FormControl component="fieldset">
        <FormGroup aria-label="position" row>
          <FormControlLabel
            value={value}
            control={
              <Switch color={color} checked={value} onChange={handleChange} />
            }
            label={label}
            labelPlacement={labelPlacement}
          />
        </FormGroup>
      </FormControl>
    </Box>
  );
}

SwitchComponent.propTypes = {
  className: PropTypes.string,
  label: PropTypes.string,
  labelPlacement: PropTypes.string,
  onChange: PropTypes.func,
  variant: PropTypes.oneOf(['standard', 'filled', 'outlined']),
  color: PropTypes.oneOf(['default', 'primary', 'secondary']),
  size: PropTypes.oneOf(['medium', 'small']),
  disabled: PropTypes.bool
};

SwitchComponent.defaultProps = {
  className: undefined,
  label: undefined,
  labelPlacement: 'start',
  onChange: () => {},
  variant: 'standard',
  color: 'primary',
  size: 'medium',
  disabled: false
};
export default SwitchComponent;
