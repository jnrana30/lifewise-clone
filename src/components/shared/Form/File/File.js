import React from 'react';
import {
  Box,
  ListItem,
  ListItemSecondaryAction,
  ListItemText
} from '@material-ui/core';
import { Spinner, Button } from 'src/components/shared';
import DeleteIcon from '@material-ui/icons/Delete';
import s3Helper from 'src/utils/s3Helper.js';
import _ from 'lodash';

function File({ file, onUpload, onDelete, folder, fileName }) {
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    if (file && !_.isEmpty(file) && file?.size) {
      doUpload();
    }
  }, []);

  const doUpload = async () => {
    setLoading(true);
    try {
      const res = await s3Helper.upload(file, folder, fileName);
      setLoading(false);
      onUpload(file, res);
    } catch (error) {
      console.log('Error uploading file : ', error);
    }
  };

  const deleteFile = async file => {
    setLoading(true);
    await s3Helper.deleteFile(file.key);
    onDelete(file);
    setLoading(false);
  };

  return (
    <ListItem>
      <a href={file.location} target="_blank">
        <ListItemText primary={file.name || file.path} />
      </a>
      <ListItemSecondaryAction>
        <Box display="flex" alignItems="center">
          {loading && (
            <>
              <Spinner size={24} color="#333333" />
            </>
          )}
          {file.location && file.location && !loading && (
            <Button
              iconButton={true}
              onClick={() => {
                deleteFile(file);
              }}
            >
              <DeleteIcon />
            </Button>
          )}
        </Box>
      </ListItemSecondaryAction>
    </ListItem>
  );
}

export default File;
