import styled from 'styled-components';

export const ContentCardContainer = styled.div`
  width: 100%;
  max-width: 1500px;
  display: block;
  box-sizing: border-box;
  margin-left: auto;
  margin-right: auto;
`;

export const ContentContainer = styled.div`
  width: 100%;
  max-width: 1500px;
  display: block;
  box-sizing: border-box;
  margin-left: auto;
  margin-right: auto;
`;

export const DropContainer = styled.div`
  width: 100%;
  border: 1px dashed rgba(0, 0, 0, 0.42);
  cursor: pointer;
  border-radius: 4px;
  margin-bottom: 20px;
  margin-top: 20px;
`;

export const Dropper = styled.div`
  padding: 0px 14px;
  margin-bottom: 20px;
`;

export const FileContainer = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  p {
    padding-left: 10px;
    margin: 0;
  }
  padding: 0px;
  margin-bottom: 6px;
  border: 1px solid rgba(0, 0, 0, 0.42);
  border-radius: 4px;
  justify-content: space-between;
  background-color: #f2f2f2;
`;

export const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16
};

export const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box'
};

export const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden'
};

export const img = {
  display: 'block',
  width: 'auto',
  height: '100%'
};
