import React from 'react';
import PropTypes from 'prop-types';
import { useDropzone } from 'react-dropzone';
import { Typography, Box, List, Divider, Paper } from '@material-ui/core';
import { DropContainer, Dropper, FileContainer } from './styles';
import File from './File';
import s3Helper from 'src/utils/s3Helper.js';
import { FormHelperText } from '@material-ui/core';

const propTypes = {
  className: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array,
    PropTypes.string
  ]),
  multiple: PropTypes.bool,
  folder: PropTypes.string,
  fileName: PropTypes.string,
  onUploadStart: PropTypes.func
};

const defaultProps = {
  className: undefined,
  value: undefined,
  multiple: false,
  folder: '',
  fileName: '',
  onUploadStart: () => {}
};

const FileInput = ({
  name,
  value,
  multiple,
  onChange,
  accept,
  clearFiles,
  folder,
  fileName,
  onUploadStart,
  ...props
}) => {
  const [files, setFiles] = React.useState([]);
  const [isMount, setIsMount] = React.useState(true);

  React.useEffect(() => {
    if (
      (typeof value === 'string' || value instanceof String) &&
      value !== ''
    ) {
      setFiles([
        {
          key: value,
          location: s3Helper.getFileUrl(value),
          path: value.split('/').pop()
        }
      ]);
    }
  }, [value]);

  React.useEffect(() => {
    if (
      (typeof value === 'array' || value instanceof Array) &&
      value.length > 0 &&
      isMount
    ) {
      const filesArr = value.map(val => {
        if ((typeof val === 'string' || val instanceof String) && val !== '') {
          return {
            key: val,
            location: s3Helper.getFileUrl(val),
            path: val.split('/').pop()
          };
        }
      });
      setFiles(filesArr);
      setIsMount(false);
    }
  }, []);

  React.useEffect(() => {
    onChange(multiple ? files : files[0], null);
  }, [files]);

  const { getRootProps, getInputProps } = useDropzone({
    accept: accept || 'image/*',
    multiple: multiple,
    onDrop: acceptedFiles => {
      if (!acceptedFiles.length) return null;
      handleFileChanges(acceptedFiles);
    }
  });

  const handleFileChanges = async acceptedFiles => {
    onUploadStart();
    await setFiles([...files, ...acceptedFiles]);
  };

  const onUpload = async (file, res) => {
    await setFiles(curr =>
      curr.map(fw => {
        if (fw === file) {
          return { ...fw, ...res };
        }
        return fw;
      })
    );
  };

  const onDelete = async file => {
    await setFiles(curr => curr.filter(fw => fw !== file));
  };

  return (
    <React.Fragment>
      <DropContainer>
        <Box p={2}>
          <Typography>{props.label}</Typography>
        </Box>
        <div {...getRootProps({ className: 'dropzone' })}>
          <input {...getInputProps()} />
          {!multiple && files.length > 0 ? (
            <></>
          ) : (
            <Dropper>
              <Typography variant="body2">
                Drag 'n' drop some files here, or click to select files
              </Typography>
            </Dropper>
          )}
        </div>
        {files.length ? (
          <Box mx={2} mb={2}>
            <Paper variant="outlined">
              <List dense={false} disablePadding={true}>
                {files.map((file, index) => {
                  return (
                    <React.Fragment
                      key={`dropper-${
                        file?.lastModified ? file?.lastModified : index
                      }`}
                    >
                      <File
                        file={file}
                        onUpload={onUpload}
                        onDelete={onDelete}
                        folder={folder}
                        fileName={fileName}
                      />
                      {files.length > 1 && files.length - 1 !== index && (
                        <Divider />
                      )}
                    </React.Fragment>
                  );
                })}
              </List>
            </Paper>
          </Box>
        ) : (
          <></>
        )}
      </DropContainer>
      {/* <FormHelperText error={true} style={{ marginTop: -14 }}>
        Documents is required
      </FormHelperText> */}
    </React.Fragment>
  );
};

FileInput.propTypes = propTypes;
FileInput.defaultProps = defaultProps;

export default FileInput;
