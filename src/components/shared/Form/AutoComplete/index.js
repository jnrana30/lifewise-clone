import React, { useState, useRef, useCallback } from 'react';
import PropTypes from 'prop-types';
import _, { debounce } from 'lodash';

import { FormControl, TextField, Chip, Avatar } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';

import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const propTypes = {
  className: PropTypes.string,
  variant: PropTypes.oneOf(['standard', 'filled', 'outlined']),
  name: PropTypes.string,
  label: PropTypes.string,
  defaultValue: PropTypes.any,
  placeholder: PropTypes.string,
  options: PropTypes.array,
  onChange: PropTypes.func.isRequired,
  multiple: PropTypes.bool,
  remoteMethod: PropTypes.func,
  autocomplete: PropTypes.bool,
  fullWidth: PropTypes.bool,
  showAvatar: PropTypes.bool,
  optLabel: PropTypes.string,
  optValue: PropTypes.string,
  remoteMethod: PropTypes.func,
  disabled: PropTypes.bool
};

const defaultProps = {
  className: undefined,
  variant: 'standard',
  name: undefined,
  label: undefined,
  defaultValue: undefined,
  placeholder: 'Select',
  multiple: false,
  autocomplete: false,
  fullWidth: false,
  showAvatar: false,
  optLabel: 'label',
  optValue: 'value',
  disabled: false
};

const InputAutocomplete = ({
  className,
  variant,
  label,
  name,
  defaultValue,
  placeholder,
  options,
  onChange,
  multiple,
  autocomplete,
  fullWidth,
  showAvatar,
  optLabel,
  optValue,
  value,
  disabled,
  remoteMethod,
  ...props
}) => {
  const [loading, setLoading] = useState(false);
  const [remoteOptions, setRemoteOptions] = useState([]);

  const onType = useCallback(
    debounce(async val => {
      if (
        typeof remoteMethod !== 'undefined' &&
        typeof remoteMethod === 'function'
      ) {
        setRemoteOptions([]);
        setLoading(true);
        const data = await getRemoteData(val);
        setRemoteOptions(data);
        setLoading(false);
      }
    }, 250),
    []
  );

  const getRemoteData = async val => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await remoteMethod(
          val?.target?.value ? val?.target?.value : ''
        );
        resolve(response);
      } catch (error) {
        reject(error);
      }
    });
  };

  const onClose = () => {
    // setRemoteOptions([]);
  };

  let optionsList = [];
  if (
    typeof remoteMethod !== 'undefined' &&
    typeof remoteMethod === 'function' &&
    remoteOptions.length > 0
  ) {
    optionsList = remoteOptions;
  } else {
    optionsList = options;
  }

  return (
    <FormControl fullWidth={fullWidth} margin="normal">
      <Autocomplete
        multiple={multiple}
        id={props.id}
        value={value}
        disabled={disabled}
        // options={[...options, ...remoteOptions]}
        options={optionsList}
        getOptionLabel={option => (option[optLabel] ? option[optLabel] : '')}
        getOptionSelected={(option, value) => {
          return option[optValue] === value[optValue];
        }}
        onChange={(event, newValue) => {
          onChange(newValue, event);
        }}
        onOpen={event => {
          onType();
        }}
        onClose={onClose}
        renderTags={(value, getTagProps) =>
          value.map((option, index) => {
            return (
              <Chip
                avatar={showAvatar ? <Avatar>T</Avatar> : null}
                variant="outlined"
                label={option[optLabel]}
                {...getTagProps({ index })}
              />
            );
          })
        }
        selectOnFocus
        clearOnBlur
        handleHomeEndKeys
        renderInput={params => {
          return (
            <TextField
              {...params}
              label={label}
              variant={variant}
              fullWidth={fullWidth}
              error={props.error}
              helperText={props.helperText}
              style={props.inputStyles ? props.inputStyles : {}}
              onChange={onType}
              disabled={disabled}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <React.Fragment>
                    {loading ? (
                      <CircularProgress color="inherit" size={20} />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </React.Fragment>
                )
              }}
            />
          );
        }}
        name={props.name}
      />
    </FormControl>
  );
};

InputAutocomplete.propTypes = propTypes;
InputAutocomplete.defaultProps = defaultProps;

export default InputAutocomplete;
