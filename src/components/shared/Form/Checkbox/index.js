import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';
import {
  FormGroup,
  FormControl,
  FormControlLabel,
  FormHelperText
} from '@material-ui/core';

const CheckboxInput = forwardRef(
  (
    {
      checked,
      className,
      label,
      onChange,
      variant,
      color,
      size,
      value,
      helperText,
      error,
      ...props
    },
    ref
  ) => {
    return (
      <React.Fragment>
        {label && label !== '' ? (
          <FormControl error={error}>
            <FormGroup row>
              <FormControlLabel
                control={
                  <Checkbox
                    classes={className}
                    variant={variant}
                    color={color}
                    size={size}
                    checked={value}
                    onChange={e => {
                      onChange(e.target.checked, e);
                    }}
                    {...props}
                  />
                }
                label={label}
              />
            </FormGroup>
            {helperText && <FormHelperText>{helperText}</FormHelperText>}
          </FormControl>
        ) : (
          <Checkbox
            classes={className}
            variant={variant}
            color={color}
            size={size}
            checked={value}
            onChange={e => {
              onChange(e.target.checked, e);
            }}
            {...props}
          />
        )}
      </React.Fragment>
    );
  }
);

CheckboxInput.propTypes = {
  checked: PropTypes.bool,
  className: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  variant: PropTypes.oneOf(['standard', 'filled', 'outlined']),
  color: PropTypes.oneOf(['default', 'primary', 'secondary']),
  size: PropTypes.oneOf(['medium', 'small'])
};

CheckboxInput.defaultProps = {
  checked: false,
  className: undefined,
  label: undefined,
  onChange: () => {},
  variant: 'standard',
  color: 'primary',
  size: 'medium'
};

export default CheckboxInput;
