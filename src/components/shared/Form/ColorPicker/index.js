import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
// import ColorPicker from 'material-ui-color-picker';
import { HexColorPicker } from 'react-colorful';

const propTypes = {
  className: PropTypes.string,
  invalid: PropTypes.bool,
  value: PropTypes.string,
  onChange: PropTypes.func
};

const defaultProps = {
  className: undefined,
  invalid: false,
  value: undefined,
  onChange: () => {}
};

const ColorPickerComponent = forwardRef(
  ({ className, invalid, onChange, value, ...otherProps }, ref) => (
    <div className="colorPicker">
      {/* <ColorPicker
        defaultValue={value}
        {...otherProps}
        onChange={color => {
          if (color !== undefined && color !== null) {
            onChange(color, null);
          }
        }}
      /> */}
      <HexColorPicker
        color={value}
        onChange={color => {
          onChange(color, null);
        }}
      />
    </div>
  )
);

ColorPickerComponent.propTypes = propTypes;
ColorPickerComponent.defaultProps = defaultProps;

export default ColorPickerComponent;
