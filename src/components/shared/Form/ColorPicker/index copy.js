import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import ColorPicker from 'material-ui-color-picker';

const propTypes = {
  className: PropTypes.string,
  invalid: PropTypes.bool,
  value: PropTypes.string,
  onChange: PropTypes.func
};

const defaultProps = {
  className: undefined,
  invalid: false,
  value: undefined,
  onChange: () => {}
};

const ColorPickerComponent = forwardRef(
  ({ className, invalid, onChange, value, ...otherProps }, ref) => (
    <React.Fragment>
      <ColorPicker
        defaultValue={value}
        {...otherProps}
        onChange={color => {
          if (color !== undefined && color !== null) {
            onChange(color, null);
          }
        }}
      />
    </React.Fragment>
  )
);

ColorPickerComponent.propTypes = propTypes;
ColorPickerComponent.defaultProps = defaultProps;

export default ColorPickerComponent;
