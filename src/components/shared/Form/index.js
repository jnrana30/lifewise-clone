import React from 'react';
import PropTypes from 'prop-types';
import {
  Formik,
  Form as FormikForm,
  Field as FormikField,
  ErrorMessage
} from 'formik';
import { get, mapValues } from 'lodash';

import toast from '../../../utils/toast';
import { is, generateErrors } from '../../../utils/validation';

import Field from './field';

const propTypes = {
  validate: PropTypes.func,
  validations: PropTypes.object,
  validateOnBlur: PropTypes.bool
};

const defaultProps = {
  validate: undefined,
  validations: undefined,
  validateOnBlur: false
};

const Form = ({ validate, validations, ...otherProps }) => {
  return (
    <Formik
      {...otherProps}
      // validateOnBlur={false}
      // validateOnChange={false}
      validate={values => {
        if (validate) {
          return validate(values);
        }
        if (validations) {
          return generateErrors(values, validations);
        }
        return false;
      }}
    />
  );
};

Form.Element = props => <FormikForm noValidate {...props} />;

Form.Field = mapValues(
  Field,
  FieldComponent =>
    ({ name, validate, ...props }) => {
      return (
        <FormikField name={name} validate={validate}>
          {({
            field,
            form: {
              touched,
              errors,
              setFieldValue,
              setFieldTouched,
              ...otherProp
            }
          }) => {
            const validateSingleField = (name, value) => {
              try {
                otherProp.validateField(name);
              } catch (error) {
                console.log('error : ', error);
              }
            };
            return (
              <FieldComponent
                {...field}
                {...props}
                name={name}
                error={get(touched, name) && get(errors, name)}
                onChange={value => {
                  if (props?.onChange && typeof props.onChange === 'function') {
                    props.onChange(value);
                  }
                  setFieldValue(name, value);
                  // validateSingleField(name, value);
                }}
                onBlur={e => {
                  setFieldTouched(name, true);
                  // validateSingleField(name, e.target.value);
                }}
              />
            );
          }}
        </FormikField>
      );
    }
);

Form.initialValues = (data, getFieldValues) =>
  getFieldValues((key, defaultValue = '') => {
    const value = get(data, key);
    return value === undefined || value === null ? defaultValue : value;
  });

Form.handleAPIError = (error, form) => {
  if (error.data.fields) {
    form.setErrors(error.data.fields);
  } else {
    toast.error(error);
  }
};

Form.is = is;

Form.propTypes = propTypes;
Form.defaultProps = defaultProps;

export default Form;
