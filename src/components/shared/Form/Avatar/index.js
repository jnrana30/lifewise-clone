import React from 'react';
import PropTypes from 'prop-types';
import {
  AvatarContainer,
  AvatarPreview,
  PlaceholderIcon,
  AvatarPlaceholder,
  AvatarImage
} from './style';
import Spinner from '../../Spinner';
import s3Helper from 'src/utils/s3Helper.js';
import toast from 'src/utils/toast';

function Avatar({ id, folder, value, ...props }) {
  const [avatar, setAvatar] = React.useState('');
  const [uploading, setUploading] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  React.useEffect(() => {
    if (
      (typeof value === 'string' || value instanceof String) &&
      value &&
      value != null &&
      value !== ''
    ) {
      setAvatar(s3Helper.getFileUrl(value));
    }
  }, [value]);

  const onFileSelect = async e => {
    const file = e.target?.files[0];
    if (file) {
      try {
        setUploading(true);
        const res = await s3Helper.upload(file, folder);
        props.onChange(res, null);
        setAvatar(res.location);
        setUploading(false);
      } catch (error) {
        toast.error('Error uploading the image. Please try again.');
      }
    }
  };

  return (
    <AvatarContainer>
      <input
        id={id}
        type="file"
        style={{ display: 'none' }}
        accept="image/*"
        onChange={e => onFileSelect(e)}
      />
      {avatar && avatar !== '' ? (
        <AvatarPreview>
          <label htmlFor={id}>
            <AvatarImage src={avatar} />
          </label>
        </AvatarPreview>
      ) : (
        <label htmlFor={id}>
          <AvatarPlaceholder>
            {/* <img src={avatarPreview || initialValues.avatar || images.IconCamera} alt='' /> */}
            {uploading ? (
              <Spinner classNam="loading" color="#187BCD" />
            ) : (
              <PlaceholderIcon />
            )}
          </AvatarPlaceholder>
        </label>
      )}
    </AvatarContainer>
  );
}

Avatar.propTypes = {
  folder: PropTypes.string
};

Avatar.defaultProps = {
  folder: ''
};

export default Avatar;
