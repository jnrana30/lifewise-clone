import React from 'react';
import PropTypes from 'prop-types';
import 'react-phone-input-2/lib/material.css';
import { PhoneInputComponent } from './styles';

function Phone({ onChange, ...props }) {
  return (
    <PhoneInputComponent
      country={'gb'}
      onlyCountries={['us', 'gb', 'au']}
      {...props}
      onChange={phone => {
        onChange(phone, null);
      }}
    />
  );
}

Phone.propTypes = {};

export default Phone;
