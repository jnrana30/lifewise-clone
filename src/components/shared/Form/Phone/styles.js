import styled from 'styled-components';
import PhoneInput from 'react-phone-input-2';

export const PhoneInputComponent = styled(PhoneInput).attrs({
  containerStyle: {
    width: '100%',
    marginTop: 20
  },
  inputStyle: {
    width: '100%'
  }
})``;
