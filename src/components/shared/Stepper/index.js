import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Stepper,
  Step,
  StepLabel,
  Typography,
  Button
} from '@material-ui/core';
import Check from '@material-ui/icons/Check';

import { ConnectorStyles, IconStyles, StepperStyles } from './styles';

function IconComponent(props) {
  const classes = IconStyles();
  const { active, completed } = props;

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed
      })}
    >
      {completed ? (
        <Check className={classes.completed} />
      ) : (
        <div className={classes.circle}>{props.icon}</div>
      )}
    </div>
  );
}

IconComponent.propTypes = {
  active: PropTypes.bool,
  completed: PropTypes.bool,
  icon: PropTypes.node
};

const StepperComponent = ({ steps, activeStep, ...props }) => {
  const classes = StepperStyles();

  return (
    <Stepper
      className={classes.root}
      alternativeLabel
      activeStep={activeStep}
      connector={<ConnectorStyles />}
    >
      {steps.map(label => (
        <Step key={label}>
          <StepLabel StepIconComponent={IconComponent}>
            <Typography variant="body2">{label}</Typography>
          </StepLabel>
        </Step>
      ))}
    </Stepper>
  );
};

StepperComponent.propTypes = {
  steps: PropTypes.array,
  activeStep: PropTypes.number
};

StepperComponent.defaultTypes = {
  steps: [],
  activeStep: 0
};

export default StepperComponent;
