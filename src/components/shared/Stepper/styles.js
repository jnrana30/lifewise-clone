import { StepConnector } from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/core/styles';

export const ConnectorStyles = withStyles(theme => ({
  alternativeLabel: {
    top: 22
  },
  active: {
    '& $line': {
      backgroundColor: theme.palette.primary.main
    }
  },
  completed: {
    '& $line': {
      backgroundColor: theme.palette.primary.main
    }
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: theme.palette.grey[200],
    borderRadius: 1,
    marginLeft: 10,
    marginRight: 10
  }
}))(StepConnector);

export const IconStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.grey[300],
    zIndex: 1,
    color: '#fff',
    width: 36,
    height: 36,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundColor: theme.palette.primary.main,
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)'
  },
  completed: {
    backgroundColor: theme.palette.primary.main
  }
}));

export const StepperStyles = makeStyles(theme => ({
  root: {
    backgroundColor: 'transparent',
    padding: 0,
    paddingBottom: 24,
  }
}));
