import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import { DialogActions, DialogContent, DialogTitle } from './styles';

export default function DialogComponent({
  open,
  children,
  onClose,
  title,
  ...props
}) {
  const handleClose = () => {
    // setOpen(false);
    onClose();
  };

  return (
    <Dialog
      onClose={handleClose}
      aria-labelledby="customized-dialog-title"
      open={open}
      fullWidth={true}
      maxWidth="sm"
      {...props}
    >
      {title && title !== '' && (
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {title}
        </DialogTitle>
      )}
      <DialogContent dividers>{children}</DialogContent>
      {/* <DialogActions>
        <Button autoFocus onClick={handleClose} color="primary">
          Save changes
        </Button>
      </DialogActions> */}
    </Dialog>
  );
}
