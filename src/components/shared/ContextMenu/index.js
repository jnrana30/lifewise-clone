import React from 'react';
import { Menu, MenuItem, ListItemIcon, IconButton } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { uniqueId } from 'lodash';

import { makeStyles } from '@material-ui/core/styles';
import { ConfirmDialog } from 'src/components/App';

export const useStyles = makeStyles(theme => ({
  listItemIcon: {
    minWidth: 36
  }
}));

export default function ContextMenu({ onChange, options, className = '' }) {
  const classes = useStyles();
  const [confirmOpen, setConfirmOpen] = React.useState(false);
  const [savedVal, setSavedVal] = React.useState(null);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const id = uniqueId('context-menu-');
  const open = Boolean(anchorEl);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const onSelect = val => {
    if (val.confirm && val.confirm == true) {
      setSavedVal(val);
      setConfirmOpen(true);
      handleClose();
    } else {
      val?.onClick();
      handleClose();
    }
  };

  const onConfirm = () => {
    if (savedVal) {
      savedVal?.onClick();
      setSavedVal(null);
    }
    setConfirmOpen(false);
    // val?.onClick();
    // handleClose();
  };

  return (
    <div className={className}>
      <IconButton
        aria-label="more"
        aria-controls={id}
        aria-haspopup="true"
        onClick={handleClick}
      >
        <MoreVertIcon color="primary" />
      </IconButton>
      <Menu
        id={id}
        anchorEl={anchorEl}
        keepMounted
        open={open}
        onClose={handleClose}
      >
        {options.map((option, index) => (
          <MenuItem
            key={`${id}-option-${index}`}
            onClick={() => onSelect(option)}
          >
            {option?.icon && (
              <ListItemIcon className={classes.listItemIcon}>
                {option.icon}
              </ListItemIcon>
            )}
            {option.label}
          </MenuItem>
        ))}
      </Menu>
      <ConfirmDialog
        title="Are you sure?"
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={onConfirm}
      >
        {savedVal && savedVal.confirmMessage && savedVal.confirmMessage
          ? savedVal.confirmMessage
          : ''}
      </ConfirmDialog>
    </div>
  );
}
