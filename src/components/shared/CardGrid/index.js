import React from 'react';
import {
  Card,
  CardContent,
  Typography,
  CardActionArea,
  CardMedia
} from '@material-ui/core';
import { Paper, Box } from '@material-ui/core';
import MuiGrid from '@material-ui/core/Grid';
import MuiCardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';
import images from 'src/config/images';

const cardStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    width: '100%',
    height: '100%',
    [theme.breakpoints.down(1615)]: {
      maxWidth: 320
    },
    [theme.breakpoints.up(1615)]: {
      maxWidth: 360
    },
    [theme.breakpoints.down(600)]: {
      maxWidth: '100%'
    }
  },
  media: {
    paddingTop: '50%'
  },
  mediaViewMorePlaceholder: {
    paddingTop: '50%'
  }
}));

const gridStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    height: 'auto',
    [theme.breakpoints.down(1615)]: {
      maxWidth: 336
    },
    [theme.breakpoints.up(1615)]: {
      maxWidth: 376
    },
    [theme.breakpoints.down(600)]: {
      maxWidth: '100%'
    }
  }
}));

export const GridContainer = ({ children, ...restProps }) => {
  const classes = gridStyles();
  return (
    <MuiGrid className={classes.root} {...restProps}>
      {children}
    </MuiGrid>
  );
};

export const CardGrid = ({ children, ...restProps }) => {
  const classes = cardStyles();

  return (
    <Card className={classes.root} {...restProps}>
      {children}
    </Card>
  );
};

export const CardMediaGrid = ({ children, ...restProps }) => {
  const classes = cardStyles();

  return (
    <MuiCardMedia className={classes.media} {...restProps}>
      {children}
    </MuiCardMedia>
  );
};

export const CardEmpty = ({ children, ...restProps }) => {
  const classes = cardStyles();
  return (
    <Card className={classes.root} {...restProps}>
      <CardContent>
        <Typography color="textSecondary" align="center">
          {restProps.title}
        </Typography>
      </CardContent>
    </Card>
  );
};

export const CardSkeleton = ({ children, ...restProps }) => {
  const classes = cardStyles();

  return (
    <Card className={classes.root}>
      <Skeleton animation="wave" height={320} style={{ marginTop: '-77px' }} />
      <Box pb={2} pl={2} pr={2} style={{ marginTop: '-36px' }}>
        <Skeleton animation="wave" width="100%" height={26} />
        <Skeleton animation="wave" width="60%" height={26} />
        <Box display="flex" flexDirection="row">
          <Skeleton
            animation="wave"
            width={50}
            height={26}
            style={{ marginRight: '10px' }}
          />
          <Skeleton
            animation="wave"
            width={50}
            height={26}
            style={{ marginRight: '10px' }}
          />
          <Skeleton
            animation="wave"
            width={50}
            height={26}
            style={{ marginRight: '10px' }}
          />
        </Box>
      </Box>
    </Card>
  );
};

export const ViewMoreCard = ({ children, onClick, ...restProps }) => {
  const classes = cardStyles();

  return (
    <Card className={classes.root} onClick={onClick}>
      {/* <Box height={320} style={{ marginTop: '-77px' }} /> */}
      <CardActionArea onClick={onClick}>
        <CardMedia
          className={classes.mediaViewMorePlaceholder}
          image={images.app.ViewMorePlaceHolder}
          title="View More"
        />
      </CardActionArea>
      <Box
        display="flex"
        alignItems="center"
        justifyContent="center"
        pt={2}
        onClick={onClick}
      >
        {children}
      </Box>
    </Card>
  );
};
