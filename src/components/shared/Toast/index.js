import React, { useState, useEffect } from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import pubsub from 'sweet-pubsub';
import { uniqueId } from 'lodash';
import { Alert, AlertTitle } from '@material-ui/lab';
import { IconButton, Box } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { Container } from './styles';

const Toast = () => {
  const [toasts, setToasts] = useState([]);

  useEffect(() => {
    const addToast = ({ type = 'success', title, message, duration = 5 }) => {
      const id = uniqueId('toast-');

      if (title === '' || title === null || typeof title === 'undefined') {
        title = type;
      }

      setToasts(currentToasts => [
        ...currentToasts,
        { id, type, title, message }
      ]);

      if (duration) {
        setTimeout(() => removeToast(id), duration * 1000);
      }
    };

    pubsub.on('toast', addToast);

    return () => {
      pubsub.off('toast', addToast);
    };
  }, []);

  const removeToast = id => {
    setToasts(currentToasts => currentToasts.filter(toast => toast.id !== id));
  };

  return (
    <Container>
      <TransitionGroup>
        {toasts.map(toast => (
          <CSSTransition key={toast.id} classNames="ba-toast" timeout={200}>
            <Box mb={1}>
              <Alert
                variant="filled"
                severity={toast.type}
                action={
                  <IconButton
                    aria-label="close"
                    color="inherit"
                    size="small"
                    onClick={() => {
                      removeToast(toast.id);
                    }}
                  >
                    <CloseIcon fontSize="inherit" />
                  </IconButton>
                }
              >
                {/* {toast.title && <AlertTitle>{toast.title}</AlertTitle>} */}
                {toast.message}
              </Alert>
            </Box>
          </CSSTransition>
        ))}
      </TransitionGroup>
    </Container>
  );
};

export default Toast;
