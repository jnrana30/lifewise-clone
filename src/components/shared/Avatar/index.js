import React from 'react';
import PropTypes from 'prop-types';

import { Image, Letter } from './styles';

const propTypes = {
  className: PropTypes.string,
  avatar: PropTypes.string,
  name: PropTypes.string,
  size: PropTypes.number
};

const defaultProps = {
  className: undefined,
  avatar: null,
  name: '',
  size: 36
};

const Avatar = ({ className, avatar, name, size, ...otherProps }) => {
  const sharedProps = {
    className,
    size,
    'data-testid': name ? `avatar:${name}` : 'avatar',
    ...otherProps
  };

  if (avatar) {
    return <Image src={avatar} {...sharedProps} />;
  }

  return (
    <Letter color={getColorFromName(name)} color="#2B6B00" {...sharedProps}>
      <span>{name.charAt(0)}</span>
    </Letter>
  );
};

const colors = [
  '#DA7657',
  '#6ADA57',
  '#5784DA',
  '#AA57DA',
  '#DA5757',
  '#DA5792',
  '#57DACA',
  '#57A5DA'
];

const getColorFromName = name =>
  colors[name.toLocaleLowerCase().charCodeAt(0) % colors.length];

Avatar.propTypes = propTypes;
Avatar.defaultProps = defaultProps;

export default Avatar;
