import styled from 'styled-components';
import { makeStyles } from '@material-ui/core/styles';
import { drawerWidth } from 'src/config/theme';
import { SwipeableDrawer, Box, Typography } from '@material-ui/core';

export const useStyles = makeStyles(theme => ({
  drawerPaper: {
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 550
    },
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerPaperFW: {
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 800
    },
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  titleBox: {
    padding: '10px 30px',
    backgroundColor: '#F6B036'
  },
  title: {
    color: '#ffffff'
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing(0),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(7)
    }
  }
}));

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Content = styled.div`
  padding: 24px 30px;
  height: calc(100vh - 144px);
  overflow-y: auto;
`;

export const Footer = styled.div`
  padding: 20px;
  background-color: #e3e6eb;
  display: flex;
  justify-content: flex-end;
`;
