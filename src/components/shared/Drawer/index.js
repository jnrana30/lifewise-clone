import React from 'react';
import PropTypes from 'prop-types';
import { SwipeableDrawer, Box, Typography } from '@material-ui/core';
import { Button } from 'src/components/shared';
import CloseIcon from '@material-ui/icons/Close';
import { useStyles, Container, Content, Footer } from './styles';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';

function Drawer({
  open,
  title,
  onClose,
  onOpen,
  fullWidth,
  buttonText,
  ...props
}) {
  const classes = useStyles();

  return (
    <SwipeableDrawer
      anchor="right"
      open={open}
      onClose={onClose}
      onOpen={onOpen}
      classes={{
        paper: !fullWidth ? classes.drawerPaper : classes.drawerPaperFW
      }}
    >
      <Container>
        <Box
          className={classes.titleBox}
          display="flex"
          alignItems="center"
          justifyContent="space-between"
        >
          <Typography variant="h6" className={classes.title}>
            {title}
          </Typography>
          <Button iconButton={true} onClick={onClose}>
            <CloseIcon style={{ color: '#ffffff' }} />
          </Button>
        </Box>
        <Content>{props.children}</Content>
        <Footer>
          <Button
            variant="contained"
            color="primary"
            endIcon={<ChevronRightIcon />}
            onClick={props.onSave}
            type="submit"
            form={props.form}
            loading={props.buttonLoading}
          >
            {buttonText}
          </Button>
        </Footer>
      </Container>
    </SwipeableDrawer>
  );
}

Drawer.propTypes = {
  title: PropTypes.string,
  buttonTitle: PropTypes.string,
  onSave: PropTypes.func,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
  form: PropTypes.string,
  fullWidth: PropTypes.bool,
  buttonText: PropTypes.string,
  buttonLoading: PropTypes.bool
};

Drawer.defaultProps = {
  title: '',
  buttonTitle: 'Save',
  onSave: () => {},
  onOpen: () => {},
  onClose: () => {},
  form: '',
  fullWidth: false,
  buttonText: 'save',
  buttonLoading: false
};

export default Drawer;
