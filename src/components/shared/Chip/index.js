import React from 'react';
import { Chip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(0.5),
  }
}));

const ChipComponent = (props) => {
  const classes = useStyles();
  const { className = '', ...restProps } = props;

  return (
    <Chip
      className={`${className} ${classes.root}`}
      {...restProps}
    />
  )
};

export default ChipComponent;