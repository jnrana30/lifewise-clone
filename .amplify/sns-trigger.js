const https = require('https')

const sendSlackNotification = ({ action, url, error }) => {
  const messageBody = {
    username: "LifeWise Frontend Deployment",
    text: action,
  };
  console.log(messageBody);
    switch (action) {
        case 'failed':
            messageBody.text = 'Release to LifeWise Frontend **production** failed.'
            messageBody.icon_emoji = ':octagonal_sign:';
            messageBody.attachments = [{
                color: '#e52e2e',
                fields: [
                  {
                    title: 'Environment',
                    value: 'production',
                    short: true
                  },
                  {
                    title: 'Error',
                    value: error,
                  },
                ]
            }];
            break;
        case 'succeeded':
            messageBody.text = 'Release to LifeWise Frontend **production** succeeded.'
            messageBody.icon_emoji = ':dart:';
            messageBody.attachments = [{
            color: '#2ee550',
            fields: [
              {
                title: 'Environment',
                value: 'production',
                short: true
              },
            ]
          }];
          break;
        case 'started':
            messageBody.text = 'Release to LifeWise Frontend **production** has started...'
            messageBody.icon_emoji = ':large_blue_diamond:';
            messageBody.attachments = [{
                color: '#2e6ee5',
                fields: [
                    {
                        title: 'Environment',
                        value: 'production',
                        short: true
                    },
                ]
            }];
            break;
    }
    return sendSlackMessage(process.env.WEBHOOK_URL, messageBody);
}
const sendSlackMessage = (webhookURL, messageBody) => {
  // make sure the incoming message body can be parsed into valid JSON
  try {
    messageBody = JSON.stringify(messageBody);
  } catch (e) {
    throw new Error('Failed to stringify messageBody', e);
  }

  // Promisify the https.request
  return new Promise((resolve, reject) => {
    // general request options, we defined that it's a POST request and content is JSON
    const requestOptions = {
      method: 'POST',
      header: {
        'Content-Type': 'application/json'
      }
    };

    // actual request
    const req = https.request(webhookURL, requestOptions, (res) => {
      let response = '';


      res.on('data', (d) => {
        response += d;
      });

      // response finished, resolve the promise with data
      res.on('end', () => {
        resolve(response);
      })
    });

    // there was an error, reject the promise
    req.on('error', (e) => {
      reject(e);
    });

    // send our message body (was parsed to JSON beforehand)
    req.write(messageBody);
    req.end();
  });
}

exports.handler = async (event) => {
  try {
    const sns = event.Records[0].Sns.Message
    let action;
    if (sns.includes('build status is FAILED')) {
        action = 'failed';
    } else if (sns.includes('build status is SUCCEED')) {
        action = 'succeeded';
    } else if (sns.includes(`build status is STARTED`)) {
        action = 'started';
    }
    console.log(await sendSlackNotification({ action }));
  } catch(err) {
    console.log(err);
  }
}